function TangentInverseDynamicsLocomotorTestCase()
%% TANGENTINVERSEDYNAMICSLOCOMOTORTESTCASE
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Setup


g = 1 / 2;
b = 1 / 4;
h = 5 * 1e-3;

bh  = b * h;
bhh = bh * h;

% Robot
rbt = SMMSFactory.Scissor();

setup(rbt);
nv = rbt.VelocityNumber;

sRng = rng();
coRng = onCleanup(@() rng(sRng));
rng(4, 'twister');



%% Test Setup

% Random state to check at
q     = configurationRand(rbt);
qdot  = 2 * ( rand(nv, 1) - 0.5 );
qddot = 2 * ( rand(nv, 1) - 0.5 );

% "Gains" of each variation
k_q     = 1;
k_qdot  = g / bh;
k_qddot = 1 / bhh;

% Random base variation
vq0     = 2 * ( rand(nv, nv) - 0.5 );
vqdot0  = 2 * ( rand(nv, nv) - 0.5 );
vqddot0 = 2 * ( rand(nv, nv) - 0.5 );

% External forces and their variation
fext  = 2 * ( rand(6, rbt.NumBody) - 0.5 );
vfext = zeros(6, nv, rbt.NumBody);

T      = tpar2tform(q(1:7));
eta    = qdot(1:6);
etadot = qddot(1:6);
incu   = rand(nv, 1) - 0.5;

% Calculate "original" variation i.e., properly scaled variation
vq     = k_q * vq0;
vqdot  = k_qdot * vqdot0;
vqddot = k_qddot * vqddot0;

% And calculate the Jacobian of the variation
[vq(1:6,1:6), vqdot(1:6,1:6), vqddot(1:6,1:6)] = jacobian_SO3xR32SE3(T, eta, etadot, incu(1:6), g / bh, 1 / bhh);



%% Positions

vq_test = vq0;

[~, expected] = smms.algorithms.tangentInverseDynamics( ...
    rbt ...
  , q, qdot, qddot ...
  , fext ...
  ... % Vq
  , vq ...
  ... % Vqdot
  , zeros(nv, nv) ...
  ... % Vqddot
  , zeros(nv, nv) ...
  ... % VFext
  , vfext ...
);
[~, actual] = smms.algorithms.tangentInverseDynamics( ...
    rbt ...
  , q, qdot, qddot ...
  , fext ...
  ... % Vq
  , vq_test ...
  ... % Vqdot
  , zeros(nv, nv) ...
  ... % Vqddot
  , zeros(nv, nv) ...
  ... % VFext
  , vfext ...
);
actual = k_q * actual;

display(allisclose(actual, expected), 'k * TIDM(vq = eye()) == TIDM(vq = k * eye())');



%% Velocities

vqdot_test = vqdot0;

[~, expected] = smms.algorithms.tangentInverseDynamics( ...
    rbt ...
  , q, qdot, qddot ...
  , fext ...
  ... % Vq
  , zeros(nv, nv) ...
  ... % Vqdot
  , vqdot ...
  ... % Vqddot
  , zeros(nv, nv) ...
  ... % VFext
  , vfext ...
);
[~, actual] = smms.algorithms.tangentInverseDynamics( ...
    rbt ...
  , q, qdot, qddot ...
  , fext ...
  ... % Vq
  , zeros(nv, nv)...
  ... % Vqdot
  , vqdot_test ...
  ... % Vqddot
  , zeros(nv, nv) ...
  ... % VFext
  , vfext ...
);
actual = k_qdot * actual;

display(allisclose(actual, expected), 'k * TIDM(vqdot = eye()) == TIDM(vqdot = k * eye())');



%% Accelerations

vqddot_test = vqddot0;

[~, expected] = smms.algorithms.tangentInverseDynamics( ...
    rbt ...
  , q, qdot, qddot ...
  , fext ...
  ... % Vq
  , zeros(nv, nv) ...
  ... % Vqdot
  , zeros(nv, nv) ...
  ... % Vqddot
  , vqddot ...
  ... % VFext
  , vfext ...
);
[~, actual] = smms.algorithms.tangentInverseDynamics( ...
    rbt ...
  , q, qdot, qddot ...
  , fext ...
  ... % Vq
  , zeros(nv, nv) ...
  ... % Vqdot
  , zeros(nv, nv) ...
  ... % Vqddot
  , vqddot_test ...
  ... % VFext
  , vfext ...
);
actual = k_qddot * actual;

display(allisclose(actual, expected), 'k * TIDM(vqddot = eye()) == TIDM(vqddot = k * eye())');



end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
