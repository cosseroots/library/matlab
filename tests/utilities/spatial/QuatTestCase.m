classdef QuatTestCase < matlab.unittest.TestCase
  %% QUATTESTCASE
  
  
  
  %% PARAMETERS FOR TESTS
  properties ( TestParameter )
    
    nn = { 1 , 13, 134 };
    
  end
  
  
  
  %% INTERNAL PROPERTIES
  properties ( Constant , Access = protected )
    
    CachedFunctions = containers.Map('KeyType', 'char', 'ValueType', 'any');
    
  end
  
  
  
  %% CONSTRUCTOR TESTS
  methods ( Test , TestTags = { 'constructor' } )
    
    function testZeros(obj, n)
      %% TESTZEROS
      
      
      
      q = quatzeros(n);
      
      import matlab.unittest.constraints.AbsoluteTolerance
      import matlab.unittest.constraints.HasSize
      import matlab.unittest.constraints.IsEqualTo
      
      assertThat(obj, q, HasSize([4, n]));
      assertThat(obj, q, IsEqualTo(repmat([ 1 ; 0 ; 0 ; 0 ], [1, n]), 'Within', AbsoluteTolerance(5 * 1e-12)));
      assertThat(obj, vecnorm(q, 2, 1), IsEqualTo(ones(1, n), 'Within', AbsoluteTolerance(5 * 1e-12)));
      
    end
    
    
    function testOnes(obj, n)
      %% TESTONES
      
      
      
      q = quatones(n);
      
      import matlab.unittest.constraints.AbsoluteTolerance
      import matlab.unittest.constraints.HasSize
      import matlab.unittest.constraints.IsEqualTo
      
      s = sqrt(1/3);
      
      assertThat(obj, q, HasSize([4, n]));
      assertThat(obj, q, IsEqualTo(repmat([ 0 ; s ; s ; s ], [1, n]), 'Within', AbsoluteTolerance(5 * 1e-12)));
      assertThat(obj, vecnorm(q, 2, 1), IsEqualTo(ones(1, n), 'Within', AbsoluteTolerance(5 * 1e-12)));
      
    end
    
  end
  
  
  
  %% CONVERSION TESTS
  methods ( Test, TestTags = { 'conversion' } )
    
    function test2axang(obj, n)
      %% TEST2AXANG
      
      
      
      q = quatrand(n);
      
      c    = quat2axang(q);
      rb_g = permute(robotutils_to(robotutilsfun(obj, 'quat2axang'), q), [2, 1, 3]);
      
      
      import matlab.unittest.constraints.AbsoluteTolerance
      import matlab.unittest.constraints.HasSize
      import matlab.unittest.constraints.IsEqualTo
      
      assertThat(obj, c, HasSize([4, n]));
      assertThat(obj, c, IsEqualTo(rb_g, 'Within', AbsoluteTolerance(5 * 1e-12)));
      
    end
    
    
    function test2rotm(obj, n)
      %% TEST2ROTM
      
      
      
      q = quatrand(n);
      
      c    = quat2rotm(q);
      rb_g = robotutils_to(robotutilsfun(obj, 'quat2rotm'), q);
      
      
      import matlab.unittest.constraints.AbsoluteTolerance
      import matlab.unittest.constraints.HasSize
      import matlab.unittest.constraints.IsEqualTo
      
      assertThat(obj, c, HasSize([3, 3, n]));
      assertThat(obj, c, IsEqualTo(rb_g, 'Within', AbsoluteTolerance(5 * 1e-12)));
      
    end
    
    
    function test2tform(obj, n)
      %% TEST2TFORM
      
      
      
      q = quatrand(n);
      
      c    = quat2tform(q);
      rb_g = robotutils_to(robotutilsfun(obj, 'quat2tform'), q);
      
      
      import matlab.unittest.constraints.AbsoluteTolerance
      import matlab.unittest.constraints.HasSize
      import matlab.unittest.constraints.IsEqualTo
      
      assertThat(obj, c, HasSize([4, 4, n]));
      assertThat(obj, c, IsEqualTo(rb_g, 'Within', AbsoluteTolerance(5 * 1e-12)));
      
    end
    
    
    function test2tpar(obj, n)
      %% TEST2TPAR
      
      
      
      q = quatrand(n);
      
      c    = quat2tpar(q);
      rb_g = cat(1, q, zeros(3, n));
      
      
      import matlab.unittest.constraints.AbsoluteTolerance
      import matlab.unittest.constraints.HasSize
      import matlab.unittest.constraints.IsEqualTo
      
      assertThat(obj, c, HasSize([7, n]));
      assertThat(obj, c, IsEqualTo(rb_g, 'Within', AbsoluteTolerance(5 * 1e-12)));
      
    end
    
    
    function testAxang2(obj, n)
      %% TESTAXANG2
      
      
      
      q = quatrand(n);
      axang = quat2axang(q);
      
      c    = axang2quat(axang);
      rb_g = robotutils_from(robotutilsfun(obj, 'axang2quat'), permute(axang, [2, 1, 3]));
      
      
      import matlab.unittest.constraints.AbsoluteTolerance
      import matlab.unittest.constraints.HasSize
      import matlab.unittest.constraints.IsEqualTo
      
      assertThat(obj, c, HasSize([4, n]));
      assertThat(obj, c, IsEqualTo(rb_g, 'Within', AbsoluteTolerance(5 * 1e-12)));
      
    end
    
    
    function testRotm2(obj, n)
      %% TESTROTM2
      
      
      
      q = quatrand(n);
      R = quat2rotm(q);
      
      c    = rotm2quat(R);
      rb_g = robotutils_from(robotutilsfun(obj, 'rotm2quat'), R);
      
      
      import matlab.unittest.constraints.AbsoluteTolerance
      import matlab.unittest.constraints.HasSize
      import matlab.unittest.constraints.IsEqualTo
      
      assertThat(obj, c, HasSize([4, n]));
      assertThat(obj, c, IsEqualTo(rb_g, 'Within', AbsoluteTolerance(5 * 1e-12)));
      
    end
    
    
    function testTForm2(obj, n)
      %% TESTTFORM2
      
      
      
      q = quatrand(n);
      T = quat2tform(q);
      
      c    = tform2quat(T);
      rb_g = robotutils_from(robotutilsfun(obj, 'tform2quat'), T);
      
      
      import matlab.unittest.constraints.AbsoluteTolerance
      import matlab.unittest.constraints.HasSize
      import matlab.unittest.constraints.IsEqualTo
      
      assertThat(obj, c, HasSize([4, n]));
      assertThat(obj, c, IsEqualTo(rb_g, 'Within', AbsoluteTolerance(5 * 1e-12)));
      
    end
    
    
    function testTpar2(obj, n)
      %% TESTTPAR2
      
      
      
      q = quatrand(n);
      g = quat2tpar(q);
      
      c    = tpar2quat(g);
      
      
      import matlab.unittest.constraints.AbsoluteTolerance
      import matlab.unittest.constraints.HasSize
      import matlab.unittest.constraints.IsEqualTo
      
      assertThat(obj, c, HasSize([4, n]));
      assertThat(obj, c, IsEqualTo(g(1:4,:), 'Within', AbsoluteTolerance(5 * 1e-12)));
      
    end
    
  end
  
  
  
  %% INTERNAL METHODS
  methods ( Access = protected  )
    
    function rf = robotutilsfun(obj, f)
      %% ROBOTICSFUN
      
      
      
      % Populate cache
      if isempty(obj.CachedFunctions)
        cwd = pwd();
        coCwd = onCleanup(@() cd(cwd));
        % Go to robotics toolbox
        cd(fullfile(matlabroot(), 'toolbox', 'shared', 'robotics', 'robotutils'));
        % Find all functions here
        rf  = dir('*.m');
        nrf = numel(rf);
        
        % Loop over each file and cache it
        for ifm = 1:nrf
          % Get file structure
          rf_ = rf(ifm);
          % Extract function name from file name
          [~, fname, ~] = fileparts(fullfile(rf_.folder, rf_.name));
          
          % Then cache the function with a wrapper over quaternions
          obj.CachedFunctions(fname) = ShadowedFunction(fullfile(rf_.folder, rf_.name));
          
        end
        
      end
      
      % Get cached value
      rf = obj.CachedFunctions(f);
      
    end
    
  end
  
end


function out = robotutils_to(fcn, q, varargin)
%% ROBOTUTILS_TO



quatin = size(q, 1) == 4;

if quatin
  q = q.';
end

out = fcn(q, varargin{:});


end


function out = robotutils_from(fcn, varargin)
%% ROBOTUTILS_FROM



out = fcn(varargin{:});

dims = 1:ndims(out);
dims([1,2]) = dims([2,1]);
out = permute(out, dims);


end
