classdef TransformTwistTestCase < matlab.unittest.TestCase
  %% TRANSFORMTWISTTESTCASE
  
  
  
  %% TEST METHODS
  methods ( Test )
    
    function testOneOneOne(obj)
      %% TESTONEONEONE
      
      
      etaa = twistrand();
      Ta = tformrand();
      Tb = tformrand();
      
      expected = Ad(Tb \ Ta) * etaa;
      actual = transformTwist(etaa, Ta, Tb);
      
      import matlab.unittest.constraints.*
      
      assertThat(obj, actual, HasSize(size(expected)));
      assertThat(obj, actual, IsEqualTo(expected, 'Within', AbsoluteTolerance(100 * eps(class(actual)))));
      
    end
    
    
    function testOneOneMultiple(obj)
      %% TESTONEONEMULTIPLE
      
      
      nt = 7;
      etaa = twistrand();
      Ta = tformrand();
      Tb = tformrand(nt);
      
      expected = zeros(6, nt);
      for ib = 1:nt
        expected(:,ib) = Ad(Tb(:,:,ib) \ Ta) * etaa;
      end
      actual = transformTwist(etaa, Ta, Tb);
      
      import matlab.unittest.constraints.*
      
      assertThat(obj, actual, HasSize(size(expected)));
      assertThat(obj, actual, IsEqualTo(expected, 'Within', AbsoluteTolerance(100 * eps(class(actual)))));
      
    end
    
    
    function testOneMultipleOne(obj)
      %% TESTONEMULTIPLEONE
      
      
      nt = 7;
      etaa = twistrand();
      Ta = tformrand(nt);
      Tb = tformrand();
      
      expected = zeros(6, nt);
      for ib = 1:nt
        expected(:,ib) = Ad(Tb \ Ta(:,:,ib)) * etaa;
      end
      actual = transformTwist(etaa, Ta, Tb);
      
      import matlab.unittest.constraints.*
      
      assertThat(obj, actual, HasSize(size(expected)));
      assertThat(obj, actual, IsEqualTo(expected, 'Within', AbsoluteTolerance(100 * eps(class(actual)))));
      
    end
    
    
    function testMultipleOneOne(obj)
      %% TESTMULTIPLEONEONE
      
      
      nt = 7;
      etaa = twistrand(nt);
      Ta = tformrand();
      Tb = tformrand();
      
      expected = zeros(6, nt);
      for ib = 1:nt
        expected(:,ib) = Ad(Tb \ Ta) * etaa(:,ib);
      end
      actual = transformTwist(etaa, Ta, Tb);
      
      import matlab.unittest.constraints.*
      
      assertThat(obj, actual, HasSize(size(expected)));
      assertThat(obj, actual, IsEqualTo(expected, 'Within', AbsoluteTolerance(100 * eps(class(actual)))));
      
    end
    
    
    function testMultipleMultipleMultiple(obj)
      %% TESTMULTIPLEMULTIPLEMULTIPLE
      
      
      nt = 7;
      etaa = twistrand(nt);
      Ta = tformrand(nt);
      Tb = tformrand(nt);
      
      expected = zeros(6, nt);
      for ib = 1:nt
        expected(:,ib) = Ad(Tb(:,:,ib) \ Ta(:,:,ib)) * etaa(:,ib);
      end
      actual = transformTwist(etaa, Ta, Tb);
      
      import matlab.unittest.constraints.*
      
      assertThat(obj, actual, HasSize(size(expected)));
      assertThat(obj, actual, IsEqualTo(expected, 'Within', AbsoluteTolerance(100 * eps(class(actual)))));
      
    end
    
  end
  
end
