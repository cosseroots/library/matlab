classdef TransformForceTestCase < matlab.unittest.TestCase
  %% TRANSFORMFORCETESTCASE
  
  
  
  %% TEST METHODS
  methods ( Test )
    
    function testOneOneOne(obj)
      %% TESTONEONEONE
      
      
      Fa = twistrand();
      Ta = tformrand();
      Tb = tformrand();
      
      expected = transpose(Ad(Ta \ Tb)) * Fa;
      actual = transformForce(Fa, Ta, Tb);
      
      import matlab.unittest.constraints.*
      
      assertThat(obj, actual, HasSize(size(expected)));
      assertThat(obj, actual, IsEqualTo(expected, 'Within', AbsoluteTolerance(100 * eps(class(actual)))));
      
    end
    
    
    function testOneOneMultiple(obj)
      %% TESTONEONEMULTIPLE
      
      
      nt = 7;
      Fa = twistrand();
      Ta = tformrand();
      Tb = tformrand(nt);
      
      expected = zeros(6, nt);
      for ib = 1:nt
        expected(:,ib) = transpose(Ad(Ta \ Tb(:,:,ib))) * Fa;
      end
      actual = transformForce(Fa, Ta, Tb);
      
      import matlab.unittest.constraints.*
      
      assertThat(obj, actual, HasSize(size(expected)));
      assertThat(obj, actual, IsEqualTo(expected, 'Within', AbsoluteTolerance(100 * eps(class(actual)))));
      
    end
    
    
    function testOneMultipleOne(obj)
      %% TESTONEMULTIPLEONE
      
      
      nt = 7;
      Fa = twistrand();
      Ta = tformrand(nt);
      Tb = tformrand();
      
      expected = zeros(6, nt);
      for ib = 1:nt
        expected(:,ib) = transpose(Ad(Ta(:,:,ib) \ Tb )) * Fa;
      end
      actual = transformForce(Fa, Ta, Tb);
      
      import matlab.unittest.constraints.*
      
      assertThat(obj, actual, HasSize(size(expected)));
      assertThat(obj, actual, IsEqualTo(expected, 'Within', AbsoluteTolerance(100 * eps(class(actual)))));
      
    end
    
    
    function testMultipleOneOne(obj)
      %% TESTMULTIPLEONEONE
      
      
      nt = 7;
      Fa = twistrand(nt);
      Ta = tformrand();
      Tb = tformrand();
      
      expected = zeros(6, nt);
      for ib = 1:nt
        expected(:,ib) = transpose(Ad(Ta \ Tb)) * Fa(:,ib);
      end
      actual = transformForce(Fa, Ta, Tb);
      
      import matlab.unittest.constraints.*
      
      assertThat(obj, actual, HasSize(size(expected)));
      assertThat(obj, actual, IsEqualTo(expected, 'Within', AbsoluteTolerance(100 * eps(class(actual)))));
      
    end
    
    
    function testMultipleMultipleMultiple(obj)
      %% TESTMULTIPLEMULTIPLEMULTIPLE
      
      
      nt = 7;
      Fa = twistrand(nt);
      Ta = tformrand(nt);
      Tb = tformrand(nt);
      
      expected = zeros(6, nt);
      for ib = 1:nt
        expected(:,ib) = transpose(Ad(Ta(:,:,ib) \ Tb(:,:,ib))) * Fa(:,ib);
      end
      actual = transformForce(Fa, Ta, Tb);
      
      import matlab.unittest.constraints.*
      
      assertThat(obj, actual, HasSize(size(expected)));
      assertThat(obj, actual, IsEqualTo(expected, 'Within', AbsoluteTolerance(100 * eps(class(actual)))));
      
    end
    
  end
  
end
