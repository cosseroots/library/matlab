classdef TFormTestCase < matlab.unittest.TestCase
  %% TFORMTESTCASE
  
  
  
  %% TEST PARAMETERS
  properties ( TestParameter )
    
    % Number of transformations to manipulate
    n = { 1 , 13 , 103 };
    
  end
  
  
  
  %% TEST METHODS
  methods ( Test )
    
    function testTFormMul(obj, n)
      %% TESTTFORMMUL
      
      
      
      T1 = tformrand(n);
      T2 = tformrand();
      
      T1T2 = tformmul(T1, T2);
      
      import matlab.unittest.constraints.AbsoluteTolerance
      import matlab.unittest.constraints.HasSize
      import matlab.unittest.constraints.IsEqualTo
      
      assertThat(obj, T1T2, HasSize([4, 4, n]));
      assertThat(obj, T1T2, IsEqualTo(pagemtimes(T1, T2), 'Within', AbsoluteTolerance(5 * 1e-12)));
      
    end
    
    
    function testTFormDiv(obj, n)
      %% TESTTFORMDIV
      
      
      
      T1 = tformrand(n);
      T2 = tformrand();
      
      T1T2 = tformdiv(T1, T2);
      
      import matlab.unittest.constraints.AbsoluteTolerance
      import matlab.unittest.constraints.HasSize
      import matlab.unittest.constraints.IsEqualTo
      
      assertThat(obj, T1T2, HasSize([4, 4, n]));
      assertThat(obj, T1T2, IsEqualTo(pagemtimes(T1, tform2inv(T2)), 'Within', AbsoluteTolerance(5 * 1e-12)));
      
    end
    
  end
  
end
