classdef AxangTestCase < matlab.unittest.TestCase
  %% AXANGTESTCASE
  
  
  
  %% PARAMETERS FOR TESTS
  properties ( TestParameter )
    
    n = { 1 , 13, 134 };
    
  end
  
  
  
  %% INTERNAL PROPERTIES
  properties ( Constant , Access = protected )
    
    CachedFunctions = containers.Map('KeyType', 'char', 'ValueType', 'any');
    
  end
  
  
  
  %% CONSTRUCTOR TESTS
  methods ( Test , TestTags = { 'constructor' } )
    
    function testZeros(obj, n)
      %% TESTZEROS
      
      
      
      aa = axangzeros(n);
      
      import matlab.unittest.constraints.AbsoluteTolerance
      import matlab.unittest.constraints.HasSize
      import matlab.unittest.constraints.IsEqualTo
      
      assertThat(obj, aa, HasSize([4, n]));
      assertThat(obj, aa, IsEqualTo(repmat([ 0 ; 0 ; 1 ; 0 ], [1, n]), 'Within', AbsoluteTolerance(5 * 1e-12)));
      assertThat(obj, vecnorm(aa(1:3,:), 2, 1), IsEqualTo(ones(1, n), 'Within', AbsoluteTolerance(5 * 1e-12)));
      
    end
    
    
    function testOnes(obj, n)
      %% TESTONES
      
      
      
      aa = axangones(n);
      
      import matlab.unittest.constraints.AbsoluteTolerance
      import matlab.unittest.constraints.HasSize
      import matlab.unittest.constraints.IsEqualTo
      
      s = sqrt(1/3);
      
      assertThat(obj, aa, HasSize([4, n]));
      assertThat(obj, aa, IsEqualTo(repmat([ s ; s ; s ; 1 ], [1, n]), 'Within', AbsoluteTolerance(5 * 1e-12)));
      assertThat(obj, vecnorm(aa(1:3,:), 2, 1), IsEqualTo(ones(1, n), 'Within', AbsoluteTolerance(5 * 1e-12)));
      
    end
    
  end
  
  
  
  %% CONVERSION TESTS
  methods ( Test, TestTags = { 'conversion' } )
    
    function test2quat(obj, n)
      %% TEST2QUAT
      
      
      
      a = axangrand(n);
      
      c    = axang2quat(a);
      rb_g = permute(robotutils_to(robotutilsfun(obj, 'axang2quat'), a), [2, 1, 3]);
      
      
      import matlab.unittest.constraints.AbsoluteTolerance
      import matlab.unittest.constraints.HasSize
      import matlab.unittest.constraints.IsEqualTo
      
      assertThat(obj, c, HasSize([4, n]));
      assertThat(obj, c, IsEqualTo(rb_g, 'Within', AbsoluteTolerance(5 * 1e-12)));
      
    end
    
    
    function test2rotm(obj, n)
      %% TEST2ROTM
      
      
      
      a = axangrand(n);
      
      c    = axang2rotm(a);
      rb_g = robotutils_to(robotutilsfun(obj, 'axang2rotm'), a);
      
      
      import matlab.unittest.constraints.AbsoluteTolerance
      import matlab.unittest.constraints.HasSize
      import matlab.unittest.constraints.IsEqualTo
      
      assertThat(obj, c, HasSize([3, 3, n]));
      assertThat(obj, c, IsEqualTo(rb_g, 'Within', AbsoluteTolerance(5 * 1e-12)));
      
    end
    
    
    function test2tform(obj, n)
      %% TEST2TFORM
      
      
      
      q = axangrand(n);
      
      c    = axang2tform(q);
      rb_g = robotutils_to(robotutilsfun(obj, 'axang2tform'), q);
      
      
      import matlab.unittest.constraints.AbsoluteTolerance
      import matlab.unittest.constraints.HasSize
      import matlab.unittest.constraints.IsEqualTo
      
      assertThat(obj, c, HasSize([4, 4, n]));
      assertThat(obj, c, IsEqualTo(rb_g, 'Within', AbsoluteTolerance(5 * 1e-12)));
      
    end
    
    
    function test2tpar(obj, n)
      %% TEST2TPAR
      
      
      
      a = axangrand(n);
      
      c    = axang2tpar(a);
      rb_g = cat(1, axang2quat(a), zeros(3, n));
      
      
      import matlab.unittest.constraints.AbsoluteTolerance
      import matlab.unittest.constraints.HasSize
      import matlab.unittest.constraints.IsEqualTo
      
      assertThat(obj, c, HasSize([7, n]));
      assertThat(obj, c, IsEqualTo(rb_g, 'Within', AbsoluteTolerance(5 * 1e-12)));
      
    end
    
    
    function testQuat2(obj, n)
      %% TESTQUAT2
      
      
      
      q = axang2quat(axangrand(n));
      
      c    = quat2axang(q);
      rb_g = robotutils_from(robotutilsfun(obj, 'quat2axang'), permute(q, [2, 1, 3]));
      
      
      import matlab.unittest.constraints.AbsoluteTolerance
      import matlab.unittest.constraints.HasSize
      import matlab.unittest.constraints.IsEqualTo
      
      assertThat(obj, c, HasSize([4, n]));
      assertThat(obj, c, IsEqualTo(rb_g, 'Within', AbsoluteTolerance(5 * 1e-12)));
      
    end
    
    
    function testRotm2(obj, n)
      %% TESTROTM2
      
      
      
      R = axang2rotm(axangrand(n));
      
      c    = rotm2axang(R);
      rb_g = robotutils_from(robotutilsfun(obj, 'rotm2axang'), R);
      
      
      import matlab.unittest.constraints.AbsoluteTolerance
      import matlab.unittest.constraints.HasSize
      import matlab.unittest.constraints.IsEqualTo
      
      assertThat(obj, c, HasSize([4, n]));
      assertThat(obj, c, IsEqualTo(rb_g, 'Within', AbsoluteTolerance(5 * 1e-12)));
      
    end
    
    
    function testTForm2(obj, n)
      %% TESTTFORM2
      
      
      
      T = axang2tform(axangrand(n));
      
      c    = tform2axang(T);
      rb_g = robotutils_from(robotutilsfun(obj, 'tform2axang'), T);
      
      
      import matlab.unittest.constraints.AbsoluteTolerance
      import matlab.unittest.constraints.HasSize
      import matlab.unittest.constraints.IsEqualTo
      
      assertThat(obj, c, HasSize([4, n]));
      assertThat(obj, c, IsEqualTo(rb_g, 'Within', AbsoluteTolerance(5 * 1e-12)));
      
    end
    
  end
  
  
  
  %% INTERNAL METHODS
  methods ( Access = protected  )
    
    function rf = robotutilsfun(obj, f)
      %% ROBOTICSFUN
      
      
      
      % Populate cache
      if isempty(obj.CachedFunctions)
        cwd = pwd();
        coCwd = onCleanup(@() cd(cwd));
        % Go to robotics toolbox
        cd(fullfile(matlabroot(), 'toolbox', 'shared', 'robotics', 'robotutils'));
        % Find all functions here
        rf  = dir('*.m');
        nrf = numel(rf);
        
        % Loop over each file and cache it
        for ifm = 1:nrf
          % Get file structure
          rf_ = rf(ifm);
          % Extract function name from file name
          [~, fname, ~] = fileparts(fullfile(rf_.folder, rf_.name));
          
          % Then cache the function with a wrapper over quaternions
          obj.CachedFunctions(fname) = ShadowedFunction(fullfile(rf_.folder, rf_.name));
          
        end
        
      end
      
      % Get cached value
      rf = obj.CachedFunctions(f);
      
    end
    
  end
  
end


function out = robotutils_to(fcn, a, varargin)
%% ROBOTUTILS_TO



ain = size(a, 1) == 4;

if ain
  a = a.';
end

out = fcn(a, varargin{:});


end


function out = robotutils_from(fcn, varargin)
%% ROBOTUTILS_FROM



out = fcn(varargin{:});

dims = 1:ndims(out);
dims([1,2]) = dims([2,1]);
out = permute(out, dims);


end
