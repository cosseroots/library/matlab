classdef TparTestCase < matlab.unittest.TestCase
  %% TPARTESTCASE
  
  
  
  %% TEST PARAMETERS
  properties ( TestParameter )
    
    % Number of transformations to manipulate
    n = { 1 , 13 , 103 };
    
  end
  
  
  
  %% TEST METHODS
  methods ( Test )
    
    function testTparMul(obj, n)
      %% TESTTPARMUL
      
      
      
      g1 = tparrand(n);
      g2 = tparrand();
      
      g1g2 = tparmul(g1, g2);
      
      T1 = tpar2tform(g1);
      T2 = tpar2tform(g2);
      
      import matlab.unittest.constraints.AbsoluteTolerance
      import matlab.unittest.constraints.HasSize
      import matlab.unittest.constraints.IsEqualTo
      
      assertThat(obj, g1g2, HasSize([7, n]));
      assertThat(obj, tpar2tform(g1g2), IsEqualTo(tformmul(T1, T2), 'Within', AbsoluteTolerance(5 * 1e-12)));
      
    end
    
    
    function testTparDiv(obj, n)
      %% TESTTPARDIV
      
      
      
      g1 = tparrand(n);
      g2 = tparrand();
      
      g1g2 = tpardiv(g1, g2);
      
      T1 = tpar2tform(g1);
      T2 = tpar2tform(g2);
      
      import matlab.unittest.constraints.AbsoluteTolerance
      import matlab.unittest.constraints.HasSize
      import matlab.unittest.constraints.IsEqualTo
      
      assertThat(obj, g1g2, HasSize([7, n]));
      assertThat(obj, tpar2tform(g1g2), IsEqualTo(tformdiv(T1, T2), 'Within', AbsoluteTolerance(5 * 1e-12)));
      
    end
    
  end
  
end
