classdef TransformBetweenTestCase < matlab.unittest.TestCase
  %% TRANSFORMBETWEENTESTCASE
  
  
  
  %% TEST METHODS
  methods ( Test )
    
    function testOneOne(obj)
      %% TESTONEONE
      
      
      Ta = tformrand();
      Tb = tformrand();
      
      expected = Ta \ Tb;
      actual = transformBetween(Ta, Tb);
      
      import matlab.unittest.constraints.*
      
      assertThat(obj, actual, HasSize(size(expected)));
      assertThat(obj, actual, IsEqualTo(expected, 'Within', AbsoluteTolerance(100 * eps(class(actual)))));
      
    end
    
    
    function testOneMultiple(obj)
      %% TESTONEMULTIPLE
      
      
      nt = 7;
      Ta = tformrand();
      Tb = tformrand(nt);
      
      expected = zeros(4, 4, nt);
      for ib = 1:nt
        expected(:,:,ib) = Ta \ Tb(:,:,ib);
      end
      actual = transformBetween(Ta, Tb);
      
      import matlab.unittest.constraints.*
      
      assertThat(obj, actual, HasSize(size(expected)));
      assertThat(obj, actual, IsEqualTo(expected, 'Within', AbsoluteTolerance(100 * eps(class(actual)))));
      
    end
    
    
    function testMultipleOne(obj)
      %% TESTMULTIPLEONE
      
      
      nt = 7;
      Ta = tformrand(nt);
      Tb = tformrand();
      
      expected = zeros(4, 4, nt);
      for ib = 1:nt
        expected(:,:,ib) = Ta(:,:,ib) \ Tb;
      end
      actual = transformBetween(Ta, Tb);
      
      import matlab.unittest.constraints.*
      
      assertThat(obj, actual, HasSize(size(expected)));
      assertThat(obj, actual, IsEqualTo(expected, 'Within', AbsoluteTolerance(100 * eps(class(actual)))));
      
    end
    
    
    function testMultipleMultiple(obj)
      %% TESTMULTIPLEMULTIPLE
      
      
      nt = 7;
      Ta = tformrand(nt);
      Tb = tformrand(nt);
      
      expected = zeros(4, 4, nt);
      for ib = 1:nt
        expected(:,:,ib) = Ta(:,:,ib) \ Tb(:,:,ib);
      end
      actual = transformBetween(Ta, Tb);
      
      import matlab.unittest.constraints.*
      
      assertThat(obj, actual, HasSize(size(expected)));
      assertThat(obj, actual, IsEqualTo(expected, 'Within', AbsoluteTolerance(100 * eps(class(actual)))));
      
    end
    
  end
  
end
