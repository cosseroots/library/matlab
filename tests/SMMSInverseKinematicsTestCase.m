classdef SMMSInverseKinematicsTestCase < matlab.unittest.TestCase
  %% SMMSINVERSEKINEMATICSTESTCASE
  
  
  
  %% TEST METHODS
  methods ( Test )
    
    function testFlexibleManipulator(obj)
      %% TESTFLEXIBLEMANIPULATOR
      
      
      
      % Create flexible manipulator object
      rbt = SMMSFactory.Manipulator();
      
      % Pick a random configuration as target configuration
      qT = configurationRand(rbt);
      % Then find its TCP pose which we use as the target pose
      TtcpT = forwardKinematics(rbt, qT);
      
      % Displace joint coordinates a little bit i.e., add +/-10% offset to each
      % generalized coordinate value
      q0 = qT .* ( 1 + 0.10 * randbnd([-1,1], size(qT)) );
      
      % Now solve the IK problem
      [q, fval, exitflag, output, jacobian] = inverseKinematics(rbt, TtcpT, q0);
      
      % Load constraints
      import matlab.unittest.constraints.*
      
      % In theory, the IK result Q should be very close to QT
      assertThat(obj, q, IsEqualTo(qT, 'Within', AbsoluteTolerance(1e-6) & RelativeTolerance(0.0025)));
      
    end
    
    
    function testDoublePendulum(obj)
      %% TESTDOUBLEPENDULUM
      
      
      
      % Create double pendulum object
      rbt = SMMSFactory.NPendulum(2);
      
      % Pick a random configuration as target configuration
      qT = configurationRand(rbt);
      % Then find its TCP pose which we use as the target pose
      TtcpT = forwardKinematics(rbt, qT);
      
      % Displace joint coordinates a little bit i.e., add +/-10% offset to each
      % generalized coordinate value
      q0 = qT .* ( 1 + 0.10 * randbnd([-1,1], size(qT)) );
      
      % Now solve the IK problem
      [q, fval, exitflag, output, jacobian] = inverseKinematics(rbt, TtcpT, q0);
      
      % Load constraints
      import matlab.unittest.constraints.*
      
      % In theory, the IK result Q should be very close to QT
      assertThat(obj, q, IsEqualTo(qT, 'Within', AbsoluteTolerance(1e-6) & RelativeTolerance(0.0025)));
      
    end
    
  end
  
end
