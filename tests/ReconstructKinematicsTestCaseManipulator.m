function ReconstructKinematicsTestCaseManipulator()
%% RECONSTRUCTKINEMATICSTESTCASEMANIPULATOR
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

rbt = SMMSFactory.Manipulator();
% rbt = SMMSFactory.NPendulum(3);
setup(rbt);
nv = rbt.VelocityNumber;

s = linspace(0, 1, 5);

q     = configurationHome(rbt);
qdot  = 1 * ( rand(nv, 1) - 0.5 );
qddot = 1 * ( rand(nv, 1) - 0.5 );

% Number of variations to check against
nd = 2;
vq     = 1 * ( rand(nv, nd) - 0.5 );
vqdot  = 1 * ( rand(nv, nd) - 0.5 );
vqddot = 1 * ( rand(nv, nd) - 0.5 );

T0      = rbt.Ground.T;
eta0    = twistzeros();
eta0dot = [ zeros(3,1) ; rbt.Gravity ];

vzeta0   = twistzeros(nd);
veta0    = twistzeros(nd);
veta0dot = twistzeros(nd);

rod = rbt.Joint{1};



%% Reconstruction

[Te, etae, etadote] = smms.algorithms.forwardRNEA(rbt, q, qdot, qddot);

[T, eta, etadot] = reconstructKinematics(rod, q, T0, qdot, eta0, qddot, eta0dot, s);

% Assert proximal kinematics is correct
assert(allisclose(T(:,:,1)   , T0)     , 'T(:,:,end) != T0');
assert(allisclose(eta(:,1)   , eta0)   , 'eta(:,end) != eta0');
assert(allisclose(etadot(:,1), eta0dot), 'etadot(:,end) != eta0dot');

% Assert distal kinematics are correct
assert(allisclose(T(:,:,end)   , Te)     , 'T(:,:,end) != Te');
assert(allisclose(eta(:,end)   , etae)   , 'eta(:,end) != eta');
assert(allisclose(etadot(:,end), etadote), 'etadot(:,end) != etadot');



%% Tangent Reconstruction

% [a,b] = tangentInverseDynamics(rbt, q, qdot, qddot, []);

[Te, etae, etadote, vzetae, vetae, vetadote] = smms.algorithms.tangentForwardRNEA(rbt, q, qdot, qddot, vq, vqdot, vqddot);

[T, eta, etadot, vzeta, veta, vetadot] = tangentReconstructKinematics(rod, q, T0, qdot, eta0, qddot, eta0dot, vq, vzeta0, vqdot, veta0, vqddot, veta0dot, s);

% Assert proximal kinematics is correct
assert(allisclose(T(:,:,1)   , T0)     , 'T(:,:,1) != T0');
assert(allisclose(eta(:,1)   , eta0)   , 'eta(:,1) != eta0');
assert(allisclose(etadot(:,1), eta0dot), 'etadot(:,1) != eta0dot');

% Assert distal kinematics are correct
assert(allisclose(T(:,:,end)   , Te)     , 'T(:,:,end) != TEE');
assert(allisclose(eta(:,end)   , etae)   , 'eta(:,end) != etaEE');
assert(allisclose(etadot(:,end), etadote), 'etadot(:,end) != etadotEE');

assert(allisclose(vzeta(:,:,1)  , vzeta0)  , 'vZeta(:,:,1) != vZeta0');
assert(allisclose(veta(:,:,1)   , veta0)   , 'veta(:,1)    != veta0');
assert(allisclose(vetadot(:,:,1), veta0dot), 'vetadot(:,1) != veta0dot');

assert(allisclose(vzeta(:,:,end)  , vzetae)   , 'vZeta(:,:,end) != vZetaEE');
assert(allisclose(veta(:,:,end)   , vetae)   , 'veta(:,end)    != vetaEE');
assert(allisclose(vetadot(:,:,end), vetadote), 'vetadot(:,end) != vetadotEE');



end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
