The rod configuration is given as

x = [ Q0 , r0 , q ]

The rod state is

y = [ x , xdot ] = [ [ Q0 , r0 , q ] , [ eta0 , qdot ] ]

The rod extended state is

ye = [ x , xdot , xddot ] = [ [ Q0 , r0 , q ] , [ eta0 , qdot ] , [ eta0dot , qddot ] ]
