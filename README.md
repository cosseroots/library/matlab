# COSSEROOTS MATLAB Library

This collection of MATLAB functions, packages, and classes is associated with the COSSEROOTS project.
It aims at providing a practical interface to modeling and simulating soft mobile multibody systems (SMMS) of any kind.
Please refer to the associated paper for more information.

## Getting Started

### Installation

This project depends on Philipp Tempel's MATLAB Tooling which first needs to be cloned and set up correctly.
Since MATLAB does still not feature a package manager, please follow the instructions over at https://gitlab.com/philipptempel/matlab-tooling on how to set up the dependency.
With the dependency set up, you can clone this repository
```bash
$ git clone https://gitlab.com/cosseroots/library/matlab.git
```
Once this is done, you can continue with the [Usage](#usage) section.

### Usage

To use COSSEROOTS MATLAB Library, you only need to register it with MATLAB's search path.
Do so by navigating into the project directory and activating the project

```matlab
>> cd('/path/to/cosseroots/library/matlab')
>> activate(MatlabProject.this())
```

If there are no error or warning messages displayed, you are all set up and ready to go.
