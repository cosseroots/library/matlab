classdef Cuboid < RigidBody
  %% CUBOID
  
  
  
  %% CONSTRUCTOR METHODS
  methods
    
    function obj = Cuboid(varargin)
      %% CUBOID
      
      
      obj@RigidBody(varargin{:});
      
    end
    
  end
  
  
  
  %% STATIC CONSTRUCTOR METHODS
  methods ( Static )
    
    function obj = rectangular(m, w, d, h, varargin)
      %% RECTANGULAR
      
      
      ww = w * w;
      dd = d * d;
      hh = h * h;
      inertia = 1 / 12 * m * diag([ dd + hh , ww + hh , ww + dd ]);
      trref = [ 0.0 ; 0.0 ; 0.0 ];
      
      
      obj = Cuboid( ...
          'SpatialInertiaMatrix', spatialInertiaMatrix(m, inertia, trref) ...
        , 'Visual', { ...
          RigidBodyVisual.cuboid(w, d, h, 'Name', 'Cuboid') ...
        } ...
        , varargin{:} ...
      );
      
    end
    
  end
  
end
