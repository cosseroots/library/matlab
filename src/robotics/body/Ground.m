classdef ( Sealed ) Ground < smms.internal.Base ...
    & smms.internal.Named
  %% GROUND
  
  
  
  %% PUBLIC PROPERTIES
  properties
    
    % Transformation from ground to inertial frame
    T (4, 4) double = tformzeros()
    
  end
  
  
  
  %% DEPENDENT PUBLIC PROPERTIES
  properties ( Dependent )
    
    T0 (4, 4) double
    
  end
  
  
  
  %% CONSTRUCTOR METHODS
  methods
    
    function obj = Ground(varargin)
      %% GROUND
      
      
      
      obj@smms.internal.Base();
      obj@smms.internal.Named();
      
      obj.Name = 'Ground';
      
      setProperties(obj, nargin, varargin{:});
      
    end
    
  end
  
  
  
  %% GETTERS
  methods
    
    function v = get.T0(obj)
      %% GET.T0
      
      
      
      v = obj.T;
      
    end
    
  end
  
  
  
  %% SETTERS
  methods
    
    function set.T0(obj, v)
      %% SET.T0
      
      
      
      obj.T = v;
      
    end
    
  end
  
end
