function [tau, vtau] = tangentInverseDynamics(obj, T, eta, etadot, fext, vzeta, veta, vetadot, vfext)%#codegen
%% TANGENTINVERSEDYNAMICS Calculate inverse dynamics of a rigid body
%
% [TAU, VTAU] = TANGENTINVERSEDYNAMICS(OBJ, G, ETA, ETADOT, FEXT, VZETA)
% calculates the tangent of the inverse dynamics VTAU given positional variation
% VZETA.
%
% [TAU, VTAU] = TANGENTINVERSEDYNAMICS(___, VZETA, VETA) includes variation of
% body twists VETA in the calculation.
%
% [TAU, VTAU] = TANGENTINVERSEDYNAMICS(___, VZETA, VETA, VETADOT) includes
% variation of body twist velocities VETADOT in the calculation.
%
% [TAU, VTAU] = TANGENTINVERSEDYNAMICS(___, VZETA, VETA, VETADOT, VFEXT)
% includes variation of external forces VFEXT in base coordinates in the
% calculation.
%
% Inputs:
%
%   OBJ                     Rigid body object.
% 
%   G                       7xN array of body positions.
% 
%   ETA                     6xN array of body twists.
% 
%   ETADOT                  6xN array of body twist velocities.
% 
%   FEXT                    6xN array of external forces in world coordinates.
%
%   VZETA                   6xVxN array of position varations.
%
%   VETA                    6xVxN array of variation of body twists.
%
%   VETADOT                 6xVxN array of variation of body twist velocities.
%
%   VFEXT                   6xVxN array of variation of external forces in base
%                           coordinates.
%
% Outputs:
%
%   VTAU                    6xVxN array of variation of generalized actuation
%                           forces.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% TANGENTINVERSEDYNAMICS(OBJ, T, ETA, ETADOT, FEXT, VZETA)
% TANGENTINVERSEDYNAMICS(OBJ, T, ETA, ETADOT, FEXT, VZETA, VETA)
% TANGENTINVERSEDYNAMICS(OBJ, T, ETA, ETADOT, FEXT, VZETA, VETA, VETADOT)
% TANGENTINVERSEDYNAMICS(OBJ, T, ETA, ETADOT, FEXT, VZETA, VETA, VETADOT, VFEXT)
narginchk(6, 9);

% TAU = TANGENTINVERSEDYNAMICS(___)
% [TAU, VTAU] = TANGENTINVERSEDYNAMICS(___)
nargoutchk(0, 2);

% Number of samples
ns = size(T, 2);
% Number of variations
nv = size(vzeta, 2);

% TANGENTINVERSEDYNAMICS(OBJ, G, ETA, ETADOT, FEXT, VZETA)
if nargin < 7 || isempty(veta)
  veta = zeros(6, nv, ns);
end

% TANGENTINVERSEDYNAMICS(OBJ, G, ETA, ETADOT, FEXT, VZETA, VETA)
if nargin < 8 || isempty(vetadot)
  vetadot = zeros(6, nv, ns);
end

% TANGENTINVERSEDYNAMICS(OBJ, G, ETA, ETADOT, FEXT, VZETA, VETA, VETADOT)
if nargin < 9 || isempty(vfext)
  vfext = zeros(6, nv, ns);
end



%% Algorithm

% Transformation from world to body coordinates.
Tb = tpar2tform(T);

% Local variables
M = obj.SpatialInertiaMatrix;

% Adjoint of transformation from A frame to B frame
Xb = Ad(Tb);

% Project variation of external forces expressed in base coordinates to body
% coordinates
vFext = pagemult(permute(Xb, [2, 1, 3]), vfext) + ... % Varation of external force
        ... % Variation of transformation of external force into body frame
        ... % ad(vzeta)^T * Ad(Tb)^T * fext => ad(vzeta)^T * v => -adt(v)^T * vzeta
        + pagemult( ...
            permute( ...
                adt( ...
                    permute( ...
                        pagemult( ...
                            permute(Xb, [2, 1, 3]) ...
                          , permute(fext, [1, 3, 2]) ...
                        ) ...
                      , [1, 3, 2] ...
                    ) ...
                  ) ...
                , [2, 1, 3] ...
              ) ...
            , vzeta ...
          );

% Project external forces from base coordinates to body coordinates
Fext = permute(pagemult(permute(Xb, [2, 1, 3]), permute(fext, [1, 3, 2])), [1, 3, 2]);

% Needed actuation
tau = M * etadot + ...
      - permute(pagemult(permute(ad(eta), [2, 1, 3]), permute(M * eta, [1, 3, 2])), [1, 3, 2]) + ...
      - Fext;

% Variation of total forces
vtau = pagemult(M, vetadot) + ...
      - pagemult( ...
          permute( ...
              ad(eta) ...
            , [2, 1, 3] ...
          ) ...
        , pagemult(M, veta) ...
      ) + ...
      ... % - transpose(ad(veta)) * M * eta => transpose(adt(M * eta), veta)
      - pagemult( ...
          permute( ...
              -adt(M * eta) ...
            , [2, 1, 3] ...
          ) ...
        , veta ...
      ) + ...
      - vFext;

end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
