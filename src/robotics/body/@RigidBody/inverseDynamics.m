function tau = inverseDynamics(obj, T, eta, etadot, fext)%#codegen
%% INVERSEDYNAMICS Calculate inverse dynamics of a rigid body
%
% TAU = INVERSEDYNAMICS(OBJ, T) calculates the actuation forces TAU needed to
% generate the rigid body's pose T.
%
% TAU = INVERSEDYNAMICS(OBJ, T, ETA) includes generalized twists ETA in the
% calculation.
%
% TAU = INVERSEDYNAMICS(OBJ, T, ETA, ETADOT) includes generalized body twist
% velocities ETADOT in the calculation.
%
% TAU = INVERSEDYNAMICS(OBJ, T, ETA, ETADOT, FEXT) considers external force FEXT
% in base coordinates in the calculation.
%
% Inputs:
%
%   OBJ                     Rigid body object.
% 
%   T                       4x4xN array of body poses.
% 
%   ETA                     6xN array of body twists.
% 
%   ETADOT                  6xN array of body twist velocities.
% 
%   FEXT                    6xN array of external forces in base coordinates.
%
% Outputs:
%
%   TAU                     6xN array of generalized actuation forces to attain
%                           T, ETA, ETADOT under FEXT.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% INVERSEDYNAMICS(OBJ, T)
% INVERSEDYNAMICS(OBJ, T, ETA)
% INVERSEDYNAMICS(OBJ, T, ETA, ETADOT)
% INVERSEDYNAMICS(OBJ, T, ETA, ETADOT, FEXT)
narginchk(2, 5);

% INVERSEDYNAMICS(___)
% TAU = INVERSEDYNAMICS(___)
nargoutchk(0, 1);

% Number of samples
ns = size(T, 3);

% INVERSEDYNAMICS(OBJ, T)
if nargin < 3 || isempty(eta)
  eta = twistzeros(ns);
end

% INVERSEDYNAMICS(OBJ, T, ETA)
if nargin < 4 || isempty(etadot)
  etadot = twistzeros(ns);
end

% INVERSEDYNAMICS(OBJ, T, ETA, ETADOT)
if nargin < 5 || isempty(fext)
  fext = zeros(6, ns);
end



%% Algorithm

% Local variables
M = obj.SpatialInertiaMatrix;

% Needed actuation
tau = M * etadot + ...
      - permute( ...
          pagemult( ...
              permute( ...
                  ad(eta) ...
                , [2, 1, 3] ...
              ) ...
            , permute( ...
                M * eta ...
              , [1, 3, 2] ...
            ) ...
          ) ...
        , [1, 3, 2] ...
      ) + ...
      - permute( ...
          pagemult( ...
              permute( ...
                  Ad(T) ...
                , [2, 1, 3] ...
              ) ...
            , permute( ...
                fext ...
              , [1, 3, 2] ...
            ) ...
          ) ...
          , [1, 3, 2] ...
        );


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
