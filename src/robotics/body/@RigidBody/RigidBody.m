classdef RigidBody < smms.internal.Base ...
    & smms.mixin.Drawable ...
    & smms.internal.Named ...
    & smms.internal.VisualContainer
  %% RIGIDBODY
  
  
  
  %% PUBLIC PROPERTIES
  properties
    
    % Joint attached to this body
    Joint (:, 1) = []
    
  end
  
  
  
  %% PUBLIC READ-ONLY PROPERTIES
  properties ( SetAccess = { ?smms.internal.InternalAccess } )
    
    % Parent body of the body
    Parent (:, 1) = []
    
  end
  
  
  
  %% PUBLIC READ-ONLY PROPERTIES
  properties ( SetAccess = { ?smms.internal.InternalAccess } )
    
    SpatialInertiaMatrix (6, 6) double = spatialInertiaMatrix(1, eye(3, 3), zeros(3, 1))
    
  end
  
  
  
  %% PUBLIC DEPENDENT PROPERTIES
  properties ( Dependent )
    
    % Linear mass of body
    Mass (1, 1) double
    
    % 3x3 moment of inertia tensor
    InertiaTensor (3, 3) double
    
    % Position of center of mass wrt point of action
    CenterOfMass  (3, 1) double
    
  end
  
  
  
  %% CONSTRUCTOR METHODS
  methods
    
    function obj = RigidBody(varargin)
      %% RIGIDBODY
      
      
      
      obj@smms.internal.Base();
      obj@smms.mixin.Drawable();
      obj@smms.internal.Named();
      obj@smms.internal.VisualContainer();
      
      setProperties(obj, nargin, varargin{:});
      
    end
    
  end
  
  
  
  %% SETTERS
  methods
    
    function set.Joint(obj, v)
      %% SET.JOINT
      
      
      
      % Set this object as the joint's parent
      v.Body = obj;
      obj.Joint = v;
      
    end
    
    
    function set.Mass(obj, v)
      %% SET.MASS
      
      
      
      obj.SpatialInertiaMatrix = spatialInertiaMatrix(v, obj.InertiaTensor, obj.CenterOfMass);
      
    end
    
    
    function set.InertiaTensor(obj, v)
      %% SET.INERTIATENSOR
      
      
      
      obj.SpatialInertiaMatrix = spatialInertiaMatrix(obj.Mass, v, obj.CenterOfMass);
      
    end
    
    
    function set.CenterOfMass(obj, v)
      %% SET.CENTEROFMASS
      
      
      
      obj.SpatialInertiaMatrix = spatialInertiaMatrix(obj.Mass, obj.InertiaTensor, v);
      
    end
    
  end
  
  
  
  %% GETTERS
  methods
    
    function v = get.Mass(obj)
      %% GET.MASS
      
      
      
      v = obj.SpatialInertiaMatrix(6,6);
      
    end
    
    
    function v = get.InertiaTensor(obj)
      %% GET.INERTIATENSOR
      
      
      
      v = obj.SpatialInertiaMatrix(1:3,1:3);
      
    end
    
    
    function v = get.CenterOfMass(obj)
      %% GET.CENTEROFMASS
      
      
      % If the body has some mass, then
      if obj.Mass > 0
        % Infer body mass from the spatial inertia matrix
        v = skew2vec(obj.SpatialInertiaMatrix(1:3,4:6)) ./ obj.Mass;
      
      % Else, no body mass i.e., no center of mass respectively it is at the
      % geometric center
      else
        v = zeros(3, 1);
        
      end
      
    end
    
  end
  
  
  
  %% CONCRETE MIXIN METHODS
  methods ( Access = protected )
    
    function setupImpl(obj)
      %% SETUPIMPL
      
      
      
      if ~isempty(obj.Joint)
        setup(obj.Joint);
      end
      
    end
    
    
    function resetImpl(obj)
      %% RESETIMPL
      
      
      
      if ~isempty(obj.Joint)
        reset(obj.Joint);
      end
      
    end
    
    
    function releaseImpl(obj)
      %% RELEASEIMPL
      
      
      
      if ~isempty(obj.Joint)
        release(obj.Joint);
      end
      
    end
    
    
    function h = drawImpl(obj, ax, varargin)
      %% DRAWIMPL
      
      
      
      % Visual group for the body and its visual shapes
      h = hggroup( ...
          ax ...
        , 'Tag', sprintf('%s Visuals', obj.Name) ...
      );
      
      % Loop over all visual shapes
      for is = 1:numel(obj.Visual)
        % Draw visual shape
        hh = draw(obj.Visual{is}, ax);
        
        % Assign visual shape to the body's transform group
        set(hh, 'Parent', h);
        
      end
      
    end
    
    
    function styleImpl(obj, h, varargin)
      %% STYLEIMPL
      
      
      
      % Loop over all visual shapes
      for is = 1:numel(obj.Visual)
        % Style visual shape
        style(obj.Visual{is}, findobj(h, 'Tag', obj.Visual{is}.Name));
        
      end
      
    end
    
  end
  
  
  
  %% DYNAMICS METHODS
  methods ( Sealed )
    
    tau = inverseDynamics(obj, g, eta, etadot, fext)
    %% INVERSEDYNAMICS
    
    
    [tau, vtau] = tangentInverseDynamics(obj, g, eta, etadot, fext, vzeta, veta, vetadot, vfext)
    %% TANGENTINVERSEDYNAMICS
    
  end
  
end
