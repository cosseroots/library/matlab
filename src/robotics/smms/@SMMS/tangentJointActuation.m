function [qad, vqad] = tangentJointActuation(obj, varargin)
%% TANGENTJOINTACTUATION
%
% TANGENTJOINTACTUATION(OBJ)
%
% TANGENTJOINTACTUATION(OBJ, Q)
%
% TANGENTJOINTACTUATION(OBJ, Q, QDOT)
%
% TANGENTJOINTACTUATION(OBJ, Q, QDOT, QDDOT)
%
% TANGENTJOINTACTUATION(OBJ, Q, QDOT, QDDOT, VQ)
%
% TANGENTJOINTACTUATION(OBJ, Q, QDOT, QDDOT, VQ, VQDOT)
%
% TANGENTJOINTACTUATION(OBJ, Q, QDOT, QDDOT, VQ, VQDOT, VQDDOT)
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% TANGENTJOINTACTUATION(OBJ)
% TANGENTJOINTACTUATION(OBJ, Q)
% TANGENTJOINTACTUATION(OBJ, Q, QDOT)
% TANGENTJOINTACTUATION(OBJ, Q, QDOT, QDDOT)
% TANGENTJOINTACTUATION(OBJ, Q, QDOT, QDDOT, VQ)
% TANGENTJOINTACTUATION(OBJ, Q, QDOT, QDDOT, VQ, VQDOT)
% TANGENTJOINTACTUATION(OBJ, Q, QDOT, QDDOT, VQ, VQDOT, VQDDOT)
narginchk(1, 7);

% TANGENTJOINTACTUATION(___)
% QAD = TANGENTJOINTACTUATION(___)
% [QAD, VQAD] = TANGENTJOINTACTUATION(___)
nargoutchk(0, 2);

% % Build cell array of default arguments merged with given arguments
% args = cell(1, 8);
% ind = [1,2,3,5,6,7];
% args(ind(1:(nargin-1))) = varargin;
% 
% % Validate input arguments and possibly create defaults
% [ q,  qdot,  qddot] = validateDynamicsInputs(obj, args{1:4});
% [vq, vqdot, vqddot] = validateTangentDynamicsInputs(obj, args{5:8});



%% Algorithm

% For now, no internal actuation supported
qad  = zeros(obj.VelocityNumber, 1);
% Thus, also no tangent of the joint actuation
vqad = zeros(obj.VelocityNumber, obj.VelocityNumber);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
