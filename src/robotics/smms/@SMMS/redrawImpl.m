function redrawImpl(obj, h, q, varargin)
%% REDRAWIMPL
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% REDRAWIMPL(OBJ, H, Q)
% REDRAWIMPL(OBJ, H, Q, ...)
narginchk(3, Inf);

% REDRAWIMPL(___)
nargoutchk(0, 0);



%% Algorithm

% Group for bodies and joints
hb = findobj(h, 'Tag', 'Body');
hj = findobj(h, 'Tag', 'Joint');

% Number of bodies/joints
nb = obj.NumBody;
pm = obj.PositionMap;

% Reconstruct displacement of each joint
[T, ~, ~] = smms.algorithms.forwardRNEA( ...
    obj ...
  , q, zeros(obj.VelocityNumber, 1), zeros(obj.VelocityNumber, 1) ...
);
% Displacement of each body
Tb = zeros(size(T));

% Loop over each body and plot it
for ib = 1:nb
  % Get parent of object
  pid = obj.Parent(ib);

  % Get location of parent object to draw joint at correct location
  if pid > 0
    Tp = T(:,:,pid);

  % No parent, so aligned with ground
  else
    Tp = obj.Ground.T;

  end

  bdy = obj.Body{ib};
  jnt = obj.Joint{ib};

  % Draw joint of body at its parent's position
  redraw(jnt, findobj(hj, 'Tag', jnt.Name), q(pm(ib,1):pm(ib,2)));
  set( ...
      findobj(hj, 'Tag', sprintf('Joint%d_Transform', ib)) ...
    , 'Matrix', tformmul(Tp, jnt.JointToParentTransform) ...
  );

  % Draw body at the given position
  Tb(:,:,ib) = tformmul(T(:,:,ib), jnt.BodyToJointTransform);
  redraw(bdy, findobj(hb, 'Tag', bdy.Name));
  set( ...
      findobj(hb, 'Tag', sprintf('Body%d_Transform', ib)) ...
    , 'Matrix', Tb(:,:,ib) ...
  );

end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
