function obj = disableGravity(obj)%#codegen
%% DISABLEGRAVITY Set gravity equal zero on an SMMS
%
% DISABLEGRAVITY(OBJ) sets gravity vector to all zeros.
%
% OBJ = DISABLEGRAVITY(OBJ) returns the robot object OBJ to allow for method
% chaining.
%
% Inputs:
%
%   OBJ                     SMMS object
%
% Outputs:
%
%   OBJ                     SMMS object for method chaining.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% DISABLEGRAVITY(OBJ)
narginchk(1, 1);

% DISABLEGRAVITY(___)
% OBJ = DISABLEGRAVITY(___)
nargoutchk(0, 1);



%% Algorithm

% Set gravity to zero
obj.Gravity = [ ...
  0.0 ; ...
  0.0 ; ...
  0.0 ; ...
];


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
