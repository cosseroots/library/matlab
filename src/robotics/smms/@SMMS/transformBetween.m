function T = transformBetween(obj, q, bodyName1, bodyName2)
%% TRANSFORMBETWEEN
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% TRANSFORMBETWEEN(OBJ, Q, BODYNAME)
% TRANSFORMBETWEEN(OBJ, Q, SOURCENAME, TARGETNAME)
narginchk(3, 4);

% TRANSFORMBETWEEN(___)
% T = TRANSFORMBETWEEN(___)
nargoutchk(0, 1);

% TRANSFORMBETWEEN(OBJ, CONF, BODYNAME)
if nargin < 4 || isempty(bodyName2)
  bodyName2 = 'ground';
end



%% Algorithm

% Calculate the forward kinematics to obtain poses for each body
Ttree = reconstructKinematics(obj, q);

% Find index of first body
bid1 = findBodyByName(obj, bodyName1);
% Ground
if bid1 == 0
  T1 = tformzeros();

% A named body
else
  T1 = Ttree(:,:,bid1);

end

% Find index of second body
bid2 = findBodyByName(obj, bodyName2);
% Ground
if bid2 == 0
  T2 = tformzeros();

% A named body
else
  T2 = Ttree(:,:,bid2);

end

T = transformBetween(T2, T1);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
