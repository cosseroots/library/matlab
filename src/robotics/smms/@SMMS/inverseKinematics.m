function varargout = inverseKinematics(obj, Ttcp, qg, options)
%% INVERSEKINEMATICS
%
% QINV = INVERSEKINEMATICS(ROBOT, TTCP)
%
% QINV = INVERSEKINEMATICS(ROBOT, TTCP, QG)
%
% QINV = INVERSEKINEMATICS(___, OPTIONS)
%
% [QINV, FVAL] = INVERSEKINEMATICS(___)
%
% [QINV, FVAL, EXITFLAG] = INVERSEKINEMATICS(___)
%
% [QINV, FVAL, EXITFLAG, OUTPUT] = INVERSEKINEMATICS(___)
%
% [QINV, FVAL, EXITFLAG, OUTPUT, JACOBIAN] = INVERSEKINEMATICS(___)
%
% Inputs:
%
%   OBJ                     SMMS object.
%
%   TTCP                    4x4 homogeneous transformation matrix of the desired
%                           end effector / TCP pose.
%
%   QG                      Px1 array of guesstimate of joint configuration
%                           variables used to initialize the Newton-Raphson loop
%                           of inverse kinematics.
%
%   OPTIONS                 Structure of options compatible with FSOLVE.
%
% Outputs:
%
%   QINV                    Px1 array of generalized joint configuration values
%                           that solve te inverse kinematics.
%
%   FVAL                    6x1 array of value of inverse kinematics residual at
%                           the final value QINV.
%
%   EXITFLAG                Integer describing the exit condition. Possible
%                           values can be found in FSOLVE's help.
%
%   OUTPUT                  Structure providing additional information on the
%                           solver output.
%
%   JACOB                   6xP array of the Jacobian of the inverse kinematics
%                           residual function at QINV.
%
% See also
%   FSOLVE
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% QINV = INVERSEKINEMATICS(ROBOT, TTCP)
% QINV = INVERSEKINEMATICS(ROBOT, TTCP, QG)
% QINV = INVERSEKINEMATICS(ROBOT, TTCP, QG, OPTIONS)
narginchk(2, 4);

% INVERSEKINEMATICS(___)
% QINV = INVERSEKINEMATICS(___)
% [QINV, FVAL] = INVERSEKINEMATICS(___)
% [QINV, FVAL, EXITFLAG] = INVERSEKINEMATICS(___)
% [QINV, FVAL, EXITFLAG, OUTPUT] = INVERSEKINEMATICS(___)
% [QINV, FVAL, EXITFLAG, OUTPUT, JACOBIAN] = INVERSEKINEMATICS(___)
nargoutchk(0, 5);

% INVERSEKINEMATICS(OBJ, T)
if nargin < 3 || isempty(qg)
  % Get initial guess for joint coordinates
  qg = configurationHome(obj);

end

% INVERSEKINEMATICS(OBJ, T, QEGUESS)
if nargin < 4 || isempty(options)
  options = struct();
end
options = struct2nvpairs(options);



%% Algorithm

persistent nopts_
if isempty(nopts_)
  nopts_ = struct( ...
      'TolFun'  , 1e-12 ...
    , 'TolX'    , 1e-12 ...
    , 'MaxIter' , 50 ...
    , 'Display' , 'off' ...
  );
end

if ~isempty(options)
  nopts = optimset(nopts_, options{:});
else
  nopts = nopts_;
end

% Number of...
np = obj.PositionNumber;
nd = obj.VelocityNumber;

% Additional variables
vq     = eye(np, nd);

% Find residual using Newton-Raphson method
[varargout{1:nargout}] = newtonraphson( ...
    @(q_) funIkGoal(obj, Ttcp, q_, vq) ...
  , qg ...
  , nopts ...
);


end


function [res, jac] = funIkGoal(rbt, Td, q, vq)
%% FUNIKGOAL
%
% RES = FUNIKGOAL(RBT, TD, Q, VQ)
%
% [RES, JAC] = FUNIKGOAL(___)



% Calculate forward tangent RNEA
[Ttcp, vZtcp] = tangentForwardKinematics(rbt, q, vq);

% Change in transformation from desired to current end effector position
vTcs = tformmul(tform2inv(Ttcp), Td);
% Increment between desired and actual pose
res = skew2vec(vTcs - eye(4, 4));

% Jacobian of the twists (note the minus sign because the error above is
% T_{i}^{-1} * T_{d} so it is somewhat similar to "desired - actual"
% thus the Jacobian of this function is "-jac(actual)"
jac = -skew2vec( ...
    pagemult( ...
        vec2skew(vZtcp) ...
      , vTcs ...
    ) ...
  );

end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
