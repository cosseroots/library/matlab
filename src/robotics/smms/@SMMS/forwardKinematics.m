function Tee = forwardKinematics(obj, varargin)
%% FORWARDKINEMATICS
%
% TEE = FORWARDKINEMATICS(OBJ)
%
% TEE = FORWARDKINEMATICS(OBJ, Q)
%
% Inputs:
%
%   OBJ                     SMMS object.
%
%   Q                       PxN array of joint configuration variables for N
%                           time values.
%
% Outputs:
%
%   TEE                     4x4xN homogeneous transformation matrices of the
%                           manipulator's end-effector pose(s).
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% FORWARDKINEMATICS(OBJ)
% FORWARDKINEMATICS(OBJ, Q)
narginchk(1, 2);

% FORWARDKINEMATICS(___)
% TEE = FORWARDKINEMATICS(___)
nargoutchk(0, 1);

% Build cell array of default arguments merged with given arguments
args = cell(1, 4);
ind = 1;
args(ind(1:(nargin-1))) = varargin;

% Parse arguments to get sane defaults
[q, qdot, qddot] = validateDynamicsInputs(obj, args{:});

% Count time values
nt = size(q, 2);



%% Algorithm

% Initialize output
Tee = tformzeros(nt);

% Loop over all time values
for it = 1:nt
  % Do forward RNEA calculation
  [T_, ~, ~] = smms.algorithms.forwardRNEA(obj, q(:,it), qdot(:,it), qddot(:,it));

  % Get EE's pose
  Tee(:,:,it) = T_(:,:,end);

end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
