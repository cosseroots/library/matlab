function [T, eta, etadot] = reconstructKinematics(obj, varargin)
%% RECONSTRUCTKINEMATICS Reconstruct each body's kinematics
%
% RECONSTRUCTKINEMATICS(OBJ)
%
% RECONSTRUCTKINEMATICS(OBJ, Q)
%
% RECONSTRUCTKINEMATICS(OBJ, Q, QDOT)
%
% RECONSTRUCTKINEMATICS(OBJ, Q, QDOT, QDDOT)
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% RECONSTRUCTKINEMATICS(OBJ)
% RECONSTRUCTKINEMATICS(OBJ, Q)
% RECONSTRUCTKINEMATICS(OBJ, Q, QDOT)
% RECONSTRUCTKINEMATICS(OBJ, Q, QDOT, QDDOT)
narginchk(1, 4);

% RECONSTRUCTKINEMATICS(___)
% T = RECONSTRUCTKINEMATICS(___)
% [T, ETA] = RECONSTRUCTKINEMATICS(___)
% [T, ETA, ETADOT] = RECONSTRUCTKINEMATICS(___)
nargoutchk(0, 3);

% Build cell array of default arguments merged with given arguments
args = cell(1, 4);
ind = [1,2,3];
args(ind(1:(nargin-1))) = varargin;

% Validate inputs
[q, qdot, qddot] = validateDynamicsInputs(obj, args{:});



%% Algorithm

% Number of...
nb = obj.NumBody;

% Solve the tangent forward of each joint
[Tj, etaj, etadotj] = smms.algorithms.forwardRNEA( ...
    obj ...
  , q, qdot, qddot ...
);

% Initialize local transformations for each joint-to-body and body-to-joint
T_B2J = tformzeros(nb);
T_J2B = tformzeros(nb);

% Loop over each body to get its relative transformation wrt to the joint
for ib = 1:nb
  jnt = obj.Joint{ib};
  T_B2J(:,:,ib) = jnt.BodyToJointTransform;
  T_J2B(:,:,ib) = jnt.JointToBodyTransform;
end

% Pre-calculate Adjoint of all transformations
X_J2B = Ad(T_J2B);



%% Assign outputs

% T = RECONSTRUCTKINEMATICS(___)
% [T, ETA] = RECONSTRUCTKINEMATICS(___)
% [T, ETA, ETADOT] = RECONSTRUCTKINEMATICS(___)
T = tformmul(Tj, T_B2J);

% [T, ETA] = RECONSTRUCTKINEMATICS(___)
% [T, ETA, ETADOT] = RECONSTRUCTKINEMATICS(___)
if nargout > 1
  eta     = permute(pagemult(X_J2B, permute(etaj, [1, 3, 2])), [1, 3, 2]);
end

% [T, ETA, ETADOT] = RECONSTRUCTKINEMATICS(___)
if nargout > 2
  etadot  = permute(pagemult(X_J2B, permute(etadotj, [1, 3, 2])), [1, 3, 2]);
end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
