function obj = enableGravity(obj, g)%#codegen
%% ENABLEGRAVITY Set gravity on an SMMS
%
% ENABLEGRAVITY(OBJ) sets gravity vector to default value of -9.81 * EZ.
%
% OBJ = DISABLEGRAVITY(OBJ) returns the robot object OBJ to allow for method
% chaining.
%
% Inputs:
%
%   OBJ                     SMMS object.
%
%   G                       3x1 vector of gravitational force direction and
%                           magnitude.
%
% Outputs:
%
%   OBJ                     SMMS object for method chaining.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% ENABLEGRAVITY(OBJ)
% ENABLEGRAVITY(OBJ, G)
narginchk(1, 2);

% ENABLEGRAVITY(___)
% OBJ = ENABLEGRAVITY(___)
nargoutchk(0, 1);

% ENABLEGRAVITY(OBJ)
if nargin < 2 || isempty(g)
  g = [ 0.0 ; 0.0 ; -9.81 ];
end



%% Algorithm

% Set gravity to given value
obj.Gravity = g;


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
