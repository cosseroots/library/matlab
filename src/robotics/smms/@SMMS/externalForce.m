function fext = externalForce(obj, bodyname, wrench, q)
%% EXTERNALFORCE
%
% EXTERNALFORCE(OBJ, BODYNAME, WRENCH)
%
% EXTERNALFORCE(OBJ, BODYNAME, WRENCH, Q)
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% EXTERNALFORCE(OBJ, BODYNAME, WRENCH)
% EXTERNALFORCE(OBJ, BODYNAME, WRENCH, Q)
narginchk(3, 4);

% EXTERNALFORCE(___)
% FEXT = EXTERNALFORCE(___)
nargoutchk(0, 1);

prepareObjectForUse(obj);



%% Algorithm

bid = findBodyByName(obj, bodyname);

if bid < 0
  error('Body not found');
end

% Transformation from body to world coordinates
X = eye(6, 6);

% If a configuration was given
if nargin > 3 && ~isempty(q)
  % Reconstruct each body's coordinates
  [T, ~, ~] = smms.algorithms.forwardRNEA( ...
      obj ...
    , q, zeros(obj.VelocityNumber, 1), zeros(obj.VelocityNumber, 1) ...
  );

  % Then calculate the adjoint transformation i -> 0 of the force
  X = Ad(tform2inv(T(:,:,bid)));

end

% Initialize outputs
fext = zeros(6, obj.NumBody, size(wrench, 2));

% And calculate force on the body
fext(:,bid,:) = permute(permute(X, [2, 1]) * wrench, [1, 3, 2]);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
