function D = dampingMatrix(obj, varargin)
%% DAMPINGMATRIX
%
% DAMPINGMATRIX(OBJ, Q)
%
% DAMPINGMATRIX(OBJ, Q, QDOT)
%
% DAMPINGMATRIX(OBJ, Q, QDOT, QDDOT)
%
% DAMPINGMATRIX(OBJ, Q, QDOT, QDDOT, FEXT)
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% DAMPINGMATRIX(OBJ, Q)
% DAMPINGMATRIX(OBJ, Q, QDOT)
% DAMPINGMATRIX(OBJ, Q, QDOT, QDDOT)
% DAMPINGMATRIX(OBJ, Q, QDOT, QDDOT, FEXT)
narginchk(2, 5);

% DAMPINGMATRIX(___)
% D = DAMPINGMATRIX(___)
nargoutchk(0, 1);

% Build cell array of default arguments merged with given arguments
args = cell(1, 4);
ind = [1,2,3,4];
args(ind(1:(nargin-1))) = varargin;

% Validate input arguments and create defaults
[q, qdot, qddot, fext] = validateDynamicsInputs(obj, args{:});



%% Algorithm

% Initialize Jacobian matrices
nv     = obj.VelocityNumber;
vq     = zeros(nv, nv);
vqdot  = eye(nv, nv);
vqddot = zeros(nv, nv);

% Jacobian of external forces
vfext = zeros(6, nv, obj.NumBody);

% Calculate damping matrix based on the TIDM algorithm using appropriate
% arguments as defined above
[~, D] = smms.algorithms.tangentInverseDynamics( ...
    obj ...
  , q, qdot, qddot ...
  , fext ...
  , vq, vqdot, vqddot ...
  , vfext ...
);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
