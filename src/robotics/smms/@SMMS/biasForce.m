function C = biasForce(obj, q, qdot, Fext)
%% BIASFORCE
%
% BIASFORCE(OBJ, Q)
%
% BIASFORCE(OBJ, Q, QDOT)
%
% BIASFORCE(OBJ, Q, QDOT, FEXT)
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% BIASFORCE(OBJ, Q)
% BIASFORCE(OBJ, Q, QDOT)
% BIASFORCE(OBJ, Q, QDOT, FEXT)
narginchk(2, 4);

% BIASFORCE(___)
% C = BIASFORCE(___)
nargoutchk(0, 1);

prepareObjectForUse(obj);

% BIASFORCE(OBJ, Q)
if nargin < 3 || isempty(qdot)
  qdot = zeros(obj.VelocityNumber, 1);
end

% BIASFORCE(OBJ, Q, QDOT)
if nargin < 4 || isempty(Fext)
  Fext = zeros(6, obj.NumBody);
end



%% Algorithm

% Eqn (6.2) of Featherstone.2008
C = smms.algorithms.inverseDynamics( ...
    obj ...
  , q, qdot, 0 * qdot ...
  , Fext ...
);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
