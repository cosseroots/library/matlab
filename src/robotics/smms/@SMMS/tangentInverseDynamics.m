function [tau, vtau] = tangentInverseDynamics(obj, varargin)
%% TANGENTINVERSEDYNAMICS
%
% [TAU, VTAU] = TANGENTINVERSEDYNAMICS(OBJ, Q, QDOT, QDDOT, FEXT, VQ)
%
% [TAU, VTAU] = TANGENTINVERSEDYNAMICS(OBJ, Q, QDOT, QDDOT, FEXT, VQ, VQDOT)
%
% [TAU, VTAU] = TANGENTINVERSEDYNAMICS(OBJ, Q, QDOT, QDDOT, FEXT, VQ, VQDOT, VQDDOT)
%
% [TAU, VTAU] = TANGENTINVERSEDYNAMICS(OBJ, Q, QDOT, QDDOT, FEXT, VQ, VQDOT, VQDDOT, VFEXT)
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% TANGENTINVERSEDYNAMICS(OBJ, Q, QDOT, QDDOT, FEXT, VQ)
% TANGENTINVERSEDYNAMICS(OBJ, Q, QDOT, QDDOT, FEXT, VQ, VQDOT)
% TANGENTINVERSEDYNAMICS(OBJ, Q, QDOT, QDDOT, FEXT, VQ, VQDOT, VQDDOT)
% TANGENTINVERSEDYNAMICS(OBJ, Q, QDOT, QDDOT, FEXT, VQ, VQDOT, VQDDOT, VFEXT)
narginchk(1, 9);

% [TAU, VTAU] = TANGENTINVERSEDYNAMICS(___)
nargoutchk(2, 2);

% Build cell array of default arguments merged with given arguments
args = cell(1, 8);
ind = [1,2,3,4,5,6,7,8];
args(ind(1:(nargin-1))) = varargin;

% Validate input arguments and possibly create defaults
[ q,  qdot,  qddot,  fext] = validateDynamicsInputs(obj, args{1:4});
[vq, vqdot, vqddot, vfext] = validateTangentDynamicsInputs(obj, args{5:8});



%% Algorithm

% Call the respective dynamics algorithm
[tau, vtau] = smms.algorithms.tangentInverseDynamics( ...
    obj ...
  , q, qdot, qddot ...
  , fext ...
  , vq, vqdot, vqddot ...
  , vfext ...
);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
