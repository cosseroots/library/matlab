function Fc = configurationForces(obj, q, Fext)
%% CONFIGURATIONFORCES
%
% CONFIGURATIONFORCES(OBJ, Q)
%
% CONFIGURATIONFORCES(OBJ, Q, FEXT)
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% CONFIGURATIONFORCES(OBJ, Q)
% CONFIGURATIONFORCES(OBJ, Q, FEXT)
narginchk(2, 3);

% CONFIGURATIONFORCES(___)
% FC = CONFIGURATIONFORCES(___)
nargoutchk(0, 1);

prepareObjectForUse(obj);

% CONFIGURATIONFORCES(OBJ, Q)
if nargin < 3 || isempty(Fext)
  Fext = zeros(6, obj.NumBody);
end



%% Algorithm

% Vector of zero joint velocities and zero joint accelerations
qz = zeros(obj.VelocityNumber, 1);

% Eqn. (33) of Boyer.2021
Fc = smms.algorithms.inverseDynamics( ...
    obj ...
  , q, qz, qz ...
  , Fext ...
);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
