function tau = gravityTorque(obj, varargin)
%% GRAVITYTORQUE
%
% TAUG = GRAVITYTORQUE(OBJ, Q)
%
% Inputs:
%
%   OBJ                     SMMS object.
%
%   Q                       PxN array of joint configuration variables for N
%                           time values.
%
% Outputs:
% 
%   TAUG                    VxN array of joint actuation torques to compensate
%                           gravitational forces acting on the system.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% GRAVITYTORQUE(OBJ, Q)
narginchk(2, 2);

% GRAVITYTORQUE(OBJ, Q)
% TAU = GRAVITYTORQUE(OBJ, Q)
nargoutchk(0, 1);

% Build cell array of default arguments merged with given arguments
args = cell(1, 4);
ind = 1;
args(ind(1:(nargin-1))) = varargin;

% Validate input arguments and create defaults
[q, qdot, qddot, fext] = validateDynamicsInputs(obj, args{:});

% Count time values
nt = size(q, 2);



%% Algorithm

% Initialize output
tau = zeros(obj.VelocityNumber, nt);

% Loop over all time values
for it = 1:nt
  % Call the respective dynamics algorithm
  tau(:,it) = smms.algorithms.inverseDynamics( ...
      obj ...
    , q(:,it), qdot(:,it), qddot(:,it) ...
    , fext(:,:,it) ...
  );

end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
