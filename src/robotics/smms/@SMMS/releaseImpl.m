function releaseImpl(obj)
%% RELEASEIMPL
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% RELEASEIMPL(OBJ)
narginchk(1, 1);

% RELEASEIMPL(___)
nargoutchk(0, 0);



%% Algorithm

% Loop over each body-joint combination
for ij = 1:obj.NumBody
  % Relase each body and its associated joint
  release(obj.Body{ij});

end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
