function h = drawImpl(obj, ax, q, varargin)
%% DRAWIMPL
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% DRAWIMPL(OBJ, AX, Q)
% DRAWIMPL(OBJ, AX, Q, ...)
narginchk(3, Inf);

% DRAWIMPL(___)
% H = DRAWIMPL(___)
nargoutchk(0, 1);

prepareObjectForUse(obj);



%% Algorithm

% Group for the whole model
h = hggroup(ax, 'Tag', 'SMMS');

% Group for bodies and joints
hb = hggroup(ax, 'Parent', h, 'Tag', 'Body');
hj = hggroup(ax, 'Parent', h, 'Tag', 'Joint');

% Number of bodies/joints
nb = obj.NumBody;
pm = obj.PositionMap;

% Reconstruct states of each joint
[T, ~, ~] = smms.algorithms.forwardRNEA( ...
    obj ...
  , q, zeros(obj.VelocityNumber, 1), zeros(obj.VelocityNumber, 1) ...
);
% Displacement of each body
Tb = zeros(size(T));

% Loop over each body and plot it
for ib = 1:nb
  % Get parent of object
  pid = obj.Parent(ib);

  % Get location of parent object to draw joint at correct location
  if pid > 0
    Tp = T(:,:,pid);

  % No parent, so aligned with ground
  else
    Tp = obj.Ground.T;

  end

  % Get body and joint object
  bdy = obj.Body{ib};
  jnt = obj.Joint{ib};

  % Draw joint of body at its parent's position
  hj_ = draw(jnt, ax, q(pm(ib,1):pm(ib,2)));
  hjt = hgtransform( ...
      ax ...
    , 'Parent', hj ...
    , 'Tag'   , sprintf('Joint%d_Transform', ib) ...
    , 'Matrix', tformmul(Tp, jnt.JointToParentTransform) ...
  );
  set(hj_, 'Parent', hjt);

  % Draw body at the given position
  Tb(:,:,ib) = tformmul(T(:,:,ib), jnt.BodyToJointTransform);
  hb_ = draw(bdy, ax);
  hbt = hgtransform( ...
      ax ...
    , 'Parent', hb ...
    , 'Tag'   , sprintf('Body%d_Transform', ib) ...
    , 'Matrix', Tb(:,:,ib) ...
  );
  set(hb_, 'Parent', hbt);

end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
