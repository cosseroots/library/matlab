function q = configurationHome(obj)
%% CONFIGURATIONHOME
%
% CONFIGURATIONHOME(OBJ)
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% CONFIGURATIONHOME(OBJ)
narginchk(1, 1);

% CONFIGURATIONHOME(___)
% Q = CONFIGURATIONHOME(___)
nargoutchk(0, 1);

prepareObjectForUse(obj);



%% Algorithm

q = zeros(obj.PositionNumber, 1);

for ib = 1:obj.NumBody
  a = obj.PositionMap(ib,:);
  q(a(1):a(2)) = configurationHome(obj.Joint{ib});
end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
