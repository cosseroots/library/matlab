function Fv = velocityForces(obj, q, qdot, Fext)
%% VELOCITYFORCES
%
% VELOCITYFORCES(OBJ, Q)
%
% VELOCITYFORCES(OBJ, Q, QDOT)
%
% VELOCITYFORCES(OBJ, Q, QDOT, FEXT)
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% VELOCITYFORCES(OBJ, Q)
% VELOCITYFORCES(OBJ, Q, QDOT)
% VELOCITYFORCES(OBJ, Q, QDOT, FEXT)
narginchk(2, 4);

% VELOCITYFORCES(___)
% FV = VELOCITYFORCES(___)
nargoutchk(0, 1);

prepareObjectForUse(obj);

% VELOCITYFORCES(OBJ, Q)
if nargin < 3 || isempty(qdot)
  qdot = zeros(obj.VelocityNumber, 1);
end

% VELOCITYFORCES(OBJ, Q, QDOT)
if nargin < 4 || isempty(Fext)
  Fext = zeros(6, obj.NumBody);
end



%% Algorithm

% Vector of zero joint velocities and zero joint accelerations
qz = zeros(obj.VelocityNumber, 1);

% Eqn. (34) of Boyer.2021
Fv = smms.algorithms.inverseDynamics( ...
        obj ...
      , q, qdot, qz ...
      , Fext ...
    ) + ...
    - smms.algorithms.inverseDynamics( ...
        obj ...
      , q, qz, qz ...
      , Fext ...
    );


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
