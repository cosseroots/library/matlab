function qad = jointActuation(obj, varargin)
%% JOINTACTUATION
%
% JOINTACTUATION(OBJ)
%
% JOINTACTUATION(OBJ, Q)
%
% JOINTACTUATION(OBJ, Q, QDOT)
%
% JOINTACTUATION(OBJ, Q, QDOT, QDDOT)

%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% JOINTACTUATION(OBJ)
% JOINTACTUATION(OBJ, Q)
% JOINTACTUATION(OBJ, Q, QDOT)
% JOINTACTUATION(OBJ, Q, QDOT, QDDOT)
narginchk(1, 4);

% JOINTACTUATION(___)
% QAD = JOINTACTUATION(___)
nargoutchk(0, 1);

% % Build cell array of default arguments merged with given arguments
% args = cell(1, 4);
% ind = [1,2,3]
% args(ind(1:(nargin-1))) = varargin;
% 
% % Validate input arguments and possibly create defaults
% [ q,  qdot,  qddot] = validateDynamicsInputs(obj, args{:});



%% Algorithm

% For now, there is no internal actuation supported
qad = zeros(obj.VelocityNumber, 1);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
