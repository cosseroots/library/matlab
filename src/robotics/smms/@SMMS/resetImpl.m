function resetImpl(obj)
%% RESETIMPL
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% RESETIMPL(OBJ)
narginchk(1, 1);

% RESETIMPL(___)
nargoutchk(0, 0);



%% Algorithm

% Loop over each joint
for ij = 1:obj.NumBody
  % Reset each body and its associated joint
  reset(obj.Body{ij});

end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
