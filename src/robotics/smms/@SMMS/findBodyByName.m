function bid = findBodyByName(obj, bname)
%% FINDBODYBYNANE
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% FINDBODYBYNANE(OBJ, BNAME)
narginchk(2, 2);

% FINDBODYBYNANE(___)
% BID = FINDBODYBYNANE(___)
nargoutchk(0, 1);



%% Algorithm

% Default output: not found
bid = -1;

% Check if ground is requested
if strcmpi(obj.Ground.Name, bname)
  bid = 0;
  return
end

% Loop over each body
for ib = 1:obj.NumBody
  % If names match
  if strcmp(obj.Body{ib}.Name, bname)
    % Break out of loop
    bid = ib;
    break;

  end

end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
