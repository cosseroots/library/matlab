function K = stiffnessMatrix(obj, varargin)
%% STIFFNESSMATRIX
%
% STIFFNESSMATRIX(OBJ, Q)
%
% STIFFNESSMATRIX(OBJ, Q, QDOT)
%
% STIFFNESSMATRIX(OBJ, Q, QDOT, QDDOT)
%
% STIFFNESSMATRIX(OBJ, Q, QDOT, QDDOT, FEXT)
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% STIFFNESSMATRIX(OBJ, Q)
% STIFFNESSMATRIX(OBJ, Q, QDOT)
% STIFFNESSMATRIX(OBJ, Q, QDOT, QDDOT)
% STIFFNESSMATRIX(OBJ, Q, QDOT, QDDOT, FEXT)
narginchk(2, 5);

% STIFFNESSMATRIX(___)
% K = STIFFNESSMATRIX(___)
nargoutchk(0, 1);

% Build cell array of default arguments merged with given arguments
args = cell(1, 4);
ind = [1,2,3,4];
args(ind(1:(nargin-1))) = varargin;

% Validate input arguments and create defaults
[q, qdot, qddot, fext] = validateDynamicsInputs(obj, args{:});



%% Algorithm

% Initialize Jacobian matrices
nv     = obj.VelocityNumber;
vq     = eye(nv, nv);
vqdot  = zeros(nv, nv);
vqddot = zeros(nv, nv);

% Jacobian of external forces
vfext = zeros(6, nv, obj.NumBody);

% Calculate stiffness matrix based on the TIDM algorithm using appropriate
% arguments as defined above
[~, K] = smms.algorithms.tangentInverseDynamics( ...
    obj ...
  , q, qdot, qddot ...
  , fext ...
  , vq, vqdot, vqddot ...
  , vfext ...
);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
