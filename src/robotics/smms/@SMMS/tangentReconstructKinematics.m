function [T, eta, etadot, vzeta, veta, vetadot] = tangentReconstructKinematics(obj, varargin)
%% TANGENTRECONSTRUCTKINEMATICS Reconstruct each body's kinematics
%
% TANGENTRECONSTRUCTKINEMATICS(OBJ)
%
% TANGENTRECONSTRUCTKINEMATICS(OBJ, Q)
%
% TANGENTRECONSTRUCTKINEMATICS(OBJ, Q, QDOT)
%
% TANGENTRECONSTRUCTKINEMATICS(OBJ, Q, QDOT, QDDOT)
%
% TANGENTRECONSTRUCTKINEMATICS(OBJ, Q, QDOT, QDDOT, VQ)
%
% TANGENTRECONSTRUCTKINEMATICS(OBJ, Q, QDOT, QDDOT, VQ, VQDOT)
%
% TANGENTRECONSTRUCTKINEMATICS(OBJ, Q, QDOT, QDDOT, VQ, VQDOT, VQDDOT)
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% TANGENTRECONSTRUCTKINEMATICS(OBJ)
% TANGENTRECONSTRUCTKINEMATICS(OBJ, Q)
% TANGENTRECONSTRUCTKINEMATICS(OBJ, Q, QDOT)
% TANGENTRECONSTRUCTKINEMATICS(OBJ, Q, QDOT, QDDOT)
% TANGENTRECONSTRUCTKINEMATICS(OBJ, Q, QDOT, QDDOT, VQ)
% TANGENTRECONSTRUCTKINEMATICS(OBJ, Q, QDOT, QDDOT, VQ, VQDOT)
% TANGENTRECONSTRUCTKINEMATICS(OBJ, Q, QDOT, QDDOT, VQ, VQDOT, VQDDOT)
narginchk(1, 7);

% TANGENTRECONSTRUCTKINEMATICS(___)
% T = TANGENTRECONSTRUCTKINEMATICS(___)
% [T, ETA] = TANGENTRECONSTRUCTKINEMATICS(___)
% [T, ETA, ETADOT] = TANGENTRECONSTRUCTKINEMATICS(___)
% [T, ETA, ETADOT, VZETA] = TANGENTRECONSTRUCTKINEMATICS(___)
% [T, ETA, ETADOT, VZETA, VETA] = TANGENTRECONSTRUCTKINEMATICS(___)
% [T, ETA, ETADOT, VZETA, VETA, VETADOT] = TANGENTRECONSTRUCTKINEMATICS(___)
nargoutchk(0, 6);

% Build cell array of default arguments merged with given arguments
args = cell(1, 8);
ind = [1,2,3,5,6,7];
args(ind(1:(nargin-1))) = varargin;

% Validate inputs
[ q,  qdot,  qddot] = validateDynamicsInputs(obj, args{1:4});
[vq, vqdot, vqddot] = validateTangentDynamicsInputs(obj, args{5:8});



%% Algorithm

% Number of...
nb = obj.NumBody;

% Solve the tangent forward of each joint
[Tj, etaj, etadotj, vzetaj, vetaj, vetadotj] = smms.algorithms.tangentForwardRNEA( ...
    obj ...
  , q, qdot, qddot ...
  , vq, vqdot, vqddot ...
);

% Initialize local transformations for each joint-to-body and body-to-joint
T_B2J = tformzeros(nb);
T_J2B = tformzeros(nb);

% Loop over each body to get its relative transformation wrt to the joint
for ib = 1:nb
  jnt = obj.Joint{ib};
  T_B2J(:,:,ib) = jnt.BodyToJointTransform;
  T_J2B(:,:,ib) = jnt.JointToBodyTransform;
end

% Pre-calculate Adjoint of all transformations
X_J2B = Ad(T_J2B);



%% Assign outputs

% T = TANGENTRECONSTRUCTKINEMATICS(___)
% [T, ETA] = TANGENTRECONSTRUCTKINEMATICS(___)
% [T, ETA, ETADOT] = TANGENTRECONSTRUCTKINEMATICS(___)
% [T, ETA, ETADOT, VZETA] = TANGENTRECONSTRUCTKINEMATICS(___)
% [T, ETA, ETADOT, VZETA, VETA] = TANGENTRECONSTRUCTKINEMATICS(___)
% [T, ETA, ETADOT, VZETA, VETA, VETADOT] = TANGENTRECONSTRUCTKINEMATICS(___)
T = tformmul(Tj, T_B2J);

% [T, ETA] = TANGENTRECONSTRUCTKINEMATICS(___)
% [T, ETA, ETADOT] = TANGENTRECONSTRUCTKINEMATICS(___)
% [T, ETA, ETADOT, VZETA] = TANGENTRECONSTRUCTKINEMATICS(___)
% [T, ETA, ETADOT, VZETA, VETA] = TANGENTRECONSTRUCTKINEMATICS(___)
% [T, ETA, ETADOT, VZETA, VETA, VETADOT] = TANGENTRECONSTRUCTKINEMATICS(___)
if nargout > 1
  eta     = permute(pagemult(X_J2B, permute(etaj, [1, 3, 2])), [1, 3, 2]);
end

% [T, ETA, ETADOT] = TANGENTRECONSTRUCTKINEMATICS(___)
% [T, ETA, ETADOT, VZETA] = TANGENTRECONSTRUCTKINEMATICS(___)
% [T, ETA, ETADOT, VZETA, VETA] = TANGENTRECONSTRUCTKINEMATICS(___)
% [T, ETA, ETADOT, VZETA, VETA, VETADOT] = TANGENTRECONSTRUCTKINEMATICS(___)
if nargout > 2
  etadot  = permute(pagemult(X_J2B, permute(etadotj, [1, 3, 2])), [1, 3, 2]);
end

% [T, ETA, ETADOT, VZETA] = TANGENTRECONSTRUCTKINEMATICS(___)
% [T, ETA, ETADOT, VZETA, VETA] = TANGENTRECONSTRUCTKINEMATICS(___)
% [T, ETA, ETADOT, VZETA, VETA, VETADOT] = TANGENTRECONSTRUCTKINEMATICS(___)
% Rigid-body transformation of variation of kinematic properties
if nargout > 3
  vzeta   = pagemult(X_J2B, vzetaj);
end

% [T, ETA, ETADOT, VZETA, VETA] = TANGENTRECONSTRUCTKINEMATICS(___)
% [T, ETA, ETADOT, VZETA, VETA, VETADOT] = TANGENTRECONSTRUCTKINEMATICS(___)
if nargout > 4
  veta    = pagemult(X_J2B, vetaj);
end

% [T, ETA, ETADOT, VZETA, VETA, VETADOT] = TANGENTRECONSTRUCTKINEMATICS(___)
if nargout > 5
  vetadot = pagemult(X_J2B, vetadotj);
end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
