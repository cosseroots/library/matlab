function tau = inverseDynamics(obj, varargin)
%% INVERSEDYNAMICS
%
% TAU = INVERSEDYNAMICS(OBJ, Q)
%
% TAU = INVERSEDYNAMICS(OBJ, Q, QDOT)
%
% TAU = INVERSEDYNAMICS(OBJ, Q, QDOT, QDDOT)
%
% TAU = INVERSEDYNAMICS(OBJ, Q, QDOT, QDDOT, FEXT)
%
% Inputs:
%
%   OBJ                     SMMS object.
%
%   Q                       PxN array of joint configuration variables for N
%                           time values.
%
%   QDOT                    VxN array of joint velocities.
%
%   QDDOT                   VxN array of joint accelerations.
%
%   FEXT                    6xBxN array of external forces acting on each body
%                           expressed in ground coordinates.
%
% Outputs
%
%   TAU                     VxN array of joint torques satisfying the SMMS'
%                           inverse dynamics.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% INVERSEDYNAMICS(OBJ, Q)
% INVERSEDYNAMICS(OBJ, Q, QDOT)
% INVERSEDYNAMICS(OBJ, Q, QDOT, QDDOT)
% INVERSEDYNAMICS(OBJ, Q, QDOT, QDDOT, FEXT)
narginchk(2, 5);

% INVERSEDYNAMICS(___)
% TAU = INVERSEDYNAMICS(___)
nargoutchk(0, 1);

% Build cell array of default arguments merged with given arguments
args = cell(1, 4);
ind = [1,2,3,4];
args(ind(1:(nargin-1))) = varargin;

% Validate input arguments and possibly create defaults
[q, qdot, qddot, fext] = validateDynamicsInputs(obj, args{:});

% Count time values
nt = size(q, 2);



%% Algorithm

% Initialize output
tau = zeros(obj.VelocityNumber, nt);

% Loop over time
for it = 1:nt
  % Call the respective dynamics algorithm
  tau(:,it) = smms.algorithms.inverseDynamics( ...
      obj ...
    , q(:,it), qdot(:,it), qddot(:,it) ...
    , fext(:,:,it) ...
  );
  
end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
