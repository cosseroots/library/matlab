function [Tee, vZee] = tangentForwardKinematics(obj, varargin)
%% TANGENTFORWARDKINEMATICS
%
% [TEE, VZEE] = TANGENTFORWARDKINEMATICS(OBJ)
%
% [TEE, VZEE] = TANGENTFORWARDKINEMATICS(OBJ, Q)
%
% [TEE, VZEE] = TANGENTFORWARDKINEMATICS(OBJ, Q, VQ)
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% TANGENTFORWARDKINEMATICS(OBJ)
% TANGENTFORWARDKINEMATICS(OBJ, Q)
% TANGENTFORWARDKINEMATICS(OBJ, Q, VQ)
narginchk(1, 3);

% [TEE, VZEE] = TANGENTFORWARDKINEMATICS(___)
nargoutchk(2, 2);

% Build cell array of default arguments merged with given arguments
args = cell(1, 8);
ind = [1,5];
args(ind(1:(nargin-1))) = varargin;

% Parse arguments to get sane defaults
[ q,  qdot,  qddot] = validateDynamicsInputs(obj, args{1:4});
[vq, vqdot, vqddot] = validateTangentDynamicsInputs(obj, args{5:8});



%% Algorithm

% Do forward RNEA calculation
[T_, ~, ~, vZ_] = smms.algorithms.tangentForwardRNEA(obj, q, qdot, qddot, vq, vqdot, vqddot);

% Get EE's pose
Tee = T_(:,:,end);
vZee = vZ_(:,:,end);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
