classdef ( Abstract ) SMMS < smms.internal.Base ...
    & smms.internal.Named ...
    & smms.mixin.Animatable
  %% SMMS
  
  
  
  %% PUBLIC PROPERTIES
  properties
    
    % Global vector of gravity
    Gravity (3, 1) double = [ 0 ; 0 ; -1 ];
    
  end
  
  
  
  %% PUBLIC DEPENDENT PROPERTIES
  properties ( Dependent )
    
    % Vector of all joints' home positions
    HomePosition (:, 1) double
    
  end
  
  
  
  %% PUBLIC READ-ONLY PROPERTIES
  properties ( SetAccess = protected )
    
    % Number of bodies in SMMS tree
    NumBody (1, 1) double
    
    % Ground object
    Ground (1, 1)
    
    % Cell arrays of all bodies and joints
    Body  (1, :) cell
    Joint (1, :) cell
    
    % Total position and velocity number of SMMS tree
    PositionNumber (1, 1) double
    VelocityNumber (1, 1) double
    
    % Map of position indices of each body (row === body)
    PositionMap (:, 2) double
    
    % Map of velocity/acceleration indices of each body (row === body)
    VelocityMap (:, 2) double
    
    % Map of body index to parent index
    Parent   (1, :) double
    % Map of parent index to child indices
    Children (1, :) cell
    
  end
  
  
  
  %% Internal properties
  properties ( Access = protected )
    
    % Array of each joint's predecessor
    Predecessor (1, :) double
    
    % Array of each joint's successors i.e., its' associated body
    Successor   (1, :) double
    
  end
  
  
  
  %% CONSTRUCTOR METHODS
  methods
    
    function obj = SMMS(varargin)
      %% SMMS
      
      
      
      obj@smms.internal.Base();
      obj@smms.internal.Named();
      obj@smms.mixin.Animatable();
      
      obj.Ground = Ground();
      
      setProperties(obj, nargin, varargin{:});
      
    end
    
  end
  
  
  
  %% SMMS TREE MANIPULATION METHODS
  methods ( Sealed )
    
    addBody(obj, body, parent)
      %% ADDBODY
    
    
    q = configurationHome(obj)
      %% CONFIGURATIONHOME
    
    
    q = configurationRand(obj, varargin)
      %% CONFIGURATIONRAND
    
    
    q = configurationRandn(obj, varargin)
      %% CONFIGURATIONRANDN
    
  end
  
  
  
  %% CONCRETE MIXIN METHODS
  methods ( Sealed , Access = protected )
    
    h = drawImpl(obj, ax, q, varargin)
      %% DRAWIMPL
    
    
    redrawImpl(obj, h, q, varargin)
      %% REDRAWIMPL
    
    
    decorateImpl(~, ax)
      %% DECORATEIMPL
    
    
    function n = drawNarginImpl(obj)
      %% DRAWNARGINIMPL
      
      
      
      % DRAW(OBJ, Q)
      n = 1;
      
    end
    
  end
  
  
  
  %% SETTERS
  methods
    
    function set.HomePosition(obj, v)
      %% SET.HOMEPOSITION
      
      
      
      % Loop over each body
      for ib = 1:obj.NumBody
        pb = obj.PositionMap(ib,:);
        % Only process if body has DoF
        if obj.Joint{ib}.PositionNumber > 0
          obj.Joint{ib}.HomePosition = v(pb(1):pb(2));
          
        end
        
      end
      
    end
    
    
    function set.Gravity(obj, v)
      %% SET.GRAVITY
      
      
      
      obj.Gravity = v(:);
      
    end
    
  end
  
  
  
  %% GETTERS
  methods
    
    function q = get.HomePosition(obj)
      %% GET.HOMEPOSITION
      
      
      
      prepareObjectForUse(obj);
      
      % Initialize array
      q = zeros(obj.PositionNumber, 1);
      
      % Loop over each body
      for ib = 1:obj.NumBody
        a = obj.PositionMap(ib,:);
        q(a(1):a(2)) = obj.Joint{ib}.HomePosition;
        
      end
      
    end
    
  end
  
  
  
  %% SEALED METHODS
  methods ( Sealed )
    
    obj = disableGravity(obj)
      %% DISABLEGRAVITY
    
    
    obj = enableGravity(obj, g)
      %% ENABLEGRAVITY
    
    
    T = transformBetween(obj, conf, bodyName1, bodyName2)
      %% TRANSFORMBETWEEN
    
    
    fext = externalForce(obj, bodyname, wrench, q)
      %% EXTERNALFORCE
    
    
    T = forwardKinematics(obj, varargin)
      %% FORWARDKINEMATICS
    
    
    [T, vZ] = tangentForwardKinematics(obj, varargin)
      %% TANGENTFORWARDKINEMATICS
    
    
    [T, eta, etadot] = reconstructKinematics(obj, varargin)
      %% RECONSTRUCTKINEMATICS Reconstruct each body's kinematics
    
    
    [T, eta, etadot, vzeta, veta, vetadot] = tangentReconstructKinematics(obj, varargin);
      %% TANGENTRECONSTRUCTKINEMATICS
    
    
    tau = gravityTorque(obj, varargin)
      %% GRAVITYTORQUE
    
    
    varargout = inverseKinematics(obj, T, qe0, options)
      %% INVERSEKINEMATICS
    
    
    Fc = configurationForces(obj, q, Fext)
      %% CONFIGURATIONFORCES
    
    
    Fv = velocityForces(obj, q, qdot, Fext)
      %% VELOCITYFORCES
    
    
    C = biasForce(obj, q, qdot, Fext)
      %% BIASFORCE
    
    
    K = stiffnessMatrix(obj, varargin)
      %% STIFFNESSMATRIX
    
    
    D = dampingMatrix(obj, varargin)
      %% DAMPINGMATRIX
    
    
    M = massMatrix(obj, varargin)
      %% MASSMATRIX
    
    
    tau = inverseDynamics(obj, varargin)
    %% INVERSEDYNAMICS
    
    
    [tau, vtau] = tangentInverseDynamics(obj, varargin)
      %% TANGENTINVERSEDYNAMICS
      
    
    qad = jointActuation(obj, q, qdot, qddot)
      %% JOINTACTUATION
    
    
    [qad, vqad] = tangentJointActuation(obj, q, qdot, qddot, vq, vqdot, vqddot)
      %% TANGENTJOINTACTUATION
    
  end
  
  
  
  %% INTERNAL METHODS
  methods ( Sealed, Access = protected )
    
    setupImpl(obj)
      %% SETUPIMPL
    
    
    resetImpl(obj)
      %% RESETIMPL
    
    
    releaseImpl(obj)
      %% RELEASEIMPL
    
    
    [out1, out2, out3, out4] = validateDynamicsInputs(obj, varargin)
      %% VALIDATEDYNAMICSINPUTS
    
    
    [out1, out2, out3, out4] = validateTangentDynamicsInputs(obj, varargin)
      %% VALIDATETANGENTDYNAMICSINPUTS
    
    
    bid = findBodyByHandle(obj, parent)
      %% FINDBODYBYHANDLE
    
    
    bid = findBodyByName(obj, bname)
      %% FINDBODYBYNANE
    
  end
  
end
