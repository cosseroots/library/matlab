function q = configurationRandn(obj, varargin)
%% CONFIGURATIONRANDN
%
% CONFIGURATIONRANDN(OBJ)
%
% CONFIGURATIONRANDN(OBJ, N)
%
% CONFIGURATIONRANDN(OBJ, S)
%
% CONFIGURATIONRANDN(OBJ, N, S)
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% CONFIGURATIONRANDN(OBJ)
% CONFIGURATIONRANDN(OBJ, S)
% CONFIGURATIONRANDN(OBJ, N)
% CONFIGURATIONRANDN(OBJ, N, S)
narginchk(1, 3);

% CONFIGURATIONRANDN(___)
% Q = CONFIGURATIONRANDN(___)
nargoutchk(0, 1);

prepareObjectForUse(obj);



%% Algorithm

q_ = cell(obj.PositionNumber, 1);

for ib = 1:obj.NumBody
  a = obj.PositionMap(ib,:);
  q_(a(1):a(2)) = num2cell(configurationRandn(obj.Joint{ib}, varargin{:}), 2);
end

q = cat(1, q_{:});


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
