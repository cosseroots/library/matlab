function addBody(obj, body, parent)
%% ADDBODY
%
% ADDBODY(OBJ, BODY)
%
% ADDBODY(OBJ, BODY, PARENT)
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% ADDBODY(OBJ, BODY)
% ADDBODY(OBJ, BODY, PARENT)
narginchk(2, 3);

% ADDBODY(___)
nargoutchk(0, 0);

% ADDBODY(OBJ, BODY)
if nargin < 3 || isempty(parent)
  parent = obj.Ground;
end



%% Algorithm

% Parent given as object handle, so extract its name
if isa(parent, 'RigidBody')
  pname = parent.Name;

% Parent given either as char or integer
else
  pname = parent;

end

% Parent given as char
if ischar(pname)
  % Find parent index
  pid = findBodyByName(obj, pname);

% Otherwise, keep value directly
else
  pid = pname;

end

% Check if parent found
if pid < 0
  error('SMMS:ADDBODY:BodyNotFound', 'No body found with name %s.', pname);
end

% Set parent of body to the correct parent object handle
if pid == 0
  pbody = obj.Ground;
else
  pbody = obj.Body{pid};
end
body.Parent    = pbody;

% Place body into array of bodies
bid = obj.NumBody + 1;
obj.Body{bid}  = body;
obj.Joint{bid} = body.Joint;

% Update indices
obj.Predecessor(bid) = pid;
obj.Successor(bid)   = bid;

% Update counter
obj.NumBody = obj.NumBody + 1;


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
