function bid = findBodyByHandle(obj, parent)
%% FINDBODYBYHANDLE
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% FINDBODYBYHANDLE(OBJ, PARENT)
narginchk(2, 2);

% FINDBODYBYHANDLE(___)
% BID = FINDBODYBYHANDLE(___)
nargoutchk(0, 1);



%% Algorithm

bid = findBodyByName(obj, parent.Name);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
