function [out1, out2, out3, out4] = validateDynamicsInputs(obj, varargin)%#codegen
%% VALIDATEDYNAMICSINPUTS
%
% [Q, QDOT, QDDOT_TAU, FEXT] = VALIDATEDYNAMICSINPUTS(OBJ, Q, QDOT, QDDOT_TAU,
% FEXT) validates the size of all inputs and matches them to each other. All
% input arguments are optional and, if not given, will assume sane defaults of
% the same number of columns as the widest input.
%
% Inputs:
%
%   OBJ                     SMMS object.
%
%   Q                       PxN array of generalized joint positions.
%
%   QDOT                    VxN array of generalized joint velocities.
%
%   QDDOT_TAU               VxN array of generalized joint accelerations or VxN
%                           array of generalized joint acuations, depending on
%                           use of the inputs.
% 
%   OUT4                    6xBxN array of external forces on each body.
%
% Outputs:
%
%   OUT1                    PxN array of generalized joint positions. Defaults
%                           to `CONFIGURATIONHOME(OBJ)`.
% 
%   OUT2                    VxN array of generalized joint velocities. Defaults
%                           to `ZEROS(V, 1)`
% 
%   OUT3                    VxN array of generalized joint accelerations or VxN
%                           array of generalized actuations, depending on use of
%                           the inputs.
% 
%   OUT4                    6xBxN array of external forces on each body.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% VALIDATEDYNAMICSINPUTS(OBJ)
% VALIDATEDYNAMICSINPUTS(OBJ, Q)
% VALIDATEDYNAMICSINPUTS(OBJ, Q, QDOT)
% VALIDATEDYNAMICSINPUTS(OBJ, Q, QDOT, QDDOT_TAU)
% VALIDATEDYNAMICSINPUTS(OBJ, Q, QDOT, QDDOT_TAU, FEXT)
narginchk(1, 5);

% [Q, QDOT, QDDOT_TAU] = VALIDATEDYNAMICSINPUTS(___)
% [Q, QDOT, QDDOT_TAU, FEXT] = VALIDATEDYNAMICSINPUTS(___)
nargoutchk(3, 4);

prepareObjectForUse(obj);



%% Algorithm

% Local variables
args  = varargin;
nargs = numel(args);
nv    = obj.VelocityNumber;
nb    = obj.NumBody;

% Initialize outputs
in1 = configurationHome(obj);
in2 = zeros(nv, 1);
in3 = zeros(nv, 1);
in4 = zeros(6, nb);

% VALIDATEDYNAMICSINPUTS(___, Q)
if nargs > 0 && ~isempty(varargin{1})
  in1 = double(varargin{1});
end

% VALIDATEDYNAMICSINPUTS(___, Q, QDOT)
if nargs > 1 && ~isempty(varargin{2})
  in2 = double(varargin{2});
end

% VALIDATEDYNAMICSINPUTS(___, Q, QDOT, QDDOT)
% VALIDATEDYNAMICSINPUTS(___, Q, QDOT, TAU)
if nargs > 2 && ~isempty(varargin{3})
  in3 = double(varargin{3});
end

% VALIDATEDYNAMICSINPUTS(___, Q, QDOT, QDDOT, FEXT)
% VALIDATEDYNAMICSINPUTS(___, Q, QDOT, TAU, FEXT)
if nargs > 3 && ~isempty(varargin{4})
  in4 = double(varargin{4});
end

% Count number of columns of all inputs and find the largest colum number
szo = [ size(in1, 2) , size(in2, 2) , size(in3, 2) , size(in4, 3) ];
nc = max(szo);

% Ensure all arguments have the same size of their last dimension
out1 = padarray(in1, [ 0 , nc - szo(1) ], 'replicate', 'post');
out2 = padarray(in2, [ 0 , nc - szo(2) ], 'replicate', 'post');
out3 = padarray(in3, [ 0 , nc - szo(3) ], 'replicate', 'post');
out4 = padarray(in4, [ 0 , nc - szo(4) ], 'replicate', 'post');


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
