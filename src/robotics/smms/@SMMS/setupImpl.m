function setupImpl(obj)
%% SETUPIMPL
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% SETUPIMPL(OBJ)
narginchk(1, 1);

% SETUPIMPL(___)
nargoutchk(0, 0);



%% Algorithm

% Number of all bodies/joints
nb = obj.NumBody;

% Create "parent" and "children" arrays
p = zeros(1, nb);
c = cell(1, nb);

% Initialize maps
obj.PositionMap = NaN(nb, 2);
obj.VelocityMap = NaN(nb, 2);
% Initialize position and velocity numbers
obj.PositionNumber = 0;
obj.VelocityNumber = 0;

% Loop over each joint
for ij = 1:nb
  % Set up each body and joint
  setup(obj.Body{ij});

  % Set Map
  obj.PositionMap(ij,:) = obj.PositionNumber + [ 1 , obj.Joint{ij}.PositionNumber ];
  obj.VelocityMap(ij,:) = obj.VelocityNumber + [ 1 , obj.Joint{ij}.VelocityNumber ];

  % And update counters
  obj.PositionNumber = obj.PositionMap(ij,2);
  obj.VelocityNumber = obj.VelocityMap(ij,2);

  % Set its parent index
  p(ij) = obj.Predecessor(ij);
  % Set its children indices
  c{ij} = find(obj.Predecessor == ij);

end

% Assign parent and children lists
obj.Parent   = p;
obj.Children = c;


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
