function M = massMatrix(obj, varargin)
%% MASSMATRIX
%
% MASSMATRIX(OBJ, Q)
%
% MASSMATRIX(OBJ, Q, QDOT)
%
% MASSMATRIX(OBJ, Q, QDOT, FEXT)
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% MASSMATRIX(OBJ, Q)
% MASSMATRIX(OBJ, Q, QDOT)
% MASSMATRIX(OBJ, Q, QDOT, FEXT)
narginchk(2, 5);

% MASSMATRIX(___)
% M = MASSMATRIX(___)
nargoutchk(0, 1);

% Build cell array of default arguments merged with given arguments
args = cell(1, 4);
ind = [1,2,4];
args(ind(1:(nargin-1))) = varargin;

% Validate input arguments and create defaults
[q, qdot, qddot, fext] = validateDynamicsInputs(obj, args{:});



%% Algorithm

% Initialize Jacobian matrices
nv     = obj.VelocityNumber;
vq     = zeros(nv, nv);
vqdot  = zeros(nv, nv);
vqddot = eye(nv, nv);

vfext = zeros(6, nv, obj.NumBody);

% Calculate mass matrix based on the TIDM algorithm using appropriate
% arguments as defined above. This formulation is faster than iterating
% over all columns of M using only the IDM algorithm
[~, M] = smms.algorithms.tangentInverseDynamics( ...
    obj ...
  , q, qdot, qddot ...
  , fext ...
  , vq, vqdot, vqddot ...
  , vfext ...
);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
