classdef ( Sealed ) SMMSFactory < smms.internal.Base
  %% SMMSFACTORY
  
  
  
  %% STATIC FACTORY METHODS
  methods ( Static )
    
    function rob = Scissor()
      %% SCISSOR
      
      
      
      rob = LocomotorSMMS('Name', 'Scissor');
      
      % Dimensions of scissor's arms
      w = 1.000;
      d = 0.100;
      h = 0.050;
      m = 1.00;
      J = 1 / 12 * m .* diag([ d*d + h*h , w*w + h*h , w*w + d*d ]);
      
      body1 = RigidBody( ...
          'Name'                , 'LeftBlade' ...
        , 'Visual'              , { ...
            RigidBodyVisual.cuboid(w, d, h, 'Name', 'Shell') ...
          } ...
        , 'SpatialInertiaMatrix', spatialInertiaMatrix(m, J, zeros(3, 1)) ...
      );
      body1.Joint = FreeJoint( ...
          'Name', 'Joint1' ...
      );
      addBody(rob, body1, rob.Ground.Name);
      
      body2 = RigidBody( ...
          'Name'                , 'RightBlade' ...
        , 'Visual'              , { ...
            RigidBodyVisual.cuboid(w, d, h, 'Name', 'Shell') ...
          } ...
        , 'SpatialInertiaMatrix', spatialInertiaMatrix(m, J, zeros(3, 1)) ...
      );
      body2.Joint = RevoluteJoint.Z( ...
          'Name'                  , 'Joint2' ...
        , 'JointToParentTransform', trvec2tform([ 0 ; 0 ; h ]) ...
      );
      addBody(rob, body2, body1);
      
    end
    
    
    function rob = Vee()
      %% VEE
      
      
      
      rob = LocomotorSMMS('Name', 'Vee');
      
      % Dimensions of vee's arms
      w = 1.0;
      d = 0.2;
      h = 0.2;
      m = 1.0;
      J_ = 1 / 12 .* diag([ h*h + d*d , w*w + h*h , w*w + d*d ]);
      
      m1 = 1 * m;
      body1 = RigidBody( ...
          'Name'                , 'LeftBlade' ...
        , 'Visual'              , { ...
            RigidBodyVisual.cuboid(w, d, h, 'Name', 'Shell') ...
          } ...
        , 'SpatialInertiaMatrix', spatialInertiaMatrix(m1, m1 * J_, zeros(3, 1)) ...
      );
      body1.Joint = FreeJoint( ...
          'Name'                , 'Joint1' ...
        , 'BodyToJointTransform', trvec2tform([ w / 2 , 0.0 , 0.0 ]) ...
      );
      addBody(rob, body1, rob.Ground.Name);
      
      m2 = 2 * m;
      body2 = RigidBody( ...
          'Name'                , 'RightBlade' ...
        , 'Visual'              , { ...
            RigidBodyVisual.cuboid(w, d, h, 'Name', 'Shell') ...
          } ...
        , 'SpatialInertiaMatrix', spatialInertiaMatrix(m2, m2 * J_, zeros(3, 1)) ...
      );
      body2.Joint = RevoluteJoint.Z( ...
          'Name'                  , 'Joint2' ...
        , 'BodyToJointTransform'  , trvec2tform([ w / 2 , 0.0 , 0.0 ]) ...
        , 'ParentToJointTransform', trvec2tform([ -w , 0.0 , 0.0 ]) ...
      );
      addBody(rob, body2, body1);
      
    end
    
    
    function rob = NPendulum(nb)
      %% NPENDULUM
      
      
      
      % NPENDULUM()
      if nargin < 1 || isempty(nb)
        nb = 1;
      end
      
      % Create system
      rob = ManipulatorSMMS( ...
          'Name', sprintf('Pendulum%d', nb) ...
      );
      pname = rob.Ground.Name;
      
      % Dimensions of rigid body
      w = 1.0;
      d = 0.2;
      h = 0.2;
      J_ = 1 / 12 .* diag([ d*d + h*h , w*w + h*h , w*w + d*d ]);
      
      % Loop over each body to create
      for ib = 1:nb
        % Body mass
        m = 1.0;
        
        % Create body
        bdy = RigidBody( ...
            'Name'                , sprintf('Body%d', ib) ...
          , 'Visual'              , { ...
              RigidBodyVisual.cuboid(w, d, h, 'Name', 'Shell') ...
            } ...
          , 'SpatialInertiaMatrix', spatialInertiaMatrix(m, m * J_, zeros(3, 1)) ...
        );
        
        % Add joint to body
        bdy.Joint = RevoluteJoint.Y( ...
            'Name'          , sprintf('Joint%d', ib) ...
          , 'PositionLimits', [ -pi , +pi ] ...
        );
        
        % If we are looking at any BUT the first body
        if ib > 1
          % Offset of joint with respect to the parent body's joint
          bdy.Joint.JointToParentTransform = trvec2tform([ w , 0 , 0 ]);
        end
        % Make sure joint is offset with respect to the body's frame
        bdy.Joint.JointToBodyTransform     = trvec2tform([ -w / 2 , 0 , 0 ]);
        
        % Finally, add body to the system
        addBody(rob, bdy, pname);
        
        % And store the body's name as the new parent body name
        pname = bdy.Name;
        
      end
      
    end
    
    
    function rob = FlexibleManipulator(varargin)
      %% FLEXIBLEMANIPULATOR
      
      
      
      persistent ip
      
      if isempty(ip)
        ip = inputParser();
        
        ip.KeepUnmatched    = true;
        ip.CaseSensitive    = false;
        ip.FunctionName     = funcname();
        ip.PartialMatching  = true;
        ip.StructExpand     = true;
        
        addParameter(ip, 'Body', []);
        addParameter(ip, 'Joint', []);
        
      end
      
      % Parse arguments
      parse(ip, varargin{:});
      
      % Get body and joint arguments
      bdy = ip.Results.Body;
      jnt = ip.Results.Joint;
      
      % Convert unmatched arguments to a cell array to pass to SMMS constructor
      rargs = transpose(cat(2, fieldnames(ip.Unmatched), struct2cell(ip.Unmatched)));
      
      % Create robot
      rob = ManipulatorSMMS( ...
          'Name', 'Manipulator' ...
        , rargs{:} ...
      );
      
      % Make a joint if none was given
      if ~isa(jnt, 'Rod')
        % Default arguments of rod
        jargs = { 'Name' , 'Rod' };
        
        % Additional rod constructor arguments passed as cell
        if iscell(jnt)
          jargs = [ jargs , jnt ];
        
        % Additional rod constructor arguments passed as structure
        elseif isstruct(jnt)
          jargs = [ jargs , transpose(cat(2, fieldnames(jnt), struct2cell(jnt))) ];
          
        end
        
        % Create joint object
        jnt = PVCRod(jargs{:});
        
      end
      
      % Make a body if none was given
      if ~isa(bdy, 'RigidBody')
        % Geometry of body
        w = 0.10;
        d = 0.10;
        h = 0.10;
        m = 0.10;
        J = 1 / 12 * m .* diag([ d*d + h*h , w*w + h*h , w*w + d*d ]);
        
        % Default body arguments
        bargs = { ...
            'Name'                , 'TCP' ...
          , 'Visual'              , { ...
              RigidBodyVisual.cuboid(w, d, h, 'Name', 'Shell') ...
            } ...
          , 'SpatialInertiaMatrix', spatialInertiaMatrix(m, J, zeros(3, 1)) ...
        };
        
        % Cell array of options passed
        if iscell(bdy)
          bargs = [ bargs , bdy ];
        
        % Options passed as structure
        elseif isstruct(bdy)
          bargs = [ bargs , transpose(cat(2, fieldnames(bdy), struct2cell(bdy))) ];
        
        end
        % Ensure that the joint is added to the body
        bargs = [ bargs , { 'Joint' , jnt } ];
        
        % Create body
        bdy = RigidBody(bargs{:});
        
      end
      
      % Add body to robot
      addBody(rob, bdy, rob.Ground.Name);
      
    end
    
    
    function rob = NSegmentFlexibleManipulator(n)
      %% NSEGMENTFLEXIBLEMANIPULATOR
      
      
      
      % NSEGMENTFLEXIBLEMANIPULATOR()
      if nargin < 1 || isempty(n)
        n = 1;
      end
      
      % Create robot
      rob = ManipulatorSMMS('Name', [ num2str(n) , '-Segment Flexible Manipulator']);
      
      % Mass of the slice between segments
      m = 0.00;
      % Width of segment connector
      w = 0.00;
      % Depth of segment connector
      d = 0.08;
      % Height of segment connector
      h = 0.08;
      
      % Initialization of previous body
      bp = rob.Ground;
      
      % Face color of respective joint
      dc = distinguishableColors(n);
      dc(1:3,1:3) = eye(3, 3);
      
      % Loop over all bodies to create
      for ib = 1:n
        % Name of body depending on whether its the last or not
        if ib == n
          bname = 'TCP';
        else
          bname = sprintf('Vertebra %0.f', ib);
        end
        
        % Make body
        bn = Cuboid.rectangular( ...
            m ...
          , w, d, h ...
          , 'Name'  , bname ...
          , 'Joint' , PVCRod( ...
              'Name'            , sprintf('Segment %0.f', ib) ...
            , 'Length'          , 0.5 ...
            , 'BasisFunction'   , LegendrePolynomial(3) ...
            , 'Styling'         , struct( ...
                'FaceColor', dc(ib,:) ...
              , 'EdgeColor', rgbdarker(dc(ib,:), 0.20) ...
            ) ...
          ) ...
        );
        
        % Add to robot
        addBody(rob, bn, bp.Name);
        
        % Update parent to point to this body
        bp = bn;
        
      end
      
    end
    
    
    function rob = FlexibleLocomotor(varargin)
      %% FLEXIBLELOCOMOTOR
      
      
      
      persistent ip
      
      if isempty(ip)
        ip = inputParser();
        
        ip.KeepUnmatched    = true;
        ip.CaseSensitive    = false;
        ip.FunctionName     = funcname();
        ip.PartialMatching  = true;
        ip.StructExpand     = true;
        
        addParameter(ip, 'LeftBody', []);
        addParameter(ip, 'RightBody', []);
        addParameter(ip, 'Joint', []);
        
      end
      
      % Parse arguments
      parse(ip, varargin{:});
      
      % Get body and joint arguments
      bdy_l = ip.Results.LeftBody;
      bdy_r = ip.Results.RightBody;
      jnt = ip.Results.Joint;
      
      % Convert unmatched arguments to a cell array to pass to SMMS constructor
      rargs = transpose(cat(2, fieldnames(ip.Unmatched), struct2cell(ip.Unmatched)));
      
      % Create robot
      rob = LocomotorSMMS( ...
          'Name', 'Flexible Locomotor' ...
        , rargs{:} ...
      );
      
      % Make a joint if none was given
      if ~isa(jnt, 'Rod')
        % Default arguments of rod
        jargs = { 'Name' , 'Rod' };
        
        % Additional rod constructor arguments passed as cell
        if iscell(jnt)
          jargs = [ jargs , jnt ];
        
        % Additional rod constructor arguments passed as structure
        elseif isstruct(jnt)
          jargs = [ jargs , transpose(cat(2, fieldnames(jnt), struct2cell(jnt))) ];
          
        end
        
        % Create joint object
        jnt = PVCRod(jargs{:});
        
      end
      
      % Make a body if none was given
      if ~isa(bdy_l, 'RigidBody')
        % Geometry of body
        w = 0.10;
        d = 0.10;
        h = 0.10;
        m = 0.10;
        J = 1 / 12 * m .* diag([ d*d + h*h , w*w + h*h , w*w + d*d ]);
        
        % Default body arguments
        bargs = { ...
            'Name'                , 'Left Body' ...
          , 'Visual'              , { ...
              RigidBodyVisual.cuboid(w, d, h, 'Name', 'Shell') ...
            } ...
          , 'SpatialInertiaMatrix', spatialInertiaMatrix(m, J, zeros(3, 1)) ...
        };
        
        % Cell array of options passed
        if iscell(bdy_l)
          bargs = [ bargs , bdy_l ];
        
        % Options passed as structure
        elseif isstruct(bdy_l)
          bargs = [ bargs , transpose(cat(2, fieldnames(bdy_l), struct2cell(bdy_l))) ];
        
        end
        
        % Create body
        bdy_l = RigidBody(bargs{:});
        
      end
      bdy_l.Joint = FreeJoint();
      
      if ~isa(bdy_r, 'RigidBody')
        % Geometry of body
        w = 0.10;
        d = 0.10;
        h = 0.10;
        m = 0.10;
        J = 1 / 12 * m .* diag([ d*d + h*h , w*w + h*h , w*w + d*d ]);
        
        % Default body arguments
        bargs = { ...
            'Name'                , 'Left Body' ...
          , 'Visual'              , { ...
              RigidBodyVisual.cuboid(w, d, h, 'Name', 'Shell') ...
            } ...
          , 'SpatialInertiaMatrix', spatialInertiaMatrix(m, J, zeros(3, 1)) ...
        };
        
        % Cell array of options passed
        if iscell(bdy_r)
          bargs = [ bargs , bdy_r ];
        
        % Options passed as structure
        elseif isstruct(bdy_r)
          bargs = [ bargs , transpose(cat(2, fieldnames(bdy_r), struct2cell(bdy_r))) ];
        
        end
        
        % Create body
        bdy_r = RigidBody(bargs{:});
        
      end
      bdy_r.Joint = jnt;
      
      % Add bodies to robot
      addBody(rob, bdy_l, rob.Ground.Name);
      addBody(rob, bdy_r, bdy_l.Name);
      
    end
    
  end
  
end
