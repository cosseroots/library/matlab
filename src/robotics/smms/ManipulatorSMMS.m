classdef ManipulatorSMMS < SMMS
  %% MANIPULATORSMMS
  
  
  
  %% READ-ONLY PUBLIC DEPENDENT PROPERTIES
  properties ( SetAccess = protected )
    
    FloatingBase (1, 1) logical = false
    
  end
  
  
  
  %% CONSTRUCTOR METHODS
  methods
    
    function obj = ManipulatorSMMS(varargin)
      %% MANIPULATORSMMS
      
      
      
      setProperties(obj, nargin, varargin{:});
      
    end
    
  end
  
end
