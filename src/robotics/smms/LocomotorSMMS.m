classdef LocomotorSMMS < SMMS
  %% LOCOMOTORSMMS
  
  
  
  %% READ-ONLY PUBLIC DEPENDENT PROPERTIES
  properties ( SetAccess = protected )
    
    FloatingBase (1, 1) logical = true
    
  end
  
  
  
  %% CONSTRUCTOR METHODS
  methods
    
    function obj = LocomotorSMMS(varargin)
      %% LOCOMOTORSMMS
      
      
      
      setProperties(obj, nargin, varargin{:});
      
    end
    
  end
  
end
