classdef Actuation < matlab.System ...
    & smms.mixin.Parentable
  %% ACTUATION
  
  
  
  %% NONTUNABLE INTERNAL PROPERTIES
  properties ( Nontunable , Access = { ?smms.internal.InternalAccess } )
    
    StepSize (1, 1) double
    
  end
  
  
  
  %% CONSTRUCTOR METHODS
  methods
    
    function obj = Actuation(varargin)
      %% ACTUATION
      
      
      
      setProperties(obj, nargin, varargin{:});
      
    end
    
  end
  
  
  
  %% CONCRETE IMPLEMENTATION METHODS
  methods ( Access = protected )
    
    function setupImpl(obj, varargin)
      %% SETUPIMPL
      
      
      
      p = parent(obj);
      obj.StepSize = p.StepSize;
      
    end
    
    
    function resetImpl(obj)
      %% RESETIMPL
    end
    
    
    function Qad = stepImpl(obj, t, q, qdot)
      %% STEPIMPL
      
      
      
      % No actuation
      Qad  = zeros(size(qdot, 1), numel(t));
      
    end
    
  end
  
end
