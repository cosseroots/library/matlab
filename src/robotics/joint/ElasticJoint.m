classdef ( Abstract , Hidden ) ElasticJoint < Joint
  %% ELASTICJOINT
  
  
  
  %% PUBLIC DEPENDENT PROPERTIES
  properties ( Dependent )
    
    % Linear mass of body
    Mass (1, 1) double
    
    % 3x3 moment of inertia tensor
    InertiaTensor (3, 3) double
    
    % Position of center of mass wrt point of action
    CenterOfMass  (3, 1) double
    
  end
  
  
  
  %% READ-ONLY PROPERTIES
  properties ( SetAccess = { ?smms.internal.InternalAccess } )
    
    % Spatial inertia matrix of joint
    SpatialInertiaMatrix (6, 6) double = spatialInertiaMatrix(0, zeros(3, 3), zeros(3, 1))
    
  end
  
  
  
  %% SETTERS
  methods
    
    function set.Mass(obj, v)
      %% SET.MASS
      
      
      
      obj.SpatialInertiaMatrix = spatialInertiaMatrix(v, obj.InertiaTensor, obj.CenterOfMass);
      
    end
    
    
    function set.InertiaTensor(obj, v)
      %% SET.INERTIATENSOR
      
      
      
      obj.SpatialInertiaMatrix = spatialInertiaMatrix(obj.Mass, v, obj.CenterOfMass);
      
    end
    
    
    function set.CenterOfMass(obj, v)
      %% SET.CENTEROFMASS
      
      
      
      obj.SpatialInertiaMatrix = spatialInertiaMatrix(obj.Mass, obj.InertiaTensor, v);
      
    end
    
  end
  
  
  
  %% GETTERS
  methods
    
    function v = get.Mass(obj)
      %% GET.MASS
      
      
      
      v = obj.SpatialInertiaMatrix(6,6);
      
    end
    
    
    function v = get.InertiaTensor(obj)
      %% GET.INERTIATENSOR
      
      
      
      v = obj.SpatialInertiaMatrix(1:3,1:3);
      
    end
    
    
    function v = get.CenterOfMass(obj)
      %% GET.CENTEROFMASS
      
      
      
      v = skew2vec(obj.SpatialInertiaMatrix(1:3,4:6)) ./ obj.Mass;
      
    end
    
  end
  
end
