classdef CylindricalJoint < FixedAxisJoint
  %% CYLINDRICALJOINT
  
  
  
  %% CONSTRUCTOR METHODS
  methods
    
    function obj = CylindricalJoint(varargin)
      %% CYLINDRICALJOINT
      
      
      
      obj@FixedAxisJoint();
      
      obj.HomePosition   = [ ...
        0.0 ; ...
        0.0 ; ...
      ];
      obj.PositionLimits = [ ...
        [ -pi  , +pi ] ; ...
        [ -0.5 , +0.5 ] ; ...
      ];
      obj.PositionNumber = 2;
      obj.VelocityNumber = 2;
      
      setProperties(obj, nargin, varargin{:});
      
    end
    
  end
  
  
  
  %% STATIC CONSTRUCTORS
  methods ( Static )
    
    function obj = X(varargin)
      %% X
      
      
      
      obj = CylindricalJoint(varargin{:}, 'JointAxis', [ 1.0 ; 0.0 ; 0.0 ]);
      
    end
    
    
    function obj = Y(varargin)
      %% Y
      
      
      
      obj = CylindricalJoint(varargin{:}, 'JointAxis', [ 0.0 ; 1.0 ; 0.0 ]);
      
    end
    
    
    function obj = Z(varargin)
      %% Z
      
      
      
      obj = CylindricalJoint(varargin{:}, 'JointAxis', [ 0.0 ; 0.0 ; 1.0 ]);
      
    end
    
  end
  
  
  
  %% CONCRETE INTERNAL METHODS
  methods ( Access = protected )
    
    function T = tformImpl(obj, q)
      %% TFORMIMPL
      
      
      
      q_ = q([1,2],:);
      
      T = tform( ...
          axang2rotm(cat( ...
              1 ...
            , repmat(obj.JointAxis, 1, size(q, 2)) ...
            , q_(1,:) ...
          )) ...
        , trvec(q_(2,:) .* obj.JointAxis) ...
      );
      
    end
    
    
    function setupImpl(obj)
      %% INITMOTIONSUBSPACE
      
      
      
      obj.MotionSubspace   = kron(eye(2, 2), obj.JointAxis);
      obj.VelocitySubspace = zeros(6, 2);
      obj.ForceSubspace    = null(transpose(obj.MotionSubspace), 'r');
      
    end
    
  end
  
end
