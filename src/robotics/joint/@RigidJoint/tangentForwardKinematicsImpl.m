function [T, eta, etadot, vzeta, veta, vetadot] = tangentForwardKinematicsImpl(obj, xq, vxq)%#codegen
%% TANGENTFORWARDKINEMATICSIMPL Tangent of a rigid joint's forward projection
%
% Inputs:
% 
%   JOINT                   Joint object.
%
%   XQ                      XQ  = {  Q ,  QDOT ,  QDDOT }
%
%   VXQ                     VXQ = { VQ , VQDOT , VQDDOT }
%
% Outputs:
%
%   VZETA                   6xVxN array of variation of joint transmitted
%                           homogeneous transformation matrices.
% 
%   VETA                    6xVxN array of variation of joint transmitted
%                           twists.
% 
%   VETADOT                 6xVxN array of variation of joint transmitted twist
%                           velocities.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse Arguments

% Joint state
q       = xq{1};
qdot    = xq{2};
qddot   = xq{3};

% Joint state variation
vq       = vxq{1};
vqdot    = vxq{2};
vqddot   = vxq{3};



%% Algorithm

% Get model information of joint
S    = obj.MotionSubspace;
Sdot = obj.VelocitySubspace;

% Local transformation of joint from right side to left side
T      = tform(obj, q);

% Joint twist
eta    = S * qdot;

% Joint twist velocities
etadot = Sdot * qdot + ...
        + S   * qddot;

% Variation of geometric transformation
vzeta   = S * vq;

% Joint twist
veta    = S * vqdot;

% Joint twist velocities
vetadot = Sdot * vqdot + ...
          + S  * vqddot;


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
