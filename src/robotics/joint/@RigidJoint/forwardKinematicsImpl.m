function [T, eta, etadot] = forwardKinematicsImpl(obj, xq)%#codegen
%% FORWARDKINEMATICSIMPL Forward projection of a rigid joint
%
% Inputs:
% 
%   JOINT                   Joint object.
%
%   XQ                      XQ = { Q , QDOT , QDDOT }
%
% Outputs:
%
%   T                       4x4xN array of joint transmitted homogeneous
%                           transformation matrices.
% 
%   ETA                     6xN array of joint transmitted twists.
% 
%   ETADOT                  6xN array of joint transmitted twist velocities.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse Arguments

% Joint state
q       = xq{1};
qdot    = xq{2};
qddot   = xq{3};

% Joint state variation
% vq       = vxq{1};
% vqdot    = vxq{2};
% vqddot   = vxq{3};



%% Algorithm

% Get subspace matrices
S    = obj.MotionSubspace;
Sdot = obj.VelocitySubspace;

% Local transformation of joint from right side to left side
T      = tform(obj, q);

% Joint twist
eta    = S * qdot;

% Joint twist velocities
etadot = Sdot * qdot + ...
        + S   * qddot;


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
