function [T, eta, etadot] = reconstructKinematicsImpl(obj, xq)%#codegen
%% RECONSTRUCTKINEMATICSIMPL
%
% Inputs:
%
%   OBJ                     Description of argument OBJ
%
%   XQ                      { Q , QDOT , QDDOT }
%
% Outputs:
%
%   T                       4x4x2 array of poses as stack of left-side and
%                           right-side relative joint poses.
% 
%   ETA                     6x2 array of joint twists.
% 
%   ETADOT                  6x2 array of joint twist velocities.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% % Proximal rigid body state
% T0      = x1{1};
% eta0    = x1{2};
% eta0dot = x1{3};
% Joint state
q       = xq{1};
qdot    = xq{2};
qddot   = xq{3};

% % Proximal rigid body variation
% vzeta0   = vx1{1};
% veta0    = vx1{2};
% veta0dot = vx1{3};
% Joint state variation
% vq       = vxq{1};
% vqdot    = vxq{2};
% vqddot   = vxq{3};



%% Algorithm

% Get model information of joint
S    = obj.MotionSubspace;
Sdot = obj.VelocitySubspace;

% Local transformation of joint from right side to left side
T = cat( ...
    3 ...
  , tformzeros() ...
  , tform(obj, q) ...
);

% Joint twist
eta = cat( ...
    2 ...
  , twistzeros() ...
  , S * qdot ...
);

% Joint twist velocities
etadot = cat( ...
    2 ...
  , twistzeros() ...
  , Sdot * qdot + ...
    + S   * qddot ...
);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
