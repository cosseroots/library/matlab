function [T, eta, etadot, vzeta, veta, vetadot] = tangentReconstructKinematicsImpl(obj, xq, vxq)%#codegen
%% TANGENTRECONSTRUCTKINEMATICSIMPL
%
% Inputs:
%
%   OBJ                     Description of argument OBJ
% 
%   Q                       Description of argument Q
% 
%   QDOT                    Description of argument QDOT
% 
%   QDDOT                   Description of argument QDDOT
% 
%   VQ                      Description of argument VQ
% 
%   VQDOT                   Description of argument VQDOT
% 
%   VQDDOT                  Description of argument VQDDOT
%
% Outputs:
%
%   T                       Description of argument T
% 
%   ETA                     Description of argument ETA
% 
%   ETADOT                  Description of argument ETADOT
% 
%   VZETA                   Description of argument VZETA
% 
%   VETA                    Description of argument VETA
% 
%   VETADOT                 Description of argument VETADOT
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% Joint state
q       = xq{1};
qdot    = xq{2};
qddot   = xq{3};

% Joint state variation
vq       = vxq{1};
vqdot    = vxq{2};
vqddot   = vxq{3};



%% Algorithm

% Get model information of joint
S    = obj.MotionSubspace;
Sdot = obj.VelocitySubspace;

% Local transformation of joint from right side to left side
T = cat( ...
    3 ...
  , tformzeros() ...
  , tform(obj, q) ...
);

% Joint twist
eta = cat( ...
    2 ...
  , twistzeros() ...
  , S * qdot ...
);

% Joint twist velocities
etadot = cat( ...
    2 ...
  , twistzeros() ...
  , Sdot * qdot + ...
    + S   * qddot ...
);

% Variation of geometric transformation
vzeta   = cat( ...
    3 ...
  , 0 * vq ...
  , S * vq ...
);

% Joint twist
veta    = cat( ...
    3 ...
  , 0 * vqdot ...
  , S * vqdot ...
);

% Joint twist velocities
vetadot = cat( ...
    3 ...
  , 0 * vqdot ...
  , Sdot * vqdot + ...
    + S  * vqddot ...
);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
