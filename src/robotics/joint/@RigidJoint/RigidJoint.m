classdef ( Abstract , Hidden ) RigidJoint < Joint
  %% RIGIDJOINT
  
  
  
  %% PUBLIC DEPENDENT PROPERTIES
  properties ( Dependent )
    
    % Linear mass of body
    Mass (1, 1) double
    
    % 3x3 moment of inertia tensor
    InertiaTensor (3, 3) double
    
    % Position of center of mass wrt point of action
    CenterOfMass  (3, 1) double
    
  end
  
  
  
  %% READ-ONLY PROPERTIES
  properties ( SetAccess = { ?smms.internal.InternalAccess } )
    
    % Spatial inertia matrix of joint
    SpatialInertiaMatrix (6, 6) double = spatialInertiaMatrix(0, zeros(3, 3), zeros(3, 1))
    
    % Matrix providing the force-subspace mapping
    ForceSubspace (6, :) double
    
    % Matrix of motion subspace of joint, sometimes referred to as S
    MotionSubspace (6, :) double
    
    % Time-derivative of motion subspace matrix, sometimes referred to as Sdot
    VelocitySubspace (6, :) double
    
  end
  
  
  
  %% SETTERS
  methods
    
    function set.Mass(obj, v)
      %% SET.MASS
      
      
      
      obj.SpatialInertiaMatrix = spatialInertiaMatrix(v, obj.InertiaTensor, obj.CenterOfMass);
      
    end
    
    
    function set.InertiaTensor(obj, v)
      %% SET.INERTIATENSOR
      
      
      
      obj.SpatialInertiaMatrix = spatialInertiaMatrix(obj.Mass, v, obj.CenterOfMass);
      
    end
    
    
    function set.CenterOfMass(obj, v)
      %% SET.CENTEROFMASS
      
      
      
      obj.SpatialInertiaMatrix = spatialInertiaMatrix(obj.Mass, obj.InertiaTensor, v);
      
    end
    
  end
  
  
  
  %% GETTERS
  methods
    
    function v = get.Mass(obj)
      %% GET.MASS
      
      
      
      v = obj.SpatialInertiaMatrix(6,6);
      
    end
    
    
    function v = get.InertiaTensor(obj)
      %% GET.INERTIATENSOR
      
      
      
      v = obj.SpatialInertiaMatrix(1:3,1:3);
      
    end
    
    
    function v = get.CenterOfMass(obj)
      %% GET.CENTEROFMASS
      
      
      
      v = skew2vec(obj.SpatialInertiaMatrix(1:3,4:6)) ./ obj.Mass;
      
    end
    
  end
  
  
  
  %% FINAL KINEMATICS METHODS
  methods ( Sealed , Access = protected )
    
    [T, eta, etadot] = reconstructKinematicsImpl(obj, xq, varargin)
      %% RECONSTRUCTIONIMPL
    
    
    [T, eta, etadot] = forwardKinematicsImpl(obj, xq)
      %% FORWARDKINEMATICSIMPL
    
    
    [F0, Qa] = backwardDynamicsImpl(obj, x1, xq, F1)
      %% BACKWARDDYNAMICSIMPL
    
    
    [T, eta, etadot, vzeta, veta, vetadot] = tangentReconstructKinematicsImpl(obj, xq, vxq, varargin)
      %% TANGENTRECONSTRUCTKINEMATICSIMPL
    
    
    [T, eta, etadot, vzeta, veta, vetadot] = tangentForwardKinematicsImpl(obj, xq, vxq);
      %% TANGENTFORWARDKINEMATICSIMPL
    
    
    [F0, Qa, vF0, vQa] = tangentBackwardDynamicsImpl(obj, x1, xq, F1, vx1, vq, vF1)
      %% TANGENTBACKWARDDYNAMICSIMPL
      
  end
  
end
