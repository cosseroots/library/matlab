function [F0, Qa] = backwardDynamicsImpl(obj, x1, xq, F1)%#codegen
%% BACKWARDDYNAMICSIMPL Backward projection of a rigid joint
%
% Inputs:
% 
%   JOINT                   Joint object.
%
%   X1                      X1 = { T1 , ETA1 , ETA1DOT }
%
%   XQ                      XQ = {  Q , QDOT , QDDOT   }
%
%   F1                      F1
%
% Outputs:
%
%   F0                      6xN array of joint transmitted forces onto the
%                           parent body expressed in the parent's body.
% 
%   QA                      VxN array of generalized joint acutation forces.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse Arguments

% Distal rigid body state
% T1      = x1{1};
% eta1    = x1{2};
% eta1dot = x1{3};
% Joint state
q       = xq{1};
% qdot    = xq{2};
% qddot   = xq{3};

% % Distal rigid body variation
% vzeta1   = vx1{1};
% veta1    = vx1{2};
% veta1dot = vx1{3};
% % Joint state variation
% vq       = vxq{1};
% vqdot    = vxq{2};
% vqddot   = vxq{3};



%% Algorithm

% Adjoint/force transformation of joint: j+ -> j-
XJ = Ad(tform2inv(tform(obj, q)));

% Force projected through
F0 = permute(pagemult(permute(XJ, [2, 1, 3]), permute(F1, [1, 3, 2])), [1, 3, 2]);
% Internal actuation force
Qa = transpose(obj.MotionSubspace) * F1;


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
