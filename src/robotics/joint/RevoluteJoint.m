classdef RevoluteJoint < FixedAxisJoint
  %% REVOLUTEJOINT
  
  
  
  %% CONSTRUCTOR METHODS
  methods
    
    function obj = RevoluteJoint(varargin)
      %% REVOLUTEJOINT
      
      
      
      obj@FixedAxisJoint();
      
      obj.JointAxis      = [ 0.0 ; 0.0 ; 1.0 ];
      
      obj.HomePosition   = 0.0;
      obj.PositionLimits = [ -pi , +pi ];
      obj.PositionNumber = 1;
      obj.VelocityNumber = 1;
      
      setProperties(obj, nargin, varargin{:});
      
    end
    
  end
  
  
  
  %% STATIC CONSTRUCTORS
  methods ( Static )
    
    function obj = X(varargin)
      %% X
      
      
      
      obj = RevoluteJoint(varargin{:}, 'JointAxis', [ 1.0 ; 0.0 ; 0.0 ]);
      
    end
    
    
    function obj = Y(varargin)
      %% Y
      
      
      
      obj = RevoluteJoint(varargin{:}, 'JointAxis', [ 0.0 ; 1.0 ; 0.0 ]);
      
    end
    
    
    function obj = Z(varargin)
      %% Z
      
      
      
      obj = RevoluteJoint(varargin{:}, 'JointAxis', [ 0.0 ; 0.0 ; 1.0 ]);
      
    end
    
  end
  
  
  
  %% CONCRETE INTERNAL METHODS
  methods ( Access = protected )
    
    function T = tformImpl(obj, q)
      %% TFORMIMPL
      
      
      
      q_ = q(1,:);
      
      T = axang2tform(cat( ...
          1 ...
        , repmat(obj.JointAxis, [1, size(q_, 2)]) ...
        , q_ ...
      ));
    
    end
    
    
    function setupImpl(obj)
      %% SETUPIMPL
      
      
      
      obj.MotionSubspace   = cat(1, obj.JointAxis, zeros(3, 1));
      obj.VelocitySubspace = zeros(6, 1);
      obj.ForceSubspace    = null(transpose(obj.MotionSubspace), 'r');
      
    end
    
  end
  
end
