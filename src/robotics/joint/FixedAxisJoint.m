classdef ( Abstract , Hidden ) FixedAxisJoint < RigidJoint
  %% FIXEDAXISJOINT
  
  
  
  %% INTERNAL PROPERTIES
  properties ( SetAccess = { ?smms.internal.InternalAccess } )
    
    JointAxis (3, 1) double
    
  end
  
  
  
  %% CONSTRUCTOR METHODS
  methods
    
    function obj = FixedAxisJoint()
      %% FIXEDAXISJOINT
      
      
      
      obj@RigidJoint();
      
    end
    
  end
  
  
  
  %% SETTERS
  methods
    
    function set.JointAxis(obj, v)
      %% SET.JOINTAXIS
      
      
      
      obj.JointAxis = mnormcol(v);
      
    end
    
  end
  
end
