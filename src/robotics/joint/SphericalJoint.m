classdef SphericalJoint < RigidJoint
  %% SPHERICALJOINT
  
  
  
  %% CONSTRUCTOR METHODS
  methods
    
    function obj = SphericalJoint(varargin)
      %% SPHERICALJOINT
      
      
      
      obj@RigidJoint();
      
      obj.HomePosition   = quatzeros();
      obj.PositionLimits = [ -Inf(4, 1) , +Inf(4, 1) ];
      obj.PositionNumber = 4;
      obj.VelocityNumber = 3;
      
      setProperties(obj, nargin, varargin{:});
      
    end
    
  end
  
  
  
  %% CONCRETE INTERNAL METHODS
  methods ( Access = protected )
    
    function T = tformImpl(~, q)
      %% TFORMIMPL
      
      
      
      q_ = q(1:4,:);
      
      T = quat2tform(q_);
    
    end
    
    
    function setupImpl(obj)
      %% SETUPIMPL
      
      
      
      obj.MotionSubspace   = cat(1, eye(3, 3), zeros(3, 3));
      obj.VelocitySubspace = zeros(6, 3);
      obj.ForceSubspace    = null(transpose(obj.MotionSubspace), 'r');
      
    end
    
    
    function q = configurationZerosImpl(~, n)
      %% CONFIGURATIONZEROSIMPL
      
      
      
      q = quatzeros(n);
      
    end
    
    
    function q = configurationRandImpl(~, s, n, varargin)
      %% CONFIGURATIONRANDIMPL
      
      
      
      q = quatrand(s, n, varargin{:});
      
    end
    
    
    function q = configurationRandnImpl(~, s, n, varargin)
      %% CONFIGURATIONRANDNIMPL
      
      
      
      q = quatrandn(s, n, varargin{:});
      
    end
    
  end
  
end
