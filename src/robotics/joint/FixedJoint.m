classdef FixedJoint < RigidJoint
  %% FIXEDJOINT
  
  
  
  %% CONSTRUCTOR METHODS
  methods
    
    function obj = FixedJoint(varargin)
      %% FIXEDJOINT
      
      
      
      obj@RigidJoint();
      
      obj.HomePosition   = [];
      obj.PositionLimits = [];
      
      obj.PositionNumber = 0;
      obj.VelocityNumber = 0;
      
      setProperties(obj, nargin, varargin{:});
      
    end
    
  end
  
  
  
  %% CONCRETE INTERNAL METHODS
  methods ( Access = protected )
    
    function T = tformImpl(~, q)
      %% TFORMIMPL
      
      
      
      T = tformzeros(size(q, 2));
      
    end
    
  end
  
  
  
  %% CONCRETE MIXIN METHODS
  methods ( Access = protected )
    
    function setupImpl(obj)
      %% SETUPIMPL
      
      
      
      obj.MotionSubspace   = zeros(6, 0);
      obj.VelocitySubspace = zeros(6, 0);
      obj.ForceSubspace    = null(transpose(obj.MotionSubspace), 'r');
      
    end
    
  end
  
end
