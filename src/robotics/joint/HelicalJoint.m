classdef HelicalJoint < FixedAxisJoint
  %% HELICALJOINT
  
  
  
  %% JOINT PROPERTIES
  properties ( SetAccess = protected )
    
    Pitch (1, 1) double
    
  end
  
  
  
  %% CONSTRUCTOR METHODS
  methods
    
    function obj = HelicalJoint(varargin)
      %% HELICALJOINT
      
      
      
      obj@FixedAxisJoint();
      
      obj.JointAxis      = [ 0.0 ; 0.0 ; 1.0 ];
      
      obj.HomePosition   = 0.0;
      obj.PositionLimits = [ -pi , +pi ];
      obj.PositionNumber = 1;
      obj.VelocityNumber = 1;
      obj.Pitch          = 0.1;
      
      setProperties(obj, nargin, varargin{:});
      
    end
    
  end
  
  
  
  %% STATIC CONSTRUCTORS
  methods ( Static )
    
    function obj = X(varargin)
      %% X
      
      
      
      obj = HelicalJoint(varargin{:}, 'JointAxis', [ 1.0 ; 0.0 ; 0.0 ]);
      
    end
    
    
    function obj = Y(varargin)
      %% Y
      
      
      
      obj = HelicalJoint(varargin{:}, 'JointAxis', [ 0.0 ; 1.0 ; 0.0 ]);
      
    end
    
    
    function obj = Z(varargin)
      %% Z
      
      
      
      obj = HelicalJoint(varargin{:}, 'JointAxis', [ 0.0 ; 0.0 ; 1.0 ]);
      
    end
    
  end
  
  
  
  %% CONCRETE INTERNAL METHODS
  methods ( Access = protected )
    
    function T = tformImpl(obj, q)
      %% TFORMIMPL
      
      
      
      q_ = q(1,:);
      
      T = tform( ...
          axang2rotm(cat( ...
              1 ...
            , repmat(obj.JointAxis, 1, size(q_, 2)) ...
            , q_ ...
          )) ...
        , trvec(obj.Pitch .* q_ .* obj.JointAxis) ...
      );
      
    end
    
    
    function setupImpl(obj)
      %% SETUPIMPL
      
      
      
      obj.MotionSubspace   = cat(1, obj.JointAxis, obj.Pitch * obj.JointAxis);
      obj.VelocitySubspace = zeros(6, 1);
      obj.ForceSubspace    = null(transpose(obj.MotionSubspace), 'r');
      
    end
    
  end
  
end
