classdef ( Abstract , Hidden ) Joint < smms.internal.Base ...
    & smms.mixin.Drawable ...
    & smms.internal.Named
  %% JOINT
  
  
  
  %% PUBLIC PROPERTIES
  properties
    
    % Px1 array of joint's zero i.e., home position values
    HomePosition (:, 1) double = zeros(0, 1)
    
    % Px2 array of [ MIN(:) , MAX(:) ] positions limits of joint
    PositionLimits (:, 2) double = zeros(0, 2)
    
  end
  
  
  
  %% READ-ONLY PUBLIC PROPERTIES
  properties ( SetAccess = { ?smms.internal.InternalAccess } )
    
    % Body object associated to this joint
    Body (1, 1)
    
    % Number of position DoF
    PositionNumber (1, 1) double = 0
    
    % Number of velocity DoF
    VelocityNumber (1, 1) double = 0
    
  end
  
  
  
  %% DEPENDENT READ-ONLY PROPERTIES
  properties ( Dependent , SetAccess = { ?smms.internal.InternalAccess } )
    
    % parent(i) -> joint-left(i) Left_TForm
    ParentToJointTransform (4, 4) double
    % joint-left(i) -> parent(i) LeftInv_TForm
    JointToParentTransform (4, 4) double
    
    % body(i) -> joint+right(i) RightInv_TForm
    BodyToJointTransform (4, 4) double
    % joint+right(i) -> body(i) Right_TForm
    JointToBodyTransform (4, 4) double
    
  end
  
  
  
  %% INTERNAL PROPERTIES
  properties ( Hidden , SetAccess = { ?smms.internal.InternalAccess } )
    
    % parent(i) -> joint-left(i)
    % ParentToJointTransform
    Left_TForm     (4, 4) double = eye(4, 4)
    Left_Ad        (6, 6) double = eye(6, 6)
    
    % joint-left(i) -> parent(i)
    % JointToParentTransform
    LeftInv_TForm  (4, 4) double = eye(4, 4)
    LeftInv_Ad     (6, 6) double = eye(6, 6)
    
    % body(i) -> joint-right(i)
    % BodyToJointTransform
    RightInv_TForm (4, 4) double = eye(4, 4)
    RightInv_Ad    (6, 6) double = eye(6, 6)
    
    % joint-right(i) -> body(i)
    % JointToBodyTransform
    Right_TForm    (4, 4) double = eye(4, 4)
    Right_Ad       (6, 6) double = eye(6, 6)
    
  end
  
  
  
  %% SETTERS
  methods
    
    function set.ParentToJointTransform(obj, v)
      %% SET.PARENTTOJOINTTRANSFORM
      
      
      
      obj.Left_TForm    = v;
      obj.LeftInv_TForm = tform2inv(obj.Left_TForm);
      
      obj.Left_Ad       = Ad(obj.Left_TForm);
      obj.LeftInv_Ad    = Ad(obj.LeftInv_TForm);
      
    end


    function set.JointToParentTransform(obj, v)
      %% SET.JOINTTOPARENTTRANSFORM
      
      
      
      obj.LeftInv_TForm = v;
      obj.Left_TForm    = tform2inv(obj.LeftInv_TForm);
      
      obj.LeftInv_Ad    = Ad(obj.LeftInv_TForm);
      obj.Left_Ad       = Ad(obj.Left_TForm);
      
    end


    function set.JointToBodyTransform(obj, v)
      %% SET.JOINTTOBODYTRANSFORM
      
      
      
      obj.Right_TForm    = v;
      obj.RightInv_TForm = tform2inv(v);
      
      obj.Right_Ad       = Ad(obj.Right_TForm);
      obj.RightInv_Ad    = Ad(obj.RightInv_TForm);
      
    end


    function set.BodyToJointTransform(obj, v)
      %% SET.BODYTOJOINTTRANSFORM
      
      
      
      obj.RightInv_TForm = v;
      obj.Right_TForm    = tform2inv(v);
      
      obj.RightInv_Ad    = Ad(obj.RightInv_TForm);
      obj.Right_Ad       = Ad(obj.Right_TForm);
      
    end
    
  end
  
  
  
  %% GETTERS
  methods
    
    function v = get.ParentToJointTransform(obj)
      %% PARENTTOJOINTTRANSFORM
      
      
      
      v = obj.Left_TForm;
      
    end
    
    
    function v = get.JointToParentTransform(obj)
      %% GET.JOINTTOPARENTTRANSFORM
      
      
      
      v = obj.LeftInv_TForm;
      
    end
    
    
    function v = get.JointToBodyTransform(obj)
      %% GET.JOINTTOBODYTRANSFORM
      
      
      
      v = obj.Right_TForm;
      
    end
    
    
    function v = get.BodyToJointTransform(obj)
      %% GET.BODYTOJOINTTRANSFORM
      
      
      
      v = obj.RightInv_TForm;
      
    end
    
  end
  
  
  
  %% KINEMATICS MHETODS
  methods ( Sealed )
    
    function T = tform(obj, q)
      %% TFORM
      
      
      
      prepareObjectForUse(obj);
      
      T = tformImpl(obj, q);
      
      % Remove singular values
      T(issingular(T, 100 * eps(class(T)))) = 0;
      
    end
    
    
    [T, eta, etadot] = reconstructKinematics(obj, q, T0, qdot, eta0, qddot, eta0dot, varargin);
      %% RECONSTRUCTKINEMATICS
    
    
    function [T, eta, etadot] = forwardKinematics(obj, varargin)
      %% FORWARDKINEMATICS
      %
      % FORWARDKINEMATICS(JOINT, Q)
      %
      % FORWARDKINEMATICS(JOINT, Q, QDOT)
      %
      % FORWARDKINEMATICS(JOINT, Q, QDOT, QDDOT)
      %
      % Inputs:
      % 
      %   JOINT                   Joint object.
      %
      %   Q                       Joint positions.
      %
      %   QDOT                    Joint velocities.
      %
      %   QDDOT                   Joint accelerations.
      
      
      
      narginchk(1, 4);
      
      prepareObjectForUse(obj);
      
      xq = validateInputForwardKinematics(obj, varargin{:});
      
      % Go through the joint
      [T, eta, etadot] = forwardKinematicsImpl(obj, xq);
      
      % Remove singular values
      T(issingular(T, 100 * eps(class(T)))) = 0;
      
    end
    
    
    function [F0, Qa] = backwardDynamics(obj, varargin)
      %% BACKWARDDYNAMICS Calculate the joint's local force transport
      %
      % [FP, QA] = BACKWARDDYNAMICS(JOINT, T1, V1, V1DOT, Q, QDOT, QDDOT, F1)
      %
      % Inputs:
      % 
      %   JOINT                   Joint object.
      %
      %   T1                      4x4 distal pose.
      %
      %   V1                      6x1 distal twist.
      %
      %   V1DOT                   6x1 distal twist velocity.
      %
      %   F1                      6x1 distal force.
      
      
      
      narginchk(1, 8);
      
      prepareObjectForUse(obj);
      
      [x1, xq, F1] = validateInputBackwardDynamics(obj, varargin{:});
      
      % Project forces through joint
      [F0, Qa] = backwardDynamicsImpl( ...
          obj ...
        , x1 ...
        , xq ...
        , F1 ...
      );
      
    end
    
    
    [T, eta, etadot, vzeta, veta, vetadot] = tangentReconstructKinematics(obj, q, T0, qdot, eta0, qddot, eta0dot, vq, vzeta0, vqdot, veta0, vqddot, veta0dot, varargin);
      %% TANGENTRECONSTRUCTKINEMATICS
    
    
    function [T, eta, etadot, vzeta, veta, vetadot] = tangentForwardKinematics(obj, varargin)
      %% TANGENTFORWARDKINEMATICS
      %
      % [T, ETA, ETADOT, VZETA, VETA, VETADOT] = TANGENTFORWARDKINEMATICS(JOINT, Q, QDOT, QDDOT, VQ, VQDOT, VQDDOT)
      %
      % Inputs:
      % 
      %   JOINT                   Joint object.
      %
      %   Q                       Joint positions.
      %
      %   QDOT                    Joint velocities.
      %
      %   QDDOT                   Joint accelerations.
      %
      %   VQ                      Variation of joint positions.
      %
      %   VQDOT                   Variation of joint velocities.
      %
      %   VQDDOT                  Variation of joint accelerations.
      
      
      
      narginchk(1, 7);
      nargoutchk(0, 6);
      
      prepareObjectForUse(obj);
      
      [xq, vxq] = validateInputForwardKinematics(obj, varargin{:});
      
      [T, eta, etadot, vzeta, veta, vetadot] = tangentForwardKinematicsImpl( ...
          obj ...
        , xq ...
        , vxq ...
      );
      
    end
    
    
    function [F0, Qa, vF0, vQa] = tangentBackwardDynamics(obj, varargin)
      %% TANGENTBACKWARD
      %
      % [VF0, VQA] = BACKWARDDYNAMICS(JOINT, T1, V1, V1DOT, Q, QDOT, QDDOT, F1,
      % VT1, VV1, VV1DOT, VQ, VQDOT, VQDDOT, VF1)
      %
      % Inputs:
      % 
      %   JOINT                   Joint object.
      %
      %   T1                      Distal pose.
      %
      %   V1                      Distal twist.
      %
      %   V1DOT                   Distal twist velocity.
      %
      %   Q                       Joint positions.
      %
      %   QDOT                    Joint velocities.
      %
      %   QDDOT                   Joint accelerations.
      %
      %   F1                      Distal force.
      %
      %   T1                      Variation of distal pose.
      %
      %   V1                      Variation of distal twist.
      %
      %   V1DOT                   Variation of distal twist velocity.
      %
      %   VQ                      Variation of joint positions.
      %
      %   VQDOT                   Variation of joint velocities.
      %
      %   VQDDOT                  Variation of joint accelerations.
      %
      %   VF1                     Variation of distal force.
      
      
      
      narginchk(1, 15);
      
      prepareObjectForUse(obj);
      
      [x1, xq, F1, vx1, vq1, vF1] = validateInputBackwardDynamics(obj, varargin{:});
      
      % Project forces backwards through joint: F1 -> F0
      [F0, Qa, vF0, vQa] = tangentBackwardDynamicsImpl( ...
          obj ...
        , x1 ...
        , xq ...
        , F1 ...
        , vx1 ...
        , vq1 ...
        , vF1 ...
      );
      
    end
    
    
    function q = configurationHome(obj, n)
      %% CONFIGURATIONHOME
      
      
      
      prepareObjectForUse(obj);
      
      % CONFIGURATIONHOME(OBJ)
      if nargin < 2 || isempty(n)
        n = 1;
      end
      
      q = repmat(obj.HomePosition, [1, n]);
      
    end
    
    
    function q = configurationZeros(obj, n)
      %% CONFIGURATIONZEROS
      
      
      
      prepareObjectForUse(obj);
      
      % CONFIGURATIONZEROS(OBJ)
      if nargin < 2 || isempty(n)
        n = 1;
      end
      
      q = scaleConfiguration(obj, configurationZerosImpl(obj, n));
      
    end
    
    
    function q = configurationRand(obj, varargin)
      %% CONFIGURATIONRAND
      
      
      
      prepareObjectForUse(obj);
      
      % CONFIGURATIONRAND(OBJ)
      % CONFIGURATIONRAND(OBJ, N)
      % CONFIGURATIONRAND(OBJ, S)
      % CONFIGURATIONRAND(OBJ, S, N)
      narginchk(1, Inf);
      
      % Check if there is a random seed generator object in any of the arguments
      [s, args, nargs] = objectcheck('RandStream', varargin{:});
      if isempty(s)
        s = RandStream.getGlobalStream();
      end
      
      % Get number of states
      n = 1;
      if nargs > 0 && ~isempty(args{1})
        n = args{1};
        args(1) = [];
      end
      
      % Uniformly distributed random numbers within the range of position limits
      q = scaleConfiguration(obj, configurationRandImpl(obj, s, n, args{:}));
      
    end
    
    
    function q = configurationRandn(obj, varargin)
      %% CONFIGURATIONRANDN
      
      
      
      prepareObjectForUse(obj);
      
      % CONFIGURATIONRANDN(OBJ)
      % CONFIGURATIONRANDN(OBJ, N)
      % CONFIGURATIONRANDN(OBJ, S)
      % CONFIGURATIONRANDN(OBJ, S, N)
      narginchk(1, Inf);
      
      % CONFIGURATIONRANDN(___)
      % Q = CONFIGURATIONRANDN(___)
      nargoutchk(0, 1);
      
      % Check if there is a random seed generator object in any of the arguments
      [s, args, nargs] = objectcheck('RandStream', varargin{:});
      if isempty(s)
        s = RandStream.getGlobalStream();
      end
      
      % Get number of states
      n = 1;
      if nargs > 0
        n = args{1};
        args(1) = [];
      end
      
      % Normally distributed random numbers in the position limits
      q = scaleConfiguration(obj, configurationRandnImpl(obj, s, n, args{:}));
      
    end
    
  end
  
  
  
  %% INTERNAL FINAL METHODS
  methods ( Sealed, Access = protected )
    
    function hp = drawImpl(obj, ax, q, varargin)
      %% DRAWIMPL
      
      
      
      % Group of components
      hp = hggroup( ...
          ax ...
        , 'Tag', obj.Name ...
      );
      
      hl = hgtransform( ...
          ax ...
        , 'Parent', hp ...
        , 'Tag'   , 'Left side' ...
        , 'Matrix', tformzeros() ...
      );
      hd = hgtransform( ...
          ax ...
        , 'Parent', hp ...
        , 'Tag'   , 'Deformation' ...
      );
      hr = hgtransform( ...
          ax ...
        , 'Parent', hp ...
        , 'Tag'   , 'Right side' ...
        , 'Matrix', tform(obj, q) ...
      );
      
      % Length of each axis
      la = 0.2;
      % Draw coordinate frames on left and right hand side
      Da = diag([ la , la , la ]) * eye(3, 3);
      hl_f = viz.coordinateFrame(ax, Da, zeros(3, 1), 'AxisStyle', { 'LineStyle' , '--' });
      hr_f = viz.coordinateFrame(ax, Da, zeros(3, 1), 'AxisStyle', { 'LineStyle' , '-' });
      
      % Draw deformation in between, which is up to the joint to decide how it
      % will be drawn
      hd_d = drawDeformation(obj, ax, q, varargin{:});
      
      % Set parent of all the plotted objects to their respective group object
      set(hl_f, 'Parent', hl);
      set(hd_d, 'Parent', hd);
      set(hr_f, 'Parent', hr);
      
    end
    
    
    function redrawImpl(obj, h, q, varargin)
      %% REDRAWIMPL
      
      
      
      % Simply update the transformation of the joint's right side
      set(findobj(h, 'Tag', 'Right side'), 'Matrix', tform(obj, q));
      
      % And redraw the deformation drawing
      redrawDeformationImpl(obj, findobj(h, 'Tag', 'Deformation'), q, varargin{:});
      
    end
    
    
    function hp = drawDeformation(obj, ax, q, varargin)
      %% DRAWDEFORMATION
      
      
      
      hp = drawDeformationImpl(obj, ax, q, varargin{:});
      
    end
    
  end
  
  
  
  %% INTERNAL METHODS
  methods ( Access = protected )
    
    function q = scaleConfiguration(obj, qf)
      %% SCALECONFIGURATION
      
      
      
      % Get lower and upper position limits
      a = obj.PositionLimits(:,1);
      b = obj.PositionLimits(:,2);
      
      % Copy to initialize output
      q = qf;
      
      % Find where limits are not infinity
      qninf = ~( isinf(abs(a)) | isinf(abs(b)) );
      
      % Scale only coordinates where its limits are not infinity
      qc = limit(q(qninf), -1.0, +1.0);
      q(qninf) = a(qninf) + ( b(qninf) - a(qninf) ) ./ 2 .* ( qc + 1 );
      
    end
    
    
    function hp = drawDeformationImpl(obj, ax, q, varargin)
      %% DRAWDEFORMATIONIMPL
      
      
      
      hp = hggroup( ...
          ax ...
        , 'Tag', 'Deformation' ...
      );
      
    end
    
    
    function redrawDeformationImpl(obj, h, q, varargin)
      %% REDRAWDEFORMATIONIMPL
      
      
    end
    
    
    [xq, vxq] = validateInputForwardKinematics(obj, varargin)
    %%  VALIDATEINPUTFORWARDKINEMATICS
    
    
    [x1, xq, F1, vx1, vxq, vF1] = validateInputBackwardDynamics(obj, varargin)
    %% VALIDATEINPUTBACKWARDDYNAMICS
    
    
    [out1, out2, out3, out4] = validateDynamicsInputs(obj, varargin);
    %% VALIDATEDYNAMICSINPUTS
    
    
    [out1, out2, out3, out4] = validateTangentDynamicsInputs(obj, varargin);
    %% VALIDATETANGENTDYNAMICSINPUTS
    
  end
  
  
  
  %% ABSTRACT CONFIGURATION METHODS
  methods ( Access = protected )
    
    function q = configurationZerosImpl(obj, n)
      %% CONFIGURATIONZEROSIMPL
      
      
      
      q = zeros(obj.PositionNumber, n);
      
    end
    
    
    function q = configurationRandImpl(obj, s, n, varargin)
      %% CONFIGURATIONRANDIMPL
      
      
      
      q = 2 * ( rand(s, obj.PositionNumber, n, varargin{:}) - 0.5 );
      
    end
    
    
    function q = configurationRandnImpl(obj, s, n, varargin)
      %% CONFIGURATIONRANDNIMPL
      
      
      
      q = 2 * ( randn(s, obj.PositionNumber, n, varargin{:}) - 0.5 );
      
    end
    
  end
  
  
  
  %% ABSTRACT KINEMATICS METHODS
  methods ( Abstract, Access = protected ) 
    
    T = tformImpl(obj, q);
    
    [T, eta, etadot] = reconstructKinematicsImpl(obj, xq, varargin);
    
    [T, eta, etadot] = forwardKinematicsImpl(obj, xq);
    
    [F0, Qa] = backwardDynamicsImpl(obj, x1, xq, F1);
    
    [T, eta, etadot, vzeta, veta, vetadot] = tangentReconstructKinematicsImpl(obj, xq, vxq, varargin);
    
    [T, eta, etadot, vzeta, veta, vetadot] = tangentForwardKinematicsImpl(obj, xq, vxq);
    
    [F0, Qa, vF0, vQa] = tangentBackwardDynamicsImpl(obj, x1, xq, F1, vx1, vq, vF1);
    
  end
  
end
