function [xq, vxq] = validateInputForwardKinematics(obj, q, qdot, qddot, vq, vqdot, vqddot)%#codegen
%% VALIDATEINPUTFORWARDKINEMATICS
% 
% VALIDATEINPUTFORWARDKINEMATICS(OBJ, Q)
% 
% VALIDATEINPUTFORWARDKINEMATICS(OBJ, Q, QDOT)
% 
% VALIDATEINPUTFORWARDKINEMATICS(OBJ, Q, QDOT, QDDOT)
% 
% VALIDATEINPUTFORWARDKINEMATICS(OBJ, Q, QDOT, QDDOT, VQ)
% 
% VALIDATEINPUTFORWARDKINEMATICS(OBJ, Q, QDOT, QDDOT, VQ, VQDOT)
% 
% VALIDATEINPUTFORWARDKINEMATICS(OBJ, Q, QDOT, QDDOT, VQ, VQDOT, VQDDOT)
% 
% XQ = VALIDATEINPUTFORWARDKINEMATICS(OBJ, ___)
% 
% [XQ, VXQ] = VALIDATEINPUTFORWARDKINEMATICS(OBJ, ___)
% 
% Inputs:
%
%   OBJ                     Description of argument OBJ
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% VALIDATEINPUTFORWARDKINEMATICS(OBJ, Q)
% VALIDATEINPUTFORWARDKINEMATICS(OBJ, Q, QDOT)
% VALIDATEINPUTFORWARDKINEMATICS(OBJ, Q, QDOT, QDDOT)
% VALIDATEINPUTFORWARDKINEMATICS(OBJ, Q, QDOT, QDDOT, VQ)
% VALIDATEINPUTFORWARDKINEMATICS(OBJ, Q, QDOT, QDDOT, VQ, VQDOT)
% VALIDATEINPUTFORWARDKINEMATICS(OBJ, Q, QDOT, QDDOT, VQ, VQDOT, VQDDOT)
narginchk(1, 7);

% XQ = VALIDATEINPUTFORWARDKINEMATICS(___)
% [XQ, VXQ] = VALIDATEINPUTFORWARDKINEMATICS(___)
nargoutchk(1, 2);



%% Algorithm

% Number of...
np = obj.PositionNumber;
nv = obj.VelocityNumber;

% Default positions
if nargin < 2 || isempty(q)
  q = configurationHome(obj);
end

% Default velocities
if nargin < 3 || isempty(qdot)
  qdot = zeros(nv, 1);
end

% Default accelerations
if nargin < 4 || isempty(qddot)
  qddot = zeros(nv, 1);
end

% Get number of columns of each variation
sv = [ size(q, 2) , size(qdot, 2) , size(qddot, 2) ];
% Find maximum to of the number of columns to pad other values
nd = max(sv);

% Create output quantity
xq = { ...
  padarray(q    , [ 0 , nd - sv(1) ], 'replicate', 'post') ; ...
  padarray(qdot , [ 0 , nd - sv(2) ], 'replicate', 'post') ; ...
  padarray(qddot, [ 0 , nd - sv(3) ], 'replicate', 'post') ; ...
};



%% Handle variations

% [XQ, VXQ] = VALIDATEINPUTFORWARDKINEMATICS(___)
if nargout > 1
  % Default joint position variation
  if nargin < 5 || isempty(vq)
    vq = zeros(np, nv);
  end
  
  % Default joint velocity variation
  if nargin < 6 || isempty(vqdot)
    vqdot = zeros(nv, nv);
  end
  
  % Default joint acceleration variation
  if nargin < 7 || isempty(vqddot)
    vqddot = zeros(nv, nv);
  end
  
  % Get number of columns of each variation
  sv = [ size(vq, 2) , size(vqdot, 2) , size(vqddot, 2) ];
  % Find maximum to of the number of columns to pad other values
  nd = max(sv);
  
  % Create output quantity
  vxq = { ...
    padarray(vq    , [ 0 , nd - sv(1) ], 'replicate', 'post') ; ...
    padarray(vqdot , [ 0 , nd - sv(2) ], 'replicate', 'post') ; ...
    padarray(vqddot, [ 0 , nd - sv(3) ], 'replicate', 'post') ; ...
  };

end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
