function [T, eta, etadot, vzeta, veta, vetadot] = tangentReconstructKinematics(obj ...
  , q, T0 ...
  , qdot, eta0 ...
  , qddot, eta0dot ...
  , vq, vzeta0 ...
  , vqdot, veta0 ...
  , vqddot, veta0dot ...
  , varargin ...
)
%% TANGENRECONSTRUCTKINEMATICS
%
% TANGENRECONSTRUCTKINEMATICS(OBJ)
%
% TANGENRECONSTRUCTKINEMATICS(OBJ, Q)
%
% TANGENRECONSTRUCTKINEMATICS(OBJ, Q, T0)
%
% TANGENRECONSTRUCTKINEMATICS(OBJ, Q, T0, QDOT)
%
% TANGENRECONSTRUCTKINEMATICS(OBJ, Q, T0, QDOT, ETA0)
%
% TANGENRECONSTRUCTKINEMATICS(OBJ, Q, T0, QDOT, ETA0, QDDOT)
%
% TANGENRECONSTRUCTKINEMATICS(OBJ, Q, T0, QDOT, ETA0, QDDOT, ETA0DOT)
%
% TANGENRECONSTRUCTKINEMATICS(OBJ, Q, T0, QDOT, ETA0, QDDOT, ETA0DOT, VQ)
%
% TANGENRECONSTRUCTKINEMATICS(OBJ, Q, T0, QDOT, ETA0, QDDOT, ETA0DOT, VQ,
% VZETA0)
%
% TANGENRECONSTRUCTKINEMATICS(OBJ, Q, T0, QDOT, ETA0, QDDOT, ETA0DOT, VQ,
% VZETA0, VQDOT)
%
% TANGENRECONSTRUCTKINEMATICS(OBJ, Q, T0, QDOT, ETA0, QDDOT, ETA0DOT, VQ,
% VZETA0, VQDOT, VETA0)
%
% TANGENRECONSTRUCTKINEMATICS(OBJ, Q, T0, QDOT, ETA0, QDDOT, ETA0DOT, VQ,
% VZETA0, VQDOT, VETA0, VQDDOT)
%
% TANGENRECONSTRUCTKINEMATICS(OBJ, Q, T0, QDOT, ETA0, QDDOT, ETA0DOT, VQ,
% VZETA0, VQDOT, VETA0, VQDDOT, VETA0DOT)
%
% Inputs:
%
%   OBJ                     Description of argument OBJ
% 
%   X                       Description of argument X
% 
%   XDOT                    Description of argument XDOT
% 
%   XDDOT                   Description of argument XDDOT
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% TANGENRECONSTRUCTKINEMATICS(OBJ)
% TANGENRECONSTRUCTKINEMATICS(OBJ, Q)
% TANGENRECONSTRUCTKINEMATICS(OBJ, Q, T0)
% TANGENRECONSTRUCTKINEMATICS(OBJ, Q, T0, QDOT)
% TANGENRECONSTRUCTKINEMATICS(OBJ, Q, T0, QDOT, ETA0)
% TANGENRECONSTRUCTKINEMATICS(OBJ, Q, T0, QDOT, ETA0, QDDOT)
% TANGENRECONSTRUCTKINEMATICS(OBJ, Q, T0, QDOT, ETA0, QDDOT, ETA0DOT)
% TANGENRECONSTRUCTKINEMATICS(OBJ, Q, T0, QDOT, ETA0, QDDOT, ETA0DOT, VQ)
% TANGENRECONSTRUCTKINEMATICS(OBJ, Q, T0, QDOT, ETA0, QDDOT, ETA0DOT, VQ,
% VZETA0)
% TANGENRECONSTRUCTKINEMATICS(OBJ, Q, T0, QDOT, ETA0, QDDOT, ETA0DOT, VQ,
% VZETA0, VQDOT)
% TANGENRECONSTRUCTKINEMATICS(OBJ, Q, T0, QDOT, ETA0, QDDOT, ETA0DOT, VQ,
% VZETA0, VQDOT, VETA0)
% TANGENRECONSTRUCTKINEMATICS(OBJ, Q, T0, QDOT, ETA0, QDDOT, ETA0DOT, VQ,
% VZETA0, VQDOT, VETA0, VQDDOT)
% TANGENRECONSTRUCTKINEMATICS(OBJ, Q, T0, QDOT, ETA0, QDDOT, ETA0DOT, VQ,
% VZETA0, VQDOT, VETA0, VQDDOT, VETA0DOT)
% TANGENRECONSTRUCTKINEMATICS(OBJ, Q, T0, QDOT, ETA0, QDDOT, ETA0DOT, VQ,
% VZETA0, VQDOT, VETA0, VQDDOT, VETA0DOT, ...)
narginchk(1, Inf);

% TANGENRECONSTRUCTKINEMATICS(___)
% T = TANGENRECONSTRUCTKINEMATICS(___)
% [T, ETA] = TANGENRECONSTRUCTKINEMATICS(___)
% [T, ETA, ETADOT] = TANGENRECONSTRUCTKINEMATICS(___)
% [T, ETA, ETADOT, VZETA] = TANGENRECONSTRUCTKINEMATICS(___)
% [T, ETA, ETADOT, VZETA, VETA] = TANGENRECONSTRUCTKINEMATICS(___)
% [T, ETA, ETADOT, VZETA, VETA, VETADOT] = TANGENRECONSTRUCTKINEMATICS(___)
nargoutchk(0, 6);

% TANGENRECONSTRUCTKINEMATICS(OBJ)
if nargin < 2 || isempty(q)
  q = configurationHome(obj);
end

% Quick access to local variables
nv = obj.VelocityNumber;

% TANGENRECONSTRUCTKINEMATICS(OBJ, Q)
if nargin < 3 || isempty(T0)
  T0 = tformzeros();
end

% TANGENRECONSTRUCTKINEMATICS(OBJ, Q, T0)
if nargin < 4 || isempty(qdot)
  qdot = zeros(nv, 1);
end

% TANGENRECONSTRUCTKINEMATICS(OBJ, Q, T0, QDOT)
if nargin < 5 || isempty(eta0)
  eta0 = twistzeros();
end

% TANGENRECONSTRUCTKINEMATICS(OBJ, Q, T0, QDOT, ETA0)
if nargin < 6 || isempty(qddot)
  qddot = zeros(nv, 1);
end

% TANGENRECONSTRUCTKINEMATICS(OBJ, Q, T0, QDOT, ETA0, QDDOT)
if nargin < 7 || isempty(eta0dot)
  eta0dot = twistzeros();
end

% TANGENRECONSTRUCTKINEMATICS(OBJ, Q, T0, QDOT, ETA0, QDDOT, ETA0DOT)
if nargin < 8 || isempty(vq)
  vq = zeros(nv, nv);
end

% Number of variations
nd = size(vq, 2);

% TANGENRECONSTRUCTKINEMATICS(OBJ, Q, T0, QDOT, ETA0, QDDOT, ETA0DOT, VQ)
if nargin < 9 || isempty(vzeta0)
  vzeta0 = zeros(6, nd);
end

% TANGENRECONSTRUCTKINEMATICS(OBJ, Q, T0, QDOT, ETA0, QDDOT, ETA0DOT, VQ,
% VZETA0)
if nargin < 10 || isempty(vqdot)
  vqdot = zeros(nv, nd);
end

% TANGENRECONSTRUCTKINEMATICS(OBJ, Q, T0, QDOT, ETA0, QDDOT, ETA0DOT, VQ,
% VZETA0, VQDOT)
if nargin < 11 || isempty(veta0)
  veta0 = zeros(6, nd);
end

% TANGENRECONSTRUCTKINEMATICS(OBJ, Q, T0, QDOT, ETA0, QDDOT, ETA0DOT, VQ,
% VZETA0, VQDOT, VETA0)
if nargin < 12 || isempty(vqddot)
  vqddot = zeros(nv, nd);
end

% TANGENRECONSTRUCTKINEMATICS(OBJ, Q, T0, QDOT, ETA0, QDDOT, ETA0DOT, VQ,
% VZETA0, VQDOT, VETA0, VQDDOT)
if nargin < 13 || isempty(veta0dot)
  veta0dot = zeros(6, nd);
end



%% Algorithm

% Reconstuct joint's relative kinematics passing additional arguments down
[TJ, etaJ, etaJdot, vzetaJ, vetaJ, vetaJdot] = tangentReconstructKinematicsImpl(obj, { q , qdot , qddot }, { vq , vqdot , vqddot }, varargin{:});

% Transform relative kinematics into inertial kinematics
T = tformmul(T0, TJ);

% [T, ETA] = TANGENRECONSTRUCTKINEMATICS(___)
% [T, ETA, ETADOT] = TANGENRECONSTRUCTKINEMATICS(___)
if nargout > 1
  XJ  = Ad(tform2inv(TJ));
  eta = permute(pagemult(XJ, eta0), [1, 3, 2]) + ...
        + etaJ;

end

% [T, ETA, ETADOT] = TANGENRECONSTRUCTKINEMATICS(___)
if nargout > 2
  etadot = permute(pagemult(XJ, eta0dot), [1, 3, 2]) + ...
          + permute(pagemult(ad(eta), permute(etaJ, [1, 3, 2])), [1, 3, 2]) + ...
          + etaJdot;

end

% [T, ETA, ETADOT, VZETA] = TANGENRECONSTRUCTKINEMATICS(___)
if nargout > 3
  % Variation of transformation
  vzeta = pagemult(XJ, vzeta0) + vzetaJ;
  
end

% [T, ETA, ETADOT, VZETA, VETA] = TANGENRECONSTRUCTKINEMATICS(___)
if nargout > 4
  % Variation of adjoint transformations
  vXJ = pagemult(-ad(vzetaJ), permute(XJ, [1, 2, 4, 3]));
  
  % Varation of twists
  veta = pagemult(XJ, veta0) + permute(pagemult(vXJ, eta0), [1, 3, 4, 2]) + ...
          + vetaJ;
  
end

% [T, ETA, ETADOT, VZETA, VETA, VETADOT] = TANGENRECONSTRUCTKINEMATICS(___)
if nargout > 5
  % Variation of twist velocities of joint
  vetadot = pagemult(XJ, veta0dot) + permute(pagemult(vXJ, eta0dot), [1, 3, 4, 2]) + ...
            - pagemult(ad(etaJ), veta) + pagemult(ad(eta), vetaJ) + ...
            + vetaJdot;
  
end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
