function [x1, xq, F1, vx1, vxq, vF1] = validateInputBackwardDynamics( ...
    obj ...
  , T1, V1, V1dot ...
  , q, qdot, qddot ...
  , F1 ...
  , vT1, vV1, vV1dot ...
  , vq, vqdot, vqddot ...
  , vF1 ...
)%#codegen
%% VALIDATEINPUTBACKWARDDYNAMICS
% 
% VALIDATEINPUTBACKWARDDYNAMICS(OBJ)
% 
% VALIDATEINPUTBACKWARDDYNAMICS(OBJ, T1)
% 
% VALIDATEINPUTBACKWARDDYNAMICS(OBJ, T1, V1)
% 
% VALIDATEINPUTBACKWARDDYNAMICS(OBJ, T1, V1, V1DOT)
% 
% VALIDATEINPUTBACKWARDDYNAMICS(OBJ, T1, V1, V1DOT, Q)
% 
% VALIDATEINPUTBACKWARDDYNAMICS(OBJ, T1, V1, V1DOT, Q, QDOT)
% 
% VALIDATEINPUTBACKWARDDYNAMICS(OBJ, T1, V1, V1DOT, Q, QDOT, QDDOT)
% 
% VALIDATEINPUTBACKWARDDYNAMICS(OBJ, T1, V1, V1DOT, Q, QDOT, QDDOT, F1)
% 
% VALIDATEINPUTBACKWARDDYNAMICS(OBJ, T1, V1, V1DOT, Q, QDOT, QDDOT, F1, VT1)
% 
% VALIDATEINPUTBACKWARDDYNAMICS(OBJ, T1, V1, V1DOT, Q, QDOT, QDDOT, F1, VT1,
% VV1)
% 
% VALIDATEINPUTBACKWARDDYNAMICS(OBJ, T1, V1, V1DOT, Q, QDOT, QDDOT, F1, VT1,
% VV1, VV1DOT)
% 
% VALIDATEINPUTBACKWARDDYNAMICS(OBJ, T1, V1, V1DOT, Q, QDOT, QDDOT, F1, VT1,
% VV1, VV1DOT, VQ)
% 
% VALIDATEINPUTBACKWARDDYNAMICS(OBJ, T1, V1, V1DOT, Q, QDOT, QDDOT, F1, VT1,
% VV1, VV1DOT, VQ, VQDOT)
% 
% VALIDATEINPUTBACKWARDDYNAMICS(OBJ, T1, V1, V1DOT, Q, QDOT, QDDOT, F1, VT1,
% VV1, VV1DOT, VQ, VQDOT, VQDDOT)
% 
% VALIDATEINPUTBACKWARDDYNAMICS(OBJ, T1, V1, V1DOT, Q, QDOT, QDDOT, F1, VT1,
% VV1, VV1DOT, VQ, VQDOT, VQDDOT, VF1)
%
% X1 = VALIDATEINPUTBACKWARDDYNAMICS(OBJ, ___)
%
% [X1, XQ] = VALIDATEINPUTBACKWARDDYNAMICS(OBJ, ___)
%
% [X1, XQ, F1] = VALIDATEINPUTBACKWARDDYNAMICS(OBJ, ___)
%
% [X1, XQ, F1, VX1] = VALIDATEINPUTBACKWARDDYNAMICS(OBJ, ___)
%
% [X1, XQ, F1, VX1, VXQ] = VALIDATEINPUTBACKWARDDYNAMICS(OBJ, ___)
%
% [X1, XQ, F1, VX1, VXQ, VF1] = VALIDATEINPUTBACKWARDDYNAMICS(OBJ, ___)
%
% Inputs:
%
%   OBJ                     Description of argument OBJ
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% VALIDATEINPUTBACKWARDDYNAMICS(OBJ)
% VALIDATEINPUTBACKWARDDYNAMICS(OBJ, T1)
% VALIDATEINPUTBACKWARDDYNAMICS(OBJ, T1, V1)
% VALIDATEINPUTBACKWARDDYNAMICS(OBJ, T1, V1, V1DOT)
% VALIDATEINPUTBACKWARDDYNAMICS(OBJ, T1, V1, V1DOT, Q)
% VALIDATEINPUTBACKWARDDYNAMICS(OBJ, T1, V1, V1DOT, Q, QDOT)
% VALIDATEINPUTBACKWARDDYNAMICS(OBJ, T1, V1, V1DOT, Q, QDOT, QDDOT)
% VALIDATEINPUTBACKWARDDYNAMICS(OBJ, T1, V1, V1DOT, Q, QDOT, QDDOT, F1)
% VALIDATEINPUTBACKWARDDYNAMICS(OBJ, T1, V1, V1DOT, Q, QDOT, QDDOT, F1, VT1)
% VALIDATEINPUTBACKWARDDYNAMICS(OBJ, T1, V1, V1DOT, Q, QDOT, QDDOT, F1, VT1,
% VV1)
% VALIDATEINPUTBACKWARDDYNAMICS(OBJ, T1, V1, V1DOT, Q, QDOT, QDDOT, F1, VT1,
% VV1, VV1DOT)
% VALIDATEINPUTBACKWARDDYNAMICS(OBJ, T1, V1, V1DOT, Q, QDOT, QDDOT, F1, VT1,
% VV1, VV1DOT, VQ)
% VALIDATEINPUTBACKWARDDYNAMICS(OBJ, T1, V1, V1DOT, Q, QDOT, QDDOT, F1, VT1,
% VV1, VV1DOT, VQ, VQDOT)
% VALIDATEINPUTBACKWARDDYNAMICS(OBJ, T1, V1, V1DOT, Q, QDOT, QDDOT, F1, VT1,
% VV1, VV1DOT, VQ, VQDOT, VQDDOT)
% VALIDATEINPUTBACKWARDDYNAMICS(OBJ, T1, V1, V1DOT, Q, QDOT, QDDOT, F1, VT1,
% VV1, VV1DOT, VQ, VQDOT, VQDDOT, VF1)
narginchk(1, 15);

% X1 = VALIDATEINPUTBACKWARDDYNAMICS(OBJ, ___)
% [X1, XQ] = VALIDATEINPUTBACKWARDDYNAMICS(OBJ, ___)
% [X1, XQ, F1] = VALIDATEINPUTBACKWARDDYNAMICS(OBJ, ___)
% [X1, XQ, F1, VX1] = VALIDATEINPUTBACKWARDDYNAMICS(OBJ, ___)
% [X1, XQ, F1, VX1, VXQ] = VALIDATEINPUTBACKWARDDYNAMICS(OBJ, ___)
% [X1, XQ, F1, VX1, VXQ, VF1] = VALIDATEINPUTBACKWARDDYNAMICS(OBJ, ___)
nargoutchk(1, 6);



%% Algorithm

nv = obj.VelocityNumber;

% Default distal pose
if nargin < 2 || isempty(T1)
  T1 = tformzeros();
end

% Default distal twist
if nargin < 3 || isempty(V1)
  V1 = twistzeros();
end

% Default distal twist velocity
if nargin < 4 || isempty(V1dot)
  V1dot = twistzeros();
end

% Default joint position
if nargin < 5 || isempty(q)
  q = configurationHome(obj);
end

% Default joint velocity
if nargin < 6 || isempty(qdot)
  qdot = zeros(nv, 1);
end

% Default joint acceleration
if nargin < 7 || isempty(qddot)
  qddot = zeros(nv, 1);
end

% Default distal force
if nargin < 8 || isempty(F1)
  F1 = zeros(6, 1);
end

% Get number of columns of each variation
sv = [ size(T1, 3) , size(V1, 2) , size(V1dot, 2) , size(F1, 2), size(q, 2) , size(qdot, 2) , size(qddot, 2) ];
% Find maximum to of the number of columns to pad other values
nd = max(sv);

% Create output quantities
x1 = { ...
  padarray(T1   , [ 0 , 0          , nd - sv(1) ], 'replicate', 'post') ; ...
  padarray(V1   , [ 0 , nd - sv(2)              ], 'replicate', 'post') ; ...
  padarray(V1dot, [ 0 , nd - sv(3)              ], 'replicate', 'post') ; ...
};
F1 = padarray(F1, [ 0 , nd - sv(4) ], 'replicate', 'post');
xq = { ...
  padarray(q    , [ 0 , nd - sv(5) ], 'replicate', 'post') ; ...
  padarray(qdot , [ 0 , nd - sv(6) ], 'replicate', 'post') ; ...
  padarray(qddot, [ 0 , nd - sv(7) ], 'replicate', 'post') ; ...
};



%% Handle Variations

if nargout > 3
  % Default distal pose variation
  if nargin < 9 || isempty(vT1)
    vT1 = twistzeros();
  end
  
  % Default distal twist variation
  if nargin < 10 || isempty(vV1)
    vV1 = twistzeros();
  end
  
  % Default distal twist velocity variation
  if nargin < 11 || isempty(vV1dot)
    vV1dot = twistzeros();
  end
  
  % Default joint position variation
  if nargin < 12 || isempty(vq)
    vq = zeros(nv, 1);
  end
  
  % Default joint velocity variation
  if nargin < 13 || isempty(vqdot)
    vqdot = zeros(nv, 1);
  end
  
  % Default joint acceleration variation
  if nargin < 14 || isempty(vqddot)
    vqddot = zeros(nv, 1);
  end
  
  % Default distal force variation
  if nargin < 15 || isempty(vF1)
    vF1 = zeros(nv, 1);
  end
  
  % Get number of columns of each variation
  sv = [ size(vT1, 2) , size(vV1, 2) , size(vV1dot, 2) , size(vF1, 2) , size(vq, 2) , size(vqdot, 2) , size(vqddot, 2)];
  % Find maximum to of the number of columns to pad other values
  nd = max(sv);
  
  % Create output quantity
  vx1 = { ...
    padarray(vT1    , [ 0 , nd - sv(1) ], 'replicate', 'post') ; ...
    padarray(vV1    , [ 0 , nd - sv(2) ], 'replicate', 'post') ; ...
    padarray(vV1dot , [ 0 , nd - sv(3) ], 'replicate', 'post') ; ...
  };
  vF1 = padarray(vF1, [ 0 , nd - sv(4) ], 'replicate', 'post');

  vxq = { ...
    padarray(vq     , [ 0 , nd - sv(5) ], 'replicate', 'post') ; ...
    padarray(vqdot  , [ 0 , nd - sv(6) ], 'replicate', 'post') ; ...
    padarray(vqddot , [ 0 , nd - sv(7) ], 'replicate', 'post') ; ...
  };
  
end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
