function [out1, out2, out3, out4] = validateTangentDynamicsInputs(obj, varargin)
%% VALIDATETANGENTDYNAMICSINPUTS
%
% [VQ, VQDOT, VQDDOT_VTAU, VF1] = VALIDATETANGENTDYNAMICSINPUTS(OBJ, VQ, VQDOT,
% VQDDOT_VTAU, VF1) validates the size of all inputs and matches them to each
% other. All input arguments are optional and, if not given, will assume sane
% defaults of the same number of columns as the widest input.
%
% Inputs:
%
%   OBJ                     Joint object.
%
%   VQ                      PxD array of variation of generalized position
%                           coordinates.
%
%   VQDOT                   VxD array of variation of generalized velocities.
%
%   VQDDOT_VTAU             VxD array of variation of generalized accelerations
%                           or VxD array of variation of generalized acuation,
%                           depending on use of the inputs.
% 
%   OUT4                    6xD array of variation of distal force F1.
%
% Outputs:
%
%   OUT1                    PxD array of variation of generalized position
%                           coordinates. Column J presents variation with
%                           respect to the J-th generalized position coordinate.
%                           Defaults to `ZEROS(P, 1)`.
% 
%   OUT2                    VxD array of variation of generalized velocities.
%                           Defaults to `ZEROS(V, 1)`
% 
%   OUT3                    VxD array of variation of generalized accelerations
%                           or VxD array of variation of generalized actuation,
%                           depending on use of the inputs.
% 
%   OUT4                    6xD array of variation of distal force F1.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% VALIDATETANGENTDYNAMICSINPUTS(OBJ)
% VALIDATETANGENTDYNAMICSINPUTS(OBJ, VQ)
% VALIDATETANGENTDYNAMICSINPUTS(OBJ, VQ, VQDOT)
% VALIDATETANGENTDYNAMICSINPUTS(OBJ, VQ, VQDOT, VQDDOT_VTAU)
% VALIDATETANGENTDYNAMICSINPUTS(OBJ, VQ, VQDOT, VQDDOT_VTAU, VF1)
narginchk(1, 5);

% [VQ, VQDOT, VQDDOT_VTAU] = VALIDATETANGENTDYNAMICSINPUTS(___)
% [VQ, VQDOT, VQDDOT_VTAU, VF1] = VALIDATETANGENTDYNAMICSINPUTS(___)
nargoutchk(3, 4);

prepareObjectForUse(obj);



%% Algorithm

% Local variables
args  = varargin;
nargs = numel(args);
nv    = obj.VelocityNumber;

% Initialize outputs
in1 = zeros(nv, 1);
in2 = zeros(nv, 1);
in3 = zeros(nv, 1);
in4 = zeros(6, 1);

% VALIDATETANGENTDYNAMICSINPUTS(OBJ, VQ)
if nargs > 0 && ~isempty(varargin{1})
  in1 = double(varargin{1});
end

% VALIDATETANGENTDYNAMICSINPUTS(OBJ, VQ, VQDOT)
if nargs > 1 && ~isempty(varargin{2})
  in2 = double(varargin{2});
end

% VALIDATETANGENTDYNAMICSINPUTS(OBJ, VQ, VQDOT, VQDDOT_VTAU)
if nargs > 2 && ~isempty(varargin{3})
  in3 = double(varargin{3});
end

% VALIDATETANGENTDYNAMICSINPUTS(OBJ, VQ, VQDOT, VQDDOT_VTAU, VF1)
if nargs > 3 && ~isempty(varargin{4})
  in4 = double(varargin{4});
end

% Count number of columns of all inputs and find the largest colum number
szo = [ size(in1, 2) , size(in2, 2) , size(in3, 2) , size(in4, 2) ];
nc = max(szo);

% Ensure all arguments have the same size of their last dimension
out1 = padarray(in1, [ 0 , nc - szo(1) , 0 ], 'replicate', 'post');
out2 = padarray(in2, [ 0 , nc - szo(2) , 0 ], 'replicate', 'post');
out3 = padarray(in3, [ 0 , nc - szo(3) , 0 ], 'replicate', 'post');
out4 = padarray(in4, [ 0 , nc - szo(4) , 0 ], 'replicate', 'post');


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
