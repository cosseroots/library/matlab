function [T, eta, etadot] = reconstructKinematics(obj, q, T0, qdot, eta0, qddot, eta0dot, varargin)
%% RECONSTRUCTKINEMATICS
%
% [T, ETA, ETADOT] = RECONSTRUCTKINEMATICS(OBJ)
%
% RECONSTRUCTKINEMATICS(OBJ, Q)
%
% RECONSTRUCTKINEMATICS(OBJ, Q, T0)
%
% RECONSTRUCTKINEMATICS(OBJ, Q, T0, QDOT)
%
% RECONSTRUCTKINEMATICS(OBJ, Q, T0, QDOT, ETA0)
%
% RECONSTRUCTKINEMATICS(OBJ, Q, T0, QDOT, ETA0, QDDOT)
%
% RECONSTRUCTKINEMATICS(OBJ, Q, T0, QDOT, ETA0, QDDOT, ETA0DOT)
%
% RECONSTRUCTKINEMATICS(___, ...) passes additional arguments down to the
% underlying joint's reconstruction implementation.
%
% Inputs:
%
%   OBJ                     Description of argument OBJ
%
%   Q                       Px1 array of joint configuration values.
%
%   T0                      4x4 transformation matrix of joint's proximal pose.
%
%   QDOT                    Vx1 array of joint velocity values.
%
%   ETA0                    6x1 inertial joint twist expressed in base
%                           coordinates.
%
%   QDDOT                   Vx1 array of joint acceleration values.
%
%   ETA0DOT                 6x1 inertial joint twist velocities expressed in
%                           base coordinates.
%
% Outputs:
%
%   T                       4x4xN array of reconstructed joint poses. The number
%                           N of poses depends on the underlying joint type.
%                           Refer to each concrete joint's
%                           RECONSTRUCTKINEMATICSIMPL.
%
%   ETA                     6xN array of joint twists expressed in base
%                           coordinates.
%
%   ETADOT                  6xN array of joint twist velocities expressed in
%                           base coordinates.
%
% See also:
%   RIGIDJOINT.RECONSTRUCTKINEMATICSIMPL ELASTICJOINT.RECONSTRUCTKINEMATICSIMPL
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% RECONSTRUCTKINEMATICS(OBJ)
% RECONSTRUCTKINEMATICS(OBJ, Q)
% RECONSTRUCTKINEMATICS(OBJ, Q, T0)
% RECONSTRUCTKINEMATICS(OBJ, Q, T0, QDOT)
% RECONSTRUCTKINEMATICS(OBJ, Q, T0, QDOT, ETA0)
% RECONSTRUCTKINEMATICS(OBJ, Q, T0, QDOT, ETA0, QDDOT)
% RECONSTRUCTKINEMATICS(OBJ, Q, T0, QDOT, ETA0, QDDOT, ETA0DOT)
narginchk(1, Inf);

% RECONSTRUCTKINEMATICS(___)
% T = RECONSTRUCTKINEMATICS(___)
% [T, ETA] = RECONSTRUCTKINEMATICS(___)
% [T, ETA, ETADOT] = RECONSTRUCTKINEMATICS(___)
nargoutchk(0, 3);

% RECONSTRUCTKINEMATICS(OBJ)
if nargin < 2 || isempty(q)
  q = configurationHome(obj);
end

% RECONSTRUCTKINEMATICS(OBJ, Q)
if nargin < 3 || isempty(T0)
  T0 = tformzeros();
end

% RECONSTRUCTKINEMATICS(OBJ, Q, T0)
if nargin < 4 || isempty(qdot)
  qdot = zeros(obj.VelocityNumber, 1);
end

% RECONSTRUCTKINEMATICS(OBJ, Q, T0, QDOT)
if nargin < 5 || isempty(eta0)
  eta0 = twistzeros();
end

% RECONSTRUCTKINEMATICS(OBJ, Q, T0, QDOT, ETA0)
if nargin < 6 || isempty(qddot)
  qddot = zeros(obj.VelocityNumber, 1);
end

% RECONSTRUCTKINEMATICS(OBJ, Q, T0, QDOT, ETA0, QDDOT)
if nargin < 7 || isempty(eta0dot)
  eta0dot = twistzeros();
end



%% Algorithm

% Reconstuct joint's relative kinematics passing additional arguments down
[TJ, etaJ, etaJdot] = reconstructKinematicsImpl(obj, { q , qdot , qddot }, varargin{:});

% Transform relative kinematics into inertial kinematics
T = tformmul(T0, TJ);

% [T, ETA] = RECONSTRUCTKINEMATICS(___)
% [T, ETA, ETADOT] = RECONSTRUCTKINEMATICS(___)
if nargout > 1
  XJ  = Ad(tform2inv(TJ));
  eta = permute(pagemult(XJ, eta0), [1, 3, 2]) + ...
        + etaJ;

end

% [T, ETA, ETADOT] = RECONSTRUCTKINEMATICS(___)
if nargout > 2
  etadot = permute(pagemult(XJ, eta0dot), [1, 3, 2]) + ...
          + permute(pagemult(ad(eta), permute(etaJ, [1, 3, 2])), [1, 3, 2]) + ...
          + etaJdot;

end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
