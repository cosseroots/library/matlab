classdef FreeJoint < RigidJoint
  %% FREEJOINT
  
  
  
  %% CONSTRUCTOR METHODS
  methods
    
    function obj = FreeJoint(varargin)
      %% FREEJOINT
      
      
      
      obj@RigidJoint();
      
      obj.HomePosition   = tparzeros();
      obj.PositionLimits = [ -Inf(7, 1) , +Inf(7, 1) ];
      
      obj.PositionNumber = 7;
      obj.VelocityNumber = 6;
      
      setProperties(obj, nargin, varargin{:});
      
    end
    
  end
  
  
  
  %% CONCRETE INTERNAL METHODS
  methods ( Access = protected )
    
    function T = tformImpl(~, q)
      %% TFORMIMPL
      
      
      
      q_ = q(1:7,:);
      
      T = tpar2tform(q_);
      
    end
    
    
    function setupImpl(obj)
      %% SETUPIMPL
      
      
      
      obj.MotionSubspace   = eye(6, 6);
      obj.VelocitySubspace = zeros(6, 6);
      obj.ForceSubspace    = null(transpose(obj.MotionSubspace), 'r');
      
    end
    
    
    function q = configurationZerosImpl(~, n)
      %% CONFIGURATIONZEROSIMPL
      
      
      
      q = tparzeros(n);
      
    end
    
    
    function q = configurationRandImpl(~, s, n, varargin)
      %% CONFIGURATIONRANDIMPL
      
      
      
      q = tparrand(s, n, varargin{:});
      
    end
    
    
    function q = configurationRandnImpl(~, s, n, varargin)
      %% CONFIGURATIONRANDNIMPL
      
      
      
      q = tparrandn(s, n, varargin{:});
      
    end
    
  end
  
end
