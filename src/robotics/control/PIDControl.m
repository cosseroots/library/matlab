classdef PIDControl < matlab.System ...
    & smms.system.mixin.ExecTime
  %% PIDCONTROL
  %
  % PIDCONTROL('Kp', KP)
  %
  % PIDCONTROL('Kp', KP, 'Ki', Ki)
  %
  % PIDCONTROL('Kp', KP, 'Ki', Ki, 'Kd', Kd)
  %
  % PIDCONTROL('Kp', KP, 'Ki', Ki, 'Kd', Kd, 'VelocityInput', vinpt)
  
  
  
  %% PUBLIC PROPERTIES
  properties ( Nontunable )
    
    % Proportional gains
    Kp (:, 1) double = 1
    
    % Integral gains
    Ki (:, 1) double = 0
    
    % Derivative gains
    Kd (:, 1) double = 0
    
    % Sample time of differentiator
    Ts (:, 1) double = 10 * 1e-3
    
    % Initial value of integrator path
    InitialIntegrator (:, 1) double
    
    % Initial value of derivator path
    InitialDerivator (:, 1) double
    
    % Flag whether velocity i.e., error derivative is an input or not
    VelocityInput (1, 1) logical = false
    
  end
  
  
  
  %% PUBLIC DEPENDENT PROPERTIES
  properties ( Nontunable , Dependent )
    
    % Integrator time constant
    Ti (:, 1) double
    
    % Derivator time constant
    Td (:, 1) double
    
  end
  
  
  
  %% INTERNAL PROPERTIES
  properties ( Access = protected )
    
    % Dimension of the error signal
    Dimension
    
  end
  
  
  
  %% INTERNAL PROPERTIES
  properties ( DiscreteState )
    
    % Last value(s) of integral path
    IntegralBacktrack
    
    % Last value(s) of error / input
    ErrorBacktrack
    
  end
  
  
  
  %% CONSTRUCTOR METHODS
  methods
    
    function obj = PIDControl(varargin)
      %% PIDCONTROL
      
      
      
      setProperties(obj, nargin, varargin{:});
      
    end
    
  end
  
  
  %% SETTERS
  methods
    
    function set.Ti(obj, ti)
      %% SET.TI
      
      
      
      ki = obj.Kp ./ ti;
      ki(isclose(ki, 0) | isnan(ki)) = 0;
      
      obj.Ki = ki;
      
    end
    
    
    function set.Td(obj, td)
      %% SET.TD
      
      
      
      obj.Kd = obj.Kp .* td;
      
    end
    
  end
  
  
  
  %% GETTERS
  methods
    
    function v = get.Ti(obj)
      %% GET.TI
      
      
      
      ti = obj.Kp ./ obj.Ki;
      ti(isclose(ti, 0) | isnan(ti) | isinf(ti)) = 0;
      
      v = ti;
      
    end
    
    
    function v = get.Td(obj)
      %% GET.TD
      
      
      
      v = obj.Kd ./ obj.Kp;
      v(isclose(v, 0) | isnan(v) | isinf(v)) = 0;
      
    end
    
  end
  
  
  
  %% CONCRETE MIXIN METHODS
  methods ( Access = protected )
    
    function setupImpl(obj, error, varargin)
      %% SETUPIMPL
      
      
      
      % Get dimension of error to initialize internal properties
      obj.Dimension = numel(error);
      ne = obj.Dimension;
      
      % Default value of integrator path if not given
      if isempty(obj.InitialIntegrator)
        obj.InitialIntegrator = zeros(ne, 1);
        
      end
      
      % Ensure initial value of integrator has same dimension as error
      if numel(obj.InitialIntegrator) < ne
        obj.InitialIntegrator = repmat(obj.InitialIntegrator, [ne, 1]);
        
      end
      
      % Default value of derivative path if not given
      if isempty(obj.InitialDerivator)
        obj.InitialDerivator = zeros(ne, 1);
        
      end
      
      % Ensure initial value of integrator has same dimension as error
      if numel(obj.InitialDerivator) < ne
        obj.InitialDerivator = repmat(obj.InitialDerivator, [ne, 1]);
        
      end
      
    end
    
    
    function y = stepImpl(obj, error, varargin)
      %% STEPIMPL
      
      
      
      % Sample time
      ts = obj.Ts;
      
      % If not provided, obtain derivative of error numerically
      if nargin < 3 || isempty(varargin{1})
        error_dot = ( error - obj.ErrorBacktrack ) ./ ts;
      
      % Error derivative provided
      else
        error_dot = varargin{1};
        
      end
      
      % Proportional part
      p_error_ = error;
      % Integral part
      i_error_ = ts .* p_error_ + obj.IntegralBacktrack;
      % Derivative part
      d_error_ = error_dot;
      
      % Store backtracking information
      obj.IntegralBacktrack = i_error_;
      obj.ErrorBacktrack    = p_error_;
      
      % Output
      y = obj.Kp .* p_error_ + ...
          + obj.Ki .* i_error_ + ...
          + obj.Kd .* d_error_;
      
    end
    
    
    function resetImpl(obj)
      %% RESETIMPL
      
      
      
      % Initialize backtracking information
      obj.IntegralBacktrack = obj.InitialIntegrator ./ obj.Ki;
      obj.ErrorBacktrack    = obj.InitialDerivator  ./ obj.Kd .* obj.Ts;
      
      % Ensure backtrack values aren't NaN in case either Ki or Kd are zero
      obj.IntegralBacktrack(isnan(obj.IntegralBacktrack)) = 0;
      obj.ErrorBacktrack(isnan(obj.ErrorBacktrack)) = 0;
      
    end
    
    
    function num = getNumInputsImpl(obj)
      %% GETNUMINPUTSIMPL
      
      
      
      num = 1 + obj.VelocityInput;
      
    end
    
    
    function [y1, y2] = getOutputSizeImpl(obj)
      %% GETOUTPUTSIZEIMPL
      
      
      
      y1 = propagatedInputSize(obj, 1);
      y2 = propagatedInputSize(obj, 2);
      
    end
    
    
    function [y1, y2] = getOutputDataTypeImpl(obj)
      %% GETOUTPUTDATATYPEIMPL
      
      
      
      y1 = propagatedInputDataType(obj, 1);
      y2 = propagatedInputDataType(obj, 2);
      
    end
    
    
    function [y1, y2] = isOutputComplexImpl(obj)
      %% ISOUTPUTCOMPLEXIMPL
      
      
      
      y1 = propagatedInputComplexity(obj, 1);
      y2 = propagatedInputComplexity(obj, 2);
      
    end
    
    
    function [y1, y2] = isOutputFixedSizeImpl(obj)
      %% ISOUTPUTFIXEDSIZEIMPL
      
      
      
      y1 = propagatedInputFixedSize(obj, 1);
      y2 = propagatedInputFixedSize(obj, 2);
      
    end
    
    
    function loadObjectImpl(obj, s, wasInUse)
      %% LOADOBJECTIMPL
      
      
      if wasInUse
        if isfield(s, 'Dimension')
          obj.Dimension = s.Dimension;
        end
        if isfield(s, 'InitialOutput')
          obj.IntegralBacktrack = s.InitialOutput;
        end
        if isfield(s, 'IntegralBacktrack')
          obj.IntegralBacktrack = s.IntegralBacktrack;
        end
        if isfield(s, 'ErrorBacktrack')
          obj.ErrorBacktrack = s.ErrorBacktrack;
        end
      end
      
      loadObjectImpl@matlab.System(obj, s, wasInUse);
      
    end
    
    
    function s = saveObjectImpl(obj)
      %% SAVEOBJECTIMPL
      
      
      s = saveObjectImpl@matlab.System(obj);
      
      if isLocked(obj)
        s.Dimension = obj.Dimension;
      end
      
    end
    
  end
  
end
