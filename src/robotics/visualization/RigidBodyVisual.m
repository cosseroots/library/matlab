classdef RigidBodyVisual < smms.internal.Base ...
    & smms.mixin.Drawable ...
    & smms.internal.Named
  %% RIGIDBODYVISUAL
  
  
  
  %% INTERNAL PROPERTIES
  properties ( SetAccess = { ?smms.internal.InternalAccess } )
    
    Faces    (:, :) single = zeros(0, 3, 'single')
    
    Vertices (:, 3) single = zeros(0, 3, 'single')
    
    TForm    (4, 4) double = eye(4, 4)
    
  end
  
  
  
  %% DEPENDENT READ-ONLY PROPERTIES
  properties ( Dependent , SetAccess = protected )
    
    NFaces (1, 1) double
    
  end
  
  
  
  %% INTERNAL PROPERTIES
  properties ( Hidden , SetAccess = { ?smms.internal.InternalAccess } )
    
    DefaultStyling (1, :) cell = cell(1, 0);
    
  end
  
  
  
  %% CONSTRUCTOR METHODS
  methods
    
    function obj = RigidBodyVisual(varargin)
      %% RIGIDBODYVISUAL
      
      
      
      obj@smms.mixin.Drawable();
      obj@smms.internal.Named();
      
      setProperties(obj, nargin, varargin{:});
      
    end
    
  end
  
  
  
  %% STATIC CONSTRUCTORS
  methods ( Static )
    
    function obj = cuboid(w, d, h, varargin)
      %% CUBOID
      
      
      
      % CUBOID()
      % CUBOID(WIDTH)
      % CUBOID(WIDTH, DEPTH)
      % CUBOID(WIDTH, DEPTH, HEIGHT)
      % CUBOID(WIDTH, DEPTH, HEIGHT, Name, Value)
      narginchk(0, Inf);
      
      % CUBOID(___)
      % OBJ = CUBOID(___)
      nargoutchk(0, 1);
      
      % CUBOID()
      if nargin < 1 || isempty(w)
        w = 1.0;
      end
      
      % CUBOID(WIDTH)
      if nargin < 2 || isempty(d)
        d = 1.0;
      end
      
      % CUBOID(WIDTH, DEPTH)
      if nargin < 3 || isempty(h)
        h = 1.0;
      end
      
      % Create mesh for cuboid
      [f, v] = meshgen.cuboid([w,d,h]);
      
      dfltstylng = { ...
          'FaceColor'         , color('Gray') ...
        , 'EdgeColor'         , rgbdarker(color('Gray'), 0.05) ...
        , 'FaceLighting'      , 'gouraud' ...
        , 'AmbientStrength'   , 0.5 ...
        , 'DiffuseStrength'   , 0.3 ...
        , 'SpecularStrength'  , 0.6 ...
        , 'BackFaceLighting'  , 'reverselit' ...
      };
      
      obj = RigidBodyVisual('DefaultStyling', dfltstylng, varargin{:}, 'Faces', f, 'Vertices', v);
      
    end
    
    
    function obj = cylinder(r, h, n, varargin)
      %% CYLINDER
      
      
      
      % CYLINDER()
      % CYLINDER(RADIUS)
      % CYLINDER(RADIUS, HEIGHT)
      % CYLINDER(RADIUS, HEIGHT, NUMFACES)
      % CYLINDER(RADIUS, HEIGHT, NUMFACES, Name, Value)
      narginchk(0, Inf);
      
      % CYLINDER(___)
      % OBJ = CYLINDER(___)
      nargoutchk(0, 1);
      
      % CYLINDER()
      if nargin < 1 || isempty(r)
        r = 1.0;
      end
      
      % CYLINDER(RADIUS)
      if nargin < 2 || isempty(h)
        h = 1.0;
      end
      
      % CYLINDER(RADIUS, HEIGHT)
      if nargin < 3 || isempty(n)
        n = 13;
      end
      % Backwards compatability
      if ~isnumeric(n) && isodd(numel(varargin))
        varargin = [ { n } , varargin ];
        n = 13;
      end
      
      % Create mesh for cylinder
      [f, v] = meshgen.cylinder([r, h], n);
      
      dfltstylng = { ...
          'FaceColor'         , color('Gray') ...
        , 'EdgeColor'         , rgbdarker(color('Gray'), 0.05) ...
        , 'FaceLighting'      , 'gouraud' ...
        , 'AmbientStrength'   , 0.5 ...
        , 'DiffuseStrength'   , 0.3 ...
        , 'SpecularStrength'  , 0.6 ...
        , 'BackFaceLighting'  , 'reverselit' ...
      };
      
      obj = RigidBodyVisual('DefaultStyling', dfltstylng, varargin{:}, 'Faces', f, 'Vertices', v);
      
    end
    
    
    function obj = ellipsoid(rx, ry, rz, varargin)
      %% ELLIPSOID
      
      
      
      % ELLIPSOID()
      % ELLIPSOID(RADIUSX)
      % ELLIPSOID(RADIUSX, RADIUSY)
      % ELLIPSOID(RADIUSX, RADIUSY, RADIUSZ)
      narginchk(0, Inf);
      
      % ELLIPSOID(___)
      % OBJ = ELLIPSOID(___)
      nargoutchk(0, 1);
      
      % ELLIPSOID()
      if nargin < 1 || isempty(rx)
        rx = 1.0;
      end
      
      % ELLIPSOID(RADIUSX)
      if nargin < 2 || isempty(ry)
        ry = 1.0;
      end
      
      % ELLIPSOID(RADIUSX, RADIUSY)
      if nargin < 3 || isempty(rz)
        rz = 1.0;
      end
      
      % Create mesh for ellipsoid
      [f, v] = meshgen.ellipsoid([rx, ry, rz]);
      
      dfltstylng = { ...
          'FaceColor'         , color('Gray') ...
        , 'EdgeColor'         , rgbdarker(color('Gray'), 0.05) ...
        , 'FaceLighting'      , 'gouraud' ...
        , 'AmbientStrength'   , 0.5 ...
        , 'DiffuseStrength'   , 0.3 ...
        , 'SpecularStrength'  , 0.6 ...
        , 'BackFaceLighting'  , 'reverselit' ...
      };
      
      obj = RigidBodyVisual('DefaultStyling', dfltstylng, varargin{:}, 'Faces', f, 'Vertices', v);
      
    end
    
  end
  
  
  
  %% GETTERS
  methods
    
    function v = get.NFaces(obj)
      %% GET.NFACES
      
      
      
      v = size(obj.Faces, 1);
      
    end
    
  end
  
  
  
  %% VISUALIZATION METHODS
  methods ( Sealed , Access = protected )
    
    function h = drawImpl(obj, ax, varargin)
      %% DRAWIMPL
      
      
      
      h  = hgtransform( ...
            ax ...
          , 'Tag'   , obj.Name ...
          , 'Matrix', obj.TForm ...
        );
      
      hf = patch( ...
          ax ...
        , 'Faces'             , obj.Faces ...
        , 'Vertices'          , obj.Vertices ...
        , 'AmbientStrength'   , 0.5 ...
        , 'DiffuseStrength'   , 0.3 ...
        , 'SpecularStrength'  , 0.6 ...
        , 'BackFaceLighting'  , 'reverselit' ...
        , 'Tag'               , 'Surface' ...
      );
      
      set(hf, 'Parent', h);
      
    end
    
    
    function styleImpl(obj, hp, varargin)
      %% STYLEIMPL
      
      
      
      set(hp.Children(1), varargin{:});
      
    end
    
    
    function s = defaultStyleImpl(obj)
      %% DEFAULTSTYLEIMPL
      
      
      
      s = obj.DefaultStyling;
      
    end
    
  end
  
end
