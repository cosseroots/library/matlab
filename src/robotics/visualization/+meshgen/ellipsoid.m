function [F, V] = ellipsoid(dims, n)
%% ELLIPSOID Create faces and vertices of an ellipsoid
%
% [F, V] = ELLIPSOID(DIMS) creates faces and vertices of triangulation of an
% ellipsoid with dimensions DIMS = [ RX , RY , RZ ].
%
% Inputs:
%
%   DIMS                    1x3 array of ellipsoid axes radius as
%                           [ RX , RY , RZ ]
%
%   N                       Number of divisions per axis.
%
% Outputs:
%
%   F                       Fx3 array of triangulated ellipsoid faces.
% 
%   V                       Vx3 array of triangulated ellipsoid vertices.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% ELLIPSOID(DIMS)
% ELLIPSOID(DIMS, N)
narginchk(1, 2)

% [F, V] = ELLIPSOID(___)
nargoutchk(2, 2);

% ELLIPSOID(DIMS)
if nargin < 2 || isempty(n)
  n = 10;
end



%% Algorithm

k = 0;
V = [];
F = [];

% Top
for in = 1:n
  for jn = 1:n
    v = [ ...
      -1 + 2 * (in - 1) / n , -1 + 2 * ( jn - 1) / n , 1 ; ...
      -1 + 2 * (in - 1) / n , -1 + 2 * ( jn - 0) / n , 1 ; ...
      -1 + 2 * (in - 0) / n , -1 + 2 * ( jn - 1) / n , 1 ; ...
      -1 + 2 * (in - 0) / n , -1 + 2 * ( jn - 0) / n , 1 ; ....
    ];
    f = [ ...
      k + 1 , k + 2 , k + 3 ; ...
      k + 3 , k + 2 , k + 4 ; ...
    ];
    k = k + 4;
    V = [ V ; v ];
    F = [ F ; f ];
    
  end
end

Vt = transpose(V);
% Bottom
V2 = transpose(axang2rotm([0 ; 1 ; 0 ; +pi / 1 ]) * Vt);
% Front
V3 = transpose(axang2rotm([0 ; 1 ; 0 ; +pi / 2 ]) * Vt);
% Back
V4 = transpose(axang2rotm([0 ; 1 ; 0 ; -pi / 2 ]) * Vt);
% Left
V5 = transpose(axang2rotm([1 ; 0 ; 0 ; +pi / 2 ]) * Vt);
% Right
V6 = transpose(axang2rotm([1 ; 0 ; 0 ; -pi / 2 ]) * Vt);

% Put all faces and all vertices together
V = [ V ; V2 ; V3 ; V4 ; V5 ; V6 ];
F = [ F ; F + k ; F + 2 * k ; F + 3 * k ; F + 4 * k ; F + 5 * k ];

% Put all unique vertices together
[V, ~, Ic] = unique(V, 'rows', 'stable');
for in = 1:size(F, 1)
  F(in,:) = [ Ic(F(in,1)) , Ic(F(in,2)) , Ic(F(in,3)) ];
end

% Normalize to make circle and then multiply by each axis' radius
V = normalize(V, 2, 'norm') .* repmat(dims, [size(V, 1), 1]);

F = F(:,[1,3,2]);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
