function [F, V] = cylinder(dims, n)
%% CYLINDER Create faces and vertices of a cylinder.
%
% [F, V] = CYLINDER(DIMS) creates faces F and vertices V of a cylinder with
% dimensiosn DIMS.
%
% Inputs:
%
%   DIMS                    1x2 array of cylinder dimensions as [ R , L ].
% 
%   N                       Number of sides of cylinder.
%
% Outputs:
%
%   F                       Fx3 array of triangulated cylinder faces.
% 
%   V                       Vx3 array of triangulated cylinder vertices.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% CYLINDER(DIMS)
% CYLINDER(DIMS, N)
narginchk(1, 2);

% [F, V] = CYLINDER(___)
nargoutchk(2, 2);

% CYLINDER(DIMS)
if nargin < 2 || isempty(n)
  n = 10;
end



%% Algorithm

r = dims(1);
l = dims(2);

theta = linspace(0, 2 * pi, n + 1);
theta = reshape(theta(1:end-1), [], 1);

m = numel(theta);

% z-axis cylinder
V = repmat([ ...
      r * cos(theta) ...
    , r * sin(theta) ...
    , -(l / 2) * ones(m, 1) ...
  ] ...
  , [2, 1] ...
);
V((m + 1):(2 * m),3) = (l / 2) * ones(m, 1);

V = [ ...
  V ; ...
  [ 0 , 0 , -l / 2 ] ; ...
  [ 0 , 0 , +l / 2 ] ; ...
]; 

F = [];

% Sides
for im = 1:m
  f = [ ...
        im ,         im + 1 ,     m + im     ; ... % side
    m + im ,         im + 1 ,     m + im + 1 ; ... % side
    m + im ,     m + im + 1 , 2 * m +      2 ; ... % cap
        im , 2 * m +      1 ,         im + 1 ; ... % bottom 
  ];
  
  if im == m
    f = [ ...
      m     ,         1 , 2 * m     ; ...
      m + m ,         1 ,     m + 1 ; ...
      m + m ,     m + 1 , 2 * m + 2 ; ...
      m     , 2 * m + 1 ,         1 ; ...
    ];
    
  end
  
  F = [ F ; f ]; %#ok<AGROW>
  
end



end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
