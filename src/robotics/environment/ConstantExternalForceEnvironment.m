classdef ConstantExternalForceEnvironment < Environment
  %% CONSTANTEXTERNALFORCEENVIRONMENT
  
  
  
  %% PUBLIC CONSTANT PROPERTIES
  properties ( Nontunable )
    
    ExternalForce = []
    
  end
  
  
  
  %% CONSTRUCTOR METHODS
  methods
    
    function obj = ConstantExternalForceEnvironment(varargin)
      %% CONSTANTEXTERNALFORCEENVIRONMENT
      
      
      
      setProperties(obj, nargin, varargin{:});
      
    end
    
  end
  
  
  
  %% CONCRETE IMPLEMENTATION METHODS
  methods ( Access = protected )
    
    function setupImpl(obj)
      %% SETUPIMPL
      
      
      setupImpl@Environment(obj);
      
      if isempty(obj.ExternalForce)
        obj.ExternalForce = zeros(6, obj.Parent.SMMSTree.NumBody);
      end
      
    end
    
    
    function resetImpl(~)
      %% RESETIMPL
    end
    
    
    function Fext = stepImpl(obj, t, ~, ~)
      %% STEPIMPL
      %
      % STEPIMPL(OBJ, T, Q, QDOT)
      
      
      Fext = repmat(obj.ExternalForce, 1, 1, numel(t));
      
    end
    
  end
  
end
