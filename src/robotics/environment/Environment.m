classdef Environment < matlab.System ...
    & smms.mixin.Parentable
  %% ENVIRONMENT
  
  
  
  %% PUBLIC PROPERTIES
  properties ( Nontunable )
    
    ExternalForce
    
  end
  
  
  
  %% NONTUNABLE INTERNAL PROPERTIES
  properties ( Nontunable , Access = { ?smms.internal.InternalAccess } )
    
    StepSize (1, 1) double
    
  end
  
  
  
  %% CONSTRUCTOR METHODS
  methods
    
    function obj = Environment(varargin)
      %% ENVIRONMENT
      
      
      
      setProperties(obj, nargin, varargin{:});
      
    end
    
  end
  
  
  
  %% CONCRETE IMPLEMENTATION METHODS
  methods ( Access = protected )
    
    function setupImpl(obj, varargin)
      %% SETUPIMPL
      
      
      p = parent(obj);
      obj.StepSize = p.StepSize;
      
      if isempty(obj.ExternalForce)
        obj.ExternalForce = zeros(6, obj.Parent.SMMSTree.NumBody);
      end
      
    end
    
    
    function Fext = stepImpl(obj, t, ~, ~)
      %% STEPIMPL
      %
      % STEPIMPL(OBJ, T, Q, QDOT)
      
      
      Fext = repmat(obj.ExternalForce, 1, 1, numel(t));
      
    end
    
  end
  
end
