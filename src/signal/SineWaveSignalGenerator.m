classdef SineWaveSignalGenerator < SignalGenerator
  %% SINEWAVESIGNALGENERATOR
  
  
  
  %% PUBLIC PROPERTIES
  properties
    
    % Amplitude of signal(s)
    Amplitude (:, 1) double = 1
    
    % Frequency of signal(s)
    Frequency (:, 1) double = 1
    
    % Phase offset of signal(s)
    Phase     (:, 1) double = 0
    
  end
  
  
  
  %% INTERNAL PROPERTIES
  properties ( Nontunable , Access = protected )
    
    AmplitudeInternal
    FrequencyInternal
    PhaseInternal
    
  end
  
  
  
  %% CONSTRUCTOR METHODS
  methods
    
    function obj = SineWaveSignalGenerator(varargin)
      %% SINEWAVESIGNALGENERATOR
      
      
      
      setProperties(obj, nargin, varargin{:});
      
    end
    
  end
  
  
  
  %% CONCRETE IMPLEMENTATION METHODS
  methods ( Access = protected )
    
    function setupImpl(obj)
      %% SETUPIMPL
      
      
      
      na = size(obj.Amplitude, 1);
      nf = size(obj.Frequency, 1);
      np = size(obj.Phase, 1);
      
      m = max([ na , nf , np ]);
      
      obj.AmplitudeInternal = diag(padarray(     obj.Amplitude, m - na, 'post', 'circular'));
      obj.FrequencyInternal = padarray(2 * pi .* obj.Frequency, m - nf, 'post', 'circular');
      obj.PhaseInternal     = padarray(          obj.Phase    , m - np, 'post', 'circular');
      
    end
    
    
    function y = stepImpl(obj, t)
      %% STEPIMPL
      
      
      
      y = obj.AmplitudeInternal * sin(obj.FrequencyInternal .* t + obj.PhaseInternal);
      
    end
    
    
    function s = saveObjectImpl(obj)
      %% SAVEOBJECTIMPL
      
      
      
      s.AmplitudeInternal = obj.AmplitudeInternal;
      s.FrequencyInternal = obj.FrequencyInternal;
      s.PhaseInternal     = obj.PhaseInternal;
      
    end
    
    
    function loadObjectImpl(obj, s, wasInUse)
      %% LOADOBJECTIMPL
      
      
      
      if wasInUse
        obj.AmplitudeInternal = s.AmplitudeInternal;
        obj.FrequencyInternal = s.FrequencyInternal;
        obj.PhaseInternal     = s.PhaseInternal;
        
      end
      
      loadObjectImpl@matlab.System(obj, s, wasInUse);
      
    end
    
  end
  
end
