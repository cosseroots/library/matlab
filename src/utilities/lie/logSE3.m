function wu = logSE3(T, stol)%#codegen
%% LOGSE3 Logarithmic mapping SE(3) -> se(3)
%
% WU = LOGSE3(T) calculates the logarithmic mapping of T in SE(3) to WU in
% se(3).
%
% LOGSE3(T, STOL) use singularity tolerance STOL when calculating angular
% components indicating a zero rotation. For any column of W with ||W|^2 < STOL,
% a Taylor series approximation of the relevant terms is used.
%
% Inputs:
%
%   T                       4x4xN array of transformation matrices in SE(3).
%
%   STOL                    Scalar value for singularity tolerance. All columns
%                           of W with || W ||^2 < STOL are assumed a vanishing
%                           rotation.
%
% Outputs:
%
%   WU                      6xN vector in se(3) composed of [ W ; U ] where W is
%                           the angular component and U is the linear component.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>
%
% See also:
%   EXPSE3 LOGSO3



%% Parse arguments

% LOGSE3(T)
% LOGSE3(T, STOL)
narginchk(1, 2);

% LOGSE3(___)
% WU = LOGSE3(___)
nargoutchk(0, 1);

% LOGSE3(T)
if nargin < 2 || isempty(stol)
  stol = 1e-8;
end



%% Algorithm

% Ensure any number of pages and higher dimensions is squeezed down onto pages
T_ = reshape(T, 4, 4, []);

% Extract rotation and translation components
R = T_(1:3,1:3,:);
t = T_(1:3,4,:);

% Error of rotation
w = logSO3(R, stol);

% Combined error in SE(3)
wu_ = cat(1, w, permute(pagemult(Vinv(w, stol), t), [1, 3, 2]));

% Input was given as pages of TFORMs
if ~ismatrix(T)
  wu = reshape(wu_, [ 6 , size(T, 3:ndims(T)) ]);
  
% Input was not given as pages of TFORMs
else
  wu = wu_;
  
end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged.
