function jy = jaclogSO3(R, stol)
%% JACLOGSO3
%
% Inputs:
%
%   R                       3x3xN array of rotation matrices.
% 
%   STOL                    Scalar tolerance at which to consider LOGSOE(R) to
%                           be the identity rotation.
%
% Outputs:
%
%   JY                      3x3xN array of Jacobian of logarithmic map on SO(3).
%
% See also:
%   LOGSO3 JACLOGSE3
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% JACLOGSO3(R)
% JACLOGSO3(R, STOL)
narginchk(1, 2);

% JACLOGSO3(___)
% JY = JACLOGSO3(___)
nargoutchk(0, 1);

% JACLOGSO3(T)
if nargin < 2
  stol = [];
end



%% Algorithm

warning('COSSEROOTS:DeprecationWarning', 'Call to deprecated function `%s`. Use %s` instead.', funcname(), 'dlogSO3(R, stol)');

jy = dlogSO3(R, stol);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
