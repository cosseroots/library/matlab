function [T, eta, etadot] = SO3xR32SE3(T, v, vdot)%#codegen
%% SO3XR32SE3 Convert quantities in SO(3) x IR(3) to SE(3)
%
% [T, ETA, ETADOT] = SO3XR32SE3(G, V, VDOT)
%
% Inputs:
%
%   T                       4x4xN array of homogeneous transformation matrices
%                           in SE(3).
% 
%   V                       6xN array of [ O , RDOT ] in SO(3) x IR(3).
% 
%   VDOT                    6xN array of [ ODOT , RDDOT ]  in SO(3) x IR(3).
%
% Outputs:
%
%   T                       4x4xN array of homogeneous transformation matrices
%                           in SE(3).
% 
%   ETA                     6xN array of [ O , V ] in SE(3).
% 
%   ETADOT                  6xN array of [ ODOT , VDOT ] in SE(3).
%
% See also:
%   SE3X2SO3R3
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% SO3XR32SE3(T, V, VDOT)
narginchk(3, 3);

% [G, ETA, ETADOT] = SO3XR32SE3(___)
nargoutchk(3, 3);



%% Algorithm

% Number of transformations
n = size(T, 3);

% Obtain rotation matrix and its transpose from homogeneous transformation
R  = tform2rotm(T);
Rt = permute(R, [2, 1, 3]);

% Velocities
O    = v(1:3,:);
rdot = v(4:6,:);

% Accelerations
Odot  = vdot(1:3,:);
rddot = vdot(4:6,:);

% Twists in SE(3)
eta = zeros(6, n);
eta(1:3,:) = O;
eta(4:6,:) = permute( ...
    pagemult( ...
        Rt ...
      , permute( ...
            rdot ...
          , [1, 3, 2] ...
      ) ...
    ) ...
  , [1, 3, 2] ...
);

% Accelerations in SE(3)
etadot = zeros(6, n);
etadot(1:3,:) = Odot;
etadot(4:6,:) = permute( ...
    pagemult( ...
        Rt ...
      , permute( ...
          rddot ...
        , [1, 3, 2] ...
      ) ...
    ) ...
  , [1, 3, 2] ...
) + ...
+ permute( ...
    pagemult( ...
      vec2skew( ...
          eta(4:6,:) ...
        ) ...
      , permute( ...
          O ...
        , [1, 3, 2] ...
      ) ...
    ) ...
  , [1, 3, 2] ...
);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
