function v = skew2vec(s)%#codegen
%% SKEW2VEC Convert skew-symmetric matrix into skew-symmetric vector
%
% VEC = SKEW2VEC(SKEW) converts skew-symmetric matrix S into its skew-symmetric
% vector form such.
%
% Inputs:
%
%   S                   3x3xN matrix of skew-symmetric matrices or 4x4xN matrix
%                       of skew-symmetric twists
%
% Outputs:
%
%   V                   3xN array of skew-symmetric vectors or 6xN array of
%                       skew-symmetric vectors.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Validate arguments

% SKEW2VEC(SKEW)
narginchk(1, 1);

% SKEW2VEC(___)
% V = SKEW2VEC(___)
nargoutchk(0, 1);



%% Process

szv = size(s, 3:max(3,ndims(s)));

% 4x4xN skew-symmetric matrix to vector
if size(s, 1) == 4
  v = reshape(skew2vec_6d(reshape(s, 4, 4, [])), [ 6 , szv ]);

% 3x3xN skew-symmetric matrix to vector
else
  v = reshape(skew2vec_3d(reshape(s, 3, 3, [])), [ 3 , szv ]);
  
end


end


function v = skew2vec_3d(s)
%% SKEW2VEC_3D



% Number of vectors to create
nvec = size(s, 3);

% Mean value to avoid for numerical issues in matrix's non-symmetry
s = ( 0.5 * ones(3, 3, nvec) ) .* ( s - permute(s, [ 2 , 1 , 3 ]) );

% Shift into the right dimension
s = permute(s, [3, 1, 2]);

% Simple concatenation
v = permute(reshape(cat(1, s(:,3,2), s(:,1,3), s(:,2,1)), [ nvec , 3 ]), [ 2 , 1 ]);


end


function v = skew2vec_6d(s)
%% SKEW2VEC_6D



% Number of samples
nv = size(s, 3);

% Initialize output
v = zeros(6, nv);
% Turn sub skew-symmetric matrix into vector
v(1:3,:) = skew2vec_3d(s(1:3,1:3,:));
% And extract the linear part
v(4:6,:) = permute(s(1:3,4,:), [ 1 , 3 , 2 ]);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changes" section of the header.
