function o = twist2ang(eta)%#codegen
%% TWIST2ANG Convert a twist to angular velocities
%
% V = TWIST2ANG(ETA) converts twist ETA to angular velocities O.
%
% Inputs:
%
%   ETA                     6xN array of twists.
%
% Outputs:
%
%   O                       3xN array of angular velocities.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% TWIST2ANG(ETA)
narginchk(1, 1);

% O = TWIST2ANG(___)
nargoutchk(0, 1);



%% Algorithm

% Extract
o = hslice(eta, 1, 1:3);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
