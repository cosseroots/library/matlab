function l = liebrkt(a, b)%#codegen
%% LIEBRKT Lie bracket operation
%
% Inputs:
%
%   A                       6xN vector.
% 
%   B                       6xN vector.
%
% Outputs:
%
%   L                       Lie-Bracket of A with B.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% LIEBRKT(A, B)
narginchk(2, 2);

% LIEBRKT(___)
% L = LIEBRKT(___)
nargoutchk(0, 1);



%% Algorithm

ada = ad(a);
adb = ad(b);

l = ad2y(pagemult(ada, adb) - pagemult(adb, ada));


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
