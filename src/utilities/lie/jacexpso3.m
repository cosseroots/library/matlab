function R = jacexpso3(w, stol)%#codegen
%% JACEXPSO3
%
% R = JACEXPSO3(W)
%
% R = JACEXPSO3(W, STOL)
%
% Inputs:
%
%   W                       3xN array of rotation vectors.
%
%   STOL                    Scalar value for singularity tolerance.
%                           Default: 1e-6
%
% Outputs:
%
%   R                       3x3xN array of rotation matrices.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% JACEXPSO3(W)
% JACEXPSO3(W, STOL)
narginchk(1, 2);

% JACEXPSO3(___)
% A = JACEXPSO3(___)
nargoutchk(0, 1);

% JACEXPSO3(W)
if nargin < 2
  stol = [];
end



%% Algorithm

warning('COSSEROOTS:DeprecationWarning', 'Call to deprecated function `%s`. Use %s` instead.', funcname(), 'dexpso3(w, stol)');

R = dexpso3(w, stol);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
