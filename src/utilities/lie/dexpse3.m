function DT = dexpse3(wu, stol)%#codegen
%% DEXPSE3 Calculate derivative of exponential mapping se(3) -> SE(3)
%
% DT = DEXPSE3(WU) calculates the derivative of the exponential mapping of WU in
% se(3) to DT in SE(3).
%
% DEXPSE3(WU, STOL) use singularity tolerance STOL when calculating angular
% components indicating a zero rotation. For any column of W with ||W|^2 < STOL,
% a Taylor series approximation of the relevant terms is used.
%
% Inputs:
%
%   WU                      6xN vector in se(3) composed of [ W ; U ] where W is
%                           the angular component and U is the linear component.
%
%   STOL                    Scalar value for singularity tolerance. All columns
%                           of WU with || W ||^2 < STOL are assumed a vanishing
%                           rotation.
%
% Outputs:
%
%   DT                      6x6xN Jacobian of EXPSE3(WU) with respect to
%                           generators of WU in SE(3).
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>
%
% See also:
%   DLOGSE3 EXPSE3



%% Parse arguments

% DEXPSE3(WU)
% DEXPSE3(WU, STOL)
narginchk(1, 2);

% DEXPSE3(___)
% DT = DEXPSE3(___)
nargoutchk(0, 1);

% DEXPSE3(WU)
if nargin < 2 || isempty(stol)
  stol = 1e-8;
end



%% Algorithm

nwu = size(wu, 2);

w = wu(1:3,:);
u = wu(4:6,:);

uskew = vec2skew(u);
wskew = vec2skew(w);

[~,b,c] = lieabc(w, stol);

DT = zeros(6, 6, nwu);
DT(1:3,1:3,:) = dexpso3(w, stol);
DT(4:6,1:3,:) = repmat(permute(b, [1, 3, 2]), 3, 3, 1) .* uskew + ...
              + repmat(permute(c, [1, 3, 2]), 3, 3, 1) .* ( pagemult(wskew, uskew) + pagemult(uskew, wskew) ) + ...
              + repmat(permute(dot(w, u, 1), [1,3,2]), 3, 3, 1) .* Q(w, stol);
DT(4:6,4:6,:) = DT(1:3,1:3,:);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged.
