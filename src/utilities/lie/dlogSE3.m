function Dwu = dlogSE3(T, stol)%#codegen
%% DLOGSE3 Derivative of logarithm mapping SE(3) -> se(3)
%
% DWU = DLOGSE3(T) calculates the derivative of the logarithm mapping of T in
% SE(3) to DWU in se(3). Due to the properties of the Lie algebra used, this
% function internally calculates INV(DEXPSE3(LOGSE3(T))).
%
% DLOGSE3(T, STOL) use singularity tolerance STOL when calculating angular
% components indicating a zero rotation. For any column of W with ||W|^2 < STOL,
% a Taylor series approximation of the relevant terms is used.
%
% Inputs:
%
%   T                       4x4xN array of homogeneous transformation matrices
%                           in SE(3).
%
%   STOL                    Scalar value for singularity tolerance. All columns
%                           of WU with || W ||^2 < STOL are assumed a vanishing
%                           rotation.
%
% Outputs:
%
%   DWU                     6xN array of derivatives of LOGSE3(T) wrt the
%                           generators of T in SE(3)
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>
%
% See also:
%   DEXPSE3 LOGSE3 EXPSE3



%% Parse arguments

% DLOGSE3(T)
% DLOGSE3(T, STOL)
narginchk(1, 2);

% DLOGSE3(___)
% DWU = DLOGSE3(___)
nargoutchk(0, 1);

% DLOGSE3(T)
if nargin < 2 || isempty(stol)
  stol = 1e-8;
end



%% Algorithm

nT = size(T, 3);

wu = logSE3(T, stol);
w = wu(1:3,:);
u = wu(4:6,:);

uskew = vec2skew(u);
% wskew = vec2skew(w);

Dlogw = pageinv(dexpso3(w, stol));

[~,b,c] = lieabc(w, stol);

ww = repmat(permute(w, [1, 3, 2]), 1, 3, 1);
uu = repmat(permute(u, [1, 3, 2]), 1, 3, 1);

B = repmat(permute(b, [1,3,2]), 3, 3, 1) .* uskew + ...
  + repmat(permute(c, [1,3,2]), 3, 3, 1) .* ( ww .* permute(uu, [2, 1, 3]) + uu .* permute(ww, [2, 1, 3]) ) + ...
  + repmat(permute(dot(w, u, 1), [1,3,2]), 3, 3, 1) .* W(w, stol);

Dwu = zeros(6,6, nT);
Dwu(1:3,1:3,:) = Dlogw;
Dwu(4:6,1:3,:) = pagemult(-Dlogw, pagemult(B, Dlogw));
Dwu(4:6,4:6,:) = Dlogw;


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged.
