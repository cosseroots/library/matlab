function R = expso3(w, stol)%#codegen
%% EXPSO3 Exponential mapping so(3) -> SO(3)
%
% R = EXPSO3(W) calculates the exponential mapping of W in so(3) to R in SO(3).
%
% EXPSO3(W, STOL) use singularity tolerance STOL when calculating angular
% components indicating a zero rotation. For any column of W with ||W|^2 < STOL,
% a Taylor series approximation of the relevant terms is used.
%
% Inputs:
%
%   W                       3xN array of angular vectors in so(3).
%
%   STOL                    Scalar value for singularity tolerance. All columns
%                           of WU with || W ||^2 < STOL are assumed a vanishing
%                           rotation.
%
% Outputs:
%
%   R                       3x3xN array of rotation matrices in SO(3) associated
%                           with W.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>
%
% See also:
%   LOGSO3



%% Parse arguments

% EXPSO3(W)
% EXPSO3(W, STOL)
narginchk(1, 2);

% R = EXPSO3(___)
nargoutchk(0, 1);

% EXPSO3(W)
if nargin < 2 || isempty(stol)
  stol = [];
end



%% Algorithm

nw = size(w, 2);

[a,b,~] = lieabc(w, stol);

wskew = vec2skew(w);

R = repmat(eye(3, 3), 1, 1, nw) + ...
  + repmat(permute(a, [1, 3, 2]), 3, 3, 1) .* wskew + ...
  + repmat(permute(b, [1, 3, 2]), 3, 3, 1) .* pagemult(wskew, wskew);

end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged.
