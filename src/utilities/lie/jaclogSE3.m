function jy = jaclogSE3(T, stol)
%% JACLOGSE3 Jacobian of LOGSO3
%
% JY = JACLOGSE3(T) calculates the Jacobian of the logarithmic map from SE(3) to
% SO(3).
%
% Inputs:
%
%   T                       4x4xN array of homogeneous transformation matrices.
% 
%   STOL                    Scalar tolerance at which to consider LOGSE3(R(T))
%                           to be the identity rotation.
%
% Outputs:
%
%   JY                      6x6xN array of Jacobian of logarithmic map on SE(3).
%
% See also:
%   JACLOGSO3
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% JACLOGSE3(T)
% JACLOGSE3(T, STOL)
narginchk(1, 2);

% JACLOGSE3(___)
% JY = JACLOGSE3(___)
nargoutchk(0, 1);

% JACLOGSE3(T)
if nargin < 2
  stol = [];
end



%% Algorithm

warning('COSSEROOTS:DeprecationWarning', 'Call to deprecated function `%s`. Use %s` instead.', funcname(), 'dlogSE3(T, stol)');

jy = dlogSE3(T, stol);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
