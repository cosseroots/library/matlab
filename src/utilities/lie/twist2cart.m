function [o, v] = twist2cart(eta)%#codegen
%% TWIST2CART Convert a twist to Cartesian velocities.
%
% [O, V] = TWIST2ANG(ETA) converts twist ETA to angular velocities O and linear
% velocities V.
%
% Inputs:
%
%   ETA                     6xN array of twists.
%
% Outputs:
%
%   O                       3xN array of angular velocities.
%
%   V                       3xN array of linear velocities.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% TWIST2CART(ETA)
narginchk(1, 1);

% [O, V] = TWIST2CART(___)
nargoutchk(2, 2);



%% Algorithm

% Dispatch to the sub functions
o = twist2ang(eta);
v = twist2lin(eta);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
