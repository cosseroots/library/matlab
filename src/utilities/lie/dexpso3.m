function DR = dexpso3(w, stol)%#codegen
%% DEXPSO3 Calculate derivative of exponential mapping so(3) -> SO(3)
%
% DR = DEXPSO3(W) calculates the derivative of the exponential mapping of W in
% so(3) to DR in SO(3).
%
% DEXPSO3(W, STOL) use singularity tolerance STOL when calculating angular
% components indicating a zero rotation. For any column of W with ||W|^2 < STOL,
% a Taylor series approximation of the relevant terms is used.
%
% Inputs:
%
%   W                       3xN array of angular components in so(3).
%
%   STOL                    Scalar value for singularity tolerance. All columns
%                           of WU with || W ||^2 < STOL are assumed a vanishing
%                           rotation.
%
% Outputs:
%
%   DR                      3x3xN array of derivatives of EXPSO(3) with respect
%                           to generators of W in so(3).
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>
%
% See also:
%   DLOGSO3 EXPSO3



%% Parse arguments

% DEXPSO3(W)
% DEXPSO3(W, STOL)
narginchk(1, 2);

% DEXPSO3(___)
% DR = DEXPSO3(___)
nargoutchk(0, 1);

% DEXPSO3
if nargin < 2 || isempty(stol)
  stol = 1e-8;
end



%% Algorithm

nw = size(w, 2);

[a,b,c] = lieabc(w, stol);

wskew = vec2skew(w);

DR = repmat(permute(a, [1, 3, 2]), 3, 3, 1) .* repmat(eye(3, 3), 1, 1, nw) + ...
   + repmat(permute(b, [1, 3, 2]), 3, 3, 1) .* wskew + ...
   + repmat(permute(c, [1, 3, 2]), 3, 3, 1) .* ( repmat(permute(w, [1, 3, 2]), 1, 3, 1) .* repmat(permute(w, [3, 1, 2]), 3, 1, 1) );


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged.
