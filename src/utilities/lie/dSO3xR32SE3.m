function [dT, dV, dVdot] = dSO3xR32SE3(T, V, Vdot, nu, pos2vel, pos2acc)
%% DSO3XR32SE3 Calculate variation on SO(3) x IR(3)
%
% [DT, DV, DVDOT] = DSO3XR32SE3(T, V, VDOT, NU, POS2VEL, POS2ACC)
%
% Inputs:
%
%   T                       4x4xN array of homogeneous transformations.
% 
%   V                       6xN array of twists.
% 
%   VDOT                    6xN array of accelerations.
% 
%   NU                      6xN array of variations in [ SO(3) , R(3) ].
%
%   POS2VEL                 Scalar scaling factor to convert position update NU
%                           to velocity update DNU;
%
%   POS2ACC                 Scalar scaling factor to convert position update NU
%                           to acceleration update DDNU.
%
% Outputs:
%
%   DT                      6x6xN array of Jacobians of configuration
%                           transformations from SO(3)xIR(3) to SE(3).
% 
%   DV                      6x6xN array of Jacobians of twist transformations
%                           from SO(3)xIR(3) to SE(3).
% 
%   DVDOT                   6x6xN array of Jacobians of acceleration
%                           transformations from SO(3)xIR(3) to SE(3).
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% DSO3XR32SE3(T, V, VDOT, NU, POS2VEL, POS2ACC)
narginchk(6, 6);

% [DT, DV, DVDOT] = DSO3XR32SE3(___)
nargoutchk(3, 3);

% DSO3XR32SE3(G, ___)
if size(T, 1) == 7
  T = tpar2tform(T);
end



%% Algorithm

% Count states
nn = size(T, 3);

% Pre-allocated variables
e_3_3 = eye(3, 3);
e_3_3_nn = repmat(e_3_3, 1, 1, nn);

% Get rotation matrix and its transpose
R  = tform2rotm(T);
Rt = permute(R, [2, 1, 3]);

% Extract angular and linear velocities and accelerations
[O, V] = twist2cart(V);
[~, Vdot] = twist2cart(Vdot);

% Pre-calculate some skew-symmetric matrices
O_skew = vec2skew(O);
V_skew = vec2skew(V);
A_skew = vec2skew(Vdot + permute(pagemult(O_skew, permute(V, [1, 3, 2])), [1, 3, 2]));

% Get rotation vector
Th  = nu([1,2,3],:);
% And its associated rotation matrix
TTh = dexpso3(Th);

% Jacobian of position mapping
dT = zeros(6, 6, nn);
dT([1,2,3],[1,2,3],:) = TTh;
dT([4,5,6],[4,5,6],:) = Rt;

% Jacobian of velocity mapping
dV = zeros(6, 6, nn);
dV([1,2,3],[1,2,3],:) = pos2vel * e_3_3_nn;
dV([4,5,6],[1,2,3],:) = pagemult(V_skew, TTh);
dV([4,5,6],[4,5,6],:) = pos2vel * Rt;

% Jacobian of acceleration mapping
dVdot = zeros(6, 6, nn);
dVdot([1,2,3],[1,2,3],:) = pos2acc * e_3_3_nn;
dVdot([4,5,6],[1,2,3],:) = ( ...
  pagemult(A_skew - pagemult(O_skew, V_skew), TTh) + ...
  + pos2vel * V_skew ...
);
dVdot([4,5,6],[4,5,6],:) = pagemult(pos2acc * e_3_3_nn - pos2vel * O_skew, Rt);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
