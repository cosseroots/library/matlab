function Dw = dlogSO3(R, stol)%#codegen
%% DLOGSO3 Derivative of logarithm mapping SO(3) -> so(3)
%
% DW = DLOGSO3(R) calculates the derivative of the logarithm mapping of R in
% SO(3) to DW in so(3). Due to the properties of the Lie algebra used, this
% function internally calculates INV(DEXPSO3(LOGSO3(R))).
%
% DLOGSO3(R, STOL) use singularity tolerance STOL when calculating angular
% components indicating a zero rotation. For any column of W with ||W|^2 < STOL,
% a Taylor series approximation of the relevant terms is used.
%
% Inputs:
%
%   R                       Description of argument R
%
%   STOL                    Scalar value for singularity tolerance. All columns
%                           of WU with || W ||^2 < STOL are assumed a vanishing
%                           rotation.
%
% Outputs:
%
%   DW                      3x3xN array of derivatives of LOGSO3(R) with respect
%                           to the generators of R in SO(3)
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>
%
% See also:
%   DEXPSO3 LOGSO3



%% Parse arguments

% DLOGSO3(W)
% DLOGSO3(W, STOL)
narginchk(1, 2);

% DLOGSO3(__)
% DR = DLOGSO3(__)
nargoutchk(0, 1);

% DLOGSO3(W)
if nargin < 2 || isempty(stol)
  stol = 1e-8;
end



%% Algorithm

nR = size(R, 3);

w = logSO3(R, stol);

[a,b,c] = lieabc(w, stol);

wskew = vec2skew(w);

c1 = repmat(0.5, 1, nR);
c2 = ( b - repmat(2, 1, nR) .* c ) ./ ( repmat(2, 1, nR) .* a );

Dw = repmat(eye(3, 3), 1, 1, nR) + ...
   - repmat(permute(c1, [1, 3, 2]), 3, 3, 1) .* wskew + ...
   + repmat(permute(c2, [1, 3, 2]), 3, 3, 1) .* pagemult(wskew, wskew);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged.
