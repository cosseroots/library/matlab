function y = ad2y(X)%#codegen
%% AD2Y Recover spatial twist vector from adjoint matrix
%
% Y = AD2Y(X) recovers spatial twist vector Y from adjoint matrix X.
%
% Inputs:
%
%   X                       6x6xN array of adjoint twist matrices.
%
% Outputs:
%
%   Y                       6xN array of twist vectors.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% AD2Y(X)
narginchk(1, 1);

% AD2Y(___)
% Y = AD2Y(___)
nargoutchk(0, 1);



%% Algorithm

y = cat( ...
  1 ...
  , skew2vec(X(1:3,1:3,:)) ...
  , skew2vec(X(4:6,1:3,:)) ...
);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
