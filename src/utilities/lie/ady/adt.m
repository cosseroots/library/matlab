function m = adt(Y)%#codegen
%% ADT "Anti"-matrix to transpose adjoint of 6-D vector
% 
% This function comes in handy when calculating `ad(Y)^T * V` in case Y is a
% matrix. Then, we can rewrite this as `-adt(V)^T * Y` yielding the same result.
%
% M = ADT(Y) calculates the anti-matrix of the transpose adjoint matrix M of
% vector Y.
%
% Inputs:
%
%   Y                       6xN  vector with columns [ W ; U ].
%
% Outputs:
%
%   ADAT                    6x6xN matrix of adjoint matrix
%                             | skew(W) , skew(U) |
%                             | skew(U) ,      0  |
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% ADT(Y)
narginchk(1, 1);

% ADT(___)
% ADA = ADT(___)
nargoutchk(0, 1);

% Count number of variables
n = size(Y, 2);



%% Algorithm

W = Y(1:3,:);
U = Y(4:6,:);

Uskew = vec2skew(U);

m = zeros(6, 6, n);
m(1:3,1:3,:) = vec2skew(W);
m(1:3,4:6,:) = Uskew;
m(4:6,1:3,:) = Uskew;


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
