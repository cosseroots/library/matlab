function X = ad(y)%#codegen
%% AD adjoint matrix of a 6-D vector.
%
% X = AD(Y) calculates adjoint matrix AD of vector Y.
%
% Inputs:
%
%   Y                       6xN vector with columns [ W ; U ].
%
% Outputs:
%
%   X                       6x6xN matrix of adjoint operation:
%                             | skew(W) ,      0  |
%                             | skew(U) , skew(W) |
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% AD(Y)
narginchk(1, 1);

% AD(___)
% X = AD(___)
nargoutchk(0, 1);



%% Algorithm

% Store original size of Y for later reshaping
szy = size(y, 2:max(2, ndims(y)));

% Turn Y into a 6xN array
y = reshape(y, 6, []);

% Count number of variables
n = size(y, 2);

W = y(1:3,:);
U = y(4:6,:);

x = zeros(6, 6, n);

x(1:3,1:3,:) = vec2skew(W);
x(4:6,1:3,:) = vec2skew(U);
x(4:6,4:6,:) = x(1:3,1:3,:);

% Reshape into original dimensions
X = reshape(x, [ 6 , 6 , szy ]);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
