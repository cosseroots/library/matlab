function w = twistzeros(n)%#codegen
%% TWISTZEROS
%
% Inputs:
% 
%   N                       Description of argument N
%
% Outputs:
%
%   W                       Description of argument W
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% TWISTZEROS()
% TWISTZEROS(N)
narginchk(0, 1);

% TWISTZEROS(___)
% W = TWISTZEROS(___)
nargoutchk(0, 1);

% TWISTZEROS()
if nargin < 1 || isempty(n)
  n = 1;
end



%% Algorithm

w = zeros(6, fix(abs(n)));


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
