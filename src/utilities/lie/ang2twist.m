function eta = ang2twist(o)%#codegen
%% ANG2TWIST Convert angular velocities to spatial twist
%
% ETA = ANG2TWIST(O) converts angular velocities O to spatial twist ETA.
%
% Inputs:
%
%   O                       3xN array of angular velocities.
%
% Outputs:
%
%   ETA                     6xN array of twists.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% ANG2TWIST(O)
narginchk(1, 1);

% ETA = ANG2TWIST(__)
nargoutchk(0, 1);



%% Algorithm

% Dispatch
eta = cart2twist(o, 0 .* o);



end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
