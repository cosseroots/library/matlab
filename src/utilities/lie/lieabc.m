function [a, b, c] = lieabc(w, stol)%#codegen
%% LIEABC Calculate common coefficients of Lie algebra mappings
%
% [A, B, C] = LIEABC(W) calculates coefficients A, B, and C given the array W in
% so(3) with the formulas:
%   X = || W ||
%   A =       SIN(X)   / X
%   B = ( 1 - COS(X) ) / X^2
%   C = ( 1 - A      ) / X^2
%
% LIEABC(W, STOL) use singularity tolerance STOL when calculating angular
% components indicating a zero rotation. For any column of W with ||W|^2 < STOL,
% a Taylor series approximation of the relevant terms is used.
%
% Inputs:
%
%   W                       3xN array of angular vectors in so(3).
%
%   STOL                    Scalar value for singularity tolerance. All columns
%                           of W with || W ||^2 < STOL are assumed a vanishing
%                           rotation.
%
% Outputs:
%
%   A                       1xN array of SIN(X) / X.
% 
%   B                       1xN array of (1 - COS(X)) / X^2.
% 
%   C                       1xN array of (1 - A) / X^2.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% LIEABC(W)
% LIEABC(W, STOL)
narginchk(1, 2);

% [A, B, C] = LIEABC(___)
nargoutchk(3, 3);

% LIEABC(W)
if nargin < 2 || isempty(stol)
  stol = 1e-8;
end



%% Algorithm

nw = size(w, 2);

theta2 = sum(w .^ 2, 1);
theta  = sqrt(theta2);

% Indices where theta is very small
indzero  = isclose(theta2, 0, stol);
nindzero = sum(indzero);

a = sin(theta) ./ theta;
b = ( ones(1, nw) - cos(theta) ) ./ theta2;
c = ( ones(1, nw) - a ) ./ theta2;

% Taylor series for singular values
if nindzero > 0
  % Select singular values of theta
  theta2zero = theta(indzero);
  
  % Powers of theta^2 from theta^0 to theta^6 in steps of 2
  theta2zeropwr = ( theta2zero .^ [0;1;2;3] );
  
  % Singular values of A, B, and C
  a(indzero) = [ +1 / 1 , -1 /   6 , +1 /  120 , -1 /   5040 ] * theta2zeropwr;
  b(indzero) = [ +1 / 2 , -1 /  24 , +1 /  720 , -1 /  40230 ] * theta2zeropwr;
  c(indzero) = [ +1 / 6 , -1 / 120 , +1 / 5040 , -1 / 362880 ] * theta2zeropwr;
  
end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged.
