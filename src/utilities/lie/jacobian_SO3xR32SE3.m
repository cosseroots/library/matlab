function [dT, dV, dVdot] = jacobian_SO3xR32SE3(T, V, Vdot, nu, pos2vel, pos2acc)
%% JACOBIAN_SO3XR32SE3 Calculate variation on SO(3) x IR(3)
%
% [DT, DV, DVDOT] = JACOBIAN_SO3XR32SE3(T, V, VDOT, NU, POS2VEL, POS2ACC)
%
% Inputs:
%
%   T                       7xN array of homogeneous transformations.
% 
%   ETA                     6xN array of twists.
% 
%   ETADOT                  6xN array of accelerations.
% 
%   NU                      6xN array of variations in [ SO(3) , R(3) ].
%
%   POS2VEL                 Scalar scaling factor to convert position update NU
%                           to velocity update DNU;
%
%   POS2ACC                 Scalar scaling factor to convert position update NU
%                           to acceleration update DDNU.
%
% Outputs:
%
%   VG                      6x6xN array of Jacobians of configuration
%                           transformations from SO(3)xIR(3) to SE(3).
% 
%   VETA                    6x6xN array of Jacobians of twist transformations
%                           from SO(3)xIR(3) to SE(3).
% 
%   VETADOT                 6x6xN array of Jacobians of acceleration
%                           transformations from SO(3)xIR(3) to SE(3).
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% JACOBIAN_SO3XR32SE3(T, V, VDOT, NU, POS2VEL, POS2ACC)
narginchk(6, 6);

% [DT, DV, DVDOT] = JACOBIAN_SO3XR32SE3(___)
nargoutchk(3, 3);



%% Algorithm

warning('COSSEROOTS:DeprecationWarning', 'Call to deprecated function `%s`. Use %s` instead.', funcname(), 'dSO3xR32SE3(T, V, VDOT, NU, POS2VEL, POS2ACC)');

[dT, dV, dVdot] = dSO3xR32SE3(T, V, Vdot, nu, pos2vel, pos2acc);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
