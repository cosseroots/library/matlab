function [T, v, vdot] = SE32SO3xR3(T, eta, etadot)%#codegen
%% SE32SO3XR3 Convert quantities in SE(3) into SO(3) x IR(3)
%
% [G, V, VDOT] = SE32SO3XR3(G, ETA, ETADOT)
%
% Inputs:
%
%   T                       4x4xN array of homogeneous transformation matrices
%                           in SE(3).
% 
%   ETA                     6xN array of [ O , V ] in SE(3).
% 
%   ETADOT                  6xN array of [ ODOT , VDOT ] in SE(3).
%
% Outputs:
%
%   T                       4x4xN array of homogeneous transformation matrices
%                           in SE(3).
% 
%   V                       6xN array of [ O , RDOT ] in SO(3) x IR(3).
% 
%   VDOT                    6xN array of [ ODOT , RDDOT ]  in SO(3) x IR(3).
%
% See also:
%   SO3XR32SE3
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% SE32SO3XR3(T, ETA, ETADOT)
narginchk(3, 3);

% [T, V, VDOT] = SE32SO3XR3(___)
nargoutchk(3, 3);



%% Algorithm

% Number of transformations
n = size(T, 3);

% Obtain rotation matrix from homogeneous transformation
R = tform2rotm(T);

% Velocities
O = eta(1:3,:);
V = eta(4:6,:);

% Accelerations
Odot = etadot(1:3,:);
Vdot = etadot(4:6,:);

% Twists in SO(3) x IR(3)
v = zeros(6, n);
v(1:3,:) = O;
v(4:6,:) = permute( ...
    pagemult( ...
        R ...
      , permute( ...
          V ...
        , [1, 3, 2] ...
      ) ...
    ) ...
  , [1, 3, 2] ...
);

% Accelerations in SO(3) x IR(3)
vdot = zeros(6, n);
vdot(1:3,:) = Odot;
vdot(4:6,:) = permute( ...
    pagemult( ...
        R ...
      , permute( ...
          Vdot ...
        , [1, 3, 2] ...
      ) + ...
      + pagemult( ...
          vec2skew(O) ...
        , permute( ...
            V ...
          , [1, 3, 2] ...
        ) ...
      ) ...
    ) ...
  , [1, 3, 2] ...
);



end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
