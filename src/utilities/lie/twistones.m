function w = twistones(n)%#codegen
%% TWISTONES Create a unit twist i.e., a twist of all ones
%
% W = TWISTONES() creates a 6D unit twist i.e., a 6x1 vector of all ones.
%
% W = TWISTONESS(N) creates a N 6x1 unit twist vectors i.e., a 6xN unit twist
% array.
%
% Inputs:
% 
%   N                       Number of twists to create.
%
% Outputs:
%
%   W                       6xN array of unit twists.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% TWISTONES()
% TWISTONES(N)
narginchk(0, 1);

% TWISTONES(___)
% W = TWISTONES(___)
nargoutchk(0, 1);

% TWISTONES()
if nargin < 1 || isempty(n)
  n = 1;
end



%% Algorithm

w = ones(6, fix(abs(n)));


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
