function y = jacexpse3(w, u, stol)%#codegen
%% JACEXPSE3
%
% M = JACEXPSE3(W, U)
%
% M = JACEXPSE3(W, U, STOL)
%
% Inputs:
%
%   W                       3xN array of rotation vectors.
%
%   U                       3xN array of displacement vectors.
%
%   STOL                    Scalar value for singularity tolerance.
%                           Default: 1e-6
%
% Outputs:
%
%   M                       6xN array of tangent on SE(3) associated with V and
%                           D.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% JACEXPSE3(W, U)
% JACEXPSE3(W, U, STOL)
narginchk(2, 3);

% JACEXPSE3(___)
% A = JACEXPSE3(___)
nargoutchk(0, 1);

% JACEXPSE3(W, U)
if nargin < 3
  stol = [];
end



%% Algorithm

warning('COSSEROOTS:DeprecationWarning', 'Call to deprecated function `%s`. Use %s` instead.', funcname(), 'dexpse3([ w ; u ], stol)');

y = dexpse3(cat(1, w, u), stol);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
