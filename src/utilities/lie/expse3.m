function T = expse3(wu, stol)%#codegen
%% EXPSE3 Exponential mapping se(3) -> SE(3)
%
% T = EXPSE3(WU) calculates the exponential mapping of WU in se(3) to T in
% SE(3).
%
% EXPSE3(WU, STOL) use singularity tolerance STOL when calculating angular
% components indicating a zero rotation. For any column of W with ||W|^2 <
% STOL, a Taylor series approximation of the relevant terms is used.
%
% Inputs:
%
%   WU                      6xN vector in se(3) composed of [ W ; U ] where W is
%                           the angular component and U is the linear component.
%
%   STOL                    Scalar value for singularity tolerance. All columns
%                           of WU with || W ||^2 < STOL are assumed a vanishing
%                           rotation.
%
% Outputs:
%
%   T                       4x4xN array of transformation matrices in SE(3).
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>
%
% See also:
%   LOGSE3 LOGSO3



%% Parse arguments

% EXPSE3(WU)
% EXPSE3(WU, STOL)
narginchk(1, 2);

% T = EXPSE3(___)
nargoutchk(0, 1);

% EXPSE3(W)
if nargin < 2 || isempty(stol)
  stol = 1e-8;
end



%% Algorithm

nwu = size(wu, 2);

w = wu(1:3,:);
u = wu(4:6,:);

T = repmat(eye(4, 4), 1, 1, nwu);
T(1:3,1:3,:) = expso3(w, stol);
T(1:3,4,:)   = pagemult(V(w, stol), permute(u, [1, 3, 2]));
T(4,4,:,:)   = ones(1, 1, nwu);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged.
