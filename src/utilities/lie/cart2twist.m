function eta = cart2twist(o, v)%#codegen
%% CART2TWIST Convert Cartesian velocities to spatial twist
%
% ETA = CART2TWIST(O, V) converts angular velocities O and linear velocities V to
% spatial twist ETA.
%
% Inputs:
%
%   O                       3xN array of angular velocities.
%
%   V                       3xN array of linear velocities.
%
% Outputs:
%
%   ETA                     6xN array of spatial twists.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% CART2TWIST(O, V)
narginchk(2, 2);

% ETA = CART2TWIST(__)
nargoutchk(0, 1);



%% Algorithm

% Dispatch
eta = cat(1, o, v);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
