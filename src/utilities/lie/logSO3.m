function w = logSO3(R, stol)%#codegen
%% LOGSO3Logarithmic mapping SO(3) -> so(3)
%
% W = LOGSO3(R) calculates the logarithmic map of R in SO(3) to W in so(3).
%
% LOGSO3(R, STOL) use singularity tolerance STOL when calculating angular
% components indicating a zero rotation. For any column of W with ||W|^2 < STOL,
% a Taylor series approximation of the relevant terms is used.
%
% Inputs:
%
%   R                       3x3xN array of rotation matrices in SO(3).
%
%   STOL                    Scalar value for singularity tolerance. All columns
%                           of WU with || W ||^2 < STOL are assumed a vanishing
%                           rotation.
%
% Outputs:
%
%   W                       3xN array of angular vectors in so(3).
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>
%
% See also:
%   EXPSO3



%% Parse arguments

% LOGSO3(R)
% LOGSO3(R, 2)
narginchk(1, 2);

% LOGSO3(___)
% W = LOGSO3(___)
nargoutchk(0, 1);

% LOGSO3(R)
if nargin < 2 || isempty(stol)
  stol = 1e-8;
end



%% Algorithm

nR = size(R, 3);

trR = permute(R(1,1,:) + R(2,2,:) + R(3,3,:), [1, 3, 2]);

theta     = acos(limit(( trR - ones(1, nR) ) ./ repmat(2, 1, nR), -1, 1));
indzero   = isclose(theta, 0, stol);
indrot0   = isclose(abs(trR - 3), 0, stol);
indrotpi  = isclose(abs(trR + 1), 0, stol);
nindzero  = sum(indzero);
nindrot0  = sum(indrot0);
nindrotpi = sum(indrotpi);

c = theta ./ ( repmat(2, 1, nR) .* sin(theta) );

% Handle singular cases where THETA is close to zero
if nindzero > 0
  c(indzero) = [ +1 / 2 , +1 / 12 , +7 / 720 , +31 / 30240 ] * ( theta(indzero) .^ [0;1;2;3] );
  
end

% Calculate all rotation vectors
w = skew2vec(repmat(permute(c, [1, 3, 2]), 3, 3, 1) .* ( R - permute(R, [2, 1, 3]) ));

% Handle cases where rotation is close to 0 or +/- 2pi
if nindrot0 > 0
  w(:,indrot0) = zeros(3, nindrot0);
end

% Handle cases where THETA is close to +/- pi or +/- 3pi
if nindrotpi > 0
  I = repmat(eye(3, 3), 1, 1, nR);
  [mx, k] = max(permute(cat(1, R(1,1,:), R(2,2,:), R(3,3,:)), [1, 3, 2]), [], 1);
  w(:,indrotpi) = pi * ( R(:,k(indrotpi),indrotpi) + I(:,k(indrotpi),indrotpi) ) ./ sqrt(2 .* ( 1 + mx(:,indrotpi) ) );
  
end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged.
