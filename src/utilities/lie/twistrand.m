function w = twistrand(n)%#codegen
%% TWISTRAND Uniformly distributed random twists.
%
% W = TWISTRANDN() creates 1 uniformly distributed random twist on the interval
% [-1 , +1].
%
% W = TWISTRANDN(N) creates N uniformly distributed random twists.
%
% Inputs:
% 
%   N                       Scalar number of how many twists to create.
%
% Outputs:
%
%   W                       6xN array of uniformly distributed random twists.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% TWISTRAND()
% TWISTRAND(N)
narginchk(0, 1);

% TWISTRAND(___)
% W = TWISTRAND(___)
nargoutchk(0, 1);

% TWISTRAND()
if nargin < 1 || isempty(n)
  n = 1;
end



%% Algorithm

w = 2 * ( rand(6, fix(abs(n))) - 0.5 );


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
