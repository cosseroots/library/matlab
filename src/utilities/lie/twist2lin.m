function v = twist2lin(eta)%#codegen
%% TWIST2LIN Convert a twist to linear velocities
%
% V = TWIST2LIN(ETA) converts twist ETA to linear velocities V.
%
% Inputs:
%
%   ETA                     6xN array of twists.
%
% Outputs:
%
%   V                       3xN array of linear velocities.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% TWIST2LIN(ETA)
narginchk(1, 1);

% V = TWIST2LIN(___)
nargoutchk(0, 1);



%% Algorithm

% Extract
v = hslice(eta, 1, 4:6);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
