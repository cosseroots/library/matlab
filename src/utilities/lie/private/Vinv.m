function Vinvw = Vinv(w, stol)%#codegen
%% VINV Calculate inverse of V used in LOGSE3
%
% VINVW = VINV(W) calculates inverse VINVW of matrix V(W) of W in so(3) as used
% in LOGSE3.
%
% VINV(W, STOL) use singularity tolerance STOL when calculating angular
% components indicating a zero rotation. For any column of W with ||W|^2 < STOL,
% a Taylor series approximation of the relevant terms is used.
%
% Inputs:
%
%   W                       3xN array of angular vectors in so(3).
%
%   STOL                    Scalar value for singularity tolerance. All columns
%                           of WU with || W ||^2 < STOL are assumed a vanishing
%                           rotation.
%
% Outputs:
%
%   VINVW                   6x6xN matrix V^{-1}(w).
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>
%
% See also:
%   LOGSE3



%% Parse arguments

% VINV(W)
% VINV(W, STOL)
narginchk(1, 2);

% VINV(___)
% W = VINV(___)
nargoutchk(0, 1);

% VINV(W)
if nargin < 2 || isempty(stol)
  stol = 1e-8;
end



%% Algorithm

nw = size(w, 2);

[a,b,~] = lieabc(w, stol);

wskew    = vec2skew(w);
theta2   = sum(w .^ 2, 1);
indzero  = isclose(theta2, 0, stol);
nindzero = sum(indzero);

c1 = repmat(0.5, 1, nw);
c2 = ones(1, nw) ./ theta2 .* ( ones(1, nw) - a ./ ( repmat(2, 1, nw) .* b ) );

% Taylor series for singular values
if nindzero > 0
  c2(:,indzero) = [ +1 / 12 , +1 / 720 , +1 / 30240 , +1 / 1209600 ] * ( theta2(indzero) .^ [0;1;2;3] );
  
end

Vinvw = repmat(eye(3, 3), 1, 1, nw) + ...
      - repmat(permute(c1, [1, 3, 2]), 3, 3, 1) .* wskew + ...
      + repmat(permute(c2, [1, 3, 2]), 3, 3, 1) .* pagemult(wskew, wskew);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged.
