function Ww = W(w, stol)%#codegen
%% W Calculate matrix W used in DLOGSE3
%
% WW = W(W) calculates matrix WW of W in so(3) used in DLOGSE3.
%
% W(W, STOL) use singularity tolerance STOL when calculating angular
% components indicating a zero rotation. For any column of W with ||W|^2 < STOL,
% a Taylor series approximation of the relevant terms is used.
%
% Inputs:
%
%   W                       3xN array of angular vectors in so(3).
%
%   STOL                    Scalar value for singularity tolerance. All columns
%                           of WU with || W ||^2 < STOL are assumed a vanishing
%                           rotation.
%
% Outputs:
%
%   WW                      6x6xN matrix W(W)
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>
%
% See also:
%   DLOGSE3



%% Parse arguments

% W(W)
% W(W, STOL)
narginchk(1, 2);

% W(___)
% WW = W(___)
nargoutchk(0, 1);

% W(W)
if nargin < 2 || isempty(stol)
  stol = 1e-8;
end



%% Algorithm

nw = size(w, 2);

theta2 = sum(w .^ 2, 1);

indzero  = isclose(theta2, 0, stol);
nindzero = sum(indzero);

[a,b,c] = lieabc(w, stol);

wskew = vec2skew(w);

c1 = c - b;
c2 = ( a - repmat(2, 1, nw) .* b ) ./ theta2;
c3 = ( b - repmat(3, 1, nw) .* c ) ./ theta2;

% Taylor series for singular values
if nindzero > 0
  % Pre-calculate powers of theta^2
  theta2zeropwr = ( theta2(indzero) .^ [0;1;2;3] );
  
  c2(:,indzero) = [ -1 / 12 , +1 /  180 , -1 /  6720 , +1 /  453600 ] * theta2zeropwr;
  c3(:,indzero) = [ -1 / 60 , +1 / 1260 , -1 / 60480 , +1 / 4989600 ] * theta2zeropwr;
  
end

Ww = repmat(permute(c1, [1, 3, 2]), 3, 3, 1) .* repmat(eye(3, 3), 1, 1, nw) + ...
   + repmat(permute(c2, [1, 3, 2]), 3, 3, 1) .* wskew + ...
   + repmat(permute(c3, [1, 3, 2]), 3, 3, 1) .* ( repmat(permute(w, [1, 3, 2]), 1, 3, 1) .* repmat(permute(w, [3, 1, 2]), 3, 1, 1) );


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged.
