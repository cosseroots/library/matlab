function Vw = V(w, stol)%#codegen
%% V Calculate matrix V used in EXPSE3
%
% VW = V(W) calculates the matrix VW of W in so(3) used in EXPSE3.
%
% V(W, STOL) use singularity tolerance STOL when calculating angular
% components indicating a zero rotation. For any column of W with ||W|^2 < STOL,
% a Taylor series approximation of the relevant terms is used.
%
% Inputs:
%
%   W                       3xN array of angular vectors in so(3).
%
%   STOL                    Scalar value for singularity tolerance. All columns
%                           of WU with || W ||^2 < STOL are assumed a vanishing
%                           rotation.
%
% Outputs:
%
%   VW                      6x6xN matrix V(w).
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>
%
% See also:
%   EXPSE3



%% Parse arguments

% V(W)
% V(W, STOL)
narginchk(1, 2);

% W = V(___)
nargoutchk(0, 1);

% V(W)
if nargin < 2 || isempty(stol)
  stol = 1e-8;
end



%% Algorithm

nw = size(w, 2);

[~,b,c] = lieabc(w, stol);

wskew = vec2skew(w);

Vw = repmat(eye(3, 3), 1, 1, nw) + ...
   + repmat(permute(b, [1, 3, 2]), 3, 3, 1) .* wskew + ...
   + repmat(permute(c, [1, 3, 2]), 3, 3, 1) .* pagemult(wskew, wskew);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged.
