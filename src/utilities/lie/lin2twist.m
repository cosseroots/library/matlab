function eta = lin2twist(v)%#codegen
%% LIN2TWIST Convert linear velocities to spatial twist
%
% ETA = LIN2TWIST(V) converts linear velocities V to spatial twist ETA.
%
% Inputs:
%
%   V                       3xN array of linear velocities.
%
% Outputs:
%
%   ETA                     6xN array of twists.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% LIN2TWIST(V)
narginchk(1, 1);

% ETA = LIN2TWIST(__)
nargoutchk(0, 1);



%% Algorithm

% Dispatch
eta = cart2twist(0 .* v, v);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
