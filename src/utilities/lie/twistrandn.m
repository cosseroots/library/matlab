function w = twistrandn(n)%#codegen
%% TWISTRANDN Normally distributed random twists.
%
% W = TWISTRANDN() creates 1 normally distributed random twist on the interval
% [-1 , +1].
%
% W = TWISTRANDN(N) creates N normally distributed random twists.
%
% Inputs:
% 
%   N                       Scalar number of how many twists to create.
%
% Outputs:
%
%   W                       6xN array of normally distributed random twists.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% TWISTRANDN()
% TWISTRANDN(N)
narginchk(0, 1);

% TWISTRANDN(___)
% W = TWISTRANDN(___)
nargoutchk(0, 1);

% TWISTRANDN()
if nargin < 1 || isempty(n)
  n = 1;
end



%% Algorithm

w = 2 * ( randn(6, fix(abs(n))) - 0.5 );


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
