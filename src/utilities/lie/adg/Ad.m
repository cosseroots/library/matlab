function X = Ad(T)%#codegen
%% AD Calculate adjoint transformation matrix
%
% X = AD(T) calculates adjoint transformation matrix M from homogeneous
% transformation matrix T.
%
% X = AD(G) calculates adjoint transformation matrix M from homogeneous
% transformation parameters G.
%
% Inputs:
%
%   T                       4x4xNx...xK homogeneous transformation matrices as
%                             | R , P |
%                             | 0 , 1 |
%
%   G                       7xNx...xK array of homogeneous transformation
%                           parameters as
%                             [ Q ; P ]
%
% Outputs:
%
%   X                       6x6xNx...xK array of adjoint transformation matrices
%                             | R           , 0 |
%                             | skew(P) * R , R |
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% AD(G)
narginchk(1, 1);

% AD(___)
% X = AD(___)
nargoutchk(0, 1);



%% Algorithm

% Convert 7xN homogeneous transformation parameters into their homogeneous
% transformation matrices
if size(T, 1) == 7
  T = tpar2tform(T);
end

% Store original size of T for later reshaping
szT = size(T, 3:max(3, ndims(T)));

% Move additional dimension into third dimension / pages
T = reshape(T, 4, 4, []);

% Count number of transformations
n = size(T, 3);

% Extract rotations and translations from homogenous transformation parameters
[R, p] = tform2cart(T);

% Initialize adjoint matrix
x = zeros(6, 6, n);

% Fill content
x(1:3,1:3,:) = R;
x(4:6,1:3,:) = pagemult(vec2skew(p), R);
x(4:6,4:6,:) = R;

% Reshape into original dimensions
X = reshape(x, [ 6 , 6 , szT ]);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
