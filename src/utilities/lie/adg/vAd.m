function vad = vAd(T, vT)%#codegen
%% VAD Variation of AD(T)
%
% This function calculates v(Ad(T)) = Ad(T) * ad(vT).
%
% M = VAD(T, VT) calculates the variation M of the adjoint AD(T) of the
% homogeneous transformation matrix T given its variation VT.
%
% Inputs:
%
%   T                       4x4xN array of homogeneous transformation matrices.
% 
%   VT                      6xN array of variations of the homogeneous
%                           transformations.
%
% Outputs:
%
%   VAD                     6x6xN array of variations of homogeneous
%                           transformations.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% VAD(T, VT)
narginchk(2, 2);

% VAD(___)
nargoutchk(0, 1);



%% Algorithm

vad = pagemult(Ad(T), ad(vT));


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
