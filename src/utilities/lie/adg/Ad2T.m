function T = Ad2T(X)%#codegen
%% AD2T Inverse adjoint transformation operator
%
% T = AD2T(X) recovers homogeneous transformation matrix T from adjoint
% transformation matrix X.
%
% Inputs:
%
%   X                       6x6xN array of adjoint transformation matrices.
%
% Outputs:
%
%   T                       4x4xN array of homogeneous transformation matrices.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% AD2T(X)
narginchk(1, 1);

% AD2T(___)
% T = AD2T(___)
nargoutchk(0, 1);



%% Algorithm

R = X(1:3,1:3,:);

% Dispatch
T = tform( ...
    R ...
  , skew2vec(pagemult(X(4:6,1:3,:), permute(R, [2, 1, 3]))) ...
);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
