function kv = makeKnotvector(degree, num_ctrlpts, unclamped)%#codegen
%% MAKE an equally spaced knot vector
%
% MAKE(DEGREE, NUMCTRLPTS) makes a knot vector for a DEGREE-th order polynomial
% with NUMCTRLPTS control points.
%
% MAKE(DEGREE, NUMCTRLPTS, UNCLAMPED) allows to flag whether the knot vector
% represents a clamped or unclamped curve.
%
% Inputs:
%
%   DEGREE              Local polynomial degree.
%
%   NUM_CTRLPS          Number of control points of curve.
%
%   UNCLAMPED           Flag to choose from clamped or unclamped knot vector
%                       options. Defaults to `false` i.e., clamped knot vector.
%                       Possible options are `true` or `false`
%
% Outputs:
%
%   KV                  Resulting, equally spaced knot vector.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse and validate arguments
if nargin < 3 || isempty(unclamped)
  unclamped = false;
end



%% Create knot vector

% Handle clmaped knot vectors
if unclamped
  % No repetitions at the start and end
  num_repeat = 0;
  
  % Should conform the rule: m = n + p + 1
  num_segments = num_ctrlpts + degree + 1;

% Handle unclaped knot vectors
else
  % Number of repetitions at the start and end of the array
  num_repeat = degree;

  % Number of knots in the middle
  num_segments = num_ctrlpts - (degree + 1);
  
end

if num_segments >= 0
  kv_i = linspace(0, 1, num_segments + 2);
else
  kv_i = 0;
end


% Concatenate different parts of knot vector
kv = horzcat( ...
  zeros(1, num_repeat) ...  % First knots
  , kv_i ...                % Inner knots
  , ones(1, num_repeat) ... % last knots
);

end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
