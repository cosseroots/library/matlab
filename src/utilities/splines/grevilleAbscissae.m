function a = grevilleAbscissae(degree, kv)%#codegen
%% GREVILLEABSCISSAE
%
% GREVILLEABSCISSAE(DEGREE, KNOTVECTOR)
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr



%% Parse arguments

% GREVILLEABSCISSAE(DEGREE, KNOTVECTOR)
narginchk(2, 2);

% GREVILLEABSCISSAE(___)
% A = GREVILLEABSCISSAE(___)
nargoutchk(0, 1);



%% Algorithm

order = degree + 1;

n = length(kv) - order;

if degree < 1
  error('GEOMETRY:ABSCISSAE:GREVILLE:wrongK', 'Invalid degree. Must be greater than or equal to 1.')

elseif n < 0
  error('GEOMETRY:ABSCISSAE:GREVILLE:tooFewKnots', 'Too few knots.');

elseif degree == 1
  a = reshape(kv(1 + (1:n)), 1, n);

else
  temp = repmat(kv, 1, degree);
  temp = sum(reshape([ temp(:) ; zeros(degree, 1) ], n + 1 + degree + 1, degree).', 1) ./ degree;
  a = temp(1 + (1:n));

end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
