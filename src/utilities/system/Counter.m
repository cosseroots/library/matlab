classdef Counter < matlab.System ...
    & matlab.system.mixin.FiniteSource
  %% COUNTER
  
  
  
  %% NONTUNABLE SYSTEM PROPERTIES
  properties ( Nontunable )
    
    % Value by which to increment at each call
    Increment (1, 1) double = 1
    
    % Initial value 
    InitialValue (1, 1) double = 0;
    
    % Maximum value of counter. Once reaching it, the System object will be
    % considered finished and ISDONE returns true
    MaxValue (1, 1) double = Inf;
    
  end
  
  
  
  %% OBJECT PROPERTIES
  properties ( DiscreteState )
    
    % Current counter value before being stepped through
    Value
    
  end
  
  
  
  %% CONSTRUCTOR METHODS
  methods
    
    function obj = Counter(varargin)
      %% COUNTER
      
      
      
      obj@matlab.System();
      obj@matlab.system.mixin.FiniteSource();
      
      setProperties(obj, nargin, varargin{:});
      
    end
    
  end
  
  
  
  %% CONCRETE SYSTEM METHODS
  methods ( Access = protected )
    
    function resetImpl(obj)
      %% RESETIMPL
      
      
      
      obj.Value = obj.InitialValue;
      
    end
    
    
    function v = outputImpl(obj)
      %% OUTPUTIMPL
      
      
      
      v = obj.Value;
      
    end
    
    
    function updateImpl(obj)
      %% UPDATEIMPL
      
      
      
      % Stop updating if counter has reached its final value
      if isDone(obj)
        return
      end
      
      obj.Value = obj.Value + obj.Increment;
      
    end
    
    
    function flag = isDoneImpl(obj)
      %% ISDONEIMPL
      
      
      
      flag = obj.Value >= obj.MaxValue;
      
    end
    
  end
  
end
