function [xl, yl, zl] = axisdatarange(hobj)
%% AXISDATARANGE
%
% Inputs:
%
%   CAX                     Description of argument CAX
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% AXISDATARANGE()
% AXISDATARANGE(HOBJ)
narginchk(0, 1);

% XL = AXISDATARANGE(___)
% [XL, YL] = AXISDATARANGE(___)
% [XL, YL, ZL] = AXISDATARANGE(___)
nargoutchk(1, 3);

% AXISDATARANGE()
if nargin < 1 || isempty(hobj)
  hobj = allchild(gca());
end



%% Algorithm

% Get all plotted objects
 hp = findall(hobj, { 'Type' , 'line' }, '-or', { 'Type' , 'quiver' }, '-or', { 'Type' , 'patch' });
nhp = numel(hp);

pd_ = cell(1, nhp);

% Loop over all plotted objects
for ihp = 1:nhp
  % Get plotted object
  hp_ = hp(ihp);
  if isequal(char(get(hp_, 'Type')), 'quiver')
    % Get its data
    xy = cat( ...
        1 ...
      , get(hp_, 'XData') ...
      , get(hp_, 'YData') ...
    );
    z = get(hp_, 'ZData');
    uv = cat( ...
        1 ...
      , get(hp_, 'UData') ...
      , get(hp_, 'VData') ...
    );
    w = get(hp_, 'WData');
    
    if isempty(z)
      z = zeros(1, size(xy, 2));
    end
    if isempty(w)
      w = zeros(1, size(uv, 2));
    end
    xyz = cat(1, xy, z);
    uvw = cat(1, uv, w);
    
    % And append the quiver data to the data points
    d = [ xyz , xyz + uvw];
    
  % Patch objects
  elseif isequal(char(get(hp_, 'Type')), 'patch')
    d = transpose(get(hp_, 'Vertices'));
    if size(d, 1) == 2
      d = cat(1, d, zeros(1, size(d, 2)));
    end
    
  else
    % Get its data
    d_ = cat( ...
        1 ...
      , get(hp_, 'XData') ...
      , get(hp_, 'YData') ...
    );
    
    z_ = get(hp_, 'ZData');
    if isempty(z_)
      z_ = zeros(size(d_, 2));
    end
    
    d = cat(1, d_, z_);
    
  end
  
  % Lets proceed through the parents. If they are hgtransform, we will
  % recursively apply their transformations to the data
  p = hp_.Parent;
  d_ = cat(1, d, ones(1, size(d, 2)));
  while ~isequal(char(get(p, 'Type')), 'axes')
    % Transform
    if isequal(char(get(p, 'Type')), 'hgtransform')
      d_ = get(p, 'Matrix') * d_;
    end
    % And shift parent
    p = p.Parent;
    
  end
  
  % Extract final data
  pd_{ihp} = d_(1:3,:);
  
end

% Combine all data
pd = cat(2, pd_{:});



%% Assign Outputs

% [XL, ___] = AXISDATARANGE(___)
xl = minmax(pd(1,:));

% [XL, YL, ___] = AXISDATARANGE(___)
if nargout > 1
  yl = minmax(pd(2,:));
end

% [XL, YL, ZL] = AXISDATARANGE(___)
if nargout > 2
  zl = minmax(pd(3,:));
end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
