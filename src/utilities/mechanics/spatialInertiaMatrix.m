function sim = spatialInertiaMatrix(mass, inertia, trref)%#codegen
%% SPATIALINERTIAMATRIX Generate a spatial inertia matrix
%
% SIM = SPATIALINERTIAMATRIX(MASS, INERTIA) creates spatial inertia matrix SIM
% from linear mass MASS and spatial moment of inertia tensor INERTIA.
%
% SIM = SPATIALINERTIAMATRIX(MASS, INERTIA, TRREF) takes into account the
% translation vector of the point of reference to the center of mass.
%
% Inputs:
%
%   M                       Scalar mass of body.
% 
%   J                       3x3 moment of inertia tensor.
% 
%   TRREF                   3x1 vector defining the position of the point of
%                           reference P with respect to the center of mass for
%                           which to calculate the spatial inertia matrix. The
%                           coordinates are given in body frames and translates
%                           the frame attached to the center of mass to the
%                           frame attached to the point P.
%
% Outputs:
%
%   SIM                     6x6 spatial inertia matrix with respect to point P
%                           located at COM from center of mass.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% SPATIALINERTIAMATRIX(MASS, INERTIA)
% SPATIALINERTIAMATRIX(MASS, INERTIA, COM)
narginchk(2, 3);

% SIM = SPATIALINERTIAMATRIX(___)
nargoutchk(0, 1);

% SPATIALINERTIAMATRIX(MASS, INERTIA)
if nargin < 3 || isempty(trref)
  trref = zeros(3, 1);
end



%% Algorithm

% Convert position of center of mass WRT to point of reference into a
% transformation matrix
T = tform2inv(trvec2tform(trref));

% Then use Ad(T)' * M * Ad(T) to calculate the spatial inertia matrix as viewed
% in point P
sim = transpose(Ad(T)) * blkdiag(inertia, mass .* eye(3, 3)) * Ad(T);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
