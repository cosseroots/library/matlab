function H = hookeanMatrix(A, J, E, G)%#codegen
%% HOOKEANMATRIX Build Hookean matrix
%
% H = HOOKEANMATRIX(A, J, E, G) builds Hookean matrix from material parameters.
% The Hookean matrix is calculated in local body coordinates and thus aligned
% with the principal axes of the body. Further, it is aligned in the YZ-plane
% that is its elastic behavior is along the X-Axis and shear is along both the Y
% and Z-axis. As such, its diagonal elements are [ GJ , EJ , EJ , EA , GA , GA ]
%
% Inputs:
%
%   A                       Scalar cross-section of body.
% 
%   J                       Scalar polar moment of area of body.
% 
%   E                       Scalar elastic modulus of material.
% 
%   G                       Scalar shear modulus of body.
%
% Outputs:
%
%   H                       6x6 Hookean matrix with diagonal elements of
%                           [ GJ , EJ , EJ , EA , GA , GA ]
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% HOOKEANMATRIX(A, J, E, G)
narginchk(4, 4);

% HOOKEANMATRIX(___)
% H = HOOKEANMATRIX(___)
nargoutchk(0, 1);



%% Algorithm

H = blkdiag( ...
    diag([ G , E , E ]) .* J ...
  , diag([ E , G , G ]) .* A ...
);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
