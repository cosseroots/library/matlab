function axang = axangzeros(n)%#codegen
%% AXANGZEROS Zero axis-angle.
%
% AXANG = AXANGZEROS() creates one zero axis-angle vector. A "zero axis-angle" is
% defined as a zero rotation about the Z-Axis i.e., [ 0 ; 0 ; 1 ; 0 ].
%
% AXANG = AXANGZEROS(N) creates N zero axis-angle vectors.
%
% Inputs:
%
%   N                       Scalar of how many zero axis-angles to create.
%
% Outputs:
%
%   AXANG                   4xN array of zero axis-angles.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% AXANGZEROS()
% AXANGZEROS(N)
narginchk(0, 1);

% AXANGZEROS(___)
% AXANG = AXANGZEROS(___)
nargoutchk(0, 1);

% AXANGZEROS()
if nargin < 1 || isempty(n)
  n = 1;
end



%% Algorithm

axang = repmat(delta(3, 4), [1, n]);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
