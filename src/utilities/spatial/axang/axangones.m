function axang = axangones(n)%#codegen
%% AXANGONES Unit axis-angles
%
% AXANG = AXANGONES() creates one unit axis-angle vector. A "unit axis-angle" is
% defined as a unit rotation of 1 about the unit axis [ 1 ; 1 ; 1 ].
%
% AXANG = AXANGONES(N) creates N unit axis-angle vectors.
%
% Inputs:
%
%   N                       Scalar of how many unit axis-angles to create.
%
% Outputs:
%
%   AXANG                   4xN array of unit axis-angles.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% AXANGONES()
% AXANGONES(N)
narginchk(0, 1);

% AXANGONES(___)
% AXANG = AXANGONES(___)
nargoutchk(0, 1);

% AXANGONES()
if nargin < 1 || isempty(n)
  n = 1;
end



%% Algorithm

axang = cat(1, mnormcol(ones(3, n)), ones(1, n));


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
