function q = axang2quat(axang)%#codegen
%% AXANG2QUAT Convert axis-angle rotation to quaternion
%
% Q = AXANG2QUAT(AXANG) converts 3D rotation axis-angle AXANG to its unit
% quaternion Q.
%
% Inputs:
%
%   AXANG                   4xN array of axis angles where the first three
%                           elements are the axis of rotation and the last
%                           element defines the amount of rotation (in radians).
%
% Outputs:
%
%   Q                       4xN array of unit quaternions with the first element
%                           being the scalar/real part
%
% See also:
%   QUAT2AXANG
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% AXANG2QUAT(AXANG)
narginchk(1, 1);

% AXANG2QUAT(___)
% Q = AXANG2QUAT(___)
nargoutchk(0, 1);



%% Algorithm

% For a single axis-angle vector [ax ay az t] the output quaternion
%   q = [ w ; x ; y ; z ], can be computed as follows:
%   w = cos(t/2)
%   x = ax * sin(t/2)
%   y = ay * sin(t/2)
%   z = az * sin(t/2)

% Ensure AXANG is column-major
axang = reshape(axang, 4, []);

% Normalize the axis
v = mnormcol(axang(1:3,:));

% Create the quaternion
th = axang(4,:) / 2;
sth = sin(th);
q = cat( ...
    1 ...
  , cos(th) ...
  , v .* sth ...
);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
