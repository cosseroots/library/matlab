function R = axang2rotm(axang)%#codegen
%% AXANG2ROTM Convert axis-angle rotation to rotation matrix
%
% R = AXANG2ROTM(AXANG) converts axis-angle rotation AXANG to its orthonormal
% rotation matrix R.
%
% Inputs:
%
%   AXANG                   4xN array of axis angles where the first three
%                           elements are the axis of rotation and the last
%                           element defines the amount of rotation (in radians).
%
% Outputs:
%
%   R                       3x3xN array of orthonormal rotation matrices.
%
% See also;
%   ROTM2AXANG
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% AXANG2ROTM(AXANG)
narginchk(1, 1);

% AXANG2ROTM(___)
% R = AXANG2ROTM(___)
nargoutchk(0, 1);



%% Algorithm

% For a single axis-angle vector [ax ay az theta] the output rotation
% matrix R can be computed as follows:
% R =  [t*x*x + c	  t*x*y - z*s	   t*x*z + y*s
%       t*x*y + z*s	  t*y*y + c	       t*y*z - x*s
%       t*x*z - y*s	  t*y*z + x*s	   t*z*z + c]
% where,
% c = cos(theta)
% s = sin(theta)
% t = 1 - c
% x = normalized axis ax coordinate
% y = normalized axis ay coordinate
% z = normalized axis az coordinate

% Normalize the axis
v = mnormcol(axang(1:3,:));

% Extract the rotation angles and shape them in depth dimension
nang = size(axang, 2);
th = zeros(1, 1, nang);
th(1,1,:) = axang(4,:);

% Compute rotation matrices
cth = cos(th);
sth = sin(th);
vth = 1 - cth;

% Preallocate input vectors
vx = zeros(1, 1, nang, 'like', axang);
vy = vx;
vz = vx;

% Shape input vectors in depth dimension
vx(1,1,:) = v(1,:);
vy(1,1,:) = v(2,:);
vz(1,1,:) = v(3,:);

% Explicitly specify concatenation dimension
tempR = cat( ...
    1 ...
  , vx .* vx .* vth + cth      , vy .* vx .* vth - vz .* sth, vz .* vx .* vth + vy .* sth ...
  , vx .* vy .* vth + vz .* sth, vy .* vy .* vth + cth      , vz .* vy .* vth - vx .* sth ...
  , vx .* vz .* vth - vy .* sth, vy .* vz .* vth + vx .* sth, vz .* vz .* vth + cth ...
);

R = permute(reshape(tempR, [3, 3, nang]), [2, 1, 3]);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
