function T = axang2tpar(axang)%#codegen
%% AXANG2TPAR Convert axis-angle rotation to homogeneous transformation matrix
%
% T = AXANG2TPAR(AXANG) converts axis-angle AXANG into its homogeneous
% tranformation matrix T.
%
% Inputs:
%
%   AXANG                   4xN array of axis angles where the first three
%                           elements are the axis of rotation and the last
%                           element defines the amount of rotation (in radians).
%
% Outputs:
%
%   T                       4x4xN array of homogeneous transformations.
%
% See also:
%   TFORM2AXANG
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% AXANG2TPAR(AXANG)
narginchk(1, 1);

% AXANG2TPAR(___)
% T = AXANG2TPAR(___)
nargoutchk(0, 1);



%% Algorithm

% Life can be so simple
T = rotm2tpar(axang2rotm(axang));


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
