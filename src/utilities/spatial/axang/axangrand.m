function axang = axangrand(varargin)%#codegen
%% AXANGRAND Random axis-angles
%
% AXANG = AXANGRAND() creates one random axis-angle vector. A "random
% axis-angle" is defined as a random rotation about a random unit vector.
%
% AXANG = AXANGRAND(N) creates N random axis-angle vectors.
%
% Inputs:
%
%   N                       Scalar of how many random axis-angles to create.
%
% Outputs:
%
%   AXANG                   4xN array of random axis-angles.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% AXANGRAND()
% AXANGRAND(N)
narginchk(0, 1);

% AXANGRAND(___)
% AXANG = AXANGRAND(___)
nargoutchk(0, 1);

% Retrieve a possibly passed `RANDSTREAM` object
[s, args, nargs] = objectcheck('RandStream', varargin{:});

% AXANGRAND()
% AXANGRAND(N)
if isempty(s)
  s = RandStream.getGlobalStream();
end

% Default arguments
n = 1;

% AXANGRAND(N)
if nargs > 0 && ~isempty(args{1})
  n = args{1};
  args(1) = [];
end



%% Algorithm

axang = cat(1, mnormcol(rand(s, 3, n, args{:}) - 0.5), rand(s, 1, n, args{:}));


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
