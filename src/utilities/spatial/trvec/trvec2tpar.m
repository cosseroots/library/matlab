function g = trvec2tpar(trvec)%#codegen
%% TRVEC2TPAR Translation vector to homogeneous transformation parameters.
%
% G = TRVEC2TPAR(P) converts translation vector TREVC into its homogeneous
% transformation parameter G.
%
% Inputs:
%
%   P                       3xN array of translation vectors.
%
% Outputs:
%
%   G                       7xN array of homogeneous transformations parameters.
%
% See also:
%   TPAR2TRVEC
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% TRVEC2TPAR(P)
narginchk(1, 1);

% TRVEC2TPAR(___)
% G = TRVEC2TPAR(___)
nargoutchk(0, 1);



%% Algorithm

% Ensure P is a column vector
trvec = reshape(trvec, 3, []);

% Create the transformations
g = tpar(quatzeros(size(trvec, 2)), trvec);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
