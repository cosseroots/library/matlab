function tr = trvec(v)%#codegen
%% TRVEC Make a translational vector
%
% TR = TRVEC(V) turns V into a translation vector.
%
% Inputs:
%
%   V                       NxM array of vectors.
%
% Outputs:
%
%   TR                      3xM array of translation vectors.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% TRVEC(V)
narginchk(1, 1);

% TRVEC(___)
% TR = TRVEC(___)
nargoutchk(0, 1);



%% Algorithm

% Ensure V is a column vector (if given as a vector)
if isvector(v)
  v = reshape(v, [], 1);
end

% Pad array to 3 rows, then extract only the first three rows
tr = hslice(padarray(v, 3 - size(v, 1), 0, 'post'), 1, 1:3);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
