function p = trveczeros(n)%#codegen
%% TRVECZEROS Zero translation vectors
%
% P = TREVCONES() creates 1 unit translation vector.
%
% P = TREVCONES(N) creates N unit translation vectors.
%
% Inputs:
%
%   N                       Scalar number of how many translation vectors to
%                           create.
%
% Outputs:
%
%   R                       3xN array of unit translation vectors.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% TRVECZEROS()
% TRVECZEROS(N)
narginchk(0, 1);

% TRVECZEROS(___)
% P = TRVECZEROS(___)
nargoutchk(0, 1);

% TRVECZEROS()
if nargin < 1 || isempty(n)
  n = 1;
end



%% Algorithm

p = zeros(3, n);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
