function T = trvec2tform(trvec)%#codegen
%% TRVEC2TFORM Convert translation vector to homogeneous transformation matrix
%
% T = TRVEC2TFORM(TRVEC) converts translation vector TRVEC to its homogeneous
% transformation matrix T.
%
% Inputs:
%
%   P                       3xN array of translation vectors.
%
% Outputs:
%
%   T                       4x4xN array of homogeneous transformations.
%
% See also:
%   TFORM2TRVEC
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% TRVEC2TFORM(P)
narginchk(1, 1);

% TRVEC2TFORM(___)
% T = TRVEC2TFORM(___)
nargoutchk(0, 1);


%% Algorithm

trvec = reshape(trvec, 3, []);

T = repmat(eye(4, 4, 'like', trvec), [ 1, 1, size(trvec, 2) ]);
T(1:3,4,:) = permute(trvec, [1, 3, 2]);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
