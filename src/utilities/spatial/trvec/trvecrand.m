function p = trvecrand(varargin)%#codegen
%% TRVECRAND Uniformly distributed random translation vectors
%
% P = TRVECRAND() creates 1 uniformly distributed random translation vector in
% the range [ -1 , +1 ].
%
% P = TRVECRAND(N) creates N uniformly distributed random translation vectors.
%
% Inputs:
%
%   N                       Scalar number of how many translation vectors to
%                           create.
%
% Outputs:
%
%   R                       3xN array of random translation vectors.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% TRVECRAND()
% TRVECRAND(N)
% TRVECRAND(S)
% TRVECRAND(S, N)
narginchk(0, 2);

% TRVECRAND(___)
% P = TRVECRAND(___)
nargoutchk(0, 1);

% Retrieve a possibly passed `RANDSTREAM` object
[s, args, nargs] = objectcheck('RandStream', varargin{:});

% TRVECRAND()
% TRVECRAND(N)
if isempty(s)
  s = RandStream.getGlobalStream();
end

% Default arguments
n = 1;

% TRVECRAND(N)
if nargs > 0 && ~isempty(args{1})
  n = args{1};
  args(1) = [];
end



%% Algorithm

p = 2 * ( rand(s, 3, n, args{:}) - 0.5 );


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
