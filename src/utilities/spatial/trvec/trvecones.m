function p = trvecones(n)%#codegen
%% TRVECONES Unit translation vectors
%
% P = TRVECONES() creates 1 unit translation vector.
%
% P = TRVECONES(N) creates N unit translation vectors.
%
% Inputs:
%
%   N                       Scalar number of how many translation vectors to
%                           create.
%
% Outputs:
%
%   R                       3xN array of unit translation vectors.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% TRVECONES()
% TRVECONES(N)
narginchk(0, 1);

% TRVECONES(___)
% R = TRVECONES(___)
nargoutchk(0, 1);

% TRVECONES()
if nargin < 1 || isempty(n)
  n = 1;
end



%% Algorithm

% Simple as that
p = mnormcol(ones(3, n));


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
