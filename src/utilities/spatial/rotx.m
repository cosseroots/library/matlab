function R = rotx(ang)%#codegen
%% ROTX Create rotation matrix from rotation about X-axis
%
% R = ROTX(ANG) creates rotation matrix R from rotation about X-axis by angles
% ANG radians.
%
% Inputs:
%
%   ANG                 1xN array of radian angles to create rotation matrices
%                       for.
%
% Outputs:
%
%   R                   3x3xN matrix of rotation matrices per angle.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% ROTX(ANG)
narginchk(1, 1);
% ROTX(ANG)
% R = ROTX(ANG)
nargoutchk(0, 1);



%% Create rotation matrix

% Number of angles
nang = numel(ang);

% Turn the vector into a 3d matrix
ang = reshape(ang, [1, 1, nang]);

% Pre-calculate sine and cosines
sa = sin(ang);
ca = cos(ang);
z = zeros(1, 1, nang);
o = ones(1, 1, nang);

% Different rotation matrices
tempR = cat(1 ...
  , o,  z,    z ...
  , z, +ca, -sa ...
  , z, +sa, +ca ...
);

% Turn our current 1x9xNA array into a 3x3xNA array
R = permute(reshape(tempR, [3, 3, nang]), [2, 1, 3]);

% Vanish singular values
R(issingular(R)) = 0;


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
