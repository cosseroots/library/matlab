function axang = tform2axang(T)%#codegen
%% TFORM2AXANG Extract axis-angle rotation from homogeneous transformation
%
% AXANG = TFORM2AXANG(T) converts homogeneous transformation T into its
% axis-angle representation
%
% Inputs:
%
%   T                       4x4xN array of homogeneous transformations.
%
% Outputs:
%
%   AXANG                   4xN array of axis angles where the first three
%                           elements are the axis of rotation and the last
%                           element defines the amount of rotation (in radians).
%
% See also:
%   AXANG2TFORM
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% TFORM2AXANG(T)
narginchk(1, 1);

% TFORM2AXANG(___)
% AXANG = TFORM2AXANG(___)
nargoutchk(0, 1);



%% Algorithm

% Daisy-chain
axang = rotm2axang(tform2rotm(T));


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
