function eul = tform2eul(T, seq)%#codegen
%% TFORM2EUL Extract Euler angles from homogeneous transformation
%
% EUL = TFORM2EUL(T, SEQ)
%
% Inputs:
%
%   T                       4x4xN array of homogeneous transformation matrices.
%
%   SEQ                     String defining the sequence to use for conversion.
%
% Outputs:
%
%   EUL                     3xN array of euler angles.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% TFORM2EUL(T, SEQ)
narginchk(2, 2);

% TFORM2EUL(___)
% EUL = TFORM2EUL(___)
nargoutchk(0, 1);



%% Algorithm

% This is a two-step process.
% 1. Extract the rotation matrix from the homogeneous transform
% 2. Convert the rotation matrix to a set of euler angles
eul = rotm2eul(tform2rotm(T), seq);

end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.

