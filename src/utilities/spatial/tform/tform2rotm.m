function R = tform2rotm(T)%#codegen
%% TFORM2ROTM Convert homogeneous transformation to rotation matrix
%
% R = TFORM2ROTM(T) converts homogeneous transformation T into its rotation
% matrix R.
%
% Inputs:
%
%   T                       4x4xN array of homogeneous transformations.
%
% Outputs:
%
%   R                       3x3xN array of orthonormal rotation matrices.
%
% See also:
%   ROTM2TFORM
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% TFORM2ROTM(T)
narginchk(1, 1);

% TFORM2ROTM(___)
% R = TFORM2ROTM(___)
nargoutchk(0, 1);



%% Algorithm

% That's the simplest of all
R = T(1:3,1:3,:);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
