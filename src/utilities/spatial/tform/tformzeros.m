function T = tformzeros(n)%#codegen
%% TFORMZEROS
%
% R = TFORMZEROS() creates 1 zero rotation matrix. A "zero rotation matrix" is
% defined as a unit matrix i.e., no rotation.
%
% R = TFORMZEROS(N) creates N such zero rotation matrices.
%
% Inputs:
%
%   N                       Scalar number of how many transformation matrices to
%                           create.
%
% Outputs:
%
%   T                       4x4xN array of zero homogeneous transformation
%                           matrices.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% TFORMZEROS()
% TFORMZEROS(N)
narginchk(0, 1);

% TFORMZEROS(___)
% R = TFORMZEROS(___)
nargoutchk(0, 1);

% TFORMZEROS()
if nargin < 1 || isempty(n)
  n = 1;
end



%% Algorithm

T = cart2tform(rotmzeros(n), trveczeros(n));


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
