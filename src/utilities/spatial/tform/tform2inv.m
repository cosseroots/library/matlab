function Ti = tform2inv(T)%#codegen
%% TFORM2INV Calculate inverse of homogeneous transformation matrix
%
% TI = TFORM2INV(T) calculates the inverse transformation matrix TI of
% transformation matrix T such that T * TI = I
%
% Inputs:
%
%   T                       4x4xN array of homogeneous transformations matrices.
%
% Outputs:
%
%   TI                      4x4xN array of inverse homogeneous transformation
%                           matrices.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% TFORM2INV(T)
narginchk(1, 1);

% TFORM2INV(___)
% TI = TFORM2INV(___)
nargoutchk(0, 1);



%% Algorithm

% Extract components from transformation matrix
Rt = permute(tform2rotm(T), [2, 1, 3]);
p = permute(tform2trvec(T), [1, 3, 2]);

% And build its inverse
Ti = tform(Rt, permute(pagemult(-Rt, p), [1, 3, 2]));


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
