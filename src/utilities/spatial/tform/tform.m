function T = tform(R, p)%#codegen
%% TFORM Create homogeneous transformation matrices
%
% T = TFORM(R, P) create homogeneous transformation matrices from rotations R
% and positions P.
%
% Inputs:
%
%   R                       3x3xN array of rotation matrices.
%
%   P                       3xN array of linear positions.
%
% Outputs:
%
%   T                       4x4xN array of homogeneous transformation matrices.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% TFORM(R, P)
narginchk(2, 2);

% TFORM(___)
% T = TFORM(___)
nargoutchk(0, 1);



%% Algorithm

% Count data
nr = size(R, 3);

% Create array
T = zeros(4, 4, nr, 'like', R);
T(1:3,1:3,:) = R;
T(1:3,4,:)   = permute(p, [1, 3, 2]);
T(4,4,:)     = ones(1, 1, nr);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
