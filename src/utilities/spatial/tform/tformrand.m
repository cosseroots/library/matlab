function T = tformrand(varargin)%#codegen
%% TFORMRAND Uniformly distributed random homogeneous transformation matrices
%
% R = TFORMRAND() creates 1 uniformly distributed random rotation matrices.
%
% R = TFORMRAND(N) creates N uniformly distributed random rotation matrices.
%
% Inputs:
%
%   N                       Scalar number of how many transformation matrices to
%                           create.
%
% Outputs:
%
%   T                       4x4xN array of random homogeneous transformation
%                           matrices.%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% TFORMRAND()
% TFORMRAND(N)
% TFORMRAND(S)
% TFORMRAND(S, N)
narginchk(0, 2);

% TFORMRAND(___)
% R = TFORMRAND(___)
nargoutchk(0, 1);

% Retrieve a possibly passed `RANDSTREAM` object
[s, args, nargs] = objectcheck('RandStream', varargin{:});

% TFORMRAND()
% TFORMRAND(N)
if isempty(s)
  s = RandStream.getGlobalStream();
end

% Default arguments
n = 1;

% TFORMRAND(N)
if nargs > 0 && ~isempty(args{1})
  n = args{1};
  args(1) = [];
end



%% Algorithm

T = cart2tform(rotmrand(s, n, args{:}), trvecrand(s, n, args{:}));


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
