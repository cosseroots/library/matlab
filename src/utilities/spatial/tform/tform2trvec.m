function trvec = tform2trvec(T)%#codegen
%% TFORM2TRVEC Convert homogeneous transformation to translation vector
%
% TRVEC = TFORM2TRVEC(T) converts homogenous transformation T to translation
% vector TRVEC.
%
% Inputs:
%
%   T                       4x4xN array of homogeneous transformations.
%
% Outputs:
%
%   TRVEC                   3xN array of normalized translation vectors.
%
% See also:
%   TRVEC2TFORM
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% TFORM2TRVEC(T)
narginchk(1, 1);

% TFORM2TRVEC(___)
% TR = TFORM2TRVEC(___)
nargoutchk(0, 1);



%% Algorithm

% Get scaling factors
s  = permute(T(4,4,:), [1, 3, 2]);

% Build translation vectors
trvec = permute(T(1:3,4,:), [1, 3, 2]);

% Find out where scaling factor of transformation matrix is non-zero
indone = ~isclose(s, 0);

% Scale where scaling factor is non-zero
if sum(indone) > 0
  trvec(:,indone) = trvec(:,indone) ./ s;
end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
