function T = tformdiv(T1, T2)%#codegen
%% TFORMDIV Divide two transformation matrices
%
% T = TFORMDIV(T1, T2) divides homogeneous transformation matrices T1 by T2 to
% obtain T = T1 / T2 = T1 * inv(T2).
%
% Inputs:
%
%   T1                      7xN or 7x1 array of homogeneous transformation
%                           matrices.
% 
%   T2                      7x1 or 7xN array of homogeneous transformation
%                           matrices.
%
% Outputs:
%
%   G                       7xN array of homogeneous transformations matrices.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% T = TFORMDIV(T1, T2)
narginchk(2, 2);

% TFORMDIV(___)
% T = TFORMDIV(__)
nargoutchk(0, 1);



%% Algorithm

% Extract components from both transformation matrices
R1  = tform2rotm(T1);
R2t = permute(tform2rotm(T2), [2, 1, 3]);

p1 = tform2trvec(T1);
p2 = permute(tform2trvec(T2), [1, 3, 2]);

R1R2t = pagemult(R1, R2t);

% Simple as that
T = tform( ...
    R1R2t ...
  , p1 + ...
      + permute(pagemult(R1R2t, -p2), [1, 3, 2]) ...
);

% Remove singular values
T(issingular(T, 100 * eps(class(T)))) = 0;


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
