function q = tform2quat(T)%#codegen
%% TFORM2QUAT Convert homogeneous transformation to Quaternion%
%
% Q = TFORM2QUAT(T) converts homogeneous transformation T into its equivalent
% unit quaternion Q.
%
% Inputs:
%
%   T                       4x4xN array of homogeneous transformations.
%
% Outputs:
%
%   Q                       4xN array of unit quaternions with the first element
%                           being the scalar/real part
%
% See also:
%   QUAT2TFORM
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% TFORM2QUAT(T)
narginchk(1, 1);

% TFORM2QUAT(___)
% Q = TFORM2QUAT(___)
nargoutchk(0, 1);



%% Algorithm

% Use already existing code
q = rotm2quat(tform2rotm(T));


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
