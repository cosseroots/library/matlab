function T = tformones(n)%#codegen
%% TFORMONES
%
% R = TFORMONES() creates 1 unit rotation matrix. A unit rotation matrix is
% defined as a rotation of one unit about the normalized vector [ 1 ; 1 ; 1].
%
% Inputs:
%
%   N                       Scalar number of how many transformation matrices to
%                           create.
%
% Outputs:
%
%   T                       4x4xN array of unit homogeneous transformation
%                           matrices.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% TFORMONES()
% TFORMONES(N)
narginchk(0, 1);

% TFORMONES(___)
% R = TFORMONES(___)
nargoutchk(0, 1);

% TFORMONES()
if nargin < 1 || isempty(n)
  n = 1;
end



%% Algorithm

T = cart2tform(rotmones(n), trvecones(n));


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
