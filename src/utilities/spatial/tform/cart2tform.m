function T = cart2tform(R, p)%#codegen
%% CART2TFORM Convert Cartesian coordinates to homogeneous transformation matrices
%
% T = CART2FORM(R, P) converts rotation R and positions P into homogeneous
% transformation matrices T.
%
% Inputs:
%
%   R                       3x3N array of rotation matrices.
%
%   P                       3xN array of linear positions.
%
% Outputs:
%
%   T                       4x4xN array of homogeneous transformation matrices.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% CART2TFORM(R, P)
narginchk(2, 2);

% CART2TFORM(__)
% T = CART2TFORM(__)
nargoutchk(0, 1);



%% Algorithm

% Convert and create
T = tform(R, p);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
