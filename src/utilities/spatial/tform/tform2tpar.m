function g = tform2tpar(T)%#codegen
%% TFORM2TPAR Turn homogeneous transformation matrix into homogeneous transformation
%
% G = TFORM2TPAR(M)
%
% Inputs:
%
%   M                       4x4xN array of homogeneous transformation matrices.
%
% Outputs:
%
%   G                       7xN array of homogeneous transformations parameters.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% TFORM2TPAR(M)
narginchk(1, 1);

% TFORM2TPAR(___)
% G = TFORM2TPAR(___)
nargoutchk(0, 1);



%% Algorithm

% Just stack quaternions and translations
g = [ ...
  rotm2quat(T(1:3,1:3,:)) ; ...
    permute(T(1:3,4,:), [1, 3, 2]) ; ...
];


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
