function [R, p] = tform2cart(T)%#codegen
%% TFORM2CART Convert homogeneous transformation matrix to Cartesian coordinates
%
% [R, P] = TFORM2CART(T) converts homogeneous transformation T to its Cartesian
% rotation R and translation P.
%
% Inputs:
%
%   T                       4x4xN array of homogeneous transformation matrices.
%
% Outputs:
%
%   R                       3x3xN array of rotation matrices.
%
%   P                       3xN array of translation vectors.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% TFORM2CART(T)
narginchk(1, 1);

% [R, P] = TFORM2CART(___)
nargoutchk(2, 2);



%% Algorithm

% Dispatch
R = tform2rotm(T);
p = tform2trvec(T);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
