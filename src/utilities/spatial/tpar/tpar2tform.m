function T = tpar2tform(g)%#codegen
%% TPAR2TFORM Convert homogeneous transformation parametrization to its matrix form
%
% T = TPAR2TFORM(G)
%
% Inputs:
%
%   G                       7xN array of homogeneous transformations parameters.
%
% Outputs:
%
%   T                       4x4xN array of homogeneous transformation matrices.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% TPAR2TFORM(G)
narginchk(1, 1);

% TPAR2TFORM(___)
% M = TPAR2TFORM(___)
nargoutchk(0, 1);



%% Algorithm

% Store original size of G for later reshaping
szg = size(g, 2:max(2, ndims(g)));

% Ensure T is a column vector
g = reshape(g, 7, []);

% Count number of transformations
nn = size(g, 2);

% Get rotation matrix and positions
R = tpar2rotm(g);
p = tpar2trvec(g);

% Build matrix
T_ = zeros(4, 4, nn);
T_(4,4,:)             = ones(1, 1, nn);
T_([1,2,3],[1,2,3],:) = R;
T_([1,2,3],4,:)       = permute(p, [1, 3, 2]);

% Reshape into original dimensions
T = reshape(T_, [ 4 , 4 , szg ]);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
