function g = tpar(Q, p)%#codegen
%% TPAR Create a homogeneous transformation object
%
% G = TPAR(Q, P)
%
% Inputs:
%
%   Q                       4xN array of quaternions.
% 
%   P                       4xN array of positions.
%
% Outputs:
%
%   G                       7xN array of homogeneous transformations.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% TPAR(Q, P)
narginchk(2, 2);

% TPAR(___)
% T = TPAR(___)
nargoutchk(0, 1);



%% Algorithm

% Ensure Q and P are a column vectors
Q = reshape(Q, 4, []);
p = reshape(p, 3, []);

% Create matrix
g = cat(1, Q, p);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
