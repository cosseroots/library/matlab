function g = cart2tpar(R, p)%#codegen
%% CART2TPAR Convert Cartesian coordinates to homogeneous transformations parameters
%
% G = CART2FORM(R, P) converts rotation R and positions P into homogeneous
% transformation G.
%
% Inputs:
%
%   R                       3x3N array of rotation matrices.
%
%   P                       3xN array of linear positions.
%
% Outputs:
%
%   G                       7xN array of homogeneous transformations parameters.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% CART2TPAR(R, P)
narginchk(2, 2);

% CART2TPAR(__)
% G = CART2TPAR(__)
nargoutchk(0, 1);



%% Algorithm

% Convert and create
g = tpar(rotm2quat(R), p);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
