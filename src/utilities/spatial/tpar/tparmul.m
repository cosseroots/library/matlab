function g = tparmul(g1, g2)%#codegen
%% TPARMUL Multiply two transformations
%
% Inputs:
%
%   G1                      7xN or 7x1 array of homogeneous transformations
%                           parameters.
% 
%   G2                      7x1 or 7xN array of homogeneous transformations
%                           parameters.
%
% Outputs:
%
%   G                       7xN array of homogeneous transformations parameters.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% TPARMUL(G1, G2)
narginchk(2, 2);

% TPARMUL(___)
% G = TPARMUL(___)
nargoutchk(0, 1);



%% Algorithm

% Extract components from both transformation matrices
Q1 = tpar2quat(g1);
Q2 = tpar2quat(g2);

p1 = tpar2trvec(g1);
p2 = padarray(tpar2trvec(g2), 1, 0, 'pre');

% Shorter form
g = tpar( ...
    quatmul(Q1, Q2) ...
  , p1 + ...
      + hslice(quatmul(quatmul(Q1, p2), quat2conj(Q1)), 1, 2:4) ...
);
% Ensure quaternions are normalized
g(1:4,:) = quatnormalized(g(1:4,:));


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
