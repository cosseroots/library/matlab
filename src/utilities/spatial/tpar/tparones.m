function g = tparones(n)%#codegen
%% TPARONES Create unit homogeneous transformations.
%
% G = TPARONES() creates a unit homogeneous transformation.
%
% TPARONES(N) creates N unit homogeneous transformations.
%
% Inputs:
%
%   N                       Scalar entry.
%
% Outputs:
%
%   G                       7xN array of "unit" homogeneous transformations
%                           parameters.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% TPARONES()
% TPARONES(N)
narginchk(0, 1);

% TPARONES(___)
% G = TPARONES(___)
nargoutchk(0, 1);

% TPARONES()
if nargin < 1 || isempty(n)
  n = 1;
end



%% Algorithm

% Dispatch
n = fix(abs(n));
g = tpar(quatones(n), ones(3, n));


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
