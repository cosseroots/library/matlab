function [R, p] = tpar2cart(g)%#codegen
%% TPAR2CART
%
% [R, P] = TPAR2CART(G)
%
% Inputs:
%
%   G                       7xN array of homogeneous transformations parameters.
%
% Outputs:
%
%   R                       3x3xN array of rotation matrices.
%
%   P                       3xN array of translation vectors.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% TPAR2CART(G)
narginchk(1, 1);

% [R, P] = TPAR2CART(___)
nargoutchk(2, 2);



%% Algorithm

% Dispatch
R = tpar2rotm(g);
p = tpar2trvec(g);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
