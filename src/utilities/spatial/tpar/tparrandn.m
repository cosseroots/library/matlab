function g = tparrandn(varargin)%#codegen
%% TPARRAND Normally distributed random homogeneous transformation parameters
%
% G = TPARRANDN() creates 1 normally distributed random homogeneous
% transformation parameter.
%
% G = TPARRANDN(N) creates N normally distributed random homogeneous
% transformation parameters.
%
% Inputs:
%
%   N                       Number of random homogeneous transformations to
%                           create.
%                           Default: 1
%
% Outputs:
%
%   G                       7xN array of homogeneous transformations parameters.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% TPARRANDN()
% TPARRANDN(N)
% TPARRANDN(S)
% TPARRANDN(S, N)
narginchk(0, 2);

% TPARRANDN(___)
% G = TPARRANDN(___)
nargoutchk(0, 1);

% Retrieve a possibly passed `RANDSTREAM` object
[s, args, nargs] = objectcheck('RandStream', varargin{:});

% TPARRANDN()
% TPARRANDN(N)
if isempty(s)
  s = RandStream.getGlobalStream();
end

% Default arguments
n = 1;

% TPARRANDN(N)
if nargs > 0 && ~isempty(args{1})
  n = args{1};
  args(1) = [];
end



%% Algorithm

g = tpar(quatrandn(s, n, args{:}), trvecrandn(s, n, args{:}));


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
