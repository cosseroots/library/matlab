function q = tpar2quat(g)%#codegen
%% TPAR2QUAT
%
% Q = TPAR2QUAT(G)
%
% Inputs:
%
%   G                       7xN array of homogeneous transformations parameters.
%
% Outputs:
%
%   Q                       4xN array of quaternions.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% TPAR2QUAT(G)
narginchk(1, 1);

% TPAR2QUAT(___)
% Q = TPAR2QUAT(___)
nargoutchk(0, 1);



%% Algorithm

% Just extract quaternions
q = hslice(g, 1, 1:4);



end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
