function g = tparzeros(n)%#codegen
%% TPARZEROS Create zero homogeneous transformations.
%
% A zero homogeneous transformation is assumed to represent a voiding rotation
% and voiding translation i.e., R = eye(3, 3), and p = zeros(3, 1).
%
% G = TPARZEROS() creates a zero homogeneous transformation.
%
% TPARZEROS(N) creates N zero homogeneous transformations.
%
% Inputs:
%
%   N                       Scalar entry.
%
% Outputs:
%
%   G                       7xN array of "zero" homogeneous transformations
%                           parameters.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% TPARZEROS()
% TPARZEROS(N)
narginchk(0, 1);

% TPARZEROS(___)
% G = TPARZEROS(___)
nargoutchk(0, 1);

% TPARZEROS()
if nargin < 1 || isempty(n)
  n = 1;
end



%% Algorithm

% Dispatch
n = fix(abs(n));
g = tpar(quatzeros(n), zeros(3, n));


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
