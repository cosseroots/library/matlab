function g = tparrand(varargin)%#codegen
%% TPARRAND Uniformly distributed random homogeneous transformation parameters
%
% G = TPARRAND() creates 1 uniformly distributed random homogeneous
% transformation parameter.
%
% G = TPARRAND(N) creates N uniformly distributed random homogeneous
% transformation parameters.
%
% Inputs:
%
%   N                       Number of random homogeneous transformations to
%                           create.
%                           Default: 1
%
% Outputs:
%
%   G                       7xN array of homogeneous transformations parameters.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% TPARRAND()
% TPARRAND(N)
% TPARRAND(S)
% TPARRAND(S, N)
narginchk(0, 2);

% TPARRAND(___)
% G = TPARRAND(___)
nargoutchk(0, 1);

% Retrieve a possibly passed `RANDSTREAM` object
[s, args, nargs] = objectcheck('RandStream', varargin{:});

% TPARRAND()
% TPARRAND(N)
if isempty(s)
  s = RandStream.getGlobalStream();
end

% Default arguments
n = 1;

% TPARRAND(N)
if nargs > 0 && ~isempty(args{1})
  n = args{1};
  args(1) = [];
end



%% Algorithm

g = tpar(quatrand(s, n, args{:}), trvecrand(s, n, args{:}));


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
