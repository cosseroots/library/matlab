function gi = tpar2inv(g)%#codegen
%% TPAR2INV Calculate inverse of homogeneous transformation matrix
%
% GI = TPAR2INV(G)
%
% Inputs:
%
%   G                       7xN array of homogeneous transformations parameters.
%
% Outputs:
%
%   GI                      7xN array of inverse homogeneous transformation
%                           matrices.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% TPAR2INV(G)
narginchk(1, 1);

% TPAR2INV(___)
% GI = TPAR2INV(___)
nargoutchk(0, 1);



%% Algorithm

% Extract components from both transformation matrices
Q = tpar2quat(g);
Qj = quat2conj(Q);

p = padarray(tpar2trvec(g), 1, 0, 'pre');

% Shorter form
gi = tpar( ...
    Qj ...
  , hslice(quatmul(quatmul(Qj, -p), Q), 1, 2:4) ...
);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
