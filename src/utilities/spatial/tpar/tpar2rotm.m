function R = tpar2rotm(g)%#codegen
%% TPAR2ROTM
%
% R = TPAR2ROTM(G)
%
% Inputs:
%
%   G                       7xN array of homogeneous transformations parameters.
%
% Outputs:
%
%   ROT                     3x3xN array of rotation matrices.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% TPAR2ROTM(G)
narginchk(1, 1);

% TPAR2ROTM(___)
% R = TPAR2ROTM(___)
nargoutchk(0, 1);



%% Algorithm

% Extract quaternion and convert to rotation matrix
R = quat2rotm(tpar2quat(g));


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
