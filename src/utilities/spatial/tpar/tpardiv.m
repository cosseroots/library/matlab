function g = tpardiv(g1, g2)%#codegen
%% TPARDIV Divide two transformation matrices
%
% G = TPARDIV(G1, G2) calculates G = G1 * inv(G2)
%
% Inputs:
%
%   G1                      7xN or 7x1 array of homogeneous transformations
%                           parameters.
% 
%   G2                      7x1 or 7xN array of homogeneous transformations
%                           parameters.
%
% Outputs:
%
%   G                       7xN array of homogeneous transformations.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% TPARDIV(G1, G2)
narginchk(2, 2);

% TPARDIV(___)
% G = TPARDIV(___)
nargoutchk(0, 1);



%% Algorithm

% Extract components from both transformation matrices
Q1 = tpar2quat(g1);
Q2 = tpar2quat(g2);

p1 = tpar2trvec(g1);
% Get second translation, but pad to a 4xN array with the first row being all
% zeros (turn it into a quaternion)
p2 = padarray(tpar2trvec(g2), 1, 0, 'pre');

% Q1 / Q2
Qr = quatdiv(Q1, Q2);

g = tpar( ...
    Qr ...
  , p1 + ...
      + hslice(quatmul(quatmul(Qr, -p2), quat2conj(Qr)), 1, 2:4) ...
);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
