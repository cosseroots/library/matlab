function p = tpar2trvec(g)%#codegen
%% TPAR2TRVEC
%
% P = TPAR2TRVEC(G)
%
% Inputs:
%
%   G                       7xN array of homogeneous transformations parameters.
%
% Outputs:
%
%   P                       3xN array of translation vectors.
%
% See also:
%   TRVEC2TPAR
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% TPAR2TRVEC(G)
narginchk(1, 1);

% TPAR2TRVEC(___)
% P = TPAR2TRVEC(___)
nargoutchk(0, 1);



%% Algorithm

% Extract
p = hslice(g, 1, 5:7);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
