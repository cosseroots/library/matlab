function q = quatrandn(varargin)%#codegen
%% QUATRANDN Create normally distributed random quaternions
%
% QUATRANDN() creates 1 normally distributed random quaternion.
%
% QUATRANDN(N) creates N normally distributed random quaternions.
%
% Inputs:
%
%   N                   Number of random quaternions to create.
%
% Outputs:
%
%   Q                   4xN array of N quaternions. Each quaternion represents
%                       a 3D rotation in form q = [w, x, y, z], with the scalar
%                       given as the first value w.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% QUATRANDN()
% QUATRANDN(N)
% QUATRANDN(S, N)
narginchk(0, 2);
% QUATRANDN(...)
% Q = QUATRANDN(...)
nargoutchk(0, 1);

% Retrieve a possibly passed `RANDSTREAM` object
[s, args, nargs] = objectcheck('RandStream', varargin{:});

% QUATRANDN()
% QUATRANDN(N)
if isempty(s)
  s = RandStream.getGlobalStream();
end

% Default arguments
n = 1;

% QUATRANDN(N)
if nargs > 0 && ~isempty(args{1})
  n = args{1};
  args(1) = [];
end



%% Create random quaternions

% We will first create random vectors in the range from [-1, 1] and then
% normalize all these
q = quatnormalized(2 * ( randn(s, 4, n, args{:}) - 0.5 ));


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
