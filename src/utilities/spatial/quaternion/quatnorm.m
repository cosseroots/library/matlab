function qn = quatnorm(q)%#codegen
%% QUATNORM Calculate norm of quaternion(s)
%
% Inputs:
%
%   Q                   4xN array of quaternions.
%
% Outputs:
%
%   QN                  1xN array of quaternion norms.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% QUATNORM(Q)
narginchk(1, 1);

% QUATNORM(Q)
% QN = QUATNORM(Q)
nargoutchk(0, 1);



%% Algorithm

% Calculate norm
qn = vecnorm(q, 2, 1);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
