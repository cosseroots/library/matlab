function eul = quat2eul(q, seq)%#codegen
%% QUAT2EUL Convert quaternion to Euler angles
%
% EUL = QUAT2EUL(Q, SEQ)
%
% Inputs:
%
%   Q                       4xN array of quaternions.
%
%   SEQ                     String defining the sequence to use for conversion.
%
% Outputs:
%
%   EUL                     3xN array of euler angles.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% QUAT2EUL(Q, SEQ)
narginchk(2, 2);

% QUAT2EUL(___)
% EUL = QUAT2EUL(___)
nargoutchk(0, 1);



%% Algorithm

% Normalize the quaternions
q = mnormcol(q);

% Extract components
qw = q(1,:);
qx = q(2,:);
qy = q(3,:);
qz = q(4,:);

% Pre-allocate output
eul = zeros(3, size(q, 2), 'like', q);

% The parsed sequence will be in all upper-case letters and validated
switch seq
    case 'ZYX'
        % Cap all inputs to asin to 1, since values >1 produce complex
        % results
        % Since the quaternion is of unit length, this should never happen,
        % but some code generation configuration seem to hit this edge case
        % under some circumstances.
        aSinInput = -2 * (qx .* qz - qw .* qy);
        aSinInput(aSinInput > +1) = +1;
        aSinInput(aSinInput < -1) = -1;
        
        eul = [ ...
          atan2( 2 * ( qx .* qy + qw .* qz ), qw .^ 2 + qx .^ 2 - qy .^ 2 - qz .^ 2 ) ; ...
          asin( aSinInput ) ; ...
          atan2( 2 * ( qy .* qz + qw .* qx ), qw .^ 2 - qx .^ 2 - qy .^ 2 + qz .^ 2 ) ; ...
        ];
        
    case 'ZYZ'
        % Need to convert to intermediate rotation matrix here to avoid
        % singularities
        eul = rotm2eul(quat2rotm(q), 'ZYZ');
        
    case 'XYZ'
        % Prevent singularities as done in ZYX case
        % Alternative to rotm2eul(quat2rotm(q), 'XYZ') with fewer
        % operations
        
        % sin(y) = R13 = 2 * (qx*qz + qy*qw)
        % tan(x) = sin(x) / cos(x) = -R23 / R33
        %        = -2 * (qy*qz - qx*qw) / (1 - 2*(qx^2 + qy^2))
        %        = -2 * (qy*qz - qx*qw) / (qw^2 - qx^2 - qy^2 + qz^2)
        % tan(z) = sin(z) / cos(z) = -R12 / R11
        %        = -2 * (qx*qy - qz*qw) / (1 - 2*(qy^2 + qz^2))
        %        = -2 * (qy*qz - qx*qw) / (qw^2 + qx^2 - qy^2 - qz^2)
        
        aSinInput = 2 * ( qx .* qz + qy .* qw);
        aSinInput(aSinInput > +1) = +1;
        aSinInput(aSinInput < -1) = -1;
        
        eul = [ ...
          atan2( -2 * ( qy .* qz - qx .* qw ), qw .^ 2 - qx .^ 2 - qy .^ 2 + qz .^ 2 ) ; ...
          asin( aSinInput ) ; ...
          atan2( -2 * (qx .* qy - qz .* qw), qw .^ 2 + qx .^ 2 - qy .^ 2 - qz .^ 2 ) ; ...
        ];
end

% Check for complex numbers
if ~isreal(eul)
  eul = real(eul);
end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
