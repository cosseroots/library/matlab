function qn = quatnormalized(q, pressign)%#codegen
%% QUATNORMALIZED Normalizes quaternion(s)
%
% QN = QUATNORMALIZED(Q) normalize quaternion to have vector norm equal to 1.
%
% Inputs:
%
%   Q                   4xN array of quaternions.
%
% Outputs:
%
%   QN                  4xN array of normalized quaternions.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% QUATNORMALIZED(Q)
% QUATNORMALIZED(Q, PRESERVESIGN)
narginchk(1, 2);

% QUATNORMALIZED(___)
% QN = QUATNORMALIZED(___)
nargoutchk(0, 1);

% QUATNORMALIZED(Q)
if nargin < 2 || isempty(pressign)
  pressign = false;
end



%% Algorithm

pressign = matlab.lang.OnOffSwitchState(pressign);

% Normalize quaternions i.e., columns
qn = mnormcol(q);
% Per convention, always keep scalar quaternion elements positive (if not
% requested otherwise by the user)
if pressign == matlab.lang.OnOffSwitchState.off
  qn(:,qn(1,:) < 0) = -qn(:,qn(1,:) < 0);
end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
