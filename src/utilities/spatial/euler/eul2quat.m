function q = eul2quat(eul, seq)%#codegen
%% EUL2QUAT Convert Euler angles to quaternion
%
% Q = EUL2QUAT(EUL, SEQ)
%
% Inputs:
%
%   EUL                     3xN array of euler angles.
%
%   SEQ                     String defining the sequence to use for conversion.
%
% Outputs:
%
%   Q                       4xN array of quaternions.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% EUL2QUAT(EUL, SEQ)
narginchk(2, 2);

% EUL2QUAT(___)
% Q = EUL2QUAT(___)
nargoutchk(0, 1);



%% Algorithm

% Pre-allocate output
q = zeros(4, size(eul, 2), 'like', eul);

% Compute sines and cosines of half angles
c = cos(eul / 2);
s = sin(eul / 2);

% Construct quaternion
switch upper(seq)
  case 'ZYX'
    q = [ ...
      c(1,:) .* c(2,:) .* c(3,:) + s(1,:) .* s(2,:) .* s(3,:) ; ...
      c(1,:) .* c(2,:) .* s(3,:) - s(1,:) .* s(2,:) .* c(3,:) ; ...
      c(1,:) .* s(2,:) .* c(3,:) + s(1,:) .* c(2,:) .* s(3,:) ; ...
      s(1,:) .* c(2,:) .* c(3,:) - c(1,:) .* s(2,:) .* s(3,:) ; ...
    ];
    
  case 'ZYZ'
    q = [ ...
      c(1,:) .* c(2,:) .* c(3,:) - s(1,:) .* c(2,:) .* s(3,:) ; ...
      c(1,:) .* s(2,:) .* s(3,:) - s(1,:) .* s(2,:) .* c(3,:) ; ...
      c(1,:) .* s(2,:) .* c(3,:) + s(1,:) .* s(2,:) .* s(3,:) ; ...
      s(1,:) .* c(2,:) .* c(3,:) + c(1,:) .* c(2,:) .* s(3,:) ; ...
    ];
    
  case 'XYZ'
    q = [ ...
       c(1,:) .* c(2,:) .* c(3,:) - s(1,:) .* s(2,:) .* s(3,:) ; ...
       s(1,:) .* c(2,:) .* c(3,:) + c(1,:) .* s(2,:) .* s(3,:) ; ...
      -s(1,:) .* c(2,:) .* s(3,:) + c(1,:) .* s(2,:) .* c(3,:) ; ...
       c(1,:) .* c(2,:) .* s(3,:) + s(1,:) .* s(2,:) .* c(3,:) ; ...
    ];
  
end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
