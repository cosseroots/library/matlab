function T = eul2tform(eul, seq)%#codegen
%% EUL2TFORM Convert Euler angles to homogeneous transformation
%
% T = EUL2ROTM(EUL, SEQ)
%
% Inputs:
%
%   EUL                     3xN array of euler angles.
%
%   SEQ                     String defining the sequence to use for conversion.
%
% Outputs:
%
%   R                       4x4xN array of homogeneous transformations.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% EUL2TFORM(EUL, SEQ)
narginchk(2, 2);

% EUL2TFORM(___)
% T = EUL2TFORM(___)
nargoutchk(0, 1);



%% Algorithm

% This is a two-step process.
% 1. Convert the Euler angles into a rotation matrix
% 2. Convert the rotation matrix into a homogeneous transform
T = rotm2tform(eul2rotm(eul, seq));


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
