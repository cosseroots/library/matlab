function T = rotm2tform(R)%#codegen
%% ROTM2TFORM Convert rotation matrix to homogeneous transform
%
% T = ROTM2TFORM(R) converts 3D rotation matrix R into homogeneous
% transformation matrix T.
%
% Inputs:
%
%   R                       3x3xN array of orthonormal rotation matrices.
%
% Outputs:
%
%   T                       4x4xN array of homogeneous transformations.
%
% See also:
%   TFORM2ROTM
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% ROTM2TFORM(R)
narginchk(1, 1);

% ROTM2TFORM(___)
% T = ROTM2TFORM(___)
nargoutchk(0, 1);



%% Algorithm

% Count rotations
nr = size(R, 3);

% Allocate and then push in
T = zeros(4, 4, nr, 'like', R);
T(1:3,1:3,:) = R;
T(4,4,:) = ones(1, 1, nr, 'like', R);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
