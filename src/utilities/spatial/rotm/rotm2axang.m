function axang = rotm2axang(R)%#codegen
%% ROTM2AXANG(R) Convert rotation matrix to axis-angle representation
%
% AXANG = ROTM2AXANG(R) converts rotation matrix R to its axis-angle
% representation.
%
% Inputs:
%
%   R                       3x3xN array of orthonormal rotation matrices.
%
% Outputs:
%
%   AXANG                   4xN array of axis angles where the first three
%                           elements are the axis of rotation and the last
%                           element defines the amount of rotation (in radians).
%
% See also
%   AXANG2ROTM
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% ROTM2AXANG(R)
narginchk(1, 1);

% ROTM2AXANG(___)
% AXANG = ROTM2AXANG(___)
nargoutchk(0, 1);



%% Algorithm

% Compute angle
th = real(acos(complex( 0.5 * ( R(1,1,:) + R(2,2,:) + R(3,3,:) - 1 ) )));

% Determine initial axis vectors from theta
v = [ ...
  R(3,2,:) - R(2,3,:) ; ...
  R(1,3,:) - R(3,1,:) ; ...
  R(2,1,:) - R(1,2,:) ; ...
] ./ repmat( 2 * sin(th), [3, 1, 1]);

% Handle the degenerate cases where theta is divisible by pi or when the axis
% consists of all zeros
singth  = mod(th, cast(pi, 'like', R)) == 0 | all(v == 0, 1);
nsingth = sum(singth, 3);

if nsingth > 0
    vspecial = zeros(3, nsingth, 'like', R);
    inds = find(singth);
    
    for i = 1:nsingth
        [~, ~, V] = svd(eye(3) - R(:,:,inds(i)));
        vspecial(:,i) = V(:,1);
    end
    v(:,1,singth) = vspecial;
end

% Extract final values
th = permute(th, [1, 3, 2]);
v  = mnormcol(permute(v, [1, 3, 2]));

% And concatenate
axang = cat(1, v, th);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.

