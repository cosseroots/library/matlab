function R = rotmones(n)%#codegen
%% ROTMONES
%
% R = ROTMONES() creates 1 unit rotation matrix. A unit rotation matrix is
% defined as a rotation of one unit about the normalized vector [ 1 ; 1 ; 1].
%
% Inputs:
%
%   N                       Scalar number of how many rotation matrices to
%                           create.
%
% Outputs:
%
%   R                       3x3xN array of unit rotation matrices.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% ROTMONES()
% ROTMONES(N)
narginchk(0, 1);

% ROTMONES(___)
% R = ROTMONES(___)
nargoutchk(0, 1);

% ROTMONES()
if nargin < 1 || isempty(n)
  n = 1;
end



%% Algorithm

% Calculate the corresponding axis-angle, then convert it into a rotation matrix
R = axang2rotm(axangones(n));


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
