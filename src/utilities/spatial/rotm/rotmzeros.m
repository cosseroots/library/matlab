function R = rotmzeros(n)%#codegen
%% ROTMZEROS
%
% R = ROTMZEROS() creates 1 zero rotation matrix. A "zero rotation matrix" is
% defined as a unit matrix i.e., no rotation.
%
% R = ROTMZEROS(N) creates N such zero rotation matrices.
%
% Inputs:
%
%   N                       Scalar number of how many rotation matrices to
%                           create.
%
% Outputs:
%
%   R                       3x3xN array of zero rotation matrices.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% ROTMZEROS()
% ROTMZEROS(N)
narginchk(0, 1);

% ROTMZEROS(___)
% R = ROTMZEROS(___)
nargoutchk(0, 1);

% ROTMZEROS()
if nargin < 1 || isempty(n)
  n = 1;
end



%% Algorithm

% As per defintion, stack N unit matrices
R = repmat(eye(3, 3), [1, 1, n]);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
