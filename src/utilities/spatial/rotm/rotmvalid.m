function flag = rotmvalid(R)%#codegen
%% ROTMVALID Check if given rotation matrix/matrices are valid
%
% FLAG = ROTMVALID(R) checks if rotation matrix R is valid i.e., if its first
% two dimensions are equal to 3, its trace is equal to 1, and its determinant
% equal to 1.
%
% Inputs:
%
%   R                       3x3xN array of rotation matrices.
%
% Outputs:
%
%   FLAG                    1xN logical array of validity of R(:,:,i) being
%                           valid rotation matrix.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% ROTMVALID(R)
narginchk(1, 1);

% FLAG = ROTMVALID(___)
nargoutchk(0, 1);

% Dimensions' indices of R
dr = 3:ndims(R);

% Number of pages etc.
nr = size(R, dr);



%% Algorithm

% Check each page and higher dimensions is 3x3 in size
flag = all([ 3 , 3 ] == size(R, [ 1 , 2 ])) ...
  ... Check each page and higher dimensions that R^T * R = I
    & permute(all(isclose(pagemult(R, permute(R, [ 2 , 1 , dr ])), repmat(eye(3, 3), [ 1 , 1 , nr ])), [ 1 , 2 ]), [ 1 , dr , 2 ]);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
