function R = rotmrand(varargin)%#codegen
%% ROTMRAND
%
% R = ROTMRAND() creates 1 uniformly distributed random rotation matrices.
%
% R = ROTMRAND(N) creates N uniformly distributed random rotation matrices.
%
% Inputs:
%
%   N                       Scalar number of how many rotation matrices to
%                           create.
%
% Outputs:
%
%   R                       3x3xN array of random rotation matrices.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% ROTMRAND()
% ROTMRAND(N)
% ROTMRAND(S)
% ROTMRAND(S, N)
narginchk(0, 2);

% ROTMRAND(___)
% R = ROTMRAND(___)
nargoutchk(0, 1);

% Retrieve a possibly passed `RANDSTREAM` object
[s, args, nargs] = objectcheck('RandStream', varargin{:});

% ROTMRAND()
% ROTMRAND(N)
if isempty(s)
  s = RandStream.getGlobalStream();
end

% Default arguments
n = 1;

% ROTMRAND(N)
if nargs > 0 && ~isempty(args{1})
  n = args{1};
  args(1) = [];
end



%% Algorithm

rr = rand(s, 3, 3, n, args{:});
R = zeros(3, 3, n, args{:});

% Loop over each random rotation matrix and normalize it
for in = 1:n
  [R_, ~] = qr(rr(:,:,in));
  R_(:,1) = R_(:,1) * ( 2 * ( rand(s) > 0.5 ) - 1);
  R_(:,2) = det(R_) * R_(:,2);
  
  R(:,:,in) = R_;
  
end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
