function g = rotm2tpar(R)%#codegen
%% ROTM2TPAR
%
% G = ROTM2TPAR(R)
%
% Inputs:
%
%   R                       3x3xN array of rotation matrices.
%
% Outputs:
%
%   G                       7xN array of homogeneous transformations parameters.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% ROTM2TPAR(R)
narginchk(1, 1);

% ROTM2TPAR(___)
% G = ROTM2TPAR(___)
nargoutchk(0, 1);



%% Algorithm

% Dispatch
g = quat2tpar(rotm2quat(R));


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
