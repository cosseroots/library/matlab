function Fb = transformForce(Fa, Ta, Tb)%#codegen
%% TRANSFORMFORCE Transform forces from one frame to another
%
% FB = TRANSFORMFORCE(FA, TA, TB) transforms spatial force FA expressed in TA
% into spatial force FB expressed in TB.
%
% Inputs:
%
%   FA                      6xN array of spatial forces expressed in TA.
% 
%   TA                      4x4xN array of source frame(s) of TA.
% 
%   TB                      4x4xN array of target frame(s) in which to express
%                           FA.
%
% Outputs:
%
%   FB                      6xN array of spatial forces FB expressed in TB.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% TRANSFORMFORCE(FA, TA)
% TRANSFORMFORCE(FA, TA, TB)
narginchk(2, 3);

% TRANSFORMFORCE(___)
% FB = TRANSFORMFORCE(___)
nargoutchk(0, 1);



%% Algorithm

% Fb = Ad(Tab)^T * Fb;
Fb = permute( ...
    pagemult( ...
        permute( ...
            Ad( ...
                tformmul( ...
                    tform2inv(Ta) ...
                  , Tb ...
                ) ...
              ) ...
            , [2, 1, 3] ...
          ) ...
        , permute( ...
            Fa ...
          , [1, 3, 2] ...
        ) ...
      ) ...
    , [1, 3, 2] ...
  );


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
