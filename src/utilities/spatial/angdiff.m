function delta = angdiff(x, y)%#codegen
%% ANGDIFF Calculate difference between angles
%
% DELTA = ANGDIFF(X, Y) calculates the angular difference DELTA between angles X
% and Y calculated through DELTA = Y - X.
%
% Inputs:
%
%   X                       NxMx...xK array of angles.
%
%   Y                       NxMx...xK array of angles.
%
% Outputs:
%
%   DELTA                   Nx(M-1)x...xK array of angle differences.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% ANGDIFF(X, Y)
narginchk(2, 2);

% ANGDIFF(___)
% DELTA = ANGDIFF(___)
nargoutchk(0, 1);



%% Algorithm

% Just the differences wrapped onto [-pi, +pi]
delta = wraptopi(y - x);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
