function Vb = transformTwist(Va, Ta, Tb)%#codegen
%% TRANSFORMTWIST Transform twist from one frame to another
%
% VB = TRANSFORMTWIST(VA, TA, TB) transform twist VA expressed in TA into twist
% VB expressed in TB.
%
% Inputs:
%
%   VA                      6xN array of twists expressed in TA.
% 
%   TA                      4x4xN array of source frame(s) of TA.
% 
%   TB                      4x4xN array of target frame(s) in which to express
%                           VA.
%
% Outputs:
%
%   VB                      6xN array of twists VB expressed in TB.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% TRANSFORMTWIST(VA, TA)
% TRANSFORMTWIST(VA, TA, TB)
narginchk(2, 3);

% TRANSFORMTWIST(___)
% VB = TRANSFORMTWIST(___)
nargoutchk(0, 1);



%% Algorithm

% Vb = Ad(Tba) * Va
Vb = permute( ...
    pagemult( ...
        Ad( ...
            tformmul( ...
                tform2inv(Tb) ...
              , Ta ...
            ) ...
          ) ...
        , permute( ...
            Va ...
          , [1, 3, 2] ...
        ) ...
      ) ...
    , [1, 3, 2] ...
  );


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
