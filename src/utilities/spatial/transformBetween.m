function Tab = transformBetween(Ta, Tb)%#codegen
%% TRANSFORMBETWEEN Calculates the relative transformation between two transformations
%
% TAB = TRANSFORMBETWEEN(TA, TB) calculates the relative transformation TAB between
% transformation TA and TB such that TAB takes TA to TB thus satisfying the
% equation TAB = TAi^{-1} * TB.
%
% Inputs:
%
%   TA                      4x4xN array of transformations that shall be
%                           expressed with respect to TB.
% 
%   TB                      4x4xN array of transformations of the new frame of
%                           references.
%
% Outputs:
%
%   TAB                     4x4xN array of relative transformations between TA
%                           and TB as TAB = TA^{-1} * TB
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% TRANSFORMBETWEEN(TA, TB)
narginchk(2, 2);

% TRANSFORMBETWEEN(___)
% TAB = TRANSFORMBETWEEN(___)
nargoutchk(0, 1);



%% Algorithm

Tab = tformmul(tform2inv(Ta), Tb);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
