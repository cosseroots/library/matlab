function d = delta(ind, dim)%#codegen
%% DELTA Create N-dim zero vector with a single 1 (one).
%
% D = DELTA(IND, DIM) creates a DIM-dimensional vector D where the IND-th
% entries are 1.
%
% D = DELTA(DIM, IND) same behavior as above, however, a different interface
% that may be more consistent with human language or some mathematicians
% thinking.
%
% Inputs:
%
%   IND                     Kx1 vector of (row)-indices that should be 1.
%
%   DIM                     Scalar value defining the number of rows of D.
%
% Outputs:
%
%   D                       Nx1 with 1's in the IND's rows.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% DELTA(IDX, DIM)
narginchk(2, 2);

% DELTA(___)
nargoutchk(0, 1);



%% Algorithm

% Add support for `DELTA(6, 1)`
if any(ind > dim)
  [ind, dim] = deal(dim, ind);
end

d = zeros(dim, 1);
% And set the IND-th entry to 1
d(ind) = 1;


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
