function v = wrapto2pi(v)%#codegen
%% WRAPTO2PI Wrap angle in radians to [0, 2pi]
%
% V = WRAPTO2PI(V) wraps angles in V to range [0, 2pi]
%
% Inputs:
%
%   V                       NxMx...xK array of values to wrap.
%
% Outputs:
%
%   V                       NxMx...xK array of wrapped values.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% WRAPTO2PI(V)
narginchk(1, 1);

% WRAPTO2PI(___)
% V = WRAPTO2PI(___)
nargoutchk(0, 1);



%% Algorithm

positiveInput = (v > 0);
v = mod(v, 2 * pi);
v((v == 0) & positiveInput) = 2 * pi;


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
