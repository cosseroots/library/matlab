function v = wraptopi(v)%#codegen
%% WRAPTOPI Wrap angle in radians to [-pi , +pi]
%
% V = WRAPTOPI(V) wraps angles in V to range [-pi , +pi];
%
% Inputs:
%
%   V                       NxMx...xK array of values to wrap.
%
% Outputs:
%
%   V                       NxMx...xK array of wrapped values.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% WRAPTOPI(V)
narginchk(1, 1);

% WRAPTOPI(___)
% V = WRAPTOPI(___)
nargoutchk(0, 1);



%% Algorithm

q = (v < -pi) | (pi < v);
v(q) = wrapto2pi(v(q) + pi) - pi;


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
