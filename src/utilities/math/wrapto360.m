function v = wrapto360(v)%#codegen
%% WRAPTO360 Wrap angle in degrees to [0, 360]
%
% V = WRAPTO360(V) wraps angles in V to range [0, 360]
%
% Inputs:
%
%   V                       NxMx...xK array of values to wrap.
%
% Outputs:
%
%   V                       NxMx...xK array of wrapped values.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% WRAPTO360(V)
narginchk(1, 1);

% WRAPTO360(___)
% V = WRAPTO360(___)
nargoutchk(0, 1);



%% Algorithm

positiveInput = (v > 0);
v = mod(v, 360);
v((v == 0) & positiveInput) = 360;


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
