function v = wrapto180(v)%#codegen
%% WRAPTO180 Wrap angle in radians to [-180 , +180]
%
% V = WRAPTO180(V) wraps angles in V to range [-180 , +180];
%
% Inputs:
%
%   V                       NxMx...xK array of values to wrap.
%
% Outputs:
%
%   V                       NxMx...xK array of wrapped values.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% WRAPTO180(V)
narginchk(1, 1);

% WRAPTO180(___)
% V = WRAPTO180(___)
nargoutchk(0, 1);



%% Algorithm

q = (v < -180) | (pi < 180);
v(q) = wrapto360(v(q) + 180) - 180;


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
