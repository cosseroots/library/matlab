function smms(varargin)
%% SMMS
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% SMMS()
% SMMS(AX)
narginchk(0, 1);

% SMMS(___)
nargoutchk(0, 0);

hax = [];
if nargin > 0 && isa(varargin{1}, 'matlab.graphics.axis.Axes')
  hax = varargin{1};
  varargin(1) = [];
end



%% Prepare axis

% Get correct axes handle
if isempty(hax)
  hax = gca();
end

% Check old hold status and make sure we are adding to the axes
if ~ishold(hax)
  hold(hax, 'on');
  coRelease = onCleanup(@() hold(hax, 'off'));
end
% hf = ancestor(hax, 'figure');



%% Axes

% Adjust all axes
viz.style.axes(hax);

% Axes labels
haxx = [ hax.XAxis ];
haxy = [ hax.YAxis ];
haxz = [ hax.ZAxis ];
set( ...
  [ haxx.Label ] ...
  , 'String'  , 'Horizontal $ x / \mathrm{m} $' ...
);
set( ...
  [ haxy.Label ] ...
  , 'String'  , 'Distal $ y / \mathrm{m} $' ...
);
set( ...
  [ haxz.Label ] ...
  , 'String'  , 'Vertical $ z / \mathrm{m} $' ...
);



%% General layout

set( ...
    hax ...
  , 'DataAspectRatio' , [ 1 , 1 , 1 ] ...
  , 'Clipping'        , 'off' ...
  , 'View'            , [ 40 , 10 ] ...
);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
