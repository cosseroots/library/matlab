function video(varargin)
%% VIDEO
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% VIDEO()
% VIDEO(AX)
narginchk(0, 1);

% VIDEO(___)
nargoutchk(0, 0);

hax = [];
if nargin > 0 && isa(varargin{1}, 'matlab.graphics.axis.Axes')
  hax = varargin{1};
  varargin(1) = [];
end



%% Prepare axis

% Get correct axes handle
if isempty(hax)
  hax = gca();
end

% Check old hold status and make sure we are adding to the axes
if ~ishold(hax)
  hold(hax, 'on');
  coRelease = onCleanup(@() hold(hax, 'off'));
end
% Get parent figure(s)
hf = unique(ancestor(hax, 'figure'));



%% General layout

% By default, take the SMMS style
viz.style.smms(hax);

% Adjust all axes
viz.style.axes(hax);

% General aspects
set( ...
    hax ...
  , 'DataAspectRatio' , [ 1 , 1 , 1 ] ...
  , 'Color'           , [ 1 , 1 , 1 ] ...
  , 'Clipping'        , 'off' ...
  , 'Projection'      , 'perspective' ...
  , 'View'            , [ 40 , 10 ] ...
  , 'Box'             , 'off' ...
  , 'XGrid'           , 'off' ...
  , 'YGrid'           , 'off' ...
  , 'ZGrid'           , 'off' ...
);

% Figure color
set( ...
    hf ...
  , 'Color', [ 1 , 1 , 1 ] ...
);



%% Styling of Plot Axes X, Y, Z

% Get X, Y, and Z axis separately
haxx = [ hax.XAxis ];
haxy = [ hax.YAxis ];
haxz = [ hax.ZAxis ];

% Color of each axis
set( ...
    [ haxx , haxy , haxz ]...
  , 'Color'  , 'none' ...
);



%% 3D Visualization Stuff

% Remove all edge colors from PATCH and SURF objects
set( ...
    findobj(hax, '-property', 'EdgeColor') ...
  , 'EdgeColor' , 'none' ...
);

% Create a camera light
hc = camlight(hax, 'right');
% Attach an event listener to the axes to always move the camera light with
% the camera
addlistener(hax, 'View', 'PostSet', @(src, evt) camlight(hc, 'right'));
addlistener(hax, 'CameraPosition', 'PostSet', @(src, evt) camlight(hc, 'right'));

% Set `SHINY` material for PATCH and SURFACE objects
hp = findobj(hax, 'type', 'patch');
hs = findobj(hax, 'type', 'surface');
material(hp, 'shiny');
material(hs, 'shiny');


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
