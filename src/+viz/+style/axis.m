function axis(name, varargin)
%% AXIS Beautify a single axis in a plot
%
% AXIS(NAME) makes axes NAME in the current axes beautiful.
%
% AXIS(AX, NAME) uses axes handle AX instead to make axes NAME beautiful.
%
% Inputs:
%
%   NAME                    Name of axes to make beautiful. Must be any of
%                           (case-insensitive)
%                             X
%                             Y
%                             Z
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% AXIS(NAME)
% AXIS(AX, ___)
narginchk(1, 2);

% AXIS(___)
nargoutchk(0, 0);

hax = [];
if nargin > 1 && isa(name, 'matlab.graphics.axis.Axes')
  hax = name;
  name = varargin{1};
  varargin(1) = [];
end



%% Prepare Axes

% Get correct axes handle
if isempty(hax)
  hax = gca();
end

% Check old hold status and make sure we are adding to the axes
if ~ishold(hax)
  hold(hax, 'on');
  coRelease = onCleanup(@() hold(hax, 'off'));
end

% Get axis handle
f = [ upper(name) , 'Axis' ];
ax = [ hax.(f) ];



%% Axes Properties

set( ...
    ax ...
  ... 
  ... % General stuff
  , 'FontSize'            , 12 ....
  ... 
  ... % Major ticks
  , 'TickLabelInterpreter', 'latex' ...
  , 'TickValuesMode'      , 'auto' ...
  ...
  ... % Minor ticks
  , 'MinorTick'           , 'on' ...
  , 'MinorTickValuesMode' , 'auto' ...
);



%% Label Properties

set( ...
    [ ax.Label ] ...
  , 'Interpreter' , 'latex' ...
  , 'FontSize'    , 12 ...
);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
