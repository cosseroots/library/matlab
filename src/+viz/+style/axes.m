function axes(varargin)
%% AXES
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% AXES()
% AXES(AX)
narginchk(0, 1);

% AXES(___)
nargoutchk(0, 0);

hax = [];
if nargin > 0 && isa(varargin{1}, 'matlab.graphics.axis.Axes')
  hax = varargin{1};
  varargin(1) = [];
end



%% Prepare Axes

% Get correct axes handle
if isempty(hax)
  hax = gca();
end

% Check old hold status and make sure we are adding to the axes
if ~ishold(hax)
  hold(hax, 'on');
  coRelease = onCleanup(@() hold(hax, 'off'));
end



%% General Styles

% Style each axis
viz.style.axis(hax, 'x');
viz.style.axis(hax, 'y');
viz.style.axis(hax, 'z');



%% Limits and Interpreters

set( ...
  hax ...
  , 'XLimitMethod', 'padded' ...
  , 'YLimitMethod', 'padded' ...
  , 'ZLimitMethod', 'padded' ...
);

% Set all text interpreters to be the same
textinterpreters(hax, 'latex');



%% Title

set( ...
    [ hax.Title ] ...
  , 'FontSize'    , 16 ...
  , 'Interpreter' , 'latex' ...
);



%% Subtitle

set( ...
    [ hax.Subtitle ] ...
  , 'FontSize'    , 14 ...
  , 'Interpreter' , 'latex' ...
);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
