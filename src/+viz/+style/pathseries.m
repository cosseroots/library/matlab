function pathseries(varargin)
%% PATHSERIES
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% PATHSERIES()
% PATHSERIES(AX)
narginchk(0, 1);

% PATHSERIES(___)
nargoutchk(0, 0);

hax = [];
if nargin > 0 && isa(varargin{1}, 'matlab.graphics.axis.Axes')
  hax = varargin{1};
  varargin(1) = [];
end



%% Prepare Axes

% Get correct axes handle
if isempty(hax)
  hax = gca();
end

% Check old hold status and make sure we are adding to the axes
if ~ishold(hax)
  hold(hax, 'on');
  coRelease = onCleanup(@() hold(hax, 'off'));
end



%% Algorithm

% Style all axes
viz.style.axes(hax);

% Ensure X-Axis is tight
set( ...
    hax ...
  , 'XLimitMethod', 'tight' ...
);

% Set X-Axis label
haxx = [ hax.XAxis ];
set( ...
    [ haxx.Label ] ...
  , 'String', 'Path $ \xi / \mathrm{ m } $' ...
);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
