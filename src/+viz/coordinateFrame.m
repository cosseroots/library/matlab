function hp  = coordinateFrame(varargin)
%% COORDINATEFRAME Draw a coordinate frame
%
% COORDINATEFRAME() draw a default coordinate frame.
%
% COORDINATEFRAME(R) draw coordinate frame with axes directions defined through
% columns of R.
%
% COORDINATEFRAME(R, P) draw coordinate frame at position P.
%
% COORDINATEFRAME(AX, ___) draws coordinate frame into the given axes object AX.
%
% Inputs:
%
%   R                       3xN array representing the orientation of the
%                           coordinate frame. Each column is an axes. In the
%                           typical use cases, the columns represent the [X,Y,Z]
%                           axes.
%                           Default value: eye(3, 3)
% 
%   P                       3x1 vector of position of the coordinate frame.
%
% Outputs:
%
%   HP                      Nx1 graphics object array containg the plotted
%                           coordinate frame axes.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% COORDINATEFRAME()
% COORDINATEFRAME(R)
% COORDINATEFRAME(R, P)
% COORDINATEFRAME(AX, ___)
narginchk(0, Inf);

% COORDINATEFRAME(___)
% HP = COORDINATEFRAME(___)
nargoutchk(0, 1);

% Split axes object from list of arguments
[ax, args, nargs] = axescheck(varargin{:});

% Default input arguments
R = [];
p = [];

% COORDINATEFRAME(R, ...)
if nargs > 0
  [R, args, nargs] = deal(args{1}, args(2:end), nargs - 1);
end
if isempty(R)
  R = eye(3, 3);
end

% COORDINATEFRAME(R, P, ...)
if nargs > 0
  [p, args, nargs] = deal(args{1}, args(2:end), nargs - 1);
end
if isempty(p)
  p = zeros(3, 1);
end

% Get axes ojbect if none given
if isempty(ax)
  ax = newplot(ax);
end

% Ensure we add to the axes object
if ~ishold(ax)
  hold(ax, 'on');
  coHold = onCleanup(@() hold(ax, 'off'));
end

% Default options
dopts = struct( ...
    'AxisStyle', {{}} ...
  , 'Axis1Style', {{}} ...
  , 'Axis2Style', {{}} ...
  , 'Axis3Style', {{}} ...
);

% User-provided options as structure
uopts = nvpairs2struct(args{:});



%% Algorithm

% Count number of coordinate axes
nc = size(R, 2);

% Common style for all axes
cstyle = { ...
    'LineStyle' , '-' ...
  , 'LineWidth' , 1.50 ...
};
% Style for each axis
lstyles = { ...
    'Color' , color('red') ...
  , 'Tag'   , 'E1' ; ...
    'Color' , color('green') ...
  , 'Tag'   , 'E2' ; ...
    'Color' , color('blue') ...
  , 'Tag'   , 'E3' ; ...
};

% User-defined common style for all axes
ucstyle = optsget(uopts, 'AxisStyle', dopts.AxisStyle);

% Initialize output
hp_ = gobjects(nc, 1);

% Loop over drawing every coordinate axes
for ic = nc:-1:1
  % Retrieve user-defined styles from the options
  ustyle = optsget(uopts, sprintf('Axis%.0fStyle', ic), dopts.(sprintf('Axis%.0fStyle', ic)));
  
  % Plot axis
  hp_(ic) = plot3( ...
      ax ...
    , p(1) + [ 0 , R(1,ic) ], p(2) + [ 0 , R(2,ic) ], p(3) + [ 0 , R(3,ic) ] ...
    , cstyle{:} ...
    , lstyles{ic,:} ...
    , ucstyle{:} ...
    , ustyle{:} ...
  );
  % Remove icon display of this line in any possible legends added
  hp_(ic).Annotation.LegendInformation.IconDisplayStyle = 'off';
  
end



%% Assign output quantities

% HP = COORDINATEFRAME(___)
if nargout > 0
  hp = hp_;
end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged.
