classdef SimpleSimulationProgress < SimulationProgress
  %% SIMPLESIMULATIONPROGRESS
  
  
  
  %% NONTUNABLE PUBLIC PROPERTIES
  properties ( Nontunable )
    
    NumBlocks (1, 1) double { mustBePositive , mustBeFinite } = 5
    
    NumDots   (1, 1) double { mustBePositive , mustBeFinite } = 10
    
  end
  
  
  
  %% INTERNAL NONTUNABLE PROPERTIES
  properties ( Nontunable , Access = protected )
    
    DisplayWidth
    
  end
  
  
  
  %% INTERNAL PROPERTIES
  properties ( Access = protected )
    
    Counter = 0
    
  end
  
  
  
  %% CONCRETE IMPLEMENTATION METHODS
  methods ( Access = protected )
    
    function setupImpl(obj)
      %% SETUPIMPL
      
      
      obj.DisplayWidth = obj.NumBlocks * obj.NumDots;
      
    end
    
    
    function resetImpl(obj)
      %% RESET
      
      
      obj.Counter = 0;
      
    end
    
    
    function releaseImpl(obj)
      %% RELEASEIMPL
      
      
      fprintf('\n');
      
    end
    
    
    function stepImpl(obj)
      %% STEPIMPL
      
      
      cnt  = obj.Counter;
      
      fprintf('.');
      
      cnt = cnt + 1;
      
      if mod(cnt, obj.DisplayWidth) == 0
        fprintf('\n');
        
      elseif mod(cnt, obj.NumDots) == 0
        fprintf(' ');
        
      end
      
      obj.Counter = cnt;
      
    end
    
    
    function s = saveObjectImpl(obj)
      %% SAVEOBJECTIMPL
      
      
      
      s = saveObjectImpl@matlab.System(obj);
      
      if isLocked(obj)
        s.Counter   = obj.Counter;
      end
      
    end
    
    
    function loadObjectImpl(obj, s, wasInUse)
      %% LOADOBJECTIMPL
      
      
      
      if wasInUse
        obj.Counter   = s.Counter;
      end
      
      loadObjectImpl@matlab.System(obj, s, wasInUse);
      
    end
    
  end
  
end
