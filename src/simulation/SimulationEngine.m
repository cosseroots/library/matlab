classdef SimulationEngine < matlab.System ...
    & matlab.system.mixin.FiniteSource ...
    & smms.internal.InternalAccess
  %% SIMULATIONENGINE
  
  
  
  %% PUBLIC PROPERTIES
  properties ( Nontunable )
    
    % Dynamics solver object
    Solver
    
    % Kinematic tree used in the engine
    SMMSTree
    
    % Actuation object
    Actuation (1, 1) Actuation = Actuation()
    
    % Environment providing external forces
    Environment (1, 1) Environment = Environment()
    
    % Time span of integration
    TimeSpan (1, 2) double { mustBeFinite } = [ 0.0 , 1.0 ]
    
    % Step size of integration
    StepSize (1, 1) double { mustBePositive , mustBeFinite }  = 50 * 1e-3
    
    % Simulation progress object
    Progress (1, 1) SimulationProgress = SimpleSimulationProgress()
    
    % Initial states of simulation
    Q0
    Q0dot
    Q0ddot
    
  end
  
  
  
  %% BACKWARDS COMPATABILITY PROPERTIES
  properties ( Dependent )
    
    Tree
    
  end
  
  
  
  %% INTERNAL NONTUNABLE PROPERTIES
  properties ( Nontunable )
    
    Signals
    
  end
  
  
  
  %% SMMS INTERNAL PROPERTIES
  properties ( Access = { ?matlab.System , ?smms.internal.InternalAccess } )
    
    % Number of time steps to perform
    MaxTimeStep
    
    % Current time step
    CurrentIndex
    
    % Flag whether forward-in-time solver requests to stop simulation or not
    SolverStop (1, 1) logical = false
    
    % Size of data chunks to append to Q, QDOT, QDDOT when data limit is reached
    ChunkSize (1, 1) double
    
    % Array of times per time integration step
    ExecutionTimes_
    
    % Cell array of statistics per time step as provided by the underlying
    % solver object
    Statistics_
    
  end
  
  
  
  %% PUBLIC DISCRETE STATES
  properties ( DiscreteState )
    
    Time
    
    Q
    
    Qdot
    
    Qddot
    
  end
  
  
  
  %% CONSTRUCTOR METHODS
  methods
    
    function obj = SimulationEngine(varargin)
      %% SIMULATIONENGINE
      
      
      % Set default actuation and environment objects
      obj.Actuation   = Actuation();
      obj.Environment = Environment();
      obj.Progress    = SimpleSimulationProgress();
      
      setProperties(obj, nargin, varargin{:});
      
    end
    
  end
  
  
  
  %% SETTERS
  methods
    
    function set.Actuation(obj, v)
      %% SET.ACTUATION
      
      
      if isa(obj.Actuation, 'smms.mixin.Parentable')
        parent(obj.Actuation, []);
      end
      
      if isa(v, 'smms.mixin.Parentable')
      	parent(v, obj);
      end
      
      obj.Actuation = v;
      
    end
    
    
    function set.Progress(obj, v)
      %% SET.PROGRESS
      
      
      if isa(obj.Progress, 'smms.mixin.Parentable')
        parent(obj.Progress, []);
      end
      
      if isa(v, 'smms.mixin.Parentable')
        parent(v, obj);
      end
      
      obj.Progress = v;
      
    end
    
    
    function set.Environment(obj, v)
      %% SET.ENVIRONMENT
      
      
      if isa(obj.Environment, 'smms.mixin.Parentable')
        parent(obj.Environment, []);
      end
      
      if isa(v, 'smms.mixin.Parentable')
      	parent(v, obj);
      end
      
      obj.Environment = v;
      
    end
    
    
    function set.SMMSTree(obj, v)
      %% SET.SMMSTREE
      
      
      validateattributes(v, { 'SMMS' }, { 'nonempty' }, 'set.SMMSTree', 'Tree');
      
      obj.SMMSTree = v;
      
    end
    
    
    function set.Solver(obj, v)
      %% SET.SOLVER
      
      
      validateattributes(v, { 'Solver' }, { 'nonempty' }, 'set.Solver', 'Solver');
      
      if isa(obj.Solver, 'smms.mixin.Parentable')
        parent(obj.Solver, []);
      end
      
      if isa(v, 'smms.mixin.Parentable')
      	parent(v, obj);
      end
      
      obj.Solver = v;
      
    end
    
    
    function set.TimeSpan(obj, v)
      %% SET.TIMESPAN
      
      
      validateattributes(v, { 'numeric' }, { 'increasing' }, 'set.TimeSpan', 'TimeSpan');
      
      obj.TimeSpan = reshape(v, 1, []);
      
    end
    
    
    function set.Tree(obj, v)
      %% SET.TREE
      
      
      
      obj.SMMSTree = v;
      
    end
    
  end
  
  
  
  %% ENGINE METHODS
  methods ( Sealed )
    
    function [t, q, qdot, qddot] = result(obj)
      %% RESULT
      
      
      narginchk(1, 1);
      
      % [T, Q] = RESULT(___)
      % [T, Q, QDOT] = RESULT(___)
      % [T, Q, QDOT, QDDOT] = RESULT(___)
      nargoutchk(2, 4);
      
      % [T, Q] = RESULT(___)
      % [T, Q, ___] = RESULT(___)
      t = obj.Signals.Data.t(1:end-1);
      q = obj.Signals.Data.q(:,1:end-1);
      
      % [T, Q, QDOT] = RESULT(___)
      % [T, Q, QDOT, ___] = RESULT(___)
      if nargout > 2
        qdot = obj.Signals.Data.qdot(:,1:end-1);
      end
      
      % [T, Q, QDOT, QDDOT] = RESULT(___)
      % [T, Q, QDOT, QDDOT, ___] = RESULT(___)
      if nargout > 3
        qddot = obj.Signals.Data.qddot(:,1:end-1);
      end
      
    end
    
    
    function s = statistics(obj)
      %% STATISTICS
      
      
      if ~isLocked(obj)
        setup(obj);
        reset(obj);
      end
      
      s = cat(2, obj.Statistics_{1:output(obj.CurrentIndex)});
      
    end
    
    
    function t = stepTimes(obj)
      %% STEPTIMES
      
      
      if ~isLocked(obj)
        setup(obj);
        reset(obj);
      end
      
      t = obj.ExecutionTimes_(:,1:min(output(obj.CurrentIndex), obj.MaxTimeStep));
      
    end
    
    
    function t = totalTime(obj)
      %% TOTALTIME
      
      
      t = sum(stepTimes(obj));
      
    end
    
    
    function [t, q, qdot, qddot] = simulate(obj, q0, qdot0, qddot0)
      %% SIMULATE
      
      
      
      % SIMULATE(OBJ, Q0, ___)
      if nargin > 1 && ~isempty(q0)
        obj.Q0 = q0;
      end
      
      % SIMULATE(OBJ, Q0, QDOT0, ___)
      if nargin > 2 && ~isempty(qdot0)
        obj.Q0dot = qdot0;
      end
      
      % SIMULATE(OBJ, Q0, QDOT0, QDDOT0)
      if nargin > 3 && ~isempty(qddot0)
        obj.Q0ddot = qddot0;
      end
      
      % Simulate
      while ~isDone(obj)
        step(obj);
      end
      
      % Get results from simulation engine
      [t, q, qdot, qddot] = result(obj);
      
    end
    
  end
  
  
  %% CONCRETE METHODS
  methods ( Access = protected )
    
    function setupImpl(obj)
      %% SETUPIMPL
      
      
      
      % Calculate total number of states
      obj.MaxTimeStep = max(1, ceil(diff(obj.TimeSpan) ./ obj.StepSize));
      obj.ChunkSize   = min(obj.MaxTimeStep, floor(2^11 / obj.SMMSTree.VelocityNumber));
      
      if isempty(obj.Q0)
        obj.Q0 = configurationHome(obj.SMMSTree);
      end
      
      if isempty(obj.Q0dot)
        obj.Q0dot = zeros(obj.SMMSTree.VelocityNumber, 1);
      end
      
      if isempty(obj.Q0ddot)
        obj.Q0ddot = 0 .* obj.Q0dot;
      end
      
      obj.Signals = SignalTracker('Name', { 't' , 'q' , 'qdot' , 'qddot' });
      
      obj.CurrentIndex = Counter('InitialValue', 0, 'MaxValue', obj.MaxTimeStep);
      
      % Set up the tree object
      if ~isempty(obj.SMMSTree)
        setup(obj.SMMSTree);
      end
      
    end
    
    
    function resetImpl(obj)
      %% RESETIMPL
      
      
      % First, reset all composite objects
      if ~isempty(obj.Progress)
        reset(obj.Progress);
      end
      if ~isempty(obj.Signals)
        reset(obj.Signals);
      end
      if ~isempty(obj.SMMSTree)
        reset(obj.SMMSTree);
      end
      % Set time stepping information
      reset(obj.CurrentIndex);
      
      % Local variables for quicker use
      chunk = obj.ChunkSize;
      
      % Set Solver flag
      obj.SolverStop = false;
      
      % Get initial values
      u0     = obj.Q0;
      u0dot  = obj.Q0dot;
      u0ddot = obj.Q0ddot;
      t0 = obj.TimeSpan(1);
      
      % Set state of simulation engine
      obj.Time  = t0;
      obj.Q     = u0;
      obj.Qdot  = u0dot;
      obj.Qddot = u0ddot;
      
      % Initialize statistics array
      obj.ExecutionTimes_ = NaN(1, chunk);
      obj.Statistics_     = cell(1, chunk);
      
      % Store the initial state in the signals object
      step(obj.Signals, obj.Time, obj.Q, obj.Qdot, obj.Qddot);
      
    end
    
    
    function releaseImpl(obj)
      %% RELEASEIMPL
      
      
      % Release composite objects
      if ~isempty(obj.SMMSTree)
        release(obj.SMMSTree);
      end
      if ~isempty(obj.Solver)
        release(obj.Solver);
      end
      if ~isempty(obj.Actuation)
        release(obj.Actuation);
      end
      if ~isempty(obj.Environment)
        release(obj.Environment);
      end
      if ~isempty(obj.Progress)
        release(obj.Progress);
      end
      if ~isempty(obj.Signals)
        release(obj.Signals);
      end
      
    end
    
    
    function updateImpl(obj)
      %% UPDATEIMPL
      
      
      % Don't continue if simulation is done
      if isDone(obj)
        return
      end
      
      % Step the progress block
      step(obj.Progress)
      
      % Current time index and current time
      step(obj.CurrentIndex);
      ind = output(obj.CurrentIndex);
      % Get step size
      h = obj.StepSize;
      
      % Get current system state
      q     = obj.Q;
      qdot  = obj.Qdot;
      qddot = obj.Qddot;
      
      % Current time
      t = obj.Time;
      
      % Start timer
      tstart = tic();
      
      % Calculate external forces and internal actuation
      fext = step(obj.Environment, t, q, qdot);
      Qad  = step(obj.Actuation, t, q, qdot);
      
      % Step the engine with the configured time step size
      [ ...
          obj.SolverStop ...
        , obj.Q, obj.Qdot, obj.Qddot ...
      ] = step(obj.Solver, q, qdot, qddot, fext, Qad);
      
      % Stop timer
      elt = toc(tstart);
      
      % Advance time, but round current time to nearest multiple of step size
      obj.Time = h * round( ( t + obj.StepSize ) / h );
      
      % Track signal of state
      step(obj.Signals, obj.Time, obj.Q, obj.Qdot, obj.Qddot);
      
      % Store additional time-step related data
      obj.ExecutionTimes_(ind) = elt;
      obj.Statistics_{ind}     = statistics(obj.Solver);
      
    end
    
    
    function [t, q, qdot, qddot] = outputImpl(obj)
      %% OUTPUTIMPL
      
      
      narginchk(1, 1);
      
      % OUTPUT(___)
      % [T, Q] = OUTPUT(___)
      % [T, Q, QDOT] = OUTPUT(___)
      % [T, Q, QDOT, QDDOT] = OUTPUT(___)
      nargoutchk(0, 4);
      
      % OUTPUT(___)
      if nargout == 0
        return
      end
      
      % [T, Q] = OUTPUT(___)
      % OUTPUT();
      t = obj.Time;
      
      % [T, Q] = OUTPUT(___)
      if nargout > 1
        q = obj.Q;
      end
      
      % [T, Q, QDOT, ___] = OUTPUT(___)
      if nargout > 2
        qdot = obj.Qdot;
      end
      
      % [T, Q, QDOT, QDDOT, ___] = OUTPUT(___)
      if nargout > 3
        qddot = obj.Qddot;
      end
      
    end
    
    
    function f = isDoneImpl(obj)
      %% ISDONEIMPL
      
      
      f = obj.Time > obj.TimeSpan(2) || obj.SolverStop == true;
      
    end
    
    
    function s = saveObjectImpl(obj)
      %% SAVEOBJECTIMPL
      
      
      
      s = saveObjectImpl@matlab.System(obj);
      
      s.MaxTimeStep     = obj.MaxTimeStep;
      s.CurrentIndex    = matlab.System.saveObject(obj.CurrentIndex);
      s.SolverStop      = obj.SolverStop;
      s.ChunkSize       = obj.ChunkSize;
      s.ExecutionTimes_ = obj.ExecutionTimes_;
      s.Statistics_     = obj.Statistics_;
      
    end
    
    
    function loadObjectImpl(obj, s, wasInUse)
      %% LOADOBJECTIMPL
      
      
      
      if wasInUse
        if isfield(s, 'MaxTimeStep')
          obj.MaxTimeStep     = s.MaxTimeStep;
        end
        if isfield(s, 'CurrentIndex')
          obj.CurrentIndex    = matlab.System.loadObject(s.CurrentIndex);
        end
        if isfield(s, 'SolverStop')
          obj.SolverStop      = s.SolverStop;
        end
        if isfield(s, 'ChunkSize')
          obj.ChunkSize       = s.ChunkSize;
        end
        if isfield(s, 'ExecutionTimes_')
          obj.ExecutionTimes_ = s.ExecutionTimes_;
        end
        if isfield(s, 'Statistics_')
          obj.Statistics_     = s.Statistics_;
        end
      end
      
      loadObjectImpl@matlab.System(obj, s, wasInUse);
      
    end
    
  end
  
end
