classdef SignalTracker < matlab.System ...
  & smms.internal.InternalAccess
  %% SIGNALTRACKER
  
  
  
  %% DEPENDENT PUBLIC PROPERTIES
  properties ( Nontunable )
    
    % Chunk size used to pre-allocate memory when the internal storage limit is
    % reached.
    % Default value of -1 calculates the chunk size automatically based on the
    % number of signals and each signals length
    ChunkSize   (1, 1) double = -1
    
  end
  
  
  
  %% DEPENDENT PUBLIC PROPERTIES
  properties ( Dependent , Nontunable )
    
    % Name of the signal(s) being traced
    Name
    
  end
  
  
  
  %% READ-ONLY PUBLIC PROPERTIES
  properties ( Dependent , SetAccess = { ?matlab.System , ?smms.internal.InternalAccess } )
    
    % Data for each signal
    Data
    
  end
  
  
  
  %% INTERNAL NONTUNABLE PROPERTIES
  properties ( Nontunable , Access = { ?smms.internal.InternalAccess } )
    
    % Internal name storage
    Name_
    
    % Internal data storage
    Data_
    
    % Size of each incoming signal. Used for reshaping the output when
    % `OBJ.DATA` is being requested
    Size        (1, :) cell
    
    % Data type of each signal
    DataType    (1, :) cell
    
    % Number of signals
    NumSignal   (1, 1) double
    
    % Number of elements of each signal
    Numel       (:, 1) double
    
    % Current size of how many sampels can fit
    MaxSamples  (1, 1) double
    
    % Index counter object to retrieve the right column inside `OBJ.DATA_` when
    % `STEP` is being called
    CallCounter
    
  end
  
  
  
  %% CONSTRUCTOR METHODS
  methods
    
    function obj = SignalTracker(varargin)
      %% SIGNALTRACKER
      
      
      setProperties(obj, nargin, varargin{:});
      
    end
    
  end
  
  
  
  %% GETTERS
  methods
    
    function v = get.Data(obj)
      %% GET.DATA
      
      
      
      % Number of signals
      nsig = obj.NumSignal;
      
      % If object not set up, return nothing
      if isempty(obj.CallCounter) || nsig == 0
        v = [];
        return
      end
      
      % Create output
      v = cell(1, nsig);
      
      % Get index of current i.e., last sample
      ind = max(output(obj.CallCounter), 1);
      
      % Loop over each signal
      for isig = 1:nsig
        % Get final size to reshape to
        sz = obj.Size{isig};
        % Reshape such that the last singleton dimension will be the time dimension
        v{isig} = reshape(obj.Data_{isig}(:,1:ind), [ sz , ind ]);
        % If the signal's size is either scalar or a column vector...
        if numel(sz) == 2 && sz(2) == 1
          % We will permute the second and third dimensions
          v{isig} = permute(v{isig}, [ 1 , 3 , 2 ]);
        end
        
      end
      
      % If one signal, just return its data
      if nsig == 1
        v = v{1};
      else
        v = cell2struct(v, obj.Name_, 2);
      end
      
    end
    
    
    function v = get.Name(obj)
      %% GET.NAME
      
      
      % Get cell array of names
      v = obj.Name_;
      
      % If one signal, return its name only
      if obj.NumSignal == 1
        v = v{1};
      end
      
    end
    
  end
  
  
  
  %% SETTERS
  methods
    
    function set.Name(obj, v)
      %% SET.Name
      
      
      % If a single name is given, we will make it a cell array for consistency
      if ~iscell(v)
        v = { v };
      end
      
      % Store signal names as valid MATLAB variable names
      obj.Name_ = cellfun(@(v_) matlab.lang.makeValidName(v_), v, 'UniformOutput', false);
      
    end
    
  end
  
  
  
  %% CONSTRUCTOR METHODS
  methods ( Access = protected )
    
    function setupImpl(obj, varargin)
      %% SETUPIMPL
      
      
      sigs          = varargin;
      nsigs         = numel(sigs);
      obj.NumSignal = nsigs;
      
      obj.DataType = cell(1, nsigs);
      obj.Numel    = zeros(nsigs, 1);
      obj.Size     = cell(1, nsigs);
      obj.Data_    = cell(1, nsigs);
      
      for isig = 1:nsigs
        % Get signal
        sig = sigs{isig};
        % Type of data for better allocation
        obj.DataType{isig} = class(sig);
        % Size and number of elements in data for reconstruction of original data
        obj.Size{isig}  = size(sig);
        obj.Numel(isig) = numel(sig);
        
      end
      
      % Set chunk size if it is not user-provided
      if obj.ChunkSize == -1
        obj.ChunkSize = max(1000, floor( 2^11 / ( obj.NumSignal * sum(obj.Numel) ) ) );
      end
      
      % Counter to keep track of the index we are looking at
      obj.CallCounter = Counter('InitialValue', 0);
      
    end
    
    
    function resetImpl(obj)
      %% RESETIMPL
      
      
      for isig = 1:obj.NumSignal
        % Pre-populate an array of appropriate size
        obj.Data_{isig}     = NaN([ obj.Numel(isig) , obj.ChunkSize ], obj.DataType{isig});
      end
      obj.MaxSamples = size(obj.Data_{isig}, 2);
      
      % Reset index counter as well
      reset(obj.CallCounter);
      
    end
    
    
    function stepImpl(obj, varargin)
      %% STEPIMPL
      
      
      % Step the index counter
      step(obj.CallCounter);
      ind = output(obj.CallCounter);
      % Grow our data field in case it's needed
      resizeImpl(obj);
      
      % Get all signals
      sig = varargin;
      
      % Store data
      for isig = 1:obj.NumSignal
        obj.Data_{isig}(:,ind) = reshape(sig{isig}, [], 1);
      end
      
    end
    
    
    function resizeImpl(obj)
      %% RESIZEIMPL
      
      
      % If the index of the current column we are looking at is outside the data
      % range
      if output(obj.CallCounter) > obj.MaxSamples
        for isig = 1:obj.NumSignal
          obj.Data_{isig} = [ obj.Data_{isig} , NaN([ obj.Numel(isig) , obj.ChunkSize ], obj.DataType{isig}) ];
        end
        
        obj.MaxSamples = size(obj.Data_{1}, 2);
        
      end
      
    end
    
    
    function s = saveObjectImpl(obj)
      %% SAVEOBJECTIMPL
      
      
      % Call parent
      s = saveObjectImpl@matlab.System(obj);
      
      % If object was used, we need to store additional data
      if isLocked(obj)
        s.CallCounter = obj.CallCounter;
        s.DataType    = obj.DataType;
        s.Data_       = obj.Data_;
        s.Name_       = obj.Name_;
        s.NumSignal   = obj.NumSignal;
        s.Size        = obj.Size;
        
      end
      
    end
    
    
    function loadObjectImpl(obj, s, wasInUse)
      %% LOADOBJECTIMPL
      
      
      % Restore data if object was in use
      if wasInUse
        obj.CallCounter = s.CallCounter;
        obj.DataType    = s.DataType;
        obj.Data_       = s.Data_;
        obj.Name_       = s.Name_;
        obj.NumSignal   = s.NumSignal;
        obj.Size        = s.Size;
        
      end
      
      % Call parent load implementation for the rest of the data
      loadObjectImpl@matlab.System(obj, s, wasInUse);
      
    end
    
  end
  
end
