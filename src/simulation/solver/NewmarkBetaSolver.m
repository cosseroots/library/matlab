classdef NewmarkBetaSolver < ImplicitSolver
  %% NEWMARKBETASOLVER
  
  
  
  %% PUBLIC PROPERTIES
  properties
    
    % Object to use for predicting/correcting in the Newmark-Beta update
    Predictor (1, 1) Predictor = SO3R3Predictor()
    Corrector (1, 1) Corrector = SO3R3Corrector()
    
    % Newmark-Beta parameters
    Beta          (1, 1) double { mustBePositive , mustBeFinite } = 1 / 4
    Gamma         (1, 1) double { mustBePositive , mustBeFinite } = 1 / 2
    MaxIterations (1, 1) double { mustBePositive , mustBeFinite } = 10
    
    % Tolerances for stopping
    FunctionTolerance (1, 1) double { mustBePositive , mustBeFinite } = 1e-6
    StepTolerance     (1, 1) double { mustBePositive , mustBeFinite } = 1e-6
    
  end
  
  
  
  %% INTERNAL STATISTICS METHODS
  properties ( GetAccess = { ?smms.internal.InternalAccess } , SetAccess = protected )
    
    % Statistics structure we capture
    Statistics_ struct = struct()
    
  end
  
  
  
  %% CONSTRUCTOR METHODS
  methods
    
    function obj = NewmarkBetaSolver(varargin)
      %% NEWMARKBETASOLVER
      
      
      obj@ImplicitSolver();
      
      setProperties(obj, nargin, varargin{:});
      
    end
    
  end
  
  
  
  %% SETTERS
  methods
    
    function set.Corrector(obj, v)
      %% SET.CORRECTOR
      
      
      % Remove association with existing corrector object if it exists
      if ~isempty(obj.Corrector)
        parent(obj.Corrector, []);
      end
      
      % Set parent to this object
      if ~isempty(v)
        parent(v, obj);
      end
      % And assign object locally
      obj.Corrector = v;
      
    end
    
    
    function set.Predictor(obj, v)
      %% SET.PREDICTOR
      
      
      % Remove association with existing predictor object if it exists
      if ~isempty(obj.Predictor)
        parent(obj.Predictor, []);
      end
      
      % Set parent to this object if v is a parentable object
      if isa(v, 'smms.mixin.Parentable')
        parent(v, obj);
      end
      % And assign object locally
      obj.Predictor = v;
      
    end
    
  end
  
  
  
  %% PUBLIC METHODS
  methods ( Sealed )
    
    function s = statistics(obj)
      %% STATISTICS
      
      
      s = obj.Statistics_;
      
    end
    
  end
  
  
  
  %% CONCRETE SYSTEM METHODS
  methods ( Access = protected )
    
    function setupImpl(obj, varargin)
      %% SETUPIMPL
      
      
      
    end
    
    
    function resetImpl(obj)
      %% RESETIMPL
      
      
      % Initialize statistics structure
      obj.Statistics_ = struct('iteration', [], 'flag', [], 'time', [], 'residual', []);
      
      % Reset the predictor and corrector objects
      reset(obj.Predictor);
      reset(obj.Corrector);
      
    end
    
    
    function releaseImpl(obj)
      %% RELEASEIMPL
      
      
      % Release the predictor and corrector objects
      release(obj.Predictor);
      release(obj.Corrector);
      
    end
    
    
    function [flag, un, udotn, uddotn] = stepImpl(obj, u, udot, uddot, Fext, Qad)
      %% TIMESTEMPIMPL
      
      
      % Variables of Newton-Raphson solver
      iteration = uint32(0);
      
      % Increment of Newmark-Beta step
      inc_k = 0 .* uddot;
      
      % Start timer
      tstart = tic();
      
      % Predict solutions at next time step
      [un_k, udotn_k, uddotn_k] = step(obj.Predictor, u, udot, uddot);
      
      % This is MATLAB's version of a do-while loop...
      while true
        % First, solve the residual for the predicted state
        [fval, jac] = timestepResidual(obj, un_k, udotn_k, uddotn_k, Fext, Qad, inc_k);
        
        % Calculate step size from residual value and Jacobian
        inc_k = -decomposition(jac) \ fval;
        
        % Check if a root has been found
        [converged, exitflag] = isConverged(obj, iteration, fval, inc_k, jac);
        
        % If the system has converged, we break out of this loop
        if converged
          break;
          
        end
        
        % If we get here, we haven't converged so we need to do a Newton-Raphson
        % step which we do using the Predictor-Corrector's `correct` function
        [un_k, udotn_k, uddotn_k] = step(obj.Corrector, un_k, udotn_k, uddotn_k, inc_k);
        
        % Increase iteration counter
        iteration = iteration + 1;
        
      end
      
      % Stop timer
      elt = toc(tstart);
      
      % Set return flag for solver whether to continue or not
      flag = exitflag <= 0;
      
      % And return the newly calculated value
      un     = un_k;
      udotn  = udotn_k;
      uddotn = uddotn_k;
      
      % Store statistics
      obj.Statistics_.iteration = iteration;
      obj.Statistics_.flag = exitflag;
      obj.Statistics_.residual = fval;
      obj.Statistics_.time = elt;
      
    end
    
    
    function s = saveObjectImpl(obj)
      %% SAVEOBJECTIMPL
      
      
      
      s = saveObjectImpl@matlab.System(obj);
      
      if isLocked(obj)
        s.Statistics_ =obj.Statistics_;
      end
      
    end
    
    
    function loadObjectImpl(obj, s, wasInUse)
      %% LOADOBJECTIMPL
      
      
      
      if wasInUse
        obj.Statistics_ = s.Statistics_;
      end
      
      loadObjectImpl@matlab.System(obj, s, wasInUse);
      
    end
    
  end
  
  
  
  %% INTERNAL METHODS
  methods ( Access = protected )
    
    function [tf, flag] = isConverged(obj, kiter, res, stp, ~)
      %% ISCONVERGED
      
      

      % True/false flag
      tf = false;
      % Flag why converged
      flag = [];
      
      % Residual too small
      if norm(res, Inf) <= obj.FunctionTolerance
        tf = true;
        flag = 3;
        
      % Step size too small
      elseif norm(stp, Inf) <= obj.StepTolerance
        tf = true;
        flag = 2;
        
      % Number of iterations exceeded
      elseif kiter >= obj.MaxIterations
        tf = true;
        flag = 0;
        
      end
      
    end
    
    
    function [fval, jac] = timestepResidual(obj, q, qdot, qddot, Fext, Qad, incu)
      %% TIMESTEPRESIDUAL
      
      
      
      nargoutchk(1, 2);
      
      % Calculate tangent increments
      [vq, vqdot, vqddot] = variation(obj, q, qdot, qddot, incu);
      
      % Evaluate inverse dynamics of kinematic tree
      [idm, vidm] = tangentInverseDynamics(obj.Parent.SMMSTree, q, qdot, qddot, Fext, vq, vqdot, vqddot);
      
      % Build residual and jacobian
      fval =  idm - Qad;
      jac  = vidm;
      
    end
    
    
    function [vq, vqdot, vqddot] = variation(obj, q, qdot, qddot, incu)
      %% VARIATION Calculate variation from tangent increment
      
      
      % Number of variations
      nv = obj.Parent.SMMSTree.VelocityNumber;
      
      b = obj.Beta;
      g = obj.Gamma;
      h = obj.StepSize;
      
      bh  = b * h;
      bhh = bh * h;
      
      % By default, all variations are unit variations
      vq     = eye(nv, nv);
      vqdot  = vq * g / bh;
      vqddot = vq     / bhh;
      
      % If tree is a floating base, we need to calculate the correct variation
      % of an SE(3) = SO(3) x IR(3) state
      if obj.Parent.SMMSTree.FloatingBase
        % Get kinematic state of floating base
        T      = tpar2tform(q(1:7));
        eta    = qdot(1:6);
        etadot = qddot(1:6);
        
        % And calculate the Jacobian of the variation
        [vq(1:6,1:6), vqdot(1:6,1:6), vqddot(1:6,1:6)] = dSO3xR32SE3(T, eta, etadot, incu(1:6), g / bh, 1 / bhh);
        
      end
      
    end
    
  end
  
end
