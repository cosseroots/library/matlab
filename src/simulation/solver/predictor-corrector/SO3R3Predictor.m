classdef SO3R3Predictor < Predictor
  %% SO3R3PREDICTOR
  
  
  
  %% INTERNAL PROPERTIES
  properties ( Nontunable )
    
    Beta     (1, 1) double
    
    Gamma    (1, 1) double
    
    StepSize (1, 1) double
    
  end
  
  
  
  %% INTERNAL PROPERTIES
  properties ( Nontunable , Access = protected )
    
    VSPredictor (1, 1) Predictor = VectorSpacePredictor()
    
  end
  
  
  
  %% CONSTRUCTOR METHODS
  methods
    
    function obj = SO3R3Predictor(varargin)
      %% SO3R3PREDICTOR
      
      
      obj.VSPredictor = VectorSpacePredictor();
      
      setProperties(obj, nargin, varargin{:});
      
    end
    
  end
  
  
  
  %% SETTERS
  methods
    
    function set.VSPredictor(obj, v)
      %% SET.VSPREDICTOR
      
      
      % Associate parent
      if isa(v, 'smms.mixin.Parentable')
        parent(v, obj);
      end
      
      % Set property
      obj.VSPredictor = v;
      
    end
    
  end
  
  
  
  %% SYSTEM METHODS
  methods ( Access = protected )
    
    function setupImpl(obj)
      %% SETUPIMPL
      
      
      p = parent(obj);
      obj.Beta     = p.Beta;
      obj.Gamma    = p.Gamma;
      obj.StepSize = p.StepSize;
      
    end
    
    
    function [up, udotp, uddotp] = stepImpl(obj, u, udot, uddot)
      %% STEPIMPL
      
      
      % Split state into components of SO3xIR3 and regular vector space
      so3_u     = u(1:7,:,:);
      so3_udot  = udot(1:6,:,:);
      so3_uddot = uddot(1:6,:,:);
      
      vs_u      = u(8:end,:,:);
      vs_udot   = udot(7:end,:,:);
      vs_uddot  = uddot(7:end,:,:);
      
      % Correct each part
      [so3_up, so3_udotp, so3_uddotp] = predict_SO3(obj, so3_u, so3_udot, so3_uddot);
      [vs_up , vs_udotp , vs_uddotp ] = step(obj.VSPredictor, vs_u, vs_udot, vs_uddot);
      
      % And put it all back together
      up     = cat(1, so3_up    , vs_up);
      udotp  = cat(1, so3_udotp , vs_udotp);
      uddotp = cat(1, so3_uddotp, vs_uddotp);
      
    end
    
    
    function [up, udotp, uddotp] = predict_SO3(obj, u, udot, uddot)
      %% PREDCIT_SO3
      
      
      % Split data at time step K
      [R_p, r_p] = tpar2cart(u);
      
      % Split twists into linear and angular components
      O_p = twist2ang(udot);
      % V_p = twist2lin(eta_p);
      Odot_p = twist2ang(uddot);
      % Vdot_p = twist2lin(etadot_p);
      
      % With this formula and DOI:10.1002/nme.1620310103 we can do a simple correction
      % of the convected relative rotation and the associated angular velocities and
      % accelerations.
      T_p = [ 0.0 ; 0.0 ; 0.0 ];
      [T_n, O_n, Odot_n] = step(obj.VSPredictor, T_p, O_p, Odot_p);
      
      % Corrected rotation matrix and its associated quaternion
      R_n  = R_p * expso3(T_n);
      Q_n  = rotm2quat(R_n);
      
      % Convert body-frame velocities, accelerations into world-frame quantities
      [~, v_p, a_p] = SE32SO3xR3(tpar2tform(u), udot, uddot);
      
      % Corrected linear positions, velocities, and accelerations in world frame
      [r_n, v_n, a_n] = step( ...
          obj.VSPredictor ...
        , r_p ...
        , twist2lin(v_p) ...
        , twist2lin(a_p) ...
      );
      
      % Project coordinates from SO(3) x IR(3) into SE(3)
      [up_, udotp, uddotp] = SO3xR32SE3( ...
          tform(quat2rotm(Q_n), r_n) ...
        , cart2twist(O_n, v_n) ...
        , cart2twist(Odot_n, a_n) ...
      );
      up = tform2tpar(up_);
      
    end
    
    
    function s = saveObjectImpl(obj)
      %% SAVEOBJECTIMPL
      
      
      
      s = saveObjectImpl@matlab.System(obj);
      
      s.VSPredictor = obj.VSPredictor;
      
    end
    
    
    function loadObjectImpl(obj, s, wasInUse)
      %% LOADOBJECTIMPL
      
      
      
      obj.VSPredictor = s.VSPredictor;
      
      loadObjectImpl@matlab.System(obj, s, wasInUse);
      
    end
    
  end
  
end
