classdef VectorSpacePredictor < Predictor
  %% VECTORSPACEPREDICTOR
  
  
  
  %% INTERNAL PROPERTIES
  properties ( Nontunable )
    
    Beta     (1, 1) double
    
    Gamma    (1, 1) double
    
    StepSize (1, 1) double
    
  end
  
  
  
  %% FINAL IMPLEMENTATION METHODS
  methods ( Access = protected )
    
    function setupImpl(obj)
      %% SETUPIMPL
      
      
      % Get properties from parent
      p = parent(obj);
      obj.Beta     = p.Beta;
      obj.Gamma    = p.Gamma;
      obj.StepSize = p.StepSize;
      
    end
    
    
    function [up, udotp, uddotp] = stepImpl(obj, u, udot, uddot)
      %% PREDICTIMPL
      
      
      
      b = obj.Beta;
      g = obj.Gamma;
      h = obj.StepSize;
      
      % Assume next accelerations to be zero
      uddotp = 0 .* uddot;
      
      % New velocities
      udotp  =         udot +     h     * ( 1 - g )    * uddot;
      
      % New positions
      up     = u + h * udot + h * h / 2 * ( 1 - 2 * b ) * uddot;
      
    end
    
  end
  
end
