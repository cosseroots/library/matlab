classdef SO3R3Corrector < Corrector
  %% SO3R3CORRECTOR
  
  
  
  %% INTERNAL PROPERTIES
  properties ( Nontunable )
    
    Beta     (1, 1) double
    
    Gamma    (1, 1) double
    
    StepSize (1, 1) double
    
  end
  
  
  
  %% INTERNAL PROPERTIES
  properties ( Nontunable , Access = protected )
    
    VSCorrector (1, 1) Corrector = VectorSpaceCorrector()
    
  end
  
  
  
  %% CONSTRUCTOR METHODS
  methods
    
    function obj = SO3R3Corrector(varargin)
      %% SO3R3CORRECTOR
      
      
      obj.VSCorrector = VectorSpaceCorrector();
      
      setProperties(obj, nargin, varargin{:});
      
    end
    
  end
  
  
  
  %% SETTERS
  methods
    
    function set.VSCorrector(obj, v)
      %% SET.VSCORRECTOR
      
      
      % Associate parent
      if isa(v, 'smms.mixin.Parentable')
        parent(v, obj);
      end
      
      % Set property
      obj.VSCorrector = v;
      
    end
    
  end
  
  
  
  %% SYSTEM METHODS
  methods ( Access = protected )
    
    function setupImpl(obj)
      %% SETUPIMPL
      
      
      p = parent(obj);
      obj.Beta     = p.Beta;
      obj.Gamma    = p.Gamma;
      obj.StepSize = p.StepSize;
      
    end
    
    
    function [up, udotp, uddotp] = stepImpl(obj, u, udot, uddot, incu)
      %% STEPIMPL
      
      
      % Split state into components of SO3xIR3 and regular vector space
      so3_u     = u(1:7,:,:);
      so3_udot  = udot(1:6,:,:);
      so3_uddot = uddot(1:6,:,:);
      so3_inc   = incu(1:6,:);
      
      vs_u      = u(8:end,:,:);
      vs_udot   = udot(7:end,:,:);
      vs_uddot  = uddot(7:end,:,:);
      vs_inc    = incu(7:end,:);
      
      % Correct each part
      [so3_up, so3_udotp, so3_uddotp] = correctSO3(obj, so3_u, so3_udot, so3_uddot, so3_inc);
      [vs_up , vs_udotp , vs_uddotp ] = step(obj.VSCorrector, vs_u, vs_udot, vs_uddot, vs_inc);
      
      % And put it all back together
      up     = cat(1, so3_up    , vs_up);
      udotp  = cat(1, so3_udotp , vs_udotp);
      uddotp = cat(1, so3_uddotp, vs_uddotp);
      
    end
    
    
    function [uc, udotc, uddotc] = correctSO3(obj, up, udotp, uddotp, inc)
      %% CORRECTSO3
      
      
      % Split data at time step K
      [R_p, r_p] = tpar2cart(up);
      
      % Split twists into linear and angular components
      O_p = twist2ang(udotp);
      % V_p = twist2lin(eta_p);
      Odot_p = twist2ang(uddotp);
      % Vdot_p = twist2lin(etadot_p);
      
      % Split increment of twists into angular and linear increments
      [inct, incp] = unbundle(inc, 3, 3);
      
      % With this formula and DOI:10.1002/nme.1620310103 we can do a simple correction
      % of the convected relative rotation and the associated angular velocities and
      % accelerations.
      T_p = [ 0.0 ; 0.0 ; 0.0 ];
      [T_n, O_n, Odot_n] = step(obj.VSCorrector, T_p, O_p, Odot_p, inct);
      
      % Corrected rotation matrix and its associated quaternion
      R_n  = R_p * expso3(T_n);
      Q_n  = rotm2quat(R_n);
      
      % Convert body-frame velocities, accelerations into world-frame quantities
      [~, v_p, a_p] = SE32SO3xR3(tpar2tform(up), udotp, uddotp);
      
      % Corrected linear positions, velocities, and accelerations in world frame
      [r_n, v_n, a_n] = step( ...
          obj.VSCorrector ...
        , r_p ...
        , twist2lin(v_p) ...
        , twist2lin(a_p) ...
        , incp ...
      );
      
      % Project coordinates from SO(3) x IR(3) into SE(3)
      [uc_, udotc, uddotc] = SO3xR32SE3( ...
          tform(quat2rotm(Q_n), r_n) ...
        , cart2twist(O_n, v_n) ...
        , cart2twist(Odot_n, a_n) ...
      );
      uc = tform2tpar(uc_);
      
    end
    
    
    function s = saveObjectImpl(obj)
      %% SAVEOBJECTIMPL
      
      
      
      s = saveObjectImpl@matlab.System(obj);
      
      s.VSCorrector = obj.VSCorrector;
      
    end
    
    
    function loadObjectImpl(obj, s, wasInUse)
      %% LOADOBJECTIMPL
      
      
      
      obj.VSCorrector = s.VSCorrector;
      
      loadObjectImpl@matlab.System(obj, s, wasInUse);
      
    end
    
  end
  
end
