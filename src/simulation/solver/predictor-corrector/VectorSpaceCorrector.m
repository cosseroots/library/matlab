classdef VectorSpaceCorrector < Corrector
  %% VECTORSPACECORRECTOR
  
  
  
  %% INTERNAL PROPERTIES
  properties ( Nontunable )
    
    Beta     (1, 1) double
    
    Gamma    (1, 1) double
    
    StepSize (1, 1) double
    
  end
  
  
  
  %% FINAL IMPLEMENTATION METHODS
  methods ( Access = protected )
    
    function setupImpl(obj)
      %% SETUPIMPL
      
      
      % Get properties from parent
      p = parent(obj);
      obj.Beta     = p.Beta;
      obj.Gamma    = p.Gamma;
      obj.StepSize = p.StepSize;
      
    end
    
    
    function [uc, udotc, uddotc] = stepImpl(obj, up, udotp, uddotp, inc)
      %% STEPIMPL
      
      
      
      % Extract parametrization of Newmark-Beta scheme from options structure
      b = obj.Beta;
      g = obj.Gamma;
      h = obj.StepSize;
      
      bh = b * h;
      
      % Corrected solutions: positions, velocities, and accelerations
      uc     = up     + inc;
      udotc  = udotp  + inc * g / ( bh );
      uddotc = uddotp + inc     / ( bh * h) ;
      
    end
    
  end
  
end
