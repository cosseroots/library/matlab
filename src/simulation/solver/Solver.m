classdef ( Abstract, Hidden ) Solver < matlab.System ...
    & smms.mixin.Parentable
  %% SOLVER
  
  
  
  %% INTERNAL PROPERTIES
  properties ( Nontunable , Access = { ?smms.internal.InternalAccess } )
    
    % Step size
    StepSize
    
    % Simulator engine
    Simulator
    
  end
  
  
  
  %% ACCESSOR METHODS
  methods
    
    function v = get.StepSize(obj)
      %% GET.STEPSIZE
      
      
      % Get parent
      p = parent(obj);
      % If there is a parent object
      if ~isempty(p)
        % Read step size from parent
        v = p.StepSize;
        
      else
        v = [];
        
      end
      
    end
    
  end
  
end
