function tau = inverseDynamics(robot, q, qdot, qddot, fext)%#codegen
%% INVERSEDYNAMICS
%
% In this algorithm, all calculations are done in the joint-fixed coordinate
% frames i.e., either in joint i-th right-side frame (which is attached to the
% i-th body) or the joint i-th left-side frame (which is attached to the
% (i-1)-th body), depending on what is needed.
%
% In fact, this relates to F1 (forces exerted on the joint) being expressed in
% T_{i} and F0 (forces exerted by the joint on the parent body) being expressed in
% T_{i-1}
%
% Inputs:
%
%   ROBOT                   SMMS robot object.
% 
%   Q                       Px1 array of generalized joint positions.
% 
%   QDOT                    Vx1 array of generalized joint velocities.
% 
%   QDDOT                   Vx1 array of generalized joint accelerations.
% 
%   FEXT                    6xB array of external forces acting on each body
%                           expressed in base coordinates.
%
% Outputs:
%
%   TAU                     Vx1 array of actuation forces needed to achieve the
%                           given motion.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Algorithm

% Number of...
nb = robot.NumBody;

% Base configuration: always fixed
T0 = robot.Ground.T;
X0 = Ad(tform2inv(T0));
V0 = zeros(6, 1);
V0dot = X0 * [ ...
     zeros(3, 1)   ; ...
    -robot.Gravity ; ...
  ];

% Spatial transform across joint i, in frame i
TJ = tformzeros(nb);
% Spatial transform from parent(i) to joint i
XJ = Ad(TJ);
% Spatial twists across joint i, in frame i
VJ = zeros(6, nb);
% Spatial twist velocities across joint i, in frame i
VJdot = zeros(6, nb);

% Spatial transform from joint i to base
Xtree = Ad(tformzeros(nb));
% Spatial transform of joint i
T = tformzeros(nb);
% Spatial twists of joint i
V = zeros(6, nb);
% Spatial twist velocities of joint i
Vdot = zeros(6, nb);

% Spatial forces exerted by body i on joint i
F1 = zeros(6, nb);
% Spatial forces exerted by joint i on parent(i)
F0 = zeros(6, nb);

% Initialize outputs
tau = zeros(size(qdot));



%% Forward Projection

% From base to tip
for ib = 1:1:nb
  % Get joint's position and velocity maps
  a = robot.PositionMap(ib,:);
  b = robot.VelocityMap(ib,:);
  
  % Parent index
  pid = robot.Parent(ib);
  
  % Parent is another body
  if pid > 0
    Tp = T(:,:,pid);
    Xp = Xtree(:,:,pid);
    Vp = V(:,pid);
    Ap = Vdot(:,pid);
    
  % Parent is base
  else
    Tp = T0;
    Xp = X0;
    Vp = V0;
    Ap = V0dot;
    
  end
  
  % Joint object, for quicker access
  jnt = robot.Joint{ib};
  
  % Go through joint: i+ -> i-
  [ TJ(:,:,ib) , VJ(:,ib) , VJdot(:,ib) ] = forwardKinematics( ...
      jnt ...
    ,     q(a(1):a(2)) ...
    ,  qdot(b(1):b(2)) ...
    , qddot(b(1):b(2)) ...
  );
  
  % Go from joint to parent: i- -> parent(i)
  TJ(:,:,ib) = tformmul(jnt.JointToParentTransform, TJ(:,:,ib));
  
  % Transformation: i -> parent(i) -> ... -> 0
  T(:,:,ib) = tformmul(Tp, TJ(:,:,ib));
  
  % Adjoint transformation: parent(i) -> i
  XJ(:,:,ib) = Ad(tform2inv(TJ(:,:,ib)));
  
  % Twists of joint
  V(:,ib) = XJ(:,:,ib) * Vp + ...
            + VJ(:,ib);
  
  % Twist velocities of joint
  Vdot(:,ib) = XJ(:,:,ib) * Ap + ...
                + ad(V(:,ib)) * VJ(:,ib) ...
                + VJdot(:,ib);
  
  % Force transformation: 0 -> 1 -> ... -> parent(i) -> i
  Xtree(:,:,ib) = Xp * Ad(TJ(:,:,ib));
  
  % Spatial inertia of body as seen from the joint
  X_IB = Ad(jnt.JointToBodyTransform);
  IB   = transpose(X_IB) * robot.Body{ib}.SpatialInertiaMatrix * X_IB;
  
  % Inertial forces on joint
  F1(:,ib) = IB * Vdot(:,ib) + ...
              - permute( ...
                  ad(V(:,ib)) ...
                , [2, 1, 3] ...
              ) * ( IB * V(:,ib) ) + ...
              - permute( ...
                  Xtree(:,:,ib) ...
                , [2, 1, 3] ...
              ) * fext(:,ib);
  
end



%% Backward Projection

% From tip to base
for ib = nb:-1:1
  a = robot.PositionMap(ib,:);
  b = robot.VelocityMap(ib,:);
  
  % Get parent of body
  pid = robot.Parent(ib);
  % Get joint
  jnt = robot.Joint{ib};
  
  % Project forces through the joint and its internal actuation
  [ F0(:,ib) , tau(b(1):b(2)) ] = backwardDynamics( ...
      jnt ...
    ,    T(:,:,ib) ...
    ,    V(:,ib) ...
    , Vdot(:,ib) ...
    ,     q(a(1):a(2)) ...
    ,  qdot(b(1):b(2)) ...
    , qddot(b(1):b(2)) ...
    , F1(:,ib) ...
  );
  
  % Apply forces onto parent if joint has a parent body
  if pid > 0
    % Transform forces of child joint's left side onto parent joint
    X_pid     = permute(Ad(jnt.ParentToJointTransform), [2, 1, 3]);
    F1(:,pid) =  F1(:,pid) + ...
                + X_pid *  F0(:,ib);
    
  end
  
end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
