function [T, V, Vdot] = forwardRNEA(robot, q, qdot, qddot)%#codegen
%% FORWARDRNEA
%
% [TJ, VJ, VJDOT] = FORWARDRNEA(ROBOT, Q, QDOT, QDDOT)
%
% In this algorithm, the values calculated are that of the joints' right-side
% frame. To obtain the i-th body's state, simply transform the values from
% F_{ji} to F_{bi} by
%
% T_{bi}      = T_{ji} * T_{jb}
% eta_{bi}    = Ad(T_{jb}) * eta_{ji}
% etadot_{bi} = Ad(T_{jb}) * etadot_{ji}
%
% Inputs:
%
%   ROBOT                   SMMS robot object.
% 
%   Q                       Px1 array of generalized joint positions.
% 
%   QDOT                    Vx1 array of generalized joint velocities.
% 
%   QDDOT                   Vx1 array of generalized joint accelerations.
% 
% Outputs:
%
%   TJ                      4x4xB array of homogeneous transformation matrices
%                           of each joint.
%
%   VJ                      6xB array of twists of each joint.
%
%   VJDOT                   6xB array of twist velocities of each joint.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% FORWARDRNEA(OBJ, Q, QDOT, QDDOT)
narginchk(4, 4);

% [T, ETA, ETADOT] = FORWARDRNEA(___)
nargoutchk(3, 3);



%% Algorithm

% Number of...
nb = robot.NumBody;

% Base configuration: always fixed
T0 = robot.Ground.T;
X0 = Ad(tform2inv(T0));
V0 = zeros(6, 1);
V0dot = X0 * [ zeros(3, 1) ; robot.Gravity ];

% Spatial transform across joint i, in frame i
TJ = tformzeros(nb);
% Spatial transform from body parent(i) to body i
XJ = Ad(TJ);
% Spatial twists across joint i, in frame i
VJ = zeros(6, nb);
% Spatial twist velocities across joint i, in frame i
VdotJ = zeros(6, nb);

% Spatial transform of body i
T = tformzeros(nb);
% Spatial twists of body i
V = zeros(6, nb);
% Spatial twist velocities of body i
Vdot = zeros(6, nb);

% Forward projection
for ib = 1:nb
  a = robot.PositionMap(ib,:);
  b = robot.VelocityMap(ib,:);
  
  % Parent index
  pid = robot.Parent(ib);
  
  % Parent is another body
  if pid > 0
    Tp    = T(:,:,pid);
    Vp    = V(:,pid);
    Vdotp = Vdot(:,pid);
    
  % Parent is base
  else
    Tp    = T0;
    Vp    = V0;
    Vdotp = V0dot;
    
  end
  
  % Get joint object for quicker access
  jnt = robot.Joint{ib};
  
  % Go through joint: i+ -> i-
  [ TJ(:,:,ib) , VJ(:,ib) , VdotJ(:,ib) ] = forwardKinematics( ...
      jnt ...
    ,     q(a(1):a(2)) ...
    ,  qdot(b(1):b(2)) ...
    , qddot(b(1):b(2)) ...
  );
  
  % Go from joint to parent: i- -> parent(i)
  TJ(:,:,ib) = tformmul(jnt.JointToParentTransform, TJ(:,:,ib));
  
  % Transformation: i -> parent(i) -> ... -> 0
  T(:,:,ib) = tformmul(Tp, TJ(:,:,ib));
  
  % Adjoint transformation: parent(i) -> i
  XJ(:,:,ib) = Ad(tform2inv(TJ(:,:,ib)));
  
  % Twists of body
  V(:,ib) = XJ(:,:,ib) * Vp + ...
            + VJ(:,ib);
  
  % Twist velocities of body
  Vdot(:,ib) = XJ(:,:,ib) * Vdotp + ...
                + ad(V(:,ib)) * VJ(:,ib) ...
                + VdotJ(:,ib);
  
end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
