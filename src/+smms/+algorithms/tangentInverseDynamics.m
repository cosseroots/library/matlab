function [tau, dtau] = tangentInverseDynamics(robot, q, qdot, qddot, fext, vq, vqdot, vqddot, vfext)%#codegen
%% TANGENTINVERSEDYNAMICS
%
% Inputs:
%
%   ROBOT                   SMMS robot object.
% 
%   Q                       Px1 array of generalized joint positions.
% 
%   QDOT                    Vx1 array of generalized joint velocities.
% 
%   QDDOT                   Vx1 array of generalized joint accelerations.
% 
%   FEXT                    6xB array of external forces acting on each body
%                           expressed in base coordinates.
% 
%   VQ                      PxD array of variation of generalized joint
%                           positions.
% 
%   VQDOT                   VxD array of variation of generalized joint
%                           velocities.
% 
%   VQDDOT                  VxD array of variation of generalized joint
%                           accelerations.
% 
%   VFEXT                   6xDxB array of external forces acting on each body
%                           expressed in base coordinates.
%
% Outputs:
%
%   TAU                     6x1 array of actuation forces needed to achieve the
%                           given motion.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Algorithm

% Number of...
nb = robot.NumBody;
nv = size(vq, 2);

% Base configuration: always fixed
T0 = robot.Ground.T;
X0 = Ad(tform2inv(T0));
V0 = zeros(6, 1);
V0dot = X0 * [ ...
     zeros(3, 1)   ; ...
    -robot.Gravity ; ...
  ];

% Base variation: always fixed
dT0 = zeros(6, nv);
dX0 = zeros(6, 6, nv);
dV0 = zeros(6, nv);
dV0dot = zeros(6, nv);

% Spatial transform and variation across joint i, in frame i
 TJ = tformzeros(nb);
dTJ = zeros(6, nv, nb);
% Spatial transform and variation from body parent(i) to joint i
 XJ  = Ad(TJ);
dXJ = zeros(6, 6, nv, nb);
% Spatial twists and variation across joint i, in frame i
 VJ = zeros(6, nb);
dVJ = zeros(6, nv, nb);
% Spatial twist and variation velocities across joint i, in frame i
 VJdot = zeros(6, nb);
dVJdot = zeros(6, nv, nb);

% Spatial transform and variation from joint i to base
 Xtree = Ad(tformzeros(nb));
dXtree = zeros(6, 6, nv, nb);
% Spatial transform and variation of joint i
 T = tformzeros(nb);
dT = zeros(6, nv, nb);
% Spatial twists and variation of joint i
 V = zeros(6, nb);
dV = zeros(6, nv, nb);
% Spatial twist velocities and variation of joint i
 Vdot = zeros(6, nb);
dVdot = zeros(6, nv, nb);

% Spatial forces and variation of spatial forces exerted by body i on joint i
 F1 = zeros(6, nb);
dF1 = zeros(6, nv, nb);
% Spatial forces and variation of spatial forces exerted by joint i on parent(i)
 F0 = zeros(6, nb);
dF0 = zeros(6, nv, nb);

% Internal actuation and variation
 tau = zeros(size(qdot));
dtau = zeros(size(vqdot));



%% Forward Reconstruction and Variation

% From base to tip
for ib = 1:1:nb
  % Get joint's position and velocity maps
  a = robot.PositionMap(ib,:);
  b = robot.VelocityMap(ib,:);
  
  % Parent index
  pid = robot.Parent(ib);
  
  % Parent is another body
  if pid > 0
    TP    = T(:,:,pid);
    XP    = Xtree(:,:,pid);
    VP    = V(:,pid);
    VPdot = Vdot(:,pid);
    
    dTP    = dT(:,:,pid);
    dXP    = dXtree(:,:,:,pid);
    dVP    = dV(:,:,pid);
    dVPdot = dVdot(:,:,pid);
    
  % Parent is base
  else
    TP = T0;
    XP = X0;
    VP = V0;
    VPdot = V0dot;
    
    dTP = dT0;
    dXP = dX0;
    dVP = dV0;
    dVPdot = dV0dot;
    
  end
  
  % Joint object for quicker access
  jnt = robot.Joint{ib};
  
  % Go through joint's variation: i -> parent(i)
  [ TJ(:,:,ib) , VJ(:,ib) , VJdot(:,ib) , dTJ(:,:,ib) , dVJ(:,:,ib) , dVJdot(:,:,ib) ] = tangentForwardKinematics( ...
      jnt ...
    ,     q(a(1):a(2)) ...
    ,  qdot(b(1):b(2)) ...
    , qddot(b(1):b(2)) ...
    ,     vq(b(1):b(2),:) ...
    ,  vqdot(b(1):b(2),:) ...
    , vqddot(b(1):b(2),:) ...
  );
  
  % Go from joint to parent: i- -> parent(i)
  TJ(:,:,ib) = tformmul(jnt.JointToParentTransform, TJ(:,:,ib));
  
  % Transformation: i -> parent(i) -> ... -> 0
  T(:,:,ib) = tformmul(TP, TJ(:,:,ib));
  
  % Adjoint transformation: parent(i) -> i
  XJ(:,:,ib) = Ad(tform2inv(TJ(:,:,ib)));
  
  % Twists of joint
  V(:,ib) = XJ(:,:,ib) * VP + ...
            + VJ(:,ib);
  
  % Twist velocities of joint
  Vdot(:,ib) = XJ(:,:,ib) * VPdot + ...
                + ad(V(:,ib)) * VJ(:,ib) ...
                + VJdot(:,ib);
  
  % Force transformation: 0 -> 1 -> ... -> parent(i) -> i
  Xtree(:,:,ib) = XP * Ad(TJ(:,:,ib));
  
  % Spatial inertia of body as seen from the joint
  X_IB = Ad(jnt.JointToBodyTransform);
  IB   = transpose(X_IB) * robot.Body{ib}.SpatialInertiaMatrix * X_IB;
  
  % Inertial forces on joint
  F1(:,ib) = IB * Vdot(:,ib) + ...
              - permute( ...
                  ad(V(:,ib)) ...
                , [2, 1, 3] ...
              ) * ( IB * V(:,ib) ) + ...
              - permute( ...
                  Xtree(:,:,ib) ...
                , [2, 1, 3] ...
              ) * fext(:,ib);
  
  %%%
  % Variation of kinematics
  %%%
  
  % Variation of adjoint transformation
  dXJ(:,:,:,ib) = pagemult(-ad(dTJ(:,:,ib)), XJ(:,:,ib));
  
  % Variation of transformation
  dT(:,:,ib) = XJ(:,:,ib) * dTP + ...
                + dTJ(:,:,ib);
  
  % Varation of twists of joint
  dV(:,:,ib) = XJ(:,:,ib) * dVP + permute(pagemult(dXJ(:,:,:,ib), VP), [1, 3, 2]) + ...
                + dVJ(:,:,ib);
  
  % Variation of twist velocities of joint
  dVdot(:,:,ib) = XJ(:,:,ib) * dVPdot + permute(pagemult(dXJ(:,:,:,ib), VPdot), [1, 3, 2]) + ...
                  - ad(VJ(:,ib)) * dV(:,:,ib) + ad(V(:,ib)) * dVJ(:,:,ib) + ...
                  + dVJdot(:,:,ib);
  
  % Varation of force transformation: 0 -> 1 -> ... -> parent(i) -> i
  dXtree(:,:,:,ib) = pagemult(dXP, Ad(TJ(:,:,ib))) + pagemult(XP * Ad(TJ(:,:,ib)), ad(dTJ(:,:,ib)));
  
  % Variation of joint-applied forces
  dF1(:,:,ib) = IB * dVdot(:,:,ib) + ...
                - ( ...
                  permute(pagemult(permute(ad(dV(:,:,ib)), [2, 1, 3]), IB * V(:,ib) ), [1, 3, 2]) + ...
                  + permute(ad(V(:,ib)), [2, 1, 3]) * IB * dV(:,:,ib) ...
                ) + ...
                - permute(pagemult(permute(dXtree(:,:,:,ib), [2, 1, 3]), fext(:,ib)), [1, 3, 2]) + ...
                - pagemult(permute(Xtree(:,:,ib), [2, 1, 3]), vfext(:,:,ib));
  
end



%% Backward Reconstruction and Variation

% From tip to base
for ib = nb:-1:1
  a = robot.PositionMap(ib,:);
  b = robot.VelocityMap(ib,:);
  
  % Get parent of body
  pid = robot.Parent(ib);
  % Joint object for quicker access
  jnt = robot.Joint{ib};
  
  % Project forces through the joint and its internal actuation
  [ F0(:,ib), tau(b(1):b(2)) , dF0(:,:,ib) , dtau(b(1):b(2),:) ] = tangentBackwardDynamics( ...
      jnt ...
    ,    T(:,:,ib) ...
    ,    V(:,ib) ...
    , Vdot(:,ib) ...
    ,    q(a(1):a(2)) ...
    ,  qdot(b(1):b(2)) ...
    , qddot(b(1):b(2)) ...
    , F1(:,ib) ...
    ,    dT(:,:,ib) ...
    ,    dV(:,:,ib) ...
    , dVdot(:,:,ib) ...
    ,     vq(b(1):b(2),:) ...
    ,  vqdot(b(1):b(2),:) ...
    , vqddot(b(1):b(2),:) ...
    , dF1(:,:,ib) ...
  );
  
  % Apply forces onto parent if joint has a parent
  if pid > 0
    % Transform forces of child joint's left side onto parent joint
    X_pid        = permute(Ad(jnt.ParentToJointTransform), [2, 1, 3]);
    F1(:,pid)    =  F1(:,pid) + ...
                    + X_pid *  F0(:,ib);
    dF1(:,:,pid) = dF1(:,:,pid) + ....
                    + X_pid * dF0(:,:,ib);
    
  end
  
end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
