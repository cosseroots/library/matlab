function [Tj, Vj, Vjdot, dZj, dVj, dVjdot] = tangentForwardRNEA(robot, q, qdot, qddot, vq, vqdot, vqddot)%#codegen
%% TANGENTFORWARDRNEA
%
% [TJ, VJ, VJDOT, VZETAJ, VVJ, VVJDOT] = TANGENTFORWARDRNEA(ROBOT, Q, QDOT, QDDOT, VQ, VQDOT, VQDDOT)
%
% Inputs:
%
%   ROBOT                   SMMS robot object.
% 
%   Q                       Px1 array of generalized joint positions.
% 
%   QDOT                    Vx1 array of generalized joint velocities.
% 
%   QDDOT                   Vx1 array of generalized joint accelerations.
% 
%   VQ                      PxD array of variation of generalized joint
%                           positions.
% 
%   VQDOT                   VxD array of variation of generalized joint
%                           velocities.
% 
%   VQDDOT                  VxD array of variation of generalized joint
%                           accelerations.
% 
% Outputs:
%
%   TJ                      4x4xB array of homogeneous transformation matrices
%                           of each joint.
%
%   VJ                      6xB array of twists of each joinr.
%
%   VJDOT                   6xB array of twist velocities of each joint.
%
%   VZJ                     6xVxB array of variation of homogeneous
%                           transformation matrices of each joint.
%
%   VVJ                     6xVxB array of variation of twists of each joint.
%
%   VVJDOT                  6xVxB array of variation of twist velocities of each
%                           joint.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Algorithm

% Number of...
nb = robot.NumBody;
nv = size(vq, 2);

% Base configuration: always fixed
T0 = robot.Ground.T;
X0 = Ad(tform2inv(T0));
V0 = zeros(6, 1);
V0dot = X0 * [ zeros(3, 1) ; robot.Gravity ];

% Base variation: always fixed
dT0 = zeros(6, nv);
dV0 = zeros(6, nv);
dV0dot = zeros(6, nv);

% Spatial transform and variation across joint i, in frame i
 TJ = tformzeros(nb);
dTJ = zeros(6, nv, nb);
% Spatial transform and variation from parent(i) to joint i
 XJ  = Ad(TJ);
dXJ = zeros(6, 6, nv, nb);
% Spatial twists and variation across joint i, in frame i
 VJ = zeros(6, nb);
dVJ = zeros(6, nv, nb);
% Spatial twist and variation velocities across joint i, in frame i
 VJdot = zeros(6, nb);
dVJdot = zeros(6, nv, nb);

% Spatial transform and variation of joint i
 Tj = tformzeros(nb);
dZj = zeros(6, nv, nb);
% Spatial twists and variation of joint i
 Vj = zeros(6, nb);
dVj = zeros(6, nv, nb);
% Spatial twist velocities and variation of joint i
 Vjdot = zeros(6, nb);
dVjdot = zeros(6, nv, nb);



%% Forward Reconstruction and Variation

% From base to tip
for ib = 1:1:nb
  % Get joint's position and velocity maps
  a = robot.PositionMap(ib,:);
  b = robot.VelocityMap(ib,:);
  
  % Parent index
  pid = robot.Parent(ib);
  
  % Parent is another body
  if pid > 0
    TP    = Tj(:,:,pid);
    VP    = Vj(:,pid);
    VPdot = Vjdot(:,pid);
    
    dTP    = dZj(:,:,pid);
    dVP    = dVj(:,:,pid);
    dVPdot = dVjdot(:,:,pid);
    
  % Parent is base
  else
    TP    = T0;
    VP    = V0;
    VPdot = V0dot;
    
    dTP    = dT0;
    dVP    = dV0;
    dVPdot = dV0dot;
    
  end
  
  % Joint object for quicker access
  jnt = robot.Joint{ib};
  
  %%%
  % Go through Joint
  %%%
  
  % Go through joint's variation: i -> parent(i)
  [ TJ(:,:,ib) , VJ(:,ib) , VJdot(:,ib) , dTJ(:,:,ib) , dVJ(:,:,ib) , dVJdot(:,:,ib) ] = tangentForwardKinematics( ...
      jnt ...
    ,      q(a(1):a(2)) ...
    ,   qdot(b(1):b(2)) ...
    ,  qddot(b(1):b(2)) ...
    ,     vq(b(1):b(2),:) ...
    ,  vqdot(b(1):b(2),:) ...
    , vqddot(b(1):b(2),:) ...
  );
  
  % Go from joint to parent: i- -> parent(i)
  TJ(:,:,ib) = tformmul(jnt.JointToParentTransform, TJ(:,:,ib));
  
  % Transformation: i -> parent(i) -> ... -> 0
  Tj(:,:,ib) = tformmul(TP, TJ(:,:,ib));
  
  % Adjoint transformation: parent(i) -> i
  XJ(:,:,ib) = Ad(tform2inv(TJ(:,:,ib)));
  
  % Twists of joint
  Vj(:,ib) = XJ(:,:,ib) * VP + ...
            + VJ(:,ib);
  
  % Twist velocities of joint
  Vjdot(:,ib) = XJ(:,:,ib) * VPdot + ...
              + ad(Vj(:,ib)) * VJ(:,ib) ...
              + VJdot(:,ib);
  
  %%%
  % Variation of kinematics
  %%%
  
  % Variation of adjoint transformation
  dXJ(:,:,:,ib) = pagemult(-ad(dTJ(:,:,ib)), XJ(:,:,ib));
  
  % Variation of transformation
  dZj(:,:,ib) = XJ(:,:,ib) * dTP + ...
                + dTJ(:,:,ib);
  
  % Varation of twists of joint
  dVj(:,:,ib) = XJ(:,:,ib) * dVP + permute(pagemult(dXJ(:,:,:,ib), VP), [1, 3, 2]) + ...
                + dVJ(:,:,ib);
  
  % Variation of twist velocities of joint
  dVjdot(:,:,ib) = XJ(:,:,ib) * dVPdot + permute(pagemult(dXJ(:,:,:,ib), VPdot), [1, 3, 2]) + ...
                  - ad(VJ(:,ib)) * dVj(:,:,ib) + ad(Vj(:,ib)) * dVJ(:,:,ib) + ...
                  + dVJdot(:,:,ib);
  
end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
