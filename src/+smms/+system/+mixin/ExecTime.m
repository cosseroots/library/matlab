classdef ExecTime < matlab.System
  %% EXECTIME
  
  
  
  %% PUBLIC NONTUNABLE PROPERTIES
  properties ( Nontunable )
    
    ExecutionTime (1, 1) double = 10 * 1e-3
    
  end
  
  
  
  %% FINAL METHODS
  methods ( Sealed )
    
    function tf = executes(obj, t)
      %% EXECUTES
      
      
      tf = isclose(rem(round(t * 1e3, 2), obj.ExecutionTime), 0);
      
    end
    
  end
  
end
