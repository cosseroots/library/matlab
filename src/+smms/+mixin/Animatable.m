classdef ( Abstract ) Animatable < smms.mixin.Drawable
  %% ANIMATABLE
  
  
  
  %% PUBLIC METHODS
  methods ( Sealed )
    
    function animate(obj, t, varargin)
      %% ANIMATE
      
      
      
      % Require at least one argument
      narginchk(2, Inf);
      
      % Get an axes object
      [cax, args] = axescheck(t, varargin{:});
      
      % Prepare an axes object if none given
      if isempty(cax) || ishghandle(cax, 'axes')
        cax = newplot(cax);
        pax = cax;
      else
        pax = cax;
        cax = ancestor(cax, 'Axes');
      end
      
      % Pop time vector off of all arguments
      t = reshape(args{1}, [], 1);
      args(1) = [];
      
      % Validate inputs
      [rargs, kvargs] = validateInputs(obj, args{:});
      
      % Get options for animation
      [opts, kvargs] = parseAnimationOptions(obj, kvargs);
      
      % Prepare required arguments to be suitable for animation
      rargs = parseAnimationArguments(obj, rargs{:});
      
      % Prepare input to match FPS data
      fps = optsget(opts, 'FPS', 25, 'fast');
      tframe = 1 / fps;
      tdelta = max(abs(diff(t)));
      if tframe - tdelta < 0 && ~isclose(tframe, tdelta)
        warning('Frame rate higher than simulation rate. Setting frame rate to simulation rate');
        tframe = tdelta;
      end
      tanim = reshape(unique([t(1):tframe:t(end), t(end)]), [], 1);
      [~, indT] = min(abs(reshape(t, 1, []) - reshape(tanim, [], 1)), [], 2);
      nframes = numel(indT);

      % Initialize animation variables
      iframe = 1;
      isdone = false;
      it = indT(iframe);
      
      % Add to current axes
      oldhold = ishold(cax);
      hold(cax, 'on');
      
      % Draw the graphics object
      hh = draw(obj, pax, rargs{:,it});
      if ~oldhold
        decorate(obj, cax);
      end
      
      % Set parent of graphics object to be the axes object
      set(hh, 'Parent', pax);
      
      % Initialize axes title
      thstr = [ '%.3fs / ' , sprintf('%.3fs', t(end)) ];
      fhTitle = @(t) sprintf(thstr, t);
      cax.Title.Interpreter = 'latex';
      cax.Title.String = fhTitle(t(it));
      
      % Put axes into foreground
      cax.Visible = 'on';
      set(cax, 'Layer', 'top');
      
      % Enable rotation on axes
      rotate3d(cax);
      
      % Run start function
      opts.StartFcn(cax, t(it), rargs{:,it});
      
      % Remove hold of axes
      if ~oldhold
        hold(cax, 'off');
      end
      
      % Loop for animation
      while ~isdone && isvalid(cax)
        % Get time index based on frame index
        it = indT(iframe);
        
        % Redraw everything
        redraw(obj, hh, rargs{:,it});
        
        % Update axes title
        cax.Title.String = fhTitle(t(it));
      
        % Run update function
        opts.UpdateFcn(cax, t(it), rargs{:,it});
        
        % Update drawing
        drawnow('limitrate');
        
        % Advance counter
        iframe = iframe + 1;
        % And check if we are done animating
        isdone = iframe > nframes;
        
      end
      
      % Run stop function
      opts.StopFcn(cax, t(it), rargs{:,it});
      
    end
    
  end
  
  
  
  %% INTERNAL METHODS
  methods ( Access = protected )
    
    function rargs = parseAnimationArguments(obj, varargin)
      %% PARSEANIMATIONARGUMENTS
      
      
      nin = getNumInputs(obj);
      args = cell(1, nin(1));
      
      [args{1:end}] = parseAnimationArgumentsImpl(obj, varargin{:});
      
      rargs = cat(2, args{:});
      
    end
    
    
    function [opts, kvargs] = parseAnimationOptions(obj, kvargs)
      %% PARSEANIMATIONOPTIONS
      
      
      
      animopts = struct( ...
          'StartFcn'  , @noop ...
        , 'StopFcn'   , @noop ...
        , 'UpdateFcn' , @noop ...
        , 'FPS'       , 25 ...
      );
      
      kvargs_ = cell2struct(kvargs(2:2:end), kvargs(1:2:end), 2);
      
      opts    = animopts * kvargs_;
      kvargs_ = kvargs_ - opts;
      
      kvargs = transpose([ fieldnames(kvargs_) , struct2cell(kvargs_) ]);
      kvargs = transpose(cat(1, kvargs(:)));
      
    end
    
  end
  
  
  
  %% ABSTRACT INTERNAL IMPLEMENTATION METHODS
  methods ( Access = protected )
    
    function varargout = parseAnimationArgumentsImpl(obj, varargin)
      %% PARSEANIMATIONARGUMENTSIMPL
      
      
      
      % Loop over every argument and turn it into a cell array splitting it
      % along its last dimension
      nargs = nargin - 1;
      varargout = cell(1, nargs);
      for iarg = 1:nargs
        varargout{iarg} = num2cell(varargin{iarg}, 1);
        
      end
      
    end
    
  end
  
end
