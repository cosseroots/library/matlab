classdef ( Abstract ) Parentable < smms.internal.InternalAccess
  %% PARENTABLE
  
  
  
  %% SMMS INTERNAL PROPERTIES
  properties ( Access = protected )
    
    Parent
    
  end
  
  
  
  %% PUBLIC METHODS
  methods
    
    function pobj = parent(obj, pobj)
      %% PARENT
      
      
      % PARENT(OBJ, POBJ)
      if nargin > 1
        obj.Parent = pobj;
        
      % POBJ = PARENT(OBJ)
      else
        pobj = obj.Parent;
        
      end
      
    end
    
  end
  
  
  
  %% SETTERS
  methods
    
    function set.Parent(obj, v)
      %% SET.PARENT
      
      
      preSetParentImpl(obj);
      obj.Parent = v;
      postSetParentImpl(obj);
      
    end
    
  end
  
  
  
  %% INTERNAL CALLBACK METHODS
  methods ( Access = protected )
    
    function preSetParentImpl(obj)
      %% PRESETPARENTIMPL
      
      
    end
    
    
    function postSetParentImpl(obj)
      %% POSTSETPARENTIMPL
      
      
    end
    
  end
  
end
