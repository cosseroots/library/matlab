classdef ( Abstract ) Drawable < handle
  %% DRAWABLE
  
  
  
  %% PUBLIC PROPERTIES
  properties ( Dependent )
    
    % Cell array of styling options used during drawing
    Styling (1, 1) struct
    
  end
  
  
  
  %% INTERNAL PROPERTIES
  properties ( Access = { ?smms.internal.InternalAccess } )
    
    % Represent styling as a cell array
    StylingCell (1, :) cell = cell(1, 0)
    
  end
  
  
  
  %% INTERNAL OBJECT PROPERTIES
  properties ( Access = private )
    
    % Number of inputs of `DRAW` as [ MIN , MAX ]
    NumInputs
    
    % Number of outputs of `DRAW` as [ MIN , MAX ]
    NumOutputs
    
  end
  
  
  
  %% PUBLIC DRAWING METHODS
  methods ( Sealed )
    
    function n = nargin(obj)
      %% NARGIN
      
      
      nin = getNumInputs(obj);
      n = 1 + nin(1);
      
    end
    
    
    function n = nargout(obj)
      %% NARGOUT
      
      
      nout = getNumOutputs(obj);
      n = nout(1);
      
    end
    
    
    function h = draw(obj, varargin)
      %% DRAW
      
      
      
      % Require at least one argument
      narginchk(1, Inf);
      
      % Get an axes object
      [cax, args] = axescheck(varargin{:});
      
      % Prepare an axes object if none given
      if isempty(cax) || ishghandle(cax, 'axes')
        cax = newplot(cax);
        pax = cax;
      else
        pax = cax;
        cax = ancestor(cax, 'Axes');
      end
      
      % Validate inputs
      [rargs, kvargs] = validateInputs(obj, args{:});
      
      % Add to current axes
      if ~ishold(cax)
        coDecorate = onCleanup(@() decorate(obj, cax));
        coHold = onCleanup(@() hold(cax, 'off'));
      end
      hold(cax, 'on');
      
      % Draw the graphics object
      hh = drawImpl(obj, pax, rargs{:});
      
      % Set parent of graphics object to be the axes object
      set(hh, 'Parent', pax);
      
      % Style the graphics object
      sargs = defaultStyle(obj);
      style(obj, hh, sargs{:}, obj.StylingCell{:}, kvargs{:});
      
      % Put axes into foreground
      cax.Visible = 'on';
      set(cax, 'Layer', 'top');
      
      % H = DRAW(OBJ, ___)
      if nargout > 0
        h = hh;
      end
      
    end
    
    
    function redraw(obj, h, varargin)
      %% REDRAW
      
      
      
      % Require at least one argument
      narginchk(2, Inf);
      
      % Validate number of inputs
      validateNumInputsOutputs(obj, numel(varargin), []);
      
      % Redraw the graphics object
      redrawImpl(obj, h, varargin{:});
      
    end
    
    
    function decorate(obj, ax, varargin)
      %% DECORATE
      
      
      
      decorateImpl(obj, ax);
      
    end
    
    
    function style(obj, h, varargin)
      %% STYLE
      
      
      
      % Call the underlying style implementation in order to let developers have
      % some freedom over how to process the style arguments
      if nargin > 2
        styleImpl(obj, h, varargin{:});
      end
      
    end
    
    
    function s = defaultStyle(obj)
      %% DEFAULTSTYLE
      
      
      
      s = defaultStyleImpl(obj);
      if isstruct(s)
        s = reshape([ fieldnames(s).' ; struct2cell(s).' ], 1, []);
      end
      
    end
    
  end
  
  
  
  %% GETTERS
  methods
    
    function v = get.Styling(obj)
      %% GET.STYLING
      
      
      
      v = struct(obj.StylingCell{:});
      
    end
    
  end
  
  
  
  %% SETTERS
  methods
    
    function set.Styling(obj, v)
      %% SET.STYLING
      
      
      
      obj.StylingCell = reshape(transpose(cat(2, fieldnames(v), struct2cell(v))), 1, []);
      
    end
    
  end
  
  
  
  %% INTERNAL DRAWING METHODS
  methods ( Sealed, Access = protected )
    
    
    function n = getNumInputs(obj)
      %% GETNUMINPUTS
      
      
      if isempty(obj.NumInputs)
        obj.NumInputs = getNumInputsImpl(obj);
        
      end
      
      n = obj.NumInputs;
      
    end
    
    
    function n = getNumOutputs(obj)
      %% GETNUMOUTPUTS
      
      
      if isempty(obj.NumOutputs)
        obj.NumOutputs = getNumOutputsImpl(obj);
        
      end
      
      n = obj.NumOutputs;
      
    end
    
    
    function n = getNumInputsImpl(obj)
      %% GETNUMINPUTSIMPL
      
      
      % Obtain number of `DRAWIMPL`s inputs by inspection
      ml = meta.class.fromName(class(obj)).MethodList;
      ms = ml(strcmp({ml.Name}, 'drawImpl'));
      namesin = ms.InputNames;
      
      % Count the number of arguments to `DRAWIMPL` excluding "OBJ" and "AX"
      n = [ numel(namesin) - 2 , Inf ];
      % If user defined `VARARGIN` as argument, then subtract it from the
      % minimum number of inputs
      if any(strcmp('varargin', namesin))
        n(1) = n(1) - 1;
      end
      
    end
    
    
    function n = getNumOutputsImpl(obj)
      %% GETNUMOUTPUTSIMPL
      
      
      % Obtain number of `DRAWIMPL`s outputs by inspection
      ml = meta.class.fromName(class(obj)).MethodList;
      ms = ml(strcmp({ml.Name}, 'drawImpl'));
      namesout = ms.OutputNames;
      
      % Find `VARARGOUT` in the list of outputs
      indVarg = find(strcmp('varargout', namesout), 1, 'first');
      if ~isempty(indVarg)
        n = [ indVarg - 1 , Inf ];
        
      % No `VARARGIN` so number of arguments can be inferred from list of
      % arguments
      elseif numel(namesout) > 1
        n = [ 1 , 1 ] .* numel(namesout);
      
      % Just one output argument, per definition this can be optional
      else
        n = [ 0 , 1 ];
        
      end
      
    end
    
    
    function validateNumInputsOutputs(obj, nin, nout)
      %% VALIDATENUMINPUTSOUTPUTS
      
      
      % Validate number of inputs
      numin  = getNumInputs(obj);
      % Too few
      if nin < numin(1)
        throwAsCaller(MException(message('MATLAB:narginchk:notEnoughInputs')));
        
      % Too many
      elseif numin(2) < nin
        throwAsCaller(MException(message('MATLAB:narginchk:tooManyInputs')));
        
      end
      
      % Validate number of outputs only if they are given (this is not the case
      % when validating inputs to the `SETUP` method
      if ~isempty(nout)
        % Validate number of outputs
        numout = getNumOutputs(obj);
      
        % Too few
        if nout < numout(1)
          throwAsCaller(MException(message('MATLAB:nargoutchk:notEnoughOutputs')));

        % Too many
        elseif numout(2) < nout
          throwAsCaller(MException(message('MATLAB:nargoutchk:tooManyOutputs')));
        end
        
      end
      
    end
    
    
    function [rargs, kvargs] = validateInputs(obj, varargin)
      %% VALIDATEINPUTS
      
      
      
      % Parse parameters into required and key/value arguments
      [rargs, kvargs] = pvparams(varargin);
      kvargs = matlab.graphics.internal.convertStringToCharArgs(kvargs);
      
      % Validate number of inputs
      validateNumInputsOutputs(obj, numel(rargs), [])
      
    end
    
  end
  
  
  
  %% INTERNAL DRAWING IMPLEMENTATION METHODS
  methods ( Access = protected )
    
    function styleImpl(~, h, varargin)
      %% STYLEIMPL
      
      
      
      set(h, varargin{:});
      
    end
    
    
    function s = defaultStyleImpl(~)
      %% DEFAULTSTYLEIMPL
      
      
      
      s = {};
      
    end
    
    
    function decorateImpl(~, ~)
      %% DECORATEIMPL
      
      
      
    end
    
    
    function redrawImpl(~, h, varargin)
      %% REDRAWIMPL
      
      
      
    end
    
  end
  
  
  
  %% ABSTRACT INTERNAL DRAWING IMPLEMENTATION METHODS
  methods ( Abstract , Access = protected )
    
    h = drawImpl(obj, cax, varargin)
      %% DRAWIMPL
    
  end
  
end
