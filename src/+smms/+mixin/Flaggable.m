classdef ( Abstract ) Flaggable < handle
  %% FLAGGABLE
  
  
  
  %% INTERNAL PROPERTIES
  properties ( Access = private )
    
    Flags      = cell(1, 0);
    FlagValues = false(1, 0);
    
  end
  
  
  
  %% CONSTRUCTOR METHODS
  methods
    
    function obj = Flaggable(varargin)
      %% FLAGGABLE
      
      
      obj@handle();
      
      % Initialize flags
      obj.Flags = varargin;
      obj.FlagValues = false(1, nargin);
      
    end
    
  end
  
  
  
  %% FLAGGING METHODS
  methods ( Sealed )
    
    function f = flagged(obj, k)
      %% FLAGGED
      
      
      f = isFlagged(obj, k);
      
    end
    
    
    function f = isFlagged(obj, k)
      %% ISFLAGGED
      
      
      f = false;
      
      ind = find(strcmp(k, obj.Flags), 1, 'first');
      if ~isempty(ind)
        f = obj.FlagValues(ind);
      end
      
    end
    
    
    function flag(obj, k)
      %% FLAG
      
      
      ind = find(strcmp(k, obj.Flags), 1, 'first');
      if isempty(ind)
        obj.Flags      = cat(2, obj.Flags, k);
        obj.FlagValues = cat(2, obj.FlagValues, false);
        ind = numel(obj.Flags);
      end
      
      obj.FlagValues(ind) = true;
      
    end
    
    
    function unflag(obj, k)
      %% UNFLAG
      
      
      ind = find(strcmp(k, obj.Flags), 1, 'first');
      if isempty(ind)
        obj.Flags      = cat(2, obj.Flags, k);
        obj.FlagValues = cat(2, obj.FlagValues, false);
        ind = numel(obj.Flags);
      end
      
      obj.FlagValues(ind) = false;
      
    end
    
  end
  
end
