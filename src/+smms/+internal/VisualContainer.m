classdef ( Abstract ) VisualContainer < handle
  %% VISUALCONTAINER
  
  
  
  %% PUBLIC PROPERTIES
  properties
    
    Visual (1, :) = cell(1, 0)
    
  end
  
  
  
  %% SETTERS
  methods
    
    function set.Visual(obj, v)
      %% SET.VISUAL
      
      
      
      % Default value if `V` is empty
      if isempty(v)
        v = cell(1, 0);
      
      % If `V` is not a cell, convert it to one
      elseif ~iscell(v)
        v = cell(v);
        
      end
      
      obj.Visual = v;
      
    end
    
  end
  
end
