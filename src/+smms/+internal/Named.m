classdef ( Abstract ) Named < smms.internal.InternalAccess
  %% NAMED
  
  
  
  %% PUBLIC READ-ONLY PROPERTIES
  properties ( SetAccess = { ?smms.internal.InternalAccess } )
    
    Name (1, :) char = 'UNNAMED';
    
  end
  
  
  
  %% CONSTRUCTOR METHODS
  methods
    
    function obj = Named()
      %% NAMED
      
      
      
      obj@smms.internal.InternalAccess();
      
    end
    
  end
  
  
  
  %% SETTERS
  methods
    
    function set.Name(obj, v)
      %% SET.NAME
      
      
      
      obj.Name = convertStringsToChars(v);
      
    end
    
  end
  
end
