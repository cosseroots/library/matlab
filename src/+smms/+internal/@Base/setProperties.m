function setProperties(obj, nargs, varargin)
%% SETPROPERTIES
%
% SETPROPERTIES(OBJ, NARGS, NAME1, VALUE1, ...) provides the name-value pair
% inputs to the System object™ constructor. Use this syntax if every input must
% specify both name and value.
%
% SETPROPERTIES(OBJ, NARGS, ARG1, ..., ARGN, PROPNAME1, ..., PROPNAMEN) provides
% the value-only inputs, which you can follow with the name-value pair inputs to
% the System object during object construction. Use this syntax if you want to
% allow users to specify one or more inputs by their values only.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% SETPROPERTIES(OBJ, NARGS)
% SETPROPERTIES(OBJ, NARGS, NAME1, VALUE1, ...)
% SETPROPERTIES(OBJ, NARGS, ARG1, ..., ARGN, PROPNAME1, ..., PROPNAMEN)
narginchk(2, Inf);

% SETPROPERTIES(___)
nargoutchk(0, 0);



%% Algorithm

% Get given arguments and the required properties
args   = varargin(1:nargs);
rprops = varargin((nargs + 1):end);

% Check we have at least as many arguments as required properties
if numel(args) < numel(rprops)
  throwAsCaller(MException( ...
      'MATLAB:narginchk:NotEnoughInputs' ...
      , 'Incorrect number of name/value pairs. Got %d %s {%s}, require at least %d {%s}.' ...
      , numel(args) ...
      , pluralize(numel(rprops), 'argument') ...
      , strjoin(rprops(1:numel(args)), ', ') ...
      , numel(rprops) ...
      , strjoin(rprops, ', ') ...
    ));
end

% Process required arguments
while numel(rprops) > 0 && numel(args) > 0
  % Get current property
  rprop = rprops{1};
  rprops(1) = [];
  
  % Get value
  rval = args{1};
  args(1) = [];
  
  % Set property
  obj.(rprop) = rval;

end

% Check we have an even number of arguments
if mod(numel(args), 2) > 0
  throwAsCaller(MException('MATLAB:narginchk:NotEnoughInputs', 'Incorrect number of name/value pairs. Must be an even number.'));
end

% Loop over all remaining arguments (which may also be Name/Value pairs of
% required arguments
while numel(args) > 0
  % Get name of Name/Value pair
  nvname = args{1};
  nvval = args{2};
  args([1,2]) = [];
  
  % Set property
  try
    obj.(nvname) = nvval;
    
  catch me
    throwAsCaller(me);
    
  end
  
end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
