classdef ( Abstract ) Base < smms.internal.InternalAccess
  %% BASE
  
  
  
  %% INTERNAL PROPERTIES
  properties ( Access = protected )
    
    % Flag whether the class object is locked or not i.e., in use or not
    BaseObjIsLocked = false;
    
  end
  
  
  
  %% CONSTRUCTOR METHODS
  methods
    
    function obj = Base()
      %% BASE
      
      
      obj@smms.internal.InternalAccess();
      
    end
    
  end
  
  
  
  %% PUBLIC METHODS
  methods
    
    function delete(obj)
      %% DELETE
      
      
      % Deleting any base object is equal to releaseing it
      release(obj);
      
    end
    
  end
  
  
  
  %% PUBLIC FINAL METHODS
  methods ( Sealed )
    
    function setup(obj, varargin)
      %% SETUP
      
      
      if isLocked(obj)
        warning('Reported by %s: The object has reinitialized because the setup method was called on an initialized object. This will affect performance.', class(obj));
        release(obj);
      end
      
      setupImpl(obj);
      lock(obj);
      
    end
    
    
    function reset(obj)
      %% RESET
      
      
      if isLocked(obj)
        resetImpl(obj);
      end
      
    end
    
    
    function release(obj)
      %% RELEASE
      
      
      % Call release function
      if isLocked(obj)
        % Unlock object
        unlock(obj);
        
        % Then release it
        releaseImpl(obj);
        
      end
      
    end
    
    
    function f = isLocked(obj)
      %% ISLOCKED
      
      
      
      f = obj.BaseObjIsLocked;
      
    end
    
  end
  
  
  
  %% PROTECTED FINAL METHODS
  methods ( Sealed , Access = { ?smms.internal.InternalAccess } )
    
    setProperties(obj, nargs, varargin)
    %% SETPROPERTIES
    %
    % SETPROPERTIES(OBJ, NARGS, NAME1, VALUE1, ...) provides the name-value pair
    % inputs to the algorithm object's constructor. Use this syntax if every
    % input must specify both name and value.
    %
    % SETPROPERTIES(OBJ, NARGS, ARG1, ..., ARGN, PROPNAME1, ..., PROPNAMEN)
    % provides the value-only inputs, which you can follow with the name-value
    % pair inputs to the algorithm object during object construction. Use this
    % syntax if you want to allow users to specify one or more inputs by their
    % values only.
    
    
    function prepareObjectForUse(obj)
      %% PREPAREOBJECTFORUSE
      
      
      if ~isLocked(obj)
        setup(obj);
        reset(obj);
        
      end
      
    end
    
    
    function lock(obj)
      %% LOCK
      
      
      obj.BaseObjIsLocked = true;
      
    end
    
    
    function unlock(obj)
      %% UNLOCK
      
      
      obj.BaseObjIsLocked = false;
      
    end
      
  end
  
  
  
  %% INTERNAL METHODS
  methods ( Access = protected )
    
    function releaseImpl(~)
      %% RELEASEIMPL
      
      
    end
    
    
    function resetImpl(~)
      %% RESETIMPL
      
      
    end
    
    
    function setupImpl(~, varargin)
      %% SETUPIMPL
      
      
    end
    
  end
  
end
