classdef JointVelocityChart < smms.graphics.mixin.TimeseriesChart
  %% JOINTVELOCITYCHART
  %
  % JOINTVELOCITYCHART('Time', T, 'Qdot', QDOT)
  %
  % Inputs:
  %
  %   T                       Nx1 array of time values.
  %
  %   QDOT                    NxV array of joint velocity values over time.
  
  
  
  %% PUBLIC PROPERTIES
  properties
    
    Qdot (:, :) double
    
  end
  
  
  
  %% DEPENDENT READ-ONLY PROPERTIES
  properties ( Dependent , SetAccess = protected )
    
    % Number of generalized coordinates
    NumQdot (1, 1) double
    
  end
  
  
  
  %% GETTERS
  methods
    
    function v = get.NumQdot(obj)
      %% GET.NUMQ
      
      
      
      v = size(obj.Qdot, 2);
      
    end
    
  end
  
  
  
  %% CONCRETE GRAPHICS METHODS
  methods ( Access = protected )
    
    function setupImpl(obj)
      %% SETUPIMPL
      
      
      
      setupImpl@smms.graphics.mixin.TimeseriesChart(obj);
      
      % Need to know how much Q to plot
      nq = obj.NumQdot;
      
      % Initialize children
      obj.Children = gobjects(nq, 1);
      
      % Get axes object
      ax = getAxes(obj);
      hold(ax, 'on');
      
      % Loop over all coordinates to plot
      for iq = 1:nq
        obj.Children(iq) = plot( ...
            ax ...
          , NaN, NaN ...
          , 'LineStyle'   , '-' ...
          , 'DisplayName' , sprintf('$ \\dot{ q }_{ %.0f } $', iq) ...
          , 'Tag'         , [ 'qdot' , num2str(iq) ] ...
        );
        
      end
      
    end
    
    
    function updateImpl(obj)
      %% UPDATEIMPL
      
      
      
      % Call parent's update method
      updateImpl@smms.graphics.mixin.TimeseriesChart(obj);
      
      nq = obj.NumQdot;
      for iq = 1:nq
        set( ...
            obj.Children(iq) ...
          , 'XData' , obj.Time ...
          , 'YData' , obj.Qdot(:,iq) ...
        );
        
      end
      
    end
    
    
    function decorateImpl(obj)
      %% DECORATEIMPL
      
      
      
      % Parent's decoration
      decorateImpl@smms.graphics.mixin.TimeseriesChart(obj);
      
      % Our decoration
      ax = getAxes(obj);
      
      % Title
      ax.Title.String = 'Joint velocities';
      
      % X-Axis
      
      % Y-Axis
      ax.YLabel.String = '$ \dot{ q }_{ i } / \mathrm{ s }^{ -1 } $';
      
      % Tagging
      ax.Tag = 'JointVelocityChart';
      
    end
    
  end
  
end
