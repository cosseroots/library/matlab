classdef SE3ErrorChartContainer < smms.graphics.TwistChartContainer
  %% SE3ERRORCHARTCONTAINER
  %
  % SE3ERRORCHARTCONTAINER('Time', T, 'Error', E)
  %
  % Inputs:
  %
  %   T                     Nx1 array of time values
  %
  %   E                     Nx6xB array of errors where B is the number of
  %                         bodies for which to display the SE3 errors.
  
  
  
  %% PUBLIC DEPENDENT PROPERTIES
  properties ( Dependent )
    
    % Error in SE(3)
    Error (:, 6, :)
    
  end
  
  
  
  %% SETTERS
  methods
    
    function set.Error(obj, v)
      %% SET.ERROR
      
      
      
      obj.WU = v;
      
    end
    
  end
  
  
  
  %% GETTERS
  methods
    
    function v = get.Error(obj)
      %% GET.ERROR
      
      
      
      v = obj.WU;
      
    end
    
  end
  
  
  
  %% CONCRETE GRAPHICS METHODS
  methods ( Access = protected )
    
    function setupImpl(obj)
      %% SETUPIMPL
      
      
      
      % First, call parent's setup method
      setupImpl@smms.graphics.mixin.TimeseriesChartContainer(obj);
      
      % Get layout
      hl = getLayout(obj);
      hl.GridSize = [ 2 , 1 ];
      
      % Create children inside the given layout
      obj.Children = [ ...
          smms.graphics.SO3ErrorChart(hl) ...
        , smms.graphics.R3ErrorChart(hl) ...
      ];
      
    end
    
    
    function decorateImpl(obj)
      %% DECORATEIMPL
      
      
      
      % Let base class decorate first
      decorateImpl@smms.graphics.TwistChartContainer(obj);
      
      % Layout object for further manipulation
      hl = getLayout(obj);
      
      % Title
      hl.Title.String = '$ \mathrm{ SE }(3) $ error';
      
      % Tagging
      hl.Tag = 'SE3ErrorChartContainer';
      
    end
    
  end
  
end
