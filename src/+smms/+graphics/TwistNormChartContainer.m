classdef TwistNormChartContainer < smms.graphics.mixin.TimeseriesChartContainer
  %% TWISTNORMCHARTCONTAINER
  %
  % TWISTNORMCHARTCONTAINER('Time', T, 'WU', WU)
  
  
  
  %% PUBLIC PROPERTIES
  properties 
    
    % Spatial twists of bodies
    WU (:, 6, :) double
    
  end
  
  
  
  %% PUBLIC DEPENDENT PROPERTIES
  properties ( Dependent )
    
    W (:, 3, :) double
    U (:, 3, :) double
    
  end
  
  
  
  %% GETTERS
  methods
    
    function v = get.W(obj)
      %% GET.W
      
      
      
      v = obj.WU(:,1:3,:);
      
    end
    
    
    function v = get.U(obj)
      %% GET.U
      
      
      
      v = obj.WU(:,4:6,:);
      
    end
    
  end
  
  
  
  
  %% CONCRETE GRAPHICS METHODS
  methods ( Access = protected )
    
    function setupImpl(obj)
      %% SETUPIMPL
      
      
      
      % First, call parent's setup method
      setupImpl@smms.graphics.mixin.TimeseriesChartContainer(obj);
      
      % Get layout
      hl = getLayout(obj);
      hl.GridSize = [ 2 , 1 ];
      
      % Create children inside the given layout
      obj.Children = [ ...
          smms.graphics.AngularTwistNormChart(hl) ...
        , smms.graphics.LinearTwistNormChart(hl) ...
      ];
      
    end
    
    
    function updateImpl(obj)
      %% UPDATEIMPL
      
      
      
      % Call parent's update implementation
      updateImpl@smms.graphics.mixin.TimeseriesChartContainer(obj);
      
      % Update children
      draw( ...
          obj.Children(1) ...
        , 'Time', obj.Time ...
        , 'W'   , obj.W ...
      );
      draw( ...
          obj.Children(2) ...
        , 'Time', obj.Time ...
        , 'U'   , obj.U ...
      );
      
    end
    
    
    function decorateImpl(obj)
      %% DECORATEIMPL
      
      
      
      % Decorate children
      decorate(obj.Children(1));
      decorate(obj.Children(2));
      
      % Call parent's decorator
      decorateImpl@smms.graphics.mixin.TimeseriesChartContainer(obj);
      
      % Decorate layout
      hl = getLayout(obj);
      
      % Title
      hl.Title.String = 'Angular and Linear Twists Norm';
      
      % Tagging
      hl.Tag = 'TwistStateNormChartContainer';
      
    end
    
  end
  
end
