classdef SMMSTreeChartContainer < smms.graphics.mixin.ChartContainer
  %% SMMSTREECHARTCONTAINER
  
  
  
  %% PUBLIC PROPERTIES
  properties
    
    % SMMS tree object
    SMMSTree
    
    % States to snapshot
    Q
    
    % Number of Charts
    NumChart (1, 1) double = 4
    
  end
  
  
  
  %% CONCRETE CONTAINER METHODS
  methods ( Access = protected )
    
    function setupImpl(obj)
      %% SETUPIMPL
      
      
      
      setupImpl@smms.graphics.mixin.ChartContainer(obj);
      
      hl = getLayout(obj);
      nc = obj.NumChart;
      obj.Children = arrayfun(@(iq) smms.graphics.SMMSTreeChart(hl, 'SMMSTree', obj.SMMSTree), reshape(1:nc, [], 1));
      
    end
    
    
    function updateImpl(obj)
      %% UPDATEIMPL
      
      
      
      updateImpl@smms.graphics.mixin.ChartContainer(obj);
      
      nc = obj.NumChart;
      for iq = 1:nc
        draw(obj.Children(iq), 'Q', permute(obj.Q(iq,:), [2, 1, 3]));
      end
      
      linkprop([ obj.Children.Axes ], { 'XLim' , 'YLim' , 'ZLim' , 'View' });
      
    end
    
    
    function decorateImpl(obj)
      %% DECORATEIMPL
      
      
      
      decorateImpl@smms.graphics.mixin.ChartContainer(obj);
      
      arrayfun(@decorate, obj.Children);
      
      linkprop([ obj.Children.Axes ], { 'XLim' , 'YLim' , 'ZLim' , 'View' });
      
    end
    
  end
  
end
