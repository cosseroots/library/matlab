classdef AngularTwistNormChart < smms.graphics.mixin.TimeseriesChart ....
    & smms.graphics.mixin.TwistChart
  %% ANGULARTWISTNORMCHART
  %
  % ANGULARTWISTNORMCHART('Time', T, 'W', W)
  
  
  
  %% PUBLIC DEPENDENT PROPERTIES
  properties ( Dependent )
    
    % Angular twists [ T , W , B ]
    W (:, 3, :)
    
  end
  
  
  
  %% GETTERS
  methods
    
    function v = get.W(obj)
      %% GET.W
      
      
      
      v = obj.Twist;
      
    end
    
  end
  
  
  
  %% SETTERS
  methods
    
    function set.W(obj, v)
      %% SET.W
      
      
      
      obj.Twist = v;
      
    end
    
  end
  
  
  
  %% CONCRETE CHART METHODS
  methods ( Access = protected )
    
    function setupImpl(obj)
      %% SETUPIMPL
      
      
      
      % Call parent's setup method
      setupImpl@smms.graphics.mixin.TimeseriesChart(obj);
      
      % Prepare place holder for graphics objects
      nb = obj.NumBody;
      obj.Children = gobjects(nb, 1);
      
      % Prepare axes object
      ax = getAxes(obj);
      hold(ax, 'on');
      
      % Loop over each body
      for ib = 1:nb
        % Simple plot
        obj.Children(ib) = plot( ...
            ax ...
          , NaN, NaN ...
          , 'LineStyle'   , '-' ...
          , 'DisplayName' , sprintf('Body $ %0.f $', ib) ...
          , 'Tag'         , sprintf('B%.0f', ib) ...
        );
        
      end
      
    end
    
    
    function updateImpl(obj)
      %% UPDATEIMPL
      
      
      
      % Call parent's update method
      updateImpl@smms.graphics.mixin.TimeseriesChart(obj);
      
      % Loop over each body
      nb = obj.NumBody;
      for ib = 1:nb
        % Update
        set( ...
            findobj( ...
                obj.Children ...
              , 'Tag', sprintf('B%.0f', ib) ...
            ) ...
          , 'XData', obj.Time ...
          , 'YData', vecnorm(obj.W(:,:,ib), 2, 2) ...
        );
          
      end
      
    end
    
    
    function decorateImpl(obj)
      %% DECORATEIMPL
      
      
      
      % First, let parent decorate the basics
      decorateImpl@smms.graphics.mixin.TimeseriesChart(obj);
      
      % Get axes object
      ax = getAxes(obj);
      
      % Title
      ax.Title.String = 'Linear Twists Norm';
      
      % X-Axis
      
      % Y-Axis
      ax.YLabel.String = sprintf('$ \\Vert %s \\Vert / \\mathrm{ rad } \\, \\mathrm{ s }^{ -1 } $', obj.AngularName);
      
      % Tagging
      ax.Tag = 'AngularTwistNormChart';
      
    end
    
  end
  
end
