classdef R3ErrorChart < smms.graphics.LinearTwistChart
  %% R3ERRORCHART
  %
  % R3ERRORCHART('Time', T, 'Error', E)
  
  
  
  %% PUBLIC DEPENDENT PROPERTIES
  properties ( Dependent )
    
    % Error in R3
    Error (:, 3, :)
    
  end
  
  
  
  %% SETTERS
  methods
    
    function set.Error(obj, v)
      %% SET.ERROR
      
      
      
      obj.Twist = v;
      
    end
    
  end
  
  
  
  %% GETTERS
  methods
    
    function v = get.Error(obj)
      %% GET.ERROR
      
      
      
      v = obj.Twist;
      
    end
    
  end
  
  
  
  %% CONCRETE GRAPHICS METHODS
  methods ( Access = protected )
    
    function decorateImpl(obj)
      %% DECORATEIMPL
      
      
      
      % Let base class decorate first
      decorateImpl@smms.graphics.LinearTwistChart(obj);
      
      % Axes objec to further manipulate it
      ax = getAxes(obj);
      
      % Title
      ax.Title.String = '$ \mathrm{ R }^{ 3 } $ Error';
      
      % Y-Axis
      ax.YAxis.Label.String = sprintf('$ %s / \\mathrm{ m } $', obj.LinearName);
      
      % Tagging
      ax.Tag = 'R3ErrorChart';
      
    end
    
  end
  
end
