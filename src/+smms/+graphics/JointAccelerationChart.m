classdef JointAccelerationChart < smms.graphics.mixin.TimeseriesChart
  %% JOINTACCELERATIONCHART
  %
  % JOINTACCELERATIONCHART('Time', T, 'Qddot', QDDOT)
  %
  % Inputs:
  %
  %   T                       Nx1 array of time values.
  %
  %   QDDOT                   NxV array of joint acceleration values over time.
  
  
  
  %% PUBLIC PROPERTIES
  properties
    
    Qddot (:, :) double
    
  end
  
  
  
  %% DEPENDENT READ-ONLY PROPERTIES
  properties ( Dependent , SetAccess = protected )
    
    % Number of generalized coordinates
    NumQddot (1, 1) double
    
  end
  
  
  
  %% GETTERS
  methods
    
    function v = get.NumQddot(obj)
      %% GET.NUMQ
      
      
      
      v = size(obj.Qddot, 2);
      
    end
    
  end
  
  
  
  %% CONCRETE GRAPHICS METHODS
  methods ( Access = protected )
    
    function setupImpl(obj)
      %% SETUPIMPL
      
      
      
      setupImpl@smms.graphics.mixin.TimeseriesChart(obj);
      
      % Need to know how much Q to plot
      nq = obj.NumQddot;
      
      % Initialize children
      obj.Children = gobjects(nq, 1);
      
      % Get axes object
      ax = getAxes(obj);
      hold(ax, 'on');
      
      % Loop over all coordinates to plot
      for iq = 1:nq
        obj.Children(iq) = plot( ...
            ax ...
          , NaN, NaN ...
          , 'LineStyle'   , '-' ...
          , 'DisplayName' , sprintf('$ \\ddot{ q }_{ %.0f } $', iq) ...
          , 'Tag'         , [ 'qddot' , num2str(iq) ] ...
        );
        
      end
      
    end
    
    
    function updateImpl(obj)
      %% UPDATEIMPL
      
      
      
      % Call parent's update method
      updateImpl@smms.graphics.mixin.TimeseriesChart(obj);
      
      nq = obj.NumQddot;
      for iq = 1:nq
        set( ...
            obj.Children(iq) ...
          , 'XData' , obj.Time ...
          , 'YData' , obj.Qddot(:,iq) ...
        );
        
      end
      
    end
    
    
    function decorateImpl(obj)
      %% DECORATEIMPL
      
      
      
      % Parent's decoration
      decorateImpl@smms.graphics.mixin.TimeseriesChart(obj);
      
      % Our decoration
      ax = getAxes(obj);
      
      % Title
      ax.Title.String = 'Joint accelerations';
      
      % X-Axis
      
      % Y-Axis
      ax.YLabel.String = '$ \ddot{ q }_{ i } / \mathrm{ s }^{ -2 } $';
      
      % Tagging
      ax.Tag = 'JointAccelerationChart';
      
    end
    
  end
  
end
