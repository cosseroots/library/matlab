classdef SMMSTreeChart < smms.graphics.mixin.SMMSTreeChart
  %% SMMSTREECHART
  %
  % SMMSTREECHART('SMMSTree', SMMSTree, 'Q', Q)
  
  
  
  %% PUBLIC PROPERTIES
  properties
    
    Q (:, 1) double
    
  end
  
  
  
  %% CONCRETE GRAPHICS METHODS
  methods ( Access = protected )
    
    function setupImpl(obj)
      %% SETUPIMPL
      
      
      
      ax = getAxes(obj);
      
      if ~isempty(obj.Q)
        obj.Children = draw(obj.SMMSTree, ax, obj.Q);
      end
      
    end
    
    
    function updateImpl(obj)
      %% DRAWIMPL
      
      
      
      redraw(obj.SMMSTree, obj.Children, obj.Q);
      
    end
    
  end
  
end
