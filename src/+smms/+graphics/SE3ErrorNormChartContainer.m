classdef SE3ErrorNormChartContainer < smms.graphics.TwistNormChartContainer
  %% SE3ERRORNORMCHARTCONTAINER
  %
  % SE3ERRORNORMCHARTCONTAINER('Time', T, 'Error', E)
  
  
  
  %% PUBLIC DEPENDENT PROPERTIES
  properties ( Dependent )
    
    % Error in SE(3)
    Error (:, 6, :)
    
  end
  
  
  
  %% SETTERS
  methods
    
    function set.Error(obj, v)
      %% SET.ERROR
      
      
      
      obj.WU = v;
      
    end
    
  end
  
  
  
  %% GETTERS
  methods
    
    function v = get.Error(obj)
      %% GET.ERROR
      
      
      
      v = obj.WU;
      
    end
    
  end
  
  
  
  %% CONCRETE GRAPHICS METHODS
  methods ( Access = protected )
    
    function setupImpl(obj)
      %% SETUPIMPL
      
      
      
      % First, call parent's setup method
      setupImpl@smms.graphics.mixin.TimeseriesChartContainer(obj);
      
      % Get layout
      hl = getLayout(obj);
      hl.GridSize = [ 2 , 1 ];
      
      % Create children inside the given layout
      obj.Children = [ ...
          smms.graphics.SO3ErrorNormChart(hl) ...
        , smms.graphics.R3ErrorNormChart(hl) ...
      ];
      
    end
    
    
    function decorateImpl(obj)
      %% DECORATEIMPL
      
      
      
      % Let base class decorate first
      decorateImpl@smms.graphics.TwistNormChartContainer(obj);
      
      % Layout object for further manipulation
      hl = getLayout(obj);
      
      % Title
      hl.Title.String = '$ \mathrm{ SE }(3) $ Error Norm';
      
      % Tagging
      hl.Tag = 'SE3ErrorNormChartContainer';
      
    end
    
  end
  
end
