classdef JointPositionChart < smms.graphics.mixin.TimeseriesChart
  %% JOINTPOSITIONCHART
  %
  % JOINTPOSITIONCHART('Time', T, 'Q', Q)
  %
  % Inputs:
  %
  %   T                       Nx1 array of time values.
  %
  %   Q                       NxV array of joint position values over time.
  
  
  
  %% PUBLIC PROPERTIES
  properties
    
    Q (:, :) double
    
  end
  
  
  
  %% DEPENDENT READ-ONLY PROPERTIES
  properties ( Dependent , SetAccess = protected )
    
    % Number of generalized coordinates
    NumQ (1, 1) double
    
  end
  
  
  
  %% GETTERS
  methods
    
    function v = get.NumQ(obj)
      %% GET.NUMQ
      
      
      
      v = size(obj.Q, 2);
      
    end
    
  end
  
  
  
  %% CONCRETE GRAPHICS METHODS
  methods ( Access = protected )
    
    function setupImpl(obj)
      %% SETUPIMPL
      
      
      
      setupImpl@smms.graphics.mixin.TimeseriesChart(obj);
      
      % Need to know how much Q to plot
      nq = obj.NumQ;
      
      % Initialize children
      obj.Children = gobjects(nq, 1);
      
      % Get axes object
      ax = getAxes(obj);
      hold(ax, 'on');
      
      % Loop over all coordinates to plot
      for iq = 1:nq
        obj.Children(iq) = plot( ...
            ax ...
          , NaN, NaN ...
          , 'LineStyle'   , '-' ...
          , 'DisplayName' , sprintf('$ { q }_{ %.0f } $', iq) ...
          , 'Tag'         , [ 'q' , num2str(iq) ] ...
        );
        
      end
      
    end
    
    
    function updateImpl(obj)
      %% UPDATEIMPL
      
      
      
      % Call parent's update method
      updateImpl@smms.graphics.mixin.TimeseriesChart(obj);
      
      nq = obj.NumQ;
      for iq = 1:nq
        set( ...
            obj.Children(iq) ...
          , 'XData' , obj.Time ...
          , 'YData' , obj.Q(:,iq) ...
        );
        
      end
      
    end
    
    
    function decorateImpl(obj)
      %% DECORATEIMPL
      
      
      
      % Parent's decoration
      decorateImpl@smms.graphics.mixin.TimeseriesChart(obj);
      
      % Our decoration
      ax = getAxes(obj);
      
      % Title
      ax.Title.String = 'Joint positions';
      
      % X-Axis
      
      % Y-Axis
      ax.YLabel.String = '$ { q }_{ i } / \cdot $';
      
      % Tagging
      ax.Tag = 'JointPositionChart';
      
    end
    
  end
  
end
