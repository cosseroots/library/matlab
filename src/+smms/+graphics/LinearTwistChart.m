classdef LinearTwistChart < smms.graphics.mixin.TimeseriesChart ...
    & smms.graphics.mixin.TwistChart
  %% LINEARTWISTCHART
  %
  % LINEARTWISTCHART('Time', T, 'U', U)
  
  
  
  %% PUBLIC DEPENDENT PROPERTIES
  properties ( Dependent )
    
    % Linear twists [ T , U , B ]
    U (:, 3, :)
    
  end
  
  
  
  %% GETTERS
  methods
    
    function v = get.U(obj)
      %% GET.U
      
      
      
      v = obj.Twist;
      
    end
    
  end
  
  
  
  %% SETTERS
  methods
    
    function set.U(obj, v)
      %% SET.U
      
      
      
      obj.Twist = v;
      
    end
    
  end
  
  
  
  %% CONCRETE CHART METHODS
  methods ( Access = protected )
    
    function setupImpl(obj)
      %% SETUPIMPL
      
      
      
      % Call parent's setup method
      setupImpl@smms.graphics.mixin.TimeseriesChart(obj);
      
      % Prepare place holder for graphics objects
      nb = obj.NumBody;
      obj.Children = gobjects(nb, 1);
      
      % Prepare axes object
      ax = getAxes(obj);
      hold(ax, 'on');
      
      % Set color order on axes as [ R ; G ; B ]
      colororder(ax, eye(3, 3));
      
      % Loop over each body
      for ib = 1:nb
        % Create a group to hold the angular and linear groups
        hg = hggroup( ...
            ax ...
          , 'Tag'         , sprintf('B%.0f', ib) ...
          , 'DisplayName' , sprintf('$ \\mathcal{ B }_{ %.0f } $', ib) ...
        );
        
        % Plot components
        for ic = 1:obj.NumTwist
          % Plot it
          plot( ...
              ax ...
            , NaN, NaN ...
            , 'Parent'      , hg ...
            , 'DisplayName' , sprintf('$ %s_{ %0.f, %.0f } $', obj.LinearName, ib, ic) ...
            , 'Tag'         , sprintf('U%.0f', ic) ...
          );
          
        end
        
        if ~isempty(hg.Children)
          obj.Children(ib) = hg;
        end
        
      end
      
    end
    
    
    function updateImpl(obj)
      %% UPDATEIMPL
      
      
      
      % Call parent's update method
      updateImpl@smms.graphics.mixin.TimeseriesChart(obj);
      
      % Loop over each body
      nb = obj.NumBody;
      for ib = 1:nb
        % Get group for body
        hc = findobj(obj.Children, 'Tag', sprintf('B%.0f', ib));
        
        % Update components
        for ic = 1:obj.NumTwist
          set( ...
              findobj( ...
                  hc ...
                , 'Tag', sprintf('U%.0f', ic) ...
              ) ...
            , 'XData', obj.Time ...
            , 'YData', obj.U(:,ic,ib) ...
          );
          
        end
        
      end
      
    end
    
    
    function decorateImpl(obj)
      %% DECORATEIMPL
      
      
      
      % First, let parent decorate the basics
      decorateImpl@smms.graphics.mixin.TimeseriesChart(obj);
      
      % Get axes object
      ax = getAxes(obj);
      
      % Title
      ax.Title.Interpreter = 'latex';
      ax.Title.String      = 'Linear Twists';
      
      % X-Axis
      
      % Y-Axis
      ax.YLabel.Interpreter = 'latex';
      ax.YLabel.String      = sprintf('$ %s / \\mathrm{ m } \\, \\mathrm{ s }^{ -1 } $', obj.LinearName);
      
      % Tagging
      ax.Tag = 'LinearTwistChart';
      
    end
    
  end
  
end
