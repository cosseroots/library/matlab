classdef JointStateChartContainer < smms.graphics.mixin.TimeseriesChartContainer
  %% JOINTSTATECHARTCONTAINER
  %
  % JOINTACTUATIONCHARTCONTAINER('Time', T, 'Q', Q, 'Qdot', QDOT)
  %
  % Inputs:
  %
  %   T                       Nx1 array of time values.
  %
  %   Q                       NxV array of joint position values over time.
  %
  %   QDOT                    NxV array of joint velocity values over time.
  
  
  
  %% PUBLIC PROPERTIES
  properties
    
    Q (:, :) double
    
    Qdot (:, :) double
    
  end
  
  
  
  %% DEPENDENT READ-ONLY PROPERTIES
  properties ( Dependent , SetAccess = protected )
    
    % Number of generalized coordinates
    NumQ (1, 1) double
    
  end
  
  
  
  %% GETTERS
  methods
    
    function v = get.NumQ(obj)
      %% GET.NUMQ
      
      
      
      v = min(size(obj.Q, 2), size(obj.Qdot, 2));
      
    end
    
  end
  
  
  
  %% CONCRETE GRAPHICS METHODS
  methods ( Access = protected )
    
    function setupImpl(obj)
      %% SETUPIMPL
      
      
      
      % First, call parent's setup method
      setupImpl@smms.graphics.mixin.TimeseriesChartContainer(obj);
      
      % Get layout
      hl = getLayout(obj);
      
      % Create children inside the given layout
      obj.Children = [ ...
          smms.graphics.JointPositionChart(hl) ...
        , smms.graphics.JointVelocityChart(hl) ...
      ];
      
    end
    
    
    function updateImpl(obj)
      %% UPDATEIMPL
      
      
      
      % Call parent's update implementation
      updateImpl@smms.graphics.mixin.TimeseriesChartContainer(obj);
      
      % Update children
      draw( ...
          obj.Children(1) ...
        , 'Time', obj.Time ...
        , 'Q'   , obj.Q ...
      );
      draw( ...
          obj.Children(2) ...
        , 'Time', obj.Time ...
        , 'Qdot', obj.Qdot ...
      );
      
    end
    
    
    function decorateImpl(obj)
      %% DECORATEIMPL
      
      
      
      % Decorate children
      decorate(obj.Children(1));
      decorate(obj.Children(2));
      
      % Call parent's decorator
      decorateImpl@smms.graphics.mixin.TimeseriesChartContainer(obj);
      
      % Decorate layout
      hl = getLayout(obj);
      
      % Title
      hl.Title.String = 'Joint state';
      
      % Tagging
      hl.Tag = 'JointStateChartContainer';
      
    end
    
  end
  
end
