classdef SO3ErrorChart < smms.graphics.AngularTwistChart
  %% SO3ERRORCHART
  %
  % SO3ERRORCHART('Time', T, 'Error', E)
  
  
  
  %% PUBLIC DEPENDENT PROPERTIES
  properties ( Dependent )
    
    % Error in SO(3)
    Error (:, 3, :)
    
  end
  
  
  
  %% SETTERS
  methods
    
    function set.Error(obj, v)
      %% SET.ERROR
      
      
      
      obj.Twist = v;
      
    end
    
  end
  
  
  
  %% GETTERS
  methods
    
    function v = get.Error(obj)
      %% GET.ERROR
      
      
      
      v = obj.Twist;
      
    end
    
  end
  
  
  
  %% CONCRETE GRAPHICS METHODS
  methods ( Access = protected )
    
    function decorateImpl(obj)
      %% DECORATEIMPL
      
      
      
      % Let base class decorate first
      decorateImpl@smms.graphics.AngularTwistChart(obj);
      
      % Axes objec to further manipulate it
      ax = getAxes(obj);
      
      % Title
      ax.Title.String = '$ \mathrm{ SO }(3) $ Error';
      
      % Y-Axis
      ax.YAxis.Label.String = sprintf('$ %s / \\mathrm{ rad } $', obj.AngularName);
      
      % Tagging
      ax.Tag = 'SO3ErrorChart';
      
    end
    
  end
  
end
