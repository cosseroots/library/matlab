classdef JointActuationChart < smms.graphics.mixin.TimeseriesChart
  %% JOINTACTUATIONCHART
  %
  % JOINTACTUATIONCHART('Time', T, 'Tau', TAU)
  %
  % Inputs:
  %
  %   T                       Nx1 array of time values.
  %
  %   TAU                     NxV array of joint actuation values over time.
  
  
  
  %% PUBLIC PROPERTIES
  properties
    
    Tau (:, :) double
    
  end
  
  
  
  %% DEPENDENT READ-ONLY PROPERTIES
  properties ( Dependent , SetAccess = protected )
    
    % Number of generalized coordinates
    NumTau (1, 1) double
    
  end
  
  
  
  %% GETTERS
  methods
    
    function v = get.NumTau(obj)
      %% GET.NUMQ
      
      
      
      v = size(obj.Tau, 2);
      
    end
    
  end
  
  
  
  %% CONCRETE GRAPHICS METHODS
  methods ( Access = protected )
    
    function setupImpl(obj)
      %% SETUPIMPL
      
      
      
      setupImpl@smms.graphics.mixin.TimeseriesChart(obj);
      
      % Need to know how much Q to plot
      nq = obj.NumTau;
      
      % Initialize children
      obj.Children = gobjects(nq, 1);
      
      % Get axes object
      ax = getAxes(obj);
      hold(ax, 'on');
      
      % Loop over all coordinates to plot
      for iq = 1:nq
        obj.Children(iq) = plot( ...
            ax ...
          , NaN, NaN ...
          , 'LineStyle'   , '-' ...
          , 'DisplayName' , sprintf('$ { \\tau } _{ %.0f } $', iq) ...
          , 'Tag'         , [ 'tau' , num2str(iq) ] ...
        );
        
      end
      
    end
    
    
    function updateImpl(obj)
      %% UPDATEIMPL
      
      
      
      % Call parent's update method
      updateImpl@smms.graphics.mixin.TimeseriesChart(obj);
      
      nq = obj.NumTau;
      for iq = 1:nq
        set( ...
            obj.Children(iq) ...
          , 'XData' , obj.Time ...
          , 'YData' , obj.Tau(:,iq) ...
        );
        
      end
      
    end
    
    
    function decorateImpl(obj)
      %% DECORATEIMPL
      
      
      
      % Parent's decoration
      decorateImpl@smms.graphics.mixin.TimeseriesChart(obj);
      
      % Our decoration
      ax = getAxes(obj);
      
      % Title
      ax.Title.String = 'Joint actuations';
      
      % X-Axis
      
      % Y-Axis
      ax.YLabel.String = '$ \tau_{ i } / \cdot $';
      
      % Tagging
      ax.Tag = 'JointActuationChart';
      
    end
    
  end
  
end
