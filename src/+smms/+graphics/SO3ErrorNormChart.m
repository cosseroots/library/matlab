classdef SO3ErrorNormChart < smms.graphics.AngularTwistNormChart
  %% SO3ERRORNORMCHART
  %
  % SO3ERRORNORMCHART('Time', T, 'Error', E)
  
  
  
  %% PUBLIC DEPENDENT PROPERTIES
  properties ( Dependent )
    
    % Error in SO(3)
    Error (:, 3, :)
    
  end
  
  
  
  %% SETTERS
  methods
    
    function set.Error(obj, v)
      %% SET.ERROR
      
      
      
      obj.Twist = v;
      
    end
    
  end
  
  
  
  %% GETTERS
  methods
    
    function v = get.Error(obj)
      %% GET.ERROR
      
      
      
      v = obj.Twist;
      
    end
    
  end
  
  
  
  %% CONCRETE GRAPHICS METHODS
  methods ( Access = protected )
    
    function decorateImpl(obj)
      %% DECORATEIMPL
      
      
      
      % Let base class decorate first
      decorateImpl@smms.graphics.AngularTwistNormChart(obj);
      
      % Axes objec to further manipulate it
      ax = getAxes(obj);
      
      % Title
      ax.Title.String = '$ \mathrm{ SO }(3) $ Error Norm';
      
      % Y-Axis
      ax.YAxis.Label.String = sprintf('$ \\Vert %s \\Vert / \\mathrm{ rad } $', obj.AngularName);
      
      % Tagging
      ax.Tag = 'SO3ErrorNormChart';
      
    end
    
  end
  
end
