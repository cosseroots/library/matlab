classdef Chart < handle ...
    & matlab.mixin.Heterogeneous ...
    & matlab.mixin.SetGet ...
    & dynamicprops
  %% CHART
  
  
  
  %% PUBLIC TRANSIENT PROPERTIES
  properties ( Transient , NonCopyable )
    
    % Child objects as drawn by the chart, can be a PLOT, a QUIVER3, SCATTER, or
    % any other mid-level graphics object
    Children (:, 1) = gobjects(0, 1)
    
    % User data field
    UserData = []
    
  end
  
  
  
  %% INTERNAL PROPERTIES
  properties ( Hidden, SetAccess = { ?smms.graphics.mixin.Chart } )
    
    % Parent graphics object of this chart
    Parent
    
    % Current axes to plot into. Needed so that subclasses can call parent's
    % method and won't override the axes
    Axes
    
    % Handle to legend object, if defined
    LegendHandle
    
  end
  
  
  
  %% INTERNAL PROPERTIES
  properties ( Access = private , Constant )
    
    ExposedProperties = { ...
        'AmbientLightColor' ...
      ... % Box
      , 'Box' ...
      , 'BoxStyle' ...
      ... % Camera
      , 'CameraPosition' ...
      , 'CameraPositionMode' ...
      , 'CameraTarget' ...
      , 'CameraTargetMode' ...
      , 'CameraUpVector' ...
      , 'CameraUpVectorMode' ...
      , 'CameraViewAngle' ...
      , 'CameraViewAngleMode' ...
      ... % Clipping
      , 'Clipping' ...
      , 'ClippingStyle' ...
      ... % Color
      , 'Color' ...
      , 'ColorOrder' ...
      , 'ColorOrderIndex' ...
      , 'ColorScale' ...
      ... % Data aspect ratio
      , 'DataAspectRatio' ...
      , 'DataAspectRatioMode' ...
      ... % Font
      , 'FontAngle' ...
      , 'FontName' ...
      , 'FontSize' ...
      , 'FontSizeMode' ...
      , 'FontSmoothing' ...
      , 'FontUnits' ...
      , 'FontWeight' ...
      ... % Grid
      , 'GridAlpha' ...
      , 'GridAlphaMode' ...
      , 'GridColor' ... ...
      , 'GridColorMode' ... ...
      , 'GridLineStyle' ...
      ... % Layout options ...
      , 'Layout' ...
      ... % Label
      , 'LabelFontSizeMultiplier' ...
      ... % Line
      , 'LineStyleOrder' ...
      , 'LineStyleOrderIndex' ...
      , 'LineWidth' ...
      ... % Minor grid ...
      , 'MinorGridAlpha' ...
      , 'MinorGridAlphaMode' ...
      , 'MinorGridColor' ...
      , 'MinorGridColorMode' ...
      , 'MinorGridLineStyle' ...
      ... % Next
      , 'NextPlot' ...
      , 'NextSeriesIndex' ...
      ... % Plot box aspect ratio
      , 'PlotBoxAspectRatio' ...
      , 'PlotBoxAspectRatioMode' ...
      ... % Positions and units
      , 'Position' ...
      , 'PositionConstraint' ...
      , 'InnerPosition' ...
      , 'OuterPosition' ...
      , 'TightInset' ...
      , 'Units' ...
      ... % Projection
      , 'Projection' ...
      ... % Subtitle
      , 'Subtitle' ...
      , 'SubtitleFontWeight' ...
      ... % Title
      , 'Title' ...
      , 'TitleFontSizeMultiplier' ...
      , 'TitleFontWeight' ...
      , 'TitleHorizontalAlignment' ...
      ... % Tag
      , 'Tag' ...
      ... % Ticks
      , 'TickDir' ...
      , 'TickDirMode' ...
      , 'TickLabelInterpreter' ...
      , 'TickLength' ...
      ... % View
      , 'View' ...
      , 'Visible' ...
      ... % X-Axis
      , 'XAxis' ...
      , 'XAxisLocation' ...
      , 'XColor' ...
      , 'XColorMode' ...
      , 'XDir' ...
      , 'XGrid' ...
      , 'XLabel' ...
      , 'XLim' ...
      , 'XLimMode' ...
      , 'XLimitMethod' ...
      , 'XMinorGrid' ...
      , 'XMinorTick' ...
      , 'XScale' ...
      , 'XTick' ...
      , 'XTickLabel' ...
      , 'XTickLabelMode' ...
      , 'XTickLabelRotation' ...
      , 'XTickLabelRotationMode' ...
      , 'XTickMode' ...
      ... % Y-Axis
      , 'YAxis' ...
      , 'YAxisLocation' ...
      , 'YColor' ...
      , 'YColorMode' ...
      , 'YDir' ...
      , 'YGrid' ...
      , 'YLabel' ...
      , 'YLim' ...
      , 'YLimMode' ...
      , 'YLimitMethod' ...
      , 'YMinorGrid' ...
      , 'YMinorTick' ...
      , 'YScale' ...
      , 'YTick' ...
      , 'YTickLabel' ...
      , 'YTickLabelMode' ...
      , 'YTickLabelRotation' ...
      , 'YTickLabelRotationMode' ...
      , 'YTickMode' ...
      ... % Z-Axis
      , 'ZAxis' ...
      , 'ZAxisLocation' ...
      , 'ZColor' ...
      , 'ZColorMode' ...
      , 'ZDir' ...
      , 'ZGrid' ...
      , 'ZLabel' ...
      , 'ZLim' ...
      , 'ZLimMode' ...
      , 'ZLimitMethod' ...
      , 'ZMinorGrid' ...
      , 'ZMinorTick' ...
      , 'ZScale' ...
      , 'ZTick' ...
      , 'ZTickLabel' ...
      , 'ZTickLabelMode' ...
      , 'ZTickLabelRotation' ...
      , 'ZTickLabelRotationMode' ...
      , 'ZTickMode' ...
    };
    
  end
  
  
  
  %% PRIVATE PROPERTIES
  properties ( Access = private )
    
    % Flag wheter the chart object is set up or not
    ChartIsSetup (1, 1) logical = false
    ChartIsDecorated (1, 1) logical = false
    
    DynamicProperties (:, 1) = meta.DynamicProperty.empty(0, 1);
    
  end
  
  
  
  %% Events
  events
    
    PreDraw
    PostDraw
    PreDecorate
    PostDecorate
    PreSetup
    PostSetup
    
  end
  
  
  
  %% CONSTRUCTOR METHODS
  methods
    
    function obj = Chart(varargin)
      %% CHART
      
      
      
      % Parents' constructors
      obj@handle();
      obj@matlab.mixin.Heterogeneous();
      obj@matlab.mixin.SetGet();
      
      % Infinitely many arguments
      narginchk(0, Inf);
      
      % Import some of MATLAB's internal functions
      import matlab.graphics.chart.internal.inputparsingutils.peelFirstArgParent
      import matlab.graphics.chart.internal.inputparsingutils.getParent
      
      % Check for first argument parent.
      throwForInvalid = true;
      supportDoubleAxesHandle = true;
      [parent, args] = peelFirstArgParent(varargin, throwForInvalid, supportDoubleAxesHandle);
      nargs = numel(args);
      % Check for any parent given the currently processed input data
      [parent, hasParent] = getParent(parent, args, 2);
      % And find a parent object
      [parent, currentAxes, nextplot] = prepareParent(parent, hasParent, true);
      
      % Associated current axes and its parent
      obj.Axes   = currentAxes;
      obj.Parent = parent;
      
      % Setup exposed properties
      exposeProperties(obj);
      
      % Setup chart, then draw to it
      if nargs > 0
        set(obj, args{:});
      end
      setup(obj);
      draw(obj, args{:});
      
    end
    
  end
  
  
  
  %% PUBLIC METHODS
  methods
    
    function flag = issetup(obj)
      %% ISSETUP
      
      
      
      flag = obj.ChartIsSetup;
      
    end
    
    
    function flag = isdecorated(obj)
      %% ISDECORATED
      
      
      
      flag = obj.ChartIsDecorated;
      
    end
    
    
    function flag = ishghandle(obj)
      %% ISHGHANDLE
      
      
      
      flag = isa(obj, 'smms.graphics.mixin.Chart');
      
    end
    
    
    function flag = isgraphics(obj)
      %% ISGRAPHICS
      
      
      
      flag = isa(obj, 'smms.graphics.mixin.Chart');
      
    end
    
  end
  
  
  
  %% CONVENIENCE METHODS
  methods
    
    function varargout = title(obj, varargin)
      %% TITLE
      
      
      
      [varargout{1:nargout}] = title(obj.Axes, varargin{:});
      
    end
    
    
    function varargout = subtitle(obj, varargin)
      %% SUBTITLE
      
      
      
      [varargout{1:nargout}] = subtitle(obj.Axes, varargin{:});
      
    end
    
    
    function varargout = xlabel(obj, varargin)
      %% XLABEL
      
      
      
      [varargout{1:nargout}] = xlabel(obj.Axes, varargin{:});
      
    end
    
    
    function varargout = ylabel(obj, varargin)
      %% YLABEL
      
      
      
      [varargout{1:nargout}] = ylabel(obj.Axes, varargin{:});
      
    end
    
    
    function varargout = zlabel(obj, varargin)
      %% ZLABEL
      
      
      
      [varargout{1:nargout}] = zlabel(obj.Axes, varargin{:});
      
    end
    
    
    function varargout = xlim(obj, varargin)
      %% XLIM
      
      
      
      [varargout{1:nargout}] = xlim(obj.Axes, varargin{:});
      
    end
    
    
    function varargout = ylim(obj, varargin)
      %% YLIM
      
      
      
      [varargout{1:nargout}] = ylim(obj.Axes, varargin{:});
      
    end
    
    
    function varargout = zlim(obj, varargin)
      %% ZLIM
      
      
      
      [varargout{1:nargout}] = zlim(obj.Axes, varargin{:});
      
    end
    
    
    function varargout = xticks(obj, varargin)
      %% XTICKS
      
      
      
      [varargout{1:nargout}] = xticks(obj.Axes, varargin{:});
      
    end
    
    
    function varargout = yticks(obj, varargin)
      %% YTICKS
      
      
      
      [varargout{1:nargout}] = yticks(obj.Axes, varargin{:});
      
    end
    
    
    function varargout = zticks(obj, varargin)
      %% ZTICKS
      
      
      
      [varargout{1:nargout}] = zticks(obj.Axes, varargin{:});
      
    end
    
    
    function varargout = xticklabels(obj, varargin)
      %% XTICKLABELS
      
      
      
      [varargout{1:nargout}] = xticklabels(obj.Axes, varargin{:});
      
    end
    
    
    function varargout = yticklabels(obj, varargin)
      %% YTICKLABELS
      
      
      
      [varargout{1:nargout}] = yticklabels(obj.Axes, varargin{:});
      
    end
    
    
    function varargout = zticklabels(obj, varargin)
      %% ZTICKLABELS
      
      
      
      [varargout{1:nargout}] = zticklabels(obj.Axes, varargin{:});
      
    end
    
    
    function varargout = view(obj, varargin)
      %% VIEW
      
      
      
      [varargout{1:nargout}] = view(obj.Axes, varargin{:});
      
    end
    
  end
  
  
  
  %% FINAL PUBLIC METHODS
  methods ( Sealed )
    
    function setup(obj)
      %% SETUP
      
      
      
      % Ensure object is set up
      if ~obj.ChartIsSetup
        % Trigger pre event
        notify(obj, 'PreSetup');
        
        % Call actual setup implementation
        setupImpl(obj);
        
        % Trigger post event
        notify(obj, 'PostSetup');
        
        % Mark chart as setup
        obj.ChartIsSetup = numel(obj.Children) > 0;
        
      end
      
      % Finally, decorate chart
      decorate(obj);
      
    end
    
    
    function draw(obj, varargin)
      %% DRAW
      
      
      
      % DRAW(OBJ, Name, Value, ...)
      if nargin > 2
        set(obj, varargin{:});
      end
      
      % Ensure chart is set up
      setup(obj);
      
      % If chart is setup, we can draw onto it
      if obj.ChartIsSetup
        % Trigger pre-event
        notify(obj, 'PreDraw');
        
        % Call user-defined update method so chart is populated with the correct
        % data
        updateImpl(obj);
        
        % Trigger post-event
        notify(obj, 'PostDraw');
        
      end
      
    end
    
    
    function decorate(obj)
      %% DECORATE
      
      
      
      % If chart has not yet been decorated, then decorate it
      if ~obj.ChartIsDecorated
        % Trigger pre event
        notify(obj, 'PreDecorate');
        
        % Call actual decorate implementation
        decorateImpl(obj);
        
        % Trigger post event
        notify(obj, 'PostDecorate');
        
        % And set flag that chart is decorated to true
        obj.ChartIsDecorated = true;
        
      end
      
    end
    
    
    function lgd = legend(obj, varargin)
      %% LEGEND
      
      
      
      % LEGEND(OBJ)
      % LEGEND(OBJ, ___)
      narginchk(1, Inf);
      
      % LEGEND(___)
      % LGD = LEGEND(___)
      nargoutchk(0, 1);
      
      % Ensure chart exists
      setup(obj);
      
      % Then create legend
      obj.LegendHandle = legendImpl(obj, varargin{:});
      
      % LGD = LEGEND(___)
      if nargout > 0
        lgd = obj.LegendHandle;
      end
      
    end
    
  end
  
  
  
  %% INTERNAL ABSTRACT CHART METHODS
  methods ( Access = protected )
    
    function setupImpl(obj)
      %% SETUPIMPL
      
      
      
    end
    
    
    function updateImpl(obj)
      %% UPDATEIMPL
      
      
      
    end
    
    
    function decorateImpl(obj)
      %% DECORATEIMPL
      
      
      
    end
    
    
    function lgd = legendImpl(obj, varargin)
      %% LEGENDIMPL
      
      
      
      % By default, just add a legend based on the last axes' childrens
      lgd = legend( ...
          obj.Children ...
        , 'Interpreter' , 'latex' ...
        , 'Location'    , 'bestoutside' ...
        , varargin{:} ...
      );
      
      % Adjust position and layout of legend
      if numel(lgd.String) > 4
        lgd.NumColumns = ceil( numel(lgd.String) / 6 );
        
      end
      
    end
    
    
    function h = getAxes(obj)
      %% GETAXES
      
      
      
      % The `Axes` property is an axes object
      h = obj.Axes;
      
    end
    
    
    function h = getLayout(obj)
      %% GETLAYOUT
      
      
      
      h = findobj(obj.Parent, 'Type', 'TiledLayout');
      
      if isempty(h)
        h = tiledlayout(obj.Parent, 'flow');
        
      end
      
    end
    
  end
  
  
  
  %% INTERNAL FINAL METHODS
  methods ( Sealed , Access = private )
    
    function exposeProperties(obj)
      %% EXPOSEPROPERTIES
      
      
      
      % Get list of properties we expose
      props = obj.ExposedProperties;
      nprops = numel(props);
      
      % Loop over all properties
      for iprop = 1:nprops
        
        % Property name
        prop = props{iprop};
        
        % Dynamic property object
        p = addprop(obj, prop);
        
        % Configure dynamic property
        p.AbortSet    = true;
        p.Dependent   = true;
        p.GetMethod   = @(o) getDynamicProperty(o, prop);
        p.NonCopyable = true;
        p.SetMethod   = @(o, v) setDynamicProperty(o, prop, v);
        p.Transient   = true;
        
        % And store dynamic property locally
        obj.DynamicProperties(iprop) = p;
        
      end
      
    end
    
    
    function varargout = getDynamicProperty(obj, v)
      %% GETDYNAMICPROPERTY
      
      
      
      [varargout{1:nargout}] = get(obj.Axes, v);
      
    end
    
    
    function setDynamicProperty(obj, p, v)
      %% SETDYNAMICPROPERTY
      
      
      
      set(obj.Axes, p, v);
      
    end
    
  end
  
end


function [parent, currentAxes, nextplot] = prepareParent(parent, hasParent, showInteractionInfoPanel)
%% PREPAREPARENT



% Some parent object was provided...
if hasParent
  % Handle scalar cases only
  if isscalar(parent)
    % If it is a figure object, create a new axes
    if isgraphics(parent, 'Figure')
      currentAxes = newplot();
      nextplot = currentAxes.NextPlot;
      
    % If it is a TiledLayout object, get a new axes inside this one
    elseif isgraphics(parent, 'TiledLayout')
      currentAxes = nexttile(parent);
      nextplot = currentAxes.NextPlot;
      
    end
  
  % Case of non-scalar parent objects
  else
    throwAsCaller(MException(message('MATLAB:graphics:axescheck:NonScalarHandle')));
    
  end

% No parent object was provided at all
else
  showInteractionInfoPanel = showInteractionInfoPanel && isempty(get(groot(), 'CurrentFigure'));
  currentAxes = newplot();
  parent      = currentAxes.Parent;
  nextplot    = currentAxes.NextPlot;
    
  if showInteractionInfoPanel
    matlab.graphics.internal.InteractionInfoPanel.maybeShow(parent);
  end
  
end


end
