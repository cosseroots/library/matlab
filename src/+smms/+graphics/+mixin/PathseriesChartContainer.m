classdef ( Abstract ) PathseriesChartContainer < smms.graphics.mixin.ChartContainer
  %% PATHSERIESCHARTCONTAINER
  %
  % PATHSERIESCHARTCONTAINER('Path', X)
  %
  % Inputs:
  % 
  %   X                       Nx1 array of path coordinates.
  
  
  
  %% PUBLIC PROPERTIES
  properties
    
    Path (:, 1) double
    
  end
  
  
  
  %% CONCRETE GRAPHICS METHODS
  methods ( Access = protected )
    
    function decorateImpl(obj)
      %% DECORATEIMPL
      
      
      
%       ax = getAxes(obj);
%       
%       viz.style.pathseries(ax);
%       
%       if ~isempty(obj.Path)
%         ax.XAxis.Limits = obj.Path([ 1 , end ]);
%       end
      
    end
    
  end
  
end
