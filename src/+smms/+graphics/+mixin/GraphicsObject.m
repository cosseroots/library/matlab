classdef GraphicsObject < handle
  %% GRAPHICSOBJECT
  
  
  
  %% CONCRETE GRAPHICS METHODS
  methods ( Access = protected )
    
    function setupImpl(obj)
      %% SETUPIMPL
      
      
      
    end
    
    
    function drawImpl(obj)
      %% DRAWIMPL
      
      
      
    end
    
    
    function decorateImpl(obj)
      %% DECORATEIMPL
      
      
      
    end
    
  end
  
end
