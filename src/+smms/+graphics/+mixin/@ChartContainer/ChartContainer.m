classdef ChartContainer < handle ...
    & matlab.mixin.Heterogeneous ...
    & matlab.mixin.SetGet
  %% CHART
  
  
  
  %% PUBLIC TRANSIENT PROPERTIES
  properties ( Transient , NonCopyable )
    
    % Child objects as drawn by the chart container, can be any
    % SMMS.GRAPHICS.*CHART object
    Children (:, 1) = gobjects(0, 1)
    
    % User data field
    UserData = []
    
  end
  
  
  
  %% PUBLIC DEPENDENT PROPERTIES
  properties ( Dependent )
    
    % Tiled chart layout options array
    Layout
    
  end
  
  
  
  %% INTERNAL PROPERTIES
  properties ( Hidden, SetAccess = { ?smms.graphics.mixin.Chart } )
    
    % Parent graphics object of this chart
    Parent
    
    % Tiled layout this container creates
    ContainerLayout
    
    % Handle to legend object, if defined
    LegendHandle
    
  end
  
  
  
  %% PRIVATE PROPERTIES
  properties ( Access = private )
    
    % Flag wheter the chart object is set up or not
    ContainerIsSetup     (1, 1) logical = false
    ContainerIsDecorated (1, 1) logical = false
    
  end
  
  
  
  %% Events
  events
    
    PreDraw
    PostDraw
    PreDecorate
    PostDecorate
    PreSetup
    PostSetup
    
  end
  
  
  
  %% CONSTRUCTOR METHODS
  methods
    
    function obj = ChartContainer(varargin)
      %% CHARTCONTAINER
      
      
      
      % Parents' constructors
      obj@handle();
      obj@matlab.mixin.Heterogeneous();
      obj@matlab.mixin.SetGet();
      
      % Import some of MATLAB's internal functions
      import matlab.graphics.chart.internal.inputparsingutils.peelFirstArgParent
      import matlab.graphics.chart.internal.inputparsingutils.getParent
      
      % Check for first argument parent.
      throwForInvalid = true;
      supportDoubleAxesHandle = true;
      [parent, args] = peelFirstArgParent(varargin, throwForInvalid, supportDoubleAxesHandle);
      nargs = numel(args);
      % Check for any parent given the currently processed input data
      [parent, hasParent] = getParent(parent, args, 2);
      % And find a parent object
      [parent, currentAxes, nextplot] = prepareParent(parent, hasParent, true);
      
      % Set parent and the container layout
      obj.Parent          = parent;
      obj.ContainerLayout = tiledlayout(obj.Parent, 'flow');
      
      % Set up chart container with data given
      if nargs > 0
        set(obj, args{:});
      end
      setup(obj);
      draw(obj, args{:});
      
    end
    
  end
  
  
  
  %% GETTERS
  methods
    
    function v = get.Layout(obj)
      %% GET.LAYOUT
      
      
      
      v = obj.ContainerLayout.Layout;
      
    end
    
  end
  
  
  
  %% SETTERS
  methods
    
    function set.Layout(obj, v)
      %% SET.LAYOUT
      
      
      
      obj.ContainerLayout.Layout = v;
      
    end
    
  end
  
  
  
  %% PUBLIC METHODS
  methods
    
    function flag = issetup(obj)
      %% ISSETUP
      
      
      
      flag = obj.ContainerIsSetup;
      
    end
    
    
    function flag = isdecorated(obj)
      %% ISDECORATED
      
      
      
      flag = obj.ContainerIsDecorated;
      
    end
    
    
    function flag = ishghandle(obj)
      %% ISHGHANDLE
      
      
      
      flag = isa(obj, 'smms.graphics.mixin.Chart');
      
    end
    
    
    function flag = isgraphics(obj)
      %% ISGRAPHICS
      
      
      
      flag = isa(obj, 'smms.graphics.mixin.Chart');
      
    end
    
  end
  
  
  
  %% FINAL PUBLIC METHODS
  methods ( Sealed )
    
    function setup(obj)
      %% SETUP
      
      
      
      % Ensure object is set up
      if ~obj.ContainerIsSetup
        % Trigger pre event
        notify(obj, 'PreSetup');
        
        % Call actual setup implementation
        setupImpl(obj);
        
        % Trigger post event
        notify(obj, 'PostSetup');
        
        % Mark chart container as setup
        obj.ContainerIsSetup = numel(obj.Children) > 0;
        
      end
      
      % Finally, decorate chart container
      decorate(obj);
      
    end
    
    
    function lgd = legend(obj, varargin)
      %% LEGEND
      
      
      
      % LEGEND(OBJ)
      % LEGEND(OBJ, ___)
      narginchk(1, Inf);
      
      % LEGEND(___)
      % LGD = LEGEND(___)
      nargoutchk(0, 1);
      
      % Ensure chart container exists
      setup(obj);
      
      % Then create legend
      obj.LegendHandle = legendImpl(obj, varargin{:});
      
      % LGD = LEGEND(___)
      if nargout > 0
        lgd = obj.LegendHandle;
      end
      
    end
    
    
    function draw(obj, varargin)
      %% DRAW
      
      
      
      % DRAW(OBJ, Name, Value, ...)
      if nargin > 2
        set(obj, varargin{:});
      end
      
      % Ensure chart container is set up, now that it also has data
      setup(obj);
      
      % If container is set up i.e., it has all its children defined, we can
      % draw onto it
      if obj.ContainerIsSetup
        % Trigger pre-event
        notify(obj, 'PreDraw');
        
        % Call user-defined update implementation in case there is more to be
        % done
        updateImpl(obj);
        
        % Trigger post-event
        notify(obj, 'PostDraw');
        
      end
      
    end
    
    
    function decorate(obj)
      %% DECORATE
      
      
      
      % If chart container has not yet been decorated, then decorate it
      if ~obj.ContainerIsDecorated
        % Trigger pre event
        notify(obj, 'PreDecorate');
        
        % Call actual decorate implementation
        decorateImpl(obj);
        
        % Trigger post event
        notify(obj, 'PostDecorate');
        
        % And set flag that chart container is decorated to true
        obj.ContainerIsDecorated = true;
        
      end
      
    end
    
  end
  
  
  
  %% INTERNAL ABSTRACT CHART CONTAINER METHODS
  methods ( Access = protected )
    
    function setupImpl(obj)
      %% SETUPIMPL
      
      
      
    end
    
    
    function updateImpl(obj)
      %% UPDATEIMPL
      
      
      
    end
    
    
    function decorateImpl(obj)
      %% DECORATEIMPL
      
      
      
    end
    
    
    function lgd = legendImpl(obj, varargin)
      %% LEGENDIMPL
      
      
      
      % By default, add a legend to the last axes, then move it to the layout
      lgd = legend(obj.Children(1), varargin{:});
      lgd.Parent = obj.ContainerLayout;
      lgd.Layout.Tile = 'north';
      
      % Adjust position and layout of legend
      lgd.Orientation = 'horizontal';
      if numel(lgd.String) > 4
        lgd.NumColumns = ceil( numel(lgd.String) / 3 );
        
      end
      
    end
    
    
    function h = getLayout(obj)
      %% GETLAYOUT
      
      
      
      h = obj.ContainerLayout;
      
    end
    
  end
  
end


function [parent, currentLayout, nextplot] = prepareParent(parent, hasParent, showInteractionInfoPanel)
%% PREPAREPARENT



nextplot = [];
% Some parent object was provided...
if hasParent
  % Handle scalar cases only
  if isscalar(parent)
    % If it is a figure object, create a new tiled layout
    if isgraphics(parent, 'Figure')
      currentLayout = tiledlayout(parent, 'flow');
      
    % If it is a TiledLayout object, get a new axes inside this one
    elseif isgraphics(parent, 'TiledLayout')
      currentLayout = parent;
      
    end
  
  % Case of non-scalar parent objects
  else
    throwAsCaller(MException(message('MATLAB:graphics:axescheck:NonScalarHandle')));
    
  end
  
% No parent object was provided at all
else
  showInteractionInfoPanel = showInteractionInfoPanel && isempty(get(groot(), 'CurrentFigure'));
  currentLayout = tiledlayout('flow');
  parent        = currentLayout.Parent;
    
  if showInteractionInfoPanel
    matlab.graphics.internal.InteractionInfoPanel.maybeShow(parent);
  end
  
end


end
