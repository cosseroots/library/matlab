classdef ( Abstract ) SMMSTreeChart < smms.graphics.mixin.Chart
  %% SMMSTREECHART
  
  
  
  %% PUBLIC PROPERTIES
  properties ( SetAccess = { ?smms.graphics.mixin.Chart } )
    
    % SMMS tree object
    SMMSTree
    
  end
  
end
