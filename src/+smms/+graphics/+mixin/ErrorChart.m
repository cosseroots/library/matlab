classdef ( Abstract ) ErrorChart < handle
  %% ERRORCHART
  
  
  
  %% PUBLIC PROPERTIES
  properties
    
    % Errors of bodies
    Error (:, 3, :) double
    
  end
  
  
  
  %% INTERNAL PROPERTIES
  properties ( Access = protected )
    
    % 
    AngularName (1,: ) char = '\Omega'
    LinearName  (1, :) char = 'U'
    
  end
  
  
  
  %% READ-ONLY DEPENDENT PROPERTIES
  properties ( Dependent , SetAccess = protected )
    
    % Number of bodies
    NumBody   (1, 1)  double
    
    % Dimension of errors
    NumError  (1, 1)  double
  
  end
  
  
  
  %% GETTERS
  methods
    
    function v = get.NumBody(obj)
      %% GET.NUMBODY
      
      
      
      v = size(obj.Error, 3);
      
    end
    
    
    function v = get.NumError(obj)
      %% GET.NUMERROR
      
      
      
      v = size(obj.Error, 2);
      
    end
    
  end
  
end
