classdef ( Abstract ) TwistChart < handle
  %% TWISTCHART
  
  
  
  %% PUBLIC PROPERTIES
  properties ( Access = protected )
    
    % Twists of bodies
    Twist (:, 3, :) double
    
  end
  
  
  
  %% INTERNAL PROPERTIES
  properties ( Access = protected )
    
    % 
    AngularName (1,: ) char = '\Omega'
    LinearName  (1, :) char = 'U'
    
  end
  
  
  
  %% READ-ONLY DEPENDENT PROPERTIES
  properties ( Dependent , SetAccess = protected )
    
    % Number of bodies
    NumBody   (1, 1)  double
    
    % Dimension of twists
    NumTwist  (1, 1)  double
  
  end
  
  
  
  %% GETTERS
  methods
    
    function v = get.NumBody(obj)
      %% GET.NUMBODY
      
      
      
      v = size(obj.Twist, 3);
      
    end
    
    
    function v = get.NumTwist(obj)
      %% GET.NUMTWIST
      
      
      
      v = size(obj.Twist, 2);
      
    end
    
  end
  
end
