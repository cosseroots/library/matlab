classdef ( Abstract ) TimeseriesChartContainer < smms.graphics.mixin.ChartContainer
  %% TIMESERIESCHARTCONTAINER
  %
  % TIMESERIESCHARTCONTAINER('Time', T)
  %
  % Inputs:
  % 
  %   T                       Nx1 array of time coordinates.
  
  
  
  %% PUBLIC PROPERTIES
  properties
    
    Time (:, 1) double
    
  end
  
  
  
  %% CONCRETE GRAPHICS METHODS
  methods ( Access = protected )
    
    function decorateImpl(obj)
      %% DECORATEIMPL
      
      
      
      hl = getLayout(obj);
      
      % Title configuration
      hl.Title.Interpreter = 'latex';
      hl.Title.FontSize    = 14;
      
      % Loop through all children to remove their X-Axes and put the on the
      % layout
      hl.XLabel.Interpreter = 'latex';
      hl.XLabel.String      = hl.Children(1).XAxis.Label.String;
      for ic = 1:numel(obj.Children)
        obj.Children(ic).Axes.XAxis.Label.String = '';
        obj.Children(ic).Axes.Title.FontSize = 0.80 * hl.Title.FontSize;
      end
      
      % Get all childrens' X-Axis
      xax = get([ obj.Children.Axes ], 'XAxis');
      xax = cat(1, xax{:});
      % Set some defaults on all X-Axis
      set( ...
          xax...
        ... % Default limits
        , 'Limits', [ 0 , 1 ] ...
        ... % Default mode for rescaling limits
        , 'LimitsMode', 'auto' ...
      );
      % Ensure X-Axis has its limits always tight
      set( ...
          [ obj.Children.Axes ] ...
        , 'XLimitMethod', 'tight' ...
      );
      
    end
    
  end
  
end
