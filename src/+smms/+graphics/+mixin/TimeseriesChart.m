classdef ( Abstract ) TimeseriesChart < smms.graphics.mixin.Chart
  %% TIMESERIESCHART
  %
  % TIMESERIESCHART('Time', T)
  %
  % Inputs:
  % 
  %   T                       Nx1 array of time coordinates.
  
  
  
  %% PUBLIC PROPERTIES
  properties
    
    Time (:, 1) double
    
  end
  
  
  
  %% CONCRETE GRAPHICS METHODS
  methods ( Access = protected )
    
    function decorateImpl(obj)
      %% DECORATEIMPL
      
      
      
      ax = getAxes(obj);
      
      viz.style.timeseries(ax);
      
      % Initial axes limits
      ax.XAxis.Limits     = [ 0 , 1];
      % Ensure that the X-axis always has auto-calculated limits
      ax.XAxis.LimitsMode = 'auto';
      % Ensure the X-axis' limits are always tight around the data i.e., no
      % margins left or right
      ax.XLimitMethod     = 'tight';
      
    end
    
  end
  
end
