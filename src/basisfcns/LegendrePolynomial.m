classdef LegendrePolynomial < PolynomialBasisFunction
  %% LEGENDREPOLYNOMIAL Legendre polynomial of degree D
  
  
  
  %% CONSTRUCTOR METHODS
  methods
    
    function obj = LegendrePolynomial(d)
      %% LEGENDREPOLYNOMIAL
      %
      % LEGENDREPOLYNOMIAL(D)
      
      
      
      narginchk(1, 1);
      nargoutchk(0, 1);
      
      obj@PolynomialBasisFunction(d);
      
    end
    
  end
  
  
  
  %% EVALUATION METHODS
  methods
    
    function v = evaluate(obj, x, ab)
      %% EVALUATE
      %
      % V = EVALUATE(OBJ, X)
      %
      % V = EVALUATE(OBJ, X, AB)
      
      
      
      % Default interval boundaries
      if nargin < 3 || isempty(ab)
        ab = [ -1 , 1 ];
        
      end
      
      d = obj.Degree;
      
      % Interval boundaries
      a = ab(1);
      b = ab(end);
      
      % Ensure X is a column vector for faster memory allocations
      x = x(:);
      nx = numel(x);
      
      % Shift given coordinates to lie in [-1, 1] interval
      x = ( 2 .* x - ( b + a ) ) ./ ( b - a );
      
      P = zeros(nx, d + 1);
      P(:,0+1) = ones(nx, 1);
      P(:,1+1) = x;
      
      % Original recursive formulation, kept for reference but updated to match
      % 1-index based arrays
      % for n = 1:d
      %   P(:,n+1 + 1) = ( ( 2 * n + 1) .* x .* P(:,n + 1) - n .* P(:,n-1 + 1) ) ./ ( n + 1 );
      % end
      
      % Loop over higher-order polynomials
      for n = 2:d
        P(:,n + 1) = ( ( 2 * n - 1 ) .* x .* P(:,n) - (n - 1) .* P(:,n - 1) ) ./ n;
      end
      
      % Select only the valid parts of the polynomial coefficients i.e., only
      % those up until and including the polynomial degree
      v = permute(P(:,1:obj.Order), [2, 1]);
      
    end
    
    
    function c = coeffs(obj)
      %% COEFFS Evaluate coefficients of polynomial in ascending power order
      %
      % C = COEFFS(OBJ)
      
      
      
      c = [ ...
        [  +1      0 ,     0 ,     0 ,      0 ,      0 ,      0 ,      0 ,       0 ,     0 ,      0 ] ./   1 ; ...
        [   0 ,   +1 ,     0 ,     0 ,      0 ,      0 ,      0 ,      0 ,       0 ,     0 ,      0 ] ./   1 ; ...
        [  -1 ,    0 ,    +3 ,     0 ,      0 ,      0 ,      0 ,      0 ,       0 ,     0 ,      0 ] ./   2 ; ...
        [   0 ,   -3 ,     0 ,    +5 ,      0 ,      0 ,      0 ,      0 ,       0 ,     0 ,      0 ] ./   2 ; ...
        [  +3 ,    0,    -30 ,     0 ,    +35 ,      0 ,      0 ,      0 ,       0 ,     0 ,      0 ] ./   8 ; ...
        [   0 ,  +15 ,     0 ,   -70 ,      0 ,    +63 ,      0 ,      0 ,       0 ,     0 ,      0 ] ./   8 ; ...
        [  -5 ,    0 ,  +105 ,     0 ,   -315 ,      0 ,   +231 ,      0 ,       0 ,     0 ,      0 ] ./  16 ; ...
        [   0 ,  -35 ,     0 ,  +315 ,      0 ,   -693 ,      0 ,   +429 ,       0 ,     0 ,      0 ] ./  16 ; ...
        [ +35 ,    0 , -1260 ,     0 ,  +6930 ,      0 , -12012 ,      0 ,   +6435 ,     0 ,      0 ] ./ 128 ; ...
        [   0 , +315 ,     0 , -4620 ,      0 , +18018 ,      0 , -25740 ,       0 , 12155 ,      0 ] ./ 128 ; ...
        [ -63 ,    0 , +3465 ,     0 , -30030 ,      0 , +90090 ,      0 , -109395 ,     0 , +46189 ] ./ 256 ; ...
      ];
      
      c = c(obj.Order,1:obj.Order);
      
    end
    
  end
  
end
