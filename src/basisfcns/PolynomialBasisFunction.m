classdef ( Abstract , Hidden ) PolynomialBasisFunction < BasisFunction
  %% POLYNOMIALBASISFUNCTION Generic class for polynomial basis functions
  
  
  
  %% PUBLIC READ-ONLY PROPERTIES
  properties ( SetAccess = protected )
    
    Degree (1, 1) double
    
    Order (1, 1) double
    
  end
  
  
  
  %% CONSTRUCTOR
  methods
    
    function obj = PolynomialBasisFunction(d)
      %% POLYNOMIALBASISFUNCTION
      
      
      
      narginchk(1, 1);
      nargoutchk(0, 1);
      
      obj@BasisFunction();
      
      obj.Degree = d;
      obj.Order  = d + 1;
      
      obj.DegreesOfFreedom = obj.Order;
      
    end
    
  end
  
  
  
  %% ABSTRACT METHODS
  methods ( Abstract )
    
    c = coeffs(obj);
      %% COEFFS Evaluate coefficients of polynomial in ascending power order
      %
      % C = COEFFS(OBJ)
    
  end
  
  
  
  %% CONVERSION
  methods
    
    function s = struct(obj)
      %% STRUCT
      
      
      so = struct();
      so.Degree = obj.Degree;
      so.Order  = obj.Order;
      
      s = mergestructs(so, struct@BasisFunction(obj));
      
    end
    
  end
  
end
