classdef BSpline < BasisFunction
  %% BSPLINE
  %
  % BSPLINE(DEGREE, KNOTVECTOR)
  %
  % BSPLINE(DEGREE, KNOTVECTOR, CTRLPTS)
  
  
  
  %% CONCRETE OBJECT PROPERTIES
  properties ( SetAccess = protected )
    
    % Whether it is a rational or non-rational curve object
    Rational (1, 1) logical = false
    
  end
  
  
  
  %% PUBLIC PROPERTIES
  properties ( SetAccess = protected )
    
    % Greville abscissae values
    GrevilleAbscissae (1, :) double
    
    % Location of curve's control points
    ControlPoints (:, :) double
    
  end
  
  
  
  %% READONLY PROPERTIES
  properties ( SetAccess = protected )
    
    % Local polynomial degree of curve
    Degree (1, 1) double = 0
    
    % Knot vector
    KnotVector (1, :) double = []
    
  end
  
  
  
  %% READONLY DEPENDENT PROPERTIES
  properties ( Dependent )
    
    % Dimensionality of the curve. Either 2 for planar or 3 for spatial
    Dimension
    
    % Number of control points as defined by the user
    NControlPoints
    
    % Order of curve is one higher than the degree
    Order
    
    % Number of segments defined through degree and number of control points
    Segments
    
  end
  
  
  
  %% CONSTRUCTOR
  methods
    
    function obj = BSpline(degree, knotvector, ctrlpts)
      %% BSPLINE
      
      
      if nargin < 3 || isempty(ctrlpts)
        ctrlpts = zeros(numel(knotvector) - 6 + 2, 0);
      end
      
      obj@BasisFunction();
      % degree, knotvector, ctrlpts, []);
      
      obj.Degree = degree;
      obj.KnotVector = knotvector;
      obj.ControlPoints = ctrlpts;
      obj.DegreesOfFreedom = obj.NControlPoints;
      
      obj.GrevilleAbscissae = grevilleAbscissae(obj.Degree, obj.KnotVector);
      
    end
    
  end
  
  
  
  %% GETTERS
  methods
    
    function v = get.Dimension(obj)
      %% GET.DIMENSION
      
      
      v = size(obj.ControlPoints, 2) - 1;
      
    end
    
    
    function v = get.NControlPoints(obj)
      %% GET.NCONTROLPOINTS
      
      
      v = numel(obj.KnotVector) - obj.Degree - 1;
      
    end
    
    
    function v = get.Order(obj)
      %% GET.ORDER
      
      
      v = obj.Degree + 1;
      
    end
    
    
    function v = get.Segments(obj)
      %% GET.SEGMENTS
      
      % Count control points
      ncp = obj.NControlPoints;
      
      v = ncp - obj.Degree;
      
    end
    
  end
  
  
  
  %% EVALUATION METHODS
  methods
    
    function b = evaluate(obj, x, varargin)
      %% EVALUATE Evaluate curve over collocation point(s)
      
      
      if nargin < 2 || isempty(x)
        x = obj.GrevilleAbscissae;
      end
      
      degree = obj.Degree;
      knotvector = obj.KnotVector;
      num_ctrlpts = obj.NControlPoints;
      spanfinder = 'linear';
      knots = x;
      
      % Get span vector and all non-zero bases
      spans = geometry.helpers.find_spans(degree, knotvector, num_ctrlpts, knots, spanfinder);
      % [nknots, ndeg]
      b_ = geometry.helpers.basis_functions(degree, knotvector, spans, knots);
      
      % Some index span vectors
      degspan = (0:degree) + 1;
      ixspan = repmat(spans(:), 1, numel(degspan)) + repmat(degspan, numel(spans), 1) - (degree + 1);
      
      % Init output
      nknots = numel(knots);
      b = zeros(num_ctrlpts, nknots);
      
      % Assign bases for the respective control point
      for iknot = 1:nknots
        b(ixspan(iknot,:),iknot) = b_(iknot, degspan);
      end
      
    end
    
  end
  
end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this class
% will be acknowledged in the "Changelog" section of the header.
