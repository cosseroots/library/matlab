% +FEM
%
% Files
%   ChebyshevPolynomialFirstKind   - CHEBYSHEVPOLYNOMIALFIRSTKIND Chebyshev polynomial of the first kind of degree D
%   GegenbauerPolynomial           - GEGENBAUERPOLYNOMIAL Gegenbauer polynomial
%   LegendrePolynomial             - LEGENDREPOLYNOMIAL Legendre polynomial of degree D
%   Monomial                       - MONOMIAL Momonial of degree D
%   PolynomialShapeFunction        - POLYNOMIALSHAPEFUNCTION Generic class for polynomial shape functions
%   ShapeFunction                  - SHAPEFUNCTION Generic class for any FEM shape function

