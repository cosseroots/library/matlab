classdef GegenbauerPolynomial < PolynomialBasisFunction
  %% GEGENBAUERPOLYNOMIAL Gegenbauer polynomial
  
  
  
  %% PUBLIC READ-ONLY PROPERTIES
  properties ( SetAccess = protected )
    
    % Alpha parameter of Gegenbauer polynomial
    Alpha
    
  end
  
  
  
  %% CONSTRUCTOR METHODS
  methods
    
    function obj = GegenbauerPolynomial(d, a)
      %% GEGENBAUERPOLYNOMIAL
      %
      % GEGENBAUERPOLYNOMIAL(D)
      %
      % GEGENBAUERPOLYNOMIAL(D, A)
      
      
      
      narginchk(1, 2);
      nargoutchk(0, 1);
      
      if nargin < 2 || isempty(a)
        a = 1;
      end
      
      obj@PolynomialBasisFunction(d);
      
      obj.Alpha = a;
      
    end
    
  end
  
  
  
  %% EVALUATION METHODS
  methods
    
    function v = evaluate(obj, x, ab)
      %% EVALUATE
      %
      % V = EVALUATE(OBJ, X)
      %
      % V = EVALUATE(OBJ, X, AB)
      
      
      
      % Default interval boundaries
      if nargin < 3 || isempty(ab)
        ab = [ -1 , 1 ];
        
      end
      
      d = obj.Degree;
      alph = obj.Alpha;
      
      % Interval boundaries
      a = ab(1);
      b = ab(end);
      
      % Ensure X is a column vector for faster memory allocations
      x = x(:);
      nx = numel(x);
      
      % Shift given coordinates to lie in [-1, 1] interval
      x = ( 2 .* x - ( b + a ) ) ./ ( b - a );
      
      C = zeros(nx, d + 1);
      C(:,0+1) = ones(nx, 1);
      C(:,1+1) = 2 * alph .* x;
      
      % Recurrence relation for higher polynomials
      for n = 2:d
        C(:,n+1) = ( 2 .* x .* (n + alph - 1) .* C(:,n) - (n + 2 * alph - 2) .* C(:,n-1) ) ./ n;
%         C(:,n) = ( 2 .* x .* (n + a - 1) .* C(:,n-1) - (n + 2 * a - 2) .* C(:,n-2) ) ./ n;
      end
      
      % Select only the valid parts of the polynomial coefficients i.e., only
      % those up until and including the polynomial degree
      v = permute(C(:,1:obj.Order), [2, 1]);
      
    end
    
    
    function c = coeffs(obj)
      %% COEFFS Evaluate coefficients of polynomial in ascending power order
      %
      % C = COEFFS(OBJ)
      
      
      c = zeros(1, obj.Order);
      
    end
    
  end
  
  
  
  %% CONVERSION
  methods
    
    function s = struct(obj)
      %% STRUCT
      
      
      so = struct();
      so.Alpha = obj.Alpha;
      
      s = mergestructs(so, struct@PolynomialBasisFunction(obj));
      
    end
    
  end
  
end
