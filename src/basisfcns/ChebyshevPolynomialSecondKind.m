classdef ChebyshevPolynomialSecondKind < PolynomialBasisFunction
  %% CHEBYSHEVPOLYNOMIALSECONDKIND Chebyshev polynomial of the second kind of degree D
  
  
  
  %% CONSTRUCTOR METHODS
  methods
    
    function obj = ChebyshevPolynomialSecondKind(d)
      %% CHEBYSHEVPOLYNOMIALSECONDKIND
      %
      % CHEBYSHEVPOLYNOMIALSECONDKIND(D)
      
      
      
      narginchk(1, 1);
      nargoutchk(0, 1);
      
      obj@PolynomialBasisFunction(d);
      
    end
    
  end
  
  
  
  %% EVALUATION METHODS
  methods
    
    function v = evaluate(obj, x, ab)
      %% EVALUATE
      %
      % V = EVALUATE(OBJ, X)
      %
      % V = EVALUATE(OBJ, X, AB)
      
      
      
      % Default interval boundaries
      if nargin < 3 || isempty(ab)
        ab = [ -1 , 1 ];
        
      end
      
      d = obj.Degree;
      
      % Interval boundaries
      a = ab(1);
      b = ab(end);
      
      % Ensure X is a column vector for faster memory allocations
      x = x(:);
      nx = numel(x);
      
      % Shift given coordinates to lie in [-1, 1] interval
      x = ( 2 .* x - ( b + a ) ) ./ ( b - a );
      
      T = zeros(nx, d + 1);
      T(:,0+1) = ones(nx, 1);
      T(:,1+1) = 2 .* x;
      
      % Loop over higher-order polynomials
      for id = 2:d
        T(:,id + 1) = 2 .* x .* T(:,id) - T(:,id-1);
      end
      
      % Select only the valid parts of the polynomial coefficients i.e., only
      % those up until and including the polynomial degree
      v = permute(T(:,1:obj.Order), [2, 1]);
      
    end
    
    
    function c = coeffs(obj)
      %% COEFFS Evaluate coefficients of polynomial in ascending power order
      %
      % C = COEFFS(OBJ)
      
      
      c = [ ...
        [ +1      0 ,     0 ,     0 ,      0 ,      0 ,      0 ,      0 ,       0 ,     0 ,      0 ] ; ...
        [  0 ,   +2 ,     0 ,     0 ,      0 ,      0 ,      0 ,      0 ,       0 ,     0 ,      0 ] ; ...
        [ -1 ,    0 ,    +4 ,     0 ,      0 ,      0 ,      0 ,      0 ,       0 ,     0 ,      0 ] ; ...
        [  0 ,   -4 ,     0 ,    +8 ,      0 ,      0 ,      0 ,      0 ,       0 ,     0 ,      0 ] ; ...
        [ +1 ,    0 ,   -12 ,     0 ,    +16 ,      0 ,      0 ,      0 ,       0 ,     0 ,      0 ] ; ...
        [  0 ,   +6 ,     0 ,   -32 ,      0 ,    +32 ,      0 ,      0 ,       0 ,     0 ,      0 ] ; ...
        [ -1 ,    0 ,   +24 ,     0 ,    -80 ,      0 ,    +64 ,      0 ,       0 ,     0 ,      0 ] ; ...
        [  0 ,   -8 ,     0 ,   +80 ,      0 ,   -192 ,      0 ,   +128 ,       0 ,     0 ,      0 ] ; ...
        [ +1 ,    0 ,   -40 ,     0 ,   +240 ,      0 ,   -448 ,      0 ,    +256 ,     0 ,      0 ] ; ...
        [  0 ,  +10 ,     0 ,  -160 ,      0 ,   +672,       0 ,  -1024 ,       0 ,   512 ,      0 ] ; ...
      ];
      
      c = c(obj.Order,1:obj.Order);
      
    end
    
  end
  
end
