classdef Monomial < PolynomialBasisFunction
  %% MONOMIAL Momonial of degree D
  
  
  
  %% CONSTRUCTOR METHODS
  methods
    
    function obj = Monomial(d)
      %% MONOMIAL
      %
      % MONOMIAL(D)
      
      
      
      narginchk(1, 1);
      nargoutchk(0, 1);
      
      obj@PolynomialBasisFunction(d);
      
    end
    
  end
  
  
  
  %% EVALUATION METHODS
  methods
    
    function v = evaluate(obj, x, varargin)
      %% EVALUATE
      %
      % V = EVALUATE(OBJ, X)
      
      
      d = 0:obj.Degree;
      
      v = x .^ ( d(:) );
      
    end
    
    
    function c = coeffs(obj)
      %% COEFFS Evaluate coefficients of polynomial in ascending power order
      %
      % C = COEFFS(OBJ)
      
      
      c = zeros(1, obj.Order);
      c(obj.Degree + 1) = 1;
      
    end
    
  end
  
end
