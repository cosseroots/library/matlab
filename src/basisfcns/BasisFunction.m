classdef ( Abstract, Hidden ) BasisFunction < handle
  %% BASISFUNCTION Generic class for any FEM shape function
  
  
  
  %% PUBLIC READ-ONLY PROPERTIES
  properties ( SetAccess = protected )
    
    DegreesOfFreedom (1, :) double
    
  end
  
  
  
  %% CONSTRUCTOR
  methods
    
    function obj = BasisFunction()
      %% BASISFUNCTION
      
      
      
      narginchk(0, 0);
      nargoutchk(0, 1);
      
      obj@handle();
      
    end
    
  end
  
  
  
  %% EVALUATION METHODS
  methods
    
    function y = evaluate(~, ~, varargin)
      %% EVALUATE
      
      
      
      y = zeros(1, 0);
      
    end
    
  end
  
  
  
  %% CONVERSION
  methods
    
    function s = struct(obj)
      %% STRUCT

      
      
      s = struct();
      
    end
    
  end
  
end
