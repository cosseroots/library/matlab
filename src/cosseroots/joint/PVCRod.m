classdef PVCRod < Rod
  %% PVCROD
  
  
  
  %% CONSTRUCTOR METHODS
  methods
    
    function obj = PVCRod(varargin)
      %% PVCROD
      
      
      narginchk(0, Inf);
      
      obj@Rod();
      
      % Diameter [ m ]
      d = 5.5 * 1e-3;
      % Radius [ m ]
      r = d / 2;
      % Cross section area [ mm2 ]
      A = pi * r ^ 2;
      % Moment of area of a circle
      J = diag(pi / 4 * r ^ 4 * [ 2 ; 1 ; 1 ]);
      
      % Length [ m ] 
      L = ( 1405 - 60 ) * 1e-3;
      % Mass of rod [ kg ]
      m = 42.2 * 1e-3;
      % Volume of rod
      V = L * A;
      % Density of rod [ kg / m3 ]
      rho = m / V;
      
      % Elastic/Young's modulus [ GPa ]
      E = 2.01 * 1e9;
      % Poisson's ratio
      nu = 0.40;
      % Shear modulus
      G = 1.05 * 1e9;
      
      % Material properties
      obj.Length               = L;
      
      % Kinematic properties
      obj.AllowedStrain        = [ 0 , 1 , 1 , 0 , 0 , 0 ];
      obj.BasisFunction        = LegendrePolynomial(2);
      obj.ReferenceStrain      = [ 0.0 , 0.0 , 0.0 , 1.0 , 0.0 , 0.0 ];
      
      % Dynamics properties
      obj.HookeanMatrix        = hookeanMatrix(A, J, E, G);
      obj.DampingMatrix        = 0.50 * obj.HookeanMatrix;
      obj.SpatialInertiaMatrix = spatialInertiaMatrix(rho * A, rho * J, zeros(3, 1));
      
      setProperties(obj, nargin, varargin{:});
      
    end
    
  end
  
end
