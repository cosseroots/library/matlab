function [vxi, vxidot, vxiddot] = tangentStrain(obj, s, vq, vqdot, vqddot)
%% TANGENTSTRAIN Calculate the tangent of strains
%
% VXI = TANGENTSTRAIN(ROD, S, VQ) evaluate the tangent strain VXI of strain
% positions given rod object ROD and the variation of generalized joint
% coordinates VQ.
%
% [VXI, VXIDOT] = TANGENTSTRAIN(___, VQDOT) also calculates the variation VXIDOT
% of strain velocities.
%
% [VXI, VXIDOT, VXIDDOT] = TANGENTSTRAINS(___, VQDDOT) also calculates the
% variation VXIDDOT of strain accelerations.
% 
% Inputs:
%
%   ROD                     Rod object.
%
%   VQ                      ExV array of variation of generalized joint
%                           positions.
%
%   VQDOT                   ExV array of variation of generalized joint
%                           velocities.
%
%   VQDDOT                  ExV array of variation of generalized joint
%                           accelerations.
% 
%   S                       1xN array of path coordinates in [ 0.0 , 1.0 ] on
%                           which to evaluate the variation of strains
%
% Outputs:
%
%   VXI                     6xVxN array of variation of generalized strain
%                           positions.
% 
%   VXIDOT                  6xVxN array of variation of generalized strain
%                           velocities.
% 
%   VXIDDOT                 6xVxN array of variation of generalized strain
%                           accelerations.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% STRAIN(ROD, S, VQ)
% STRAIN(ROD, S, VQ, VQDOT)
% STRAIN(ROD, S, VQ, VQDOT, VQDDOT)
narginchk(3, 5);

% TANGENTSTRAIN(___)
% VXI = TANGENTSTRAIN(___)
% [VXI, VXIDOT] = TANGENTSTRAIN(___)
% [VXI, VXIDOT, VXIDDOT] = TANGENTSTRAIN(___)
nargoutchk(0, 3);

% STRAIN(ROD, S, VQ)
% STRAIN(ROD, S, VQ, [], VQDDOT)
if nargin < 4 || isempty(vqdot)
  vqdot = [];
end

% STRAIN(ROD, S, VQ, VQDOT)
if nargin < 5 || isempty(vqddot)
  vqddot = [];
end




%% Algorithm

% Build state of variation of deformation coordinates
% 6 x V x 1
BPhi    = pagemult(obj.B, shapefuns(obj, s));

% XI = STRAIN(ROD, S, VQ)
% [XI, XIDOT] = STRAIN(ROD, S, VQ, VQDOT)
% [XI, XIDOT, XIDDOT] = STRAIN(ROD, S, VQ, VQDOT, VQDDOT)
vxi = pagemult(BPhi, vq);

% [XI, XIDOT] = STRAIN(ROD, S, VQ, VQDOT)
% [XI, XIDOT, XIDDOT] = STRAIN(ROD, S, VQ, VQDOT, VQDDOT)
if nargout > 1 && ~isempty(vqdot)
  vxidot = pagemult(BPhi, vqdot);
end

% [XI, XIDOT, XIDDOT] = STRAIN(ROD, S, VQ, VQDOT, VQDDOT)
if nargout > 2 && ~isempty(vqddot)
  vxiddot = pagemult(BPhi, vqddot);
end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
