function Kee = generalizedStiffness(obj, mode, options)%#codegen
%% GENERALIZEDSTIFFNESS Calculate a rod's generalized stiffness matrix
%
% KEE = GENERALIZEDSTIFFNESS(ROD, MODE)
%
% KEE = GENERALIZEDSTIFFNESS(ROD, MODE, OPTIONS)
%
% Inputs:
%
%   ROD                     Rod object.
%
%   MODE                    Mode to calculate generalized stiffness. Can be one
%                           of the following values (case-insensitive):
%                           "integral"  Using MATLAB's `INTEGRAL` function;
%                           "ode"       Using MATLAB's `ODE45` function.
%
%   OPTIONS                 Options structure for the underlying integrating
%                           function.
%
% Outputs:
%
%   KEE                     ExE matrix of generalized stiffness of the elastic
%                           part of the rod dynamics.
%
% See als:
%   ODE45 INTEGRAL ROD.GENERALIZEDDAMPING
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% GENERALIZEDSTIFFNESS(ROD, MODE)
% GENERALIZEDSTIFFNESS(ROD, MODE, OPTIONS)
narginchk(2, 3);

% GENERALIZEDSTIFFNESS(___)
% KEE = GENERALIZEDSTIFFNESS(___)
nargoutchk(0, 1);

% Default integration `MODE`
if nargin < 3 || isempty(options)
  options = struct();
end



%% Calculate

% Number of generalized coordinates needed for reshaping
nq = obj.PositionNumber;

switch lower(mode)
  case 'ode'
    % Calculate generalized stiffness formulating it as an ODE-problem
    [~, Kee] = ode45( ...
        @(s, ~) ode_rhs(obj, s) ...
      , [0, 1] ...
      , zeros(nq * nq, 1) ...
      , odeset(options) ...
    );
    Kee = permute(Kee(end,:), [2, 1]);
  
  otherwise
    % Turn options structure into a proper name/value cell array
    opts = struct2nvpairs(options);
    
    % Calculate generalized stiffness using the standard `INTEGRAL` form
    Kee = integral( ...
        @(s) ode_rhs(obj, s) ...
        , 0, 1 ...
        , opts{:} ...
        , 'ArrayValued', true ...
      );
  
end

% Reshape into proper square matrix form
Kee = reshape(Kee, nq, nq);


end


function dkdx = ode_rhs(obj, s)
%% ODE_RHS


% Evaluate basis function
phi = shapefuns(obj, s);

% Integrand part (in matrix form)
dkdx = obj.Length * transpose(phi) * obj.ReducedHookeanMatrix * phi;

% Turn integrand into a column vector
dkdx = reshape(dkdx, [], 1);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
