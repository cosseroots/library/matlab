function [A, b] = odeR(obj, s, k, d, p)%#codegen
%% ODER Differential equation of positions with respect to path coordinate
%
% DRDS = ODER(OBJ, S, K, D, P)
%
% [A, B] = ODER(___)
%
% Inputs:
%
%   OBJ                     Rod object.
%
%   S                       Nx1 array of path coordinates.
%
%   K                       Kinematics structure with fields
%                             g       7xN
%                             eta     6xN
%                             etadot  6xN
%
%   D                       Deformation state structure with fields
%                             xi      ExN
%                             xidot   ExN
%                             xiddot  ExN
%
%   P                       Parameter structure with fields
%                             rod     1x1
%                             time    1x1
%
% Outputs:
%
%   DRDS                    3xN array of changes of position over path
%                           coordinate.
%
%   A                       3x3xN array of linear form of the ODE.
%
%   B                       3xN vector of linear form of the ODE.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% ODER(OBJ, S, K, D, P)
narginchk(5, 5);

% ODER(___)
% DRDS = ODER(___)
% [A, B] = ODER(___)
nargoutchk(0, 2);



%% Algorithm

% Quick variables
L = obj.Length;

% Extract variables from states
g      = hslice(k, 1, 1:7);
% eta    = hslice(k, 1, 8:13);
% etadot = hslice(k, 1, 14:19);
xi     = hslice(d, 1, 1:6);
% xidot  = hslice(d, 1, 7:12);
% xiddot = hslice(d, 1, 13:18);

% Extract linear strain from spatial strain
G = hslice(xi, 1, 4:6);

% Build matrices for linear ODE
A = zeros(3, 3, numel(s));
b = permute(pagemult(tpar2rotm(g), permute(G, [1, 3, 2])), [1, 3, 2]);

% Scaling to take ODE boundary and rod length into account
A = L * A;
b = L * b;

% DRDS = ODER(___)
if nargout < 2
  % Compose right hand side of ODE
  A = permute(pagemult(A, permute(tpar2trans(g), [1, 3, 2])), [1, 3, 2]) + b;

end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
