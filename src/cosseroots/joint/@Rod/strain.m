function [xi, xidot, xiddot] = strain(obj, s, q, qdot, qddot)
%% STRAIN
%
% Inputs:
%
%   OBJ                     Rod object.
% 
%   S                       1xN array of path coordiantes at which to evaluate
%                           strains. Defaults to spectrla node abscissaes.
% 
%   Q                       Ex1 array of generalized joint positions.
% 
%   QDOT                    Ex1 array of generalized joint velocities.
% 
%   QDDOT                   Ex1 array of generalized joint accelerations.
%
% Outputs:
%
%   XI                      6xN array of strain positions.
% 
%   XIDOT                   6xN array of strain velocities.
% 
%   XIDDOT                  6xN array of strain accelerations.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% STRAIN(OBJ, S, Q)
% STRAIN(OBJ, S, Q, QDOT)
% STRAIN(OBJ, S, Q, QDOT, QDDOT)
narginchk(2, 5);

% STRAIN(OBJ, S, Q)
% XI = STRAIN(OBJ, S, Q)
% [XI, XIDOT] = STRAIN(OBJ, S, Q, QDOT)
% [XI, XIDOT, XIDDOT] = STRAIN(OBJ, S, Q, QDOT, QDDOT)
nargoutchk(0, nargin - 2);

% STRAIN(OBJ, S, Q)
% STRAIN(OBJ, S, Q, [], QDDOT)
if nargin < 4 || isempty(qdot)
  qdot = [];
end

% STRAIN(OBJ, S, Q, QDOT)
if nargin < 5 || isempty(qddot)
  qddot = [];
end



%% Algorithm

% Evaluate basis function along path coordinates
phi = shapefuns(obj, s);

% Selector matrices of rod
B    = obj.B;
Bbar = obj.BBar;

% Reference strain
xi_a0 = obj.ReferenceStrainAllowed;
% Constrained strains
xi_c  = obj.ReferenceStrainConstrained;

% Strains
% XI = STRAIN(___)
% [XI, XIDOT] = STRAIN(___)
% [XI, XIDOT, XIDDOT] = STRAIN(___)
xi = pagemult(B, permute(sum(phi .* permute(q, [3, 1, 4, 2]), 2), [1, 3, 4, 2]) + xi_a0) + Bbar * xi_c;

% Strain rates
% [XI, XIDOT] = STRAIN(___)
% [XI, XIDOT, XIDDOT] = STRAIN(___)
if nargout > 1 && ~isempty(qdot)
  xidot = pagemult(B, permute(sum(phi .* permute(qdot, [3, 1, 4, 2]), 2), [1, 3, 4, 2]));

end

% Strain accelerations
% [XI, XIDOT, XIDDOT] = STRAIN(___)
if nargout > 2 && ~isempty(qddot)
  xiddot = pagemult(B, permute(sum(phi .* permute(qddot, [3, 1, 4, 2]), 2), [1, 3, 4, 2]));

end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
