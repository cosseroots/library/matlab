function Dee = generalizedDamping(obj, mode, options)%#codegen
%% GENERALIZEDDAMPING Calculate a rod's generalized damping matrix
%
% DEE = GENERALIZEDDAMPING(ROD, MODE)
%
% DEE = GENERALIZEDDAMPING(ROD, MODE, OPTIONS)
%
% Inputs:
%
%   ROD                     Rod object.
%
%   MODE                    Mode to calculate generalized stiffness. Can be one
%                           of the following case-insensitive values:
%                           "integral"  Using MATLAB's `INTEGRAL` function
%                           "ode"       Using MATLAB's `ODE45` function
%
%   OPTIONS                 Options structure for the underlying integrating
%                           function.
%
% Outputs:
%
%   DEE                     QxQ matrix of generalized damping of the elastic
%                           part of the rod dynamics.
%
% See als:
%   ODE45 INTEGRAL ROD.GENERALIZEDSTIFFNESS
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% GENERALIZEDDAMPING(ROD, MODE)
% GENERALIZEDDAMPING(ROD, MODE, OPTIONS)
narginchk(2, 3);

% GENERALIZEDDAMPING(___)
% DEE = GENERALIZEDDAMPING(___)
nargoutchk(0, 1);

% GENERALIZEDDAMPING(ROD, MODE)
if nargin < 3 || isempty(options)
  options = struct();
end



%% Calculate

% Number of generalized coordinates needed for reshaping
nq = obj.PositionNumber;

switch lower(mode)
  case 'ode'
    % Calculate generalized damping formulating it as an ODE-problem
    [~, Dee] = ode45( ...
        @(s, ~) ode_rhs(obj, s) ...
      , [0, 1] ...
      , zeros(nq * nq, 1) ...
      , odeset(options) ...
    );
    Dee = permute(Dee(end,:), [2, 1]);
  
  otherwise
    % Turn options structure into a proper name/value cell array
    opts = struct2nvpairs(options);
    
    % Calculate generalized damping using the standard `INTEGRAL` form
    Dee = integral( ...
          @(s) ode_rhs(obj, s) ...
        , 0, 1 ...
        , opts{:} ...
        , 'ArrayValued', true ...
      );
  
end

% Reshape into proper square matrix form
Dee = reshape(Dee, nq, nq);


end


function dddx = ode_rhs(obj, s)
%% ODE_RHS


% Evaluate basis function
phi = shapefuns(obj, s);

% Integrand part (in matrix form)
dddx = obj.Length * transpose(phi) * obj.ReducedDampingMatrix * phi;

% Turn integrand into a column vector
dddx = reshape(dddx, [], 1);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
