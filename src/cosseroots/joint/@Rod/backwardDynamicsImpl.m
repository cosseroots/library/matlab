function [F0, Qa] = backwardDynamicsImpl(obj, x1, xq, F1)%#codegen
%% BACKWARDRNEAIMPL
%
% Inputs:
% 
%   JOINT                   Joint object.
%
%   X1                      X1 = { T1 , ETA1 , ETA1DOT }
%
%   XQ                      XQ = {  Q , QDOT , QDDOT   }
%
%   F1                      F1
%
% Outputs:
%
%   F0                      6xN array of joint transmitted forces onto the
%                           parent body expressed in the parent's body.
% 
%   QA                      VxN array of generalized joint acutation forces.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse Arguments

% Distal rigid body state
T1      = x1{1};
eta1    = x1{2};
eta1dot = x1{3};
% Joint state
q       = xq{1};
qdot    = xq{2};
qddot   = xq{3};

% % Distal rigid body variation
% vzeta1   = vx1{1};
% veta1    = vx1{2};
% veta1dot = vx1{3};
% % Joint state variation
% vq       = vxq{1};
% vqdot    = vxq{2};
% vqddot   = vxq{3};



%% Setup of Integration

% Integration options for spectral integration
specopts = obj.SpectralIntegrationOptions;
nn = specopts.Nodes;
nq = obj.PositionNumber;

% Interval of integration
sspan = [ 1.0 , 0.0 ];
% Node points on forward abscissae
snodes = flip(obj.SpectralNodes);

% Evaluate strains
[xi, xidot, xiddot] = strain(obj, snodes, q, qdot, qddot);

% Build state of deformation coordinates
d_ = cat(1, xi, xidot, xiddot);

% Result of integrations:
% Kinematics
k_ = cat(1, tparzeros(nn), twistzeros(nn), twistzeros(nn));
% Efforts
e_ = zeros(6 + nq, nn);

% Empty parameter structure
p_ = struct();

% Convert distal homogeneous transformation matrix to its homogeneous parameters
g = tform2tpar(T1);



%% Kinematic Reconstruction on Backward

% Quaternions
Q_0 = tpar2quat(g);

[A, b]    = odeQ(obj, snodes, k_, d_, p_);
[~, Q_]   = odespec({ A , b }, sspan, Q_0, specopts);
k_(1:4,:) = quatnormalized(transpose(Q_), true);


% Positions
r_0 = tpar2trvec(g);

[A, b]    = odeR(obj, snodes, k_, d_, p_);
[~, r_]   = odespec({ A , b }, sspan, r_0, specopts);
k_(5:7,:) = transpose(r_);


% Twists
eta_0    = eta1;

[A, b]     = odeEta(obj, snodes, k_, d_, p_);
[~, eta_]  = odespec({ A , b }, sspan, eta_0, specopts);
k_(8:13,:) = transpose(eta_);


% Twist Velocities
etadot_0 = eta1dot;

[A, b]       = odeEtadot(obj, snodes, k_, d_, p_);
[~, etadot_] = odespec({ A , b }, sspan, etadot_0, specopts);
k_(14:19,:)  = transpose(etadot_);



%% Backward Propagation

% Internal stresses
F0_0 = -F1(:,1);

[A, b]       = odeLambda(obj, snodes, k_, d_, e_, p_);
[~, lambda_] = odespec({ A , b }, sspan, F0_0, specopts);
e_(1:6,:)    = transpose(lambda_);

% Proximal forces
F0 = -e_(1:6,end);


% Internal actuation
Qa_0 = zeros(nq, 1);

[A, b]      = odeQa(obj, snodes, k_, d_, e_, p_);
[~, Qa_]    = odespec({ A , b }, sspan, Qa_0, specopts);
e_(7:end,:) = transpose(Qa_);

% Proximal internal actuation
Qa = -e_(7:end,end) + ...
                    + obj.GeneralizedDampingMatrix   * qdot + ...
                    + obj.GeneralizedStiffnessMatrix * q ; ...


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
