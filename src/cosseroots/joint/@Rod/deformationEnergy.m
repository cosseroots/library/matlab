function e = deformationEnergy(obj, q)
%% DEFORMATIONENERGY
%
% E = DEFORMATIONENERGY(ROD, Q)
%
% Inputs:
%
%   ROD                     Rod object.
%
%   Q                       Px1 vector of generalized position variables.
%
% Outputs:
%
%   E                       Vx1 scalar deformation energy of rod.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% DEFORMATIONENERGY(OBJ, Q)
narginchk(2, 2);

% DEFORMATIONENERGY(___)
% E = DEFORMATIONENERGY(___)
nargoutchk(0, 1);



%% Integrate

e = sum(generalizedDeformationEnergy(obj, q, ones(6, 1)), 1);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
