function e = twistEnergy(obj, q)
%% TWISTENERGY
%
% E = TWISTENERGY(ROD, Q)
%
% Inputs:
%
%   ROD                     Rod object.
%
%   Q                       Px1 vector of generalized position variables.
%
% Outputs:
%
%   E                       1x1 scalar deformation energy of rod.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% TWISTENERGY(OBJ, Q)
narginchk(2, 2);

% TWISTENERGY(___)
% E = TWISTENERGY(___)
nargoutchk(0, 1);



%% Integrate

e = generalizedDeformationEnergy(obj, q, delta(1, 6));


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
