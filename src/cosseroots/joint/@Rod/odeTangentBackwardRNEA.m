function dvkds = odeTangentBackward(obj, s, k, d, e, vk, vd, ve, p)%#codegen
%% ODETANGENTBACKWARD ODE of the tangent backward projection
%
% DVKDS = ODETANGENTBACKWARD(OBJ, S, K, D, E, VK, VD, VE, P)
%
% Inputs:
%
%   OBJ                     Rod object.
%
%   S                       1x1 scalar path coordinate.
%
%   K                       Backward kinematics structure with fields
%                             g       7x1
%                             eta     6x1
%                             etadot  6x1
%
%   D                       Deformation state structure with fields
%                             xi      Ex1
%                             xidot   Ex1
%                             xiddot  Ex1
%
%   E                       Effort state structure with fields
%                             f       6x1
%                             qa      Qx1
%
%   VK                      Backward kinematics variation structure with fields
%                             zeta    6xV
%                             eta     6xV
%                             etadot  6xV
%
%   VD                      Deformation variation state structure with fields
%                             xi      ExV
%                             xidot   ExV
%                             xiddot  ExV
%
%   VE                      Effort variation state structure with fields
%                             f       6xV
%                             qa      QxV
%
%   P                       Parameter structure with fields
%                             rod     Rod object
%                             time    Scalar time
%
% Outputs:
%
%   DVKDS                    (25+E)x(1+K) array of (25+E)x1 change of kinematic
%                           state and (25+E)xK change of kinematic state
%                           variation.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% ODETANGENTBACKWARD(OBJ, S, K, D, E, VK, VD, VE, P)
narginchk(9, 9)

% ODETANGENTBACKWARD(___)
% DVKDS = ODETANGENTBACKWARD(___)
nargoutchk(0, 1);



%% Algorithm

dvkds = cat( ...
    1 ...
  ,   odeTangentZeta(obj, s, k, d, vk, vd, p) ...
  ,    odeTangentEta(obj, s, k, d, vk, vd, p) ...
  , odeTangentEtadot(obj, s, k, d, vk, vd, p) ...
  , odeTangentLambda(obj, s, k, d, e, vk, vd, ve, p) ...
  ,     odeTangentQa(obj, s, k, d, e, vk, vd, ve, p) ...
);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
