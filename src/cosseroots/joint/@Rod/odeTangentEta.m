function [A, b] = odeTangentEta(obj, s, k, d, vk, vd, p)%#codegen
%% ODETANGENTETA ODE of tangents of twists
%
% DVETADS = ODETANGENTETA(OBJ, S, K, D, VK, VD, P)
%
% [A, B] = ODETANGENTETA(___)
%
% Inputs:
%
%   OBJ                     Rod object.
%
%   S                       1xN array of path coordinates.
%
%   K                       Reconstruction kinematics, forward kinematics, or
%                           backward kinematics structure with fields
%                             g       7xN
%                             eta     6xN
%                             etadot  6xN
%
%   D                       Deformation state structure with fields
%                             xi      ExN
%                             xidot   ExN
%                             xiddot  ExN
%
%   VK                      Reconstruction kinematics variation, forward
%                           kinematics variation, or backward kinematics
%                           variation structure with fields
%                             zeta    6xVxN
%                             eta     6xVxN
%                             etadot  6xVxN
%
%   VD                      Deformation variation state structure with fields
%                             xi      ExVxN
%                             xidot   ExVxN
%                             xiddot  ExVxN
%
%   P                       Parameter structure with fields
%                             rod     Rod object
%                             time    Scalar time
%
% Outputs:
%
%   A                       Description of argument A
%
%   B                       Description of argument B
%
%   DVETADS                 Description of argument DVETADS
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% ODETANGENTETA(OBJ, S, K, D, VK, VD, P)
narginchk(7, 7);

% ODETANGENTETA(___)
% DVETADS = ODETANGENTETA(___)
% [A, B] = ODETANGENTETA(___)
nargoutchk(0, 2);



%% Algorithm

% Quick variables
L = obj.Length;

% Extract values from state vectors
g      = hslice(k, 1, 1:7);
eta    = hslice(k, 1, 8:13);
% etadot = hslice(k, 1, 14:19);
xi     = hslice(d, 1, 1:6);
% xidot  = hslice(d, 1, 7:12);
% xiddot = hslice(d, 1, 13:18);
% f      = hslice(e, 1, 1:6);
% qa     = hslice(e, 1, 7:size(e, 1));

% vzeta   = hslice(vk, 1, 1:6);
veta    = hslice(vk, 1, 7:12);
% vetadot = hslice(vk, 1, 13:18);
vxi     = hslice(vd, 1, 1:6);
vxidot  = hslice(vd, 1, 7:12);
% vxiddot = hslice(vd, 1, 13:18);
% vf      = hslice(ve, 1, 1:6);
% vqa     = hslice(ve, 1, 7:size(ve, 1));

% Build matrices for linear ODE
A = -ad(xi);
b = pagemult(ad(eta), vxi) + ...
  + vxidot;

% Scaling to take ODE boundary and rod length into account
A = L * A;
b = L * b;

% DVETADS = ODETANGENTETA(___)
if nargout < 2
  % Compose right hand side of ODE
  A = pagemult(A, veta) + b;

end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
