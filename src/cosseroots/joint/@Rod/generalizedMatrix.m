function K = generalizedMatrix(obj, m, mode, options)
%% GENERALIZEDMATRIX
%
% K = GENERALIZEDMATRIX(ROD, M, MODE)
%
% K = GENERALIZEDMATRIX(ROD, M, MODE, OPTIONS)
%
% Inputs:
%
%   ROD                     Rod object.
%
%   M                       Base matrix to calculate the generalized matrix of.
%
%   MODE                    Mode to calculate generalized stiffness. Can be one
%                           of the following case-insensitive values:
%                           "integral"  Using MATLAB's `INTEGRAL` function
%                           "ode"       Using MATLAB's `ODE45` function
%
%   OPTIONS                 Options structure for the underlying integrating
%                           function.
%
% Outputs:
%
%   K                       Generalized matrix of internal properties integrated
%                           over material coordinate.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% GENERALIZEDMATRIX(OBJ, M, MODE)
% GENERALIZEDMATRIX(OBJ, M, MODE, OPTIONS)
narginchk(3, 4);

% GENERALIZEDMATRIX(___)
% K = GENERALIZEDMATRIX(___)
nargoutchk(0, 1);

% GENERALIZEDMATRIX(OBJ, M, MODE)
if nargin < 4 || isempty(options)
  options = struct();
end



%% Integrate

% Number of generalized coordinates needed for reshaping
nq = obj.PositionNumber;

switch lower(mode)
  case 'ode'
    % Calculate generalized damping formulating it as an ODE-problem
    [~, K] = ode45( ...
        @(s, ~) odeGeneralizedMatrix(obj, m, s) ...
      , [0, 1] ...
      , zeros(nq * nq, 1) ...
      , odeset(options) ...
    );
    K = permute(K(end,:), [2, 1]);

  otherwise
    % Turn options structure into a cell array 
    opts = struct2nvpairs(options);
    
    % Calculate generalized damping using the standard `INTEGRAL` form
    K = integral( ...
          @(s) odeGeneralizedMatrix(obj, m, s) ...
        , 0, 1 ...
        , opts{:} ...
        , 'ArrayValued', true ...
      );

end

% Reshape into proper square matrix form
K = reshape(K, nq, nq);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
