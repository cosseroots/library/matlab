function [A, b] = odeTangentQa(obj, s, k, d, e, vk, vd, ve, p)%#codegen
%% ODETANGENTQA
%
% DVQADS = ODETANGENTQA(OBJ, S, K, D, E, VK, VD, VE, P)
%
% [A, B] = ODETANGENTQA(___)
%
% Inputs:
%
%   OBJ                     Rod object.
%
%   S                       1xN array of path coordinates.
%
%   K                       Backward kinematics structure with fields
%                             g       7xN
%                             eta     6xN
%                             etadot  6xN
%
%   D                       Deformation state structure with fields
%                             xi      ExN
%                             xidot   ExN
%                             xiddot  ExN
%
%   E                       Efforts structure with fields
%                             f       6xN
%                             qa      6xN
%
%   VK                      Backward kinematics variation structure with fields
%                             zeta    6xVxN
%                             eta     6xVxN
%                             etadot  6xVxN
%
%   VD                      Deformation variation structure with fields
%                             xi      ExVxN
%                             xidot   ExVxN
%                             xiddot  ExVxN
%
%   VE                      Efforts variation structure with fields
%                             f       6xVxN
%                             qa      6xVxN
%
%   P                       Parameter structure with fields
%                             rod     Rod object
%                             time    Scalar time
%
% Outputs:
%
%   A                       Description of argument A
%
%   B                       Description of argument B
%
%   DVQADS                  Description of argument DVQADS
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% ODETANGENTQA(OBJ, S, K, D, E, VK, VD, VE, P)
narginchk(9, 9);

% ODETANGENTQA(___)
% DVQADS = ODETANGENTQA(___)
% [A, B] = ODETANGENTQA(___)
nargoutchk(0, 2);



%% Algorithm

% Quick variables
L   = obj.Length;
nq  = obj.PositionNumber;

% Extract values from state vectors
% g      = hslice(k, 1, 1:7);
% eta    = hslice(k, 1, 8:13);
% etadot = hslice(k, 1, 14:19);
% xi     = hslice(d, 1, 1:6);
% xidot  = hslice(d, 1, 7:12);
% xiddot = hslice(d, 1, 13:18);
% f      = hslice(e, 1, 1:6);
% qa     = hslice(e, 1, 7:size(e, 1));

% vzeta   = hslice(vk, 1, 1:6);
% veta    = hslice(vk, 1, 7:12);
% vetadot = hslice(vk, 1, 13:18);
% vxi     = hslice(vd, 1, 1:6);
% vxidot  = hslice(vd, 1, 7:12);
% vxiddot = hslice(vd, 1, 13:18);
vf      = hslice(ve, 1, 1:6);
vqa     = hslice(ve, 1, 7:size(ve, 1));

% Build matrices for linear ODE
A = zeros(nq, nq, numel(s));
b = pagemult( ...
    permute( ...
        pagemult( ...
            -obj.B ...
          , shapefuns(obj, s) ...
        ) ...
      , [2, 1, 3] ...
    ) ...
  , vf ...
);

% Scaling to take ODE boundary and rod length into account
A = L * A;
b = L * b;

% DVQADS = ODETANGENTQA(___)
if nargout < 2
  % Compose right hand side of ODE
  A = pagemult(A, vqa) + b;

end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
