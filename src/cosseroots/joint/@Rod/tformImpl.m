function T = tformImpl(obj, q)%#codegen
%% TFORMIMPL
%
% Inputs:
%
%   OBJ                     Rod object.
% 
%   Q                       PxN array of generalized joint positions.
%
% Outputs:
%
%   T                       4x4xN array of joint transmitted homogeneous
%                           transformation matrices.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% TFORMIMPL(OBJ, Q)
narginchk(2, 2);

% TFORMIMPL(___)
% T = TFORMIMPL(___)
nargoutchk(0, 1);



%% Setup of Integration

% Integration options for spectral integration
specopts = obj.SpectralIntegrationOptions;
nn = specopts.Nodes;

% Interval of integration
sspan = [ 0.0 , 1.0 ];
% Node points on forward and backward abscissae
snodes = obj.SpectralNodes;

% Evaluate strains
xi = strain(obj, snodes, q);

% Build state of strain coordinates
d_ = xi;

% Result of integrations:
% Kinematics
k_ = tparzeros(nn);

% Empty parameter structure
p_ = struct();



%% Forward Reconstruction

% Quaternions
Q_0 = quatzeros();

[A, b]    = odeQ(obj, snodes, k_, d_, p_);
[~, Q]    = odespec({ A , b }, sspan, Q_0, specopts);
k_(1:4,:) = quatnormalized(transpose(Q), true);


% Positions
r_0 = trveczeros();

[A, b]    = odeR(obj, snodes, k_, d_, p_);
[~, r]    = odespec({ A , b }, sspan, r_0, specopts);
k_(5:7,:) = transpose(r);

% Transformation through joint from parent to child
T = tpar2tform(k_(1:7,end));


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
