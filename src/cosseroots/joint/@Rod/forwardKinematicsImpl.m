function [T, eta, etadot] = forwardKinematicsImpl(obj, xq)%#codegen
%% FORWARDKINEMATICSIMPL
%
% Inputs:
% 
%   JOINT                   Joint object.
%
%   XQ                      XQ = { Q , QDOT , QDDOT }
%
% Outputs:
%
%   T                       4x4xN array of joint transmitted homogeneous
%                           transformation matrices.
% 
%   ETA                     6xN array of joint transmitted twists.
% 
%   ETADOT                  6xN array of joint transmitted twist velocities.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse Arguments

% Joint state
q       = xq{1};
qdot    = xq{2};
qddot   = xq{3};

% Joint state variation
% vq       = vxq{1};
% vqdot    = vxq{2};
% vqddot   = vxq{3};



%% Setup of Integration

% Integration options for spectral integration
specopts = obj.SpectralIntegrationOptions;
nn = specopts.Nodes;

% Interval of integration
sspan = [ 0.0 , 1.0 ];
% Node points on forward and backward abscissae
snodes = obj.SpectralNodes;

% Evaluate strains
[xi, xidot, xiddot] = strain(obj, snodes, q, qdot, qddot);

% Build state of strain coordinates
d_ = cat(1, xi, xidot, xiddot);

% Result of integrations:
% Kinematics
k_ = cat(1, tparzeros(nn), twistzeros(nn), twistzeros(nn));

% Empty parameter structure
p_ = struct();



%% Forward Reconstruction

% Quaternions
Q_0 = quatzeros();

[A, b]    = odeQ(obj, snodes, k_, d_, p_);
[~, Q]    = odespec({ A , b }, sspan, Q_0, specopts);
k_(1:4,:) = quatnormalized(transpose(Q), true);


% Positions
r_0 = trveczeros();

[A, b]    = odeR(obj, snodes, k_, d_, p_);
[~, r]    = odespec({ A , b }, sspan, r_0, specopts);
k_(5:7,:) = transpose(r);

% Transformation through joint from parent to child
T = tpar2tform(k_(1:7,end));

% Twists
eta_0    = twistzeros();

[A, b]     = odeEta(obj, snodes, k_, d_, p_);
[~, eta_]  = odespec({ A , b }, sspan, eta_0, specopts);
k_(8:13,:) = transpose(eta_);

eta    = k_(8:13,end);


% Twist Velocities
etadot_0 = twistzeros();

[A, b]       = odeEtadot(obj, snodes, k_, d_, p_);
[~, etadot_] = odespec({ A , b }, sspan, etadot_0, specopts);
k_(14:19,:)  = transpose(etadot_);

etadot = k_(14:19,end);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
