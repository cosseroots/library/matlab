function dxds = odeBackward(obj, s, k, d, e, p)%#codegen
%% ODEBACKWARD Right-hand side of the path-backward ODE
%
% DKDS = ODEBACKWARD(OBJ, S, K, D, E, P)
%
% Inputs:
%
%   OBJ                     Rod object.
%
%   S                       Nx1 array of path coordinates.
%
%   K                       Kinematics structure with fields
%                             g       7xN
%                             eta     6xN
%                             etadot  6xN
%
%   D                       Deformation state structure with fields
%                             xi      6xN
%                             xidot   6xN
%                             xiddot  6xN
%
%   E                       Effort structure with fields
%                             f       6xN
%                             qa      QxN
%
%   P                       Parameter structure with fields
%                             rod     1x1
%                             time    1x1
%
% Outputs:
%
%   DKDS                    (25+Q)xN array of changes of state of backward ODE.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% ODEBACKWARD(OBJ, S, K, D, E, P)
narginchk(6, 6)

% ODEBACKWARD(___)
% DKDS = ODEBACKWARD(___)
nargoutchk(0, 1);



%% Algorithm

% Change of state
dxds = cat( ...
    1 ...
  ,      odeG(obj, s, k, d, p) ...
  ,    odeEta(obj, s, k, d, p) ...
  , odeEtadot(obj, s, k, d, p) ...
  , odeLambda(obj, s, k, d, e, p) ...
  ,     odeQa(obj, s, k, d, e, p) ...
);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
