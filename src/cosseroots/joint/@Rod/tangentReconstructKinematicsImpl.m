function [T, eta, etadot, vzeta, veta, vetadot] = tangentReconstructKinematicsImpl(obj, xq, vxq, sout)%#codegen
%% TANGENTRECONSTRUCTKINEMATICSIMPL
%
% Inputs:
%
%   OBJ                     Description of argument OBJ
% 
%   Q                       Description of argument Q
% 
%   QDOT                    Description of argument QDOT
% 
%   QDDOT                   Description of argument QDDOT
% 
%   VQ                      Description of argument VQ
% 
%   VQDOT                   Description of argument VQDOT
% 
%   VQDDOT                  Description of argument VQDDOT
%
% Outputs:
%
%   T                       Description of argument T
% 
%   ETA                     Description of argument ETA
% 
%   ETADOT                  Description of argument ETADOT
% 
%   VZETA                   Description of argument VZETA
% 
%   VETA                    Description of argument VETA
% 
%   VETADOT                 Description of argument VETADOT
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse Arguments

% TANGENTRECONSTRUCTKINEMATICSIMPL(OBJ, XQ, VXQ)
if nargin < 4 || isempty(sout)
  sout = [];
end

% % Proximal rigid body state
% T0      = x1{1};
% eta0    = x1{2};
% eta0dot = x1{3};
% Joint state
q       = xq{1};
qdot    = xq{2};
qddot   = xq{3};

% % Proximal rigid body variation
% vzeta0   = vx1{1};
% veta0    = vx1{2};
% veta0dot = vx1{3};
% Joint state variation
vq       = vxq{1};
vqdot    = vxq{2};
vqddot   = vxq{3};



%% Setup of Integration

% Integration options for spectral integration
specopts = obj.SpectralIntegrationOptions;
nn = specopts.Nodes;
nv = size(vq, 2);

% Interval of integration
sspan = [ 0.0 , 1.0 ];
% Node points on forward and backward abscissae
snodes = obj.SpectralNodes;
if isempty(sout)
  sout = snodes;
end
nout = numel(sout);

% Evaluate strains and their variation
[ xi,  xidot,  xiddot] = strain(obj, snodes, q, qdot, qddot);
[vxi, vxidot, vxiddot] = tangentStrain(obj, snodes, vq, vqdot, vqddot);

% Build state of deformation coordinates
% 18 x N
d_ = cat(1, xi, xidot, xiddot);

% State vector of variations of deformations
% 18 x V x N
vd_ = cat(1, vxi, vxidot, vxiddot);

% Result of integrations
% Kinematics
 k_ = cat(1, tparzeros(nn), twistzeros(nn), twistzeros(nn));
% Variation of kinematics
vk_ = zeros(18, nv, nn);

% Options structure
p_ = struct();



%% Forward Propagation of Kinematics and Tangent

% Quaternion
Q_0 = quatzeros();

[A, b]    = odeQ(obj, snodes, k_, d_, p_);
[~, Q]    = odespec({ A , b }, sspan, Q_0, specopts);
k_(1:4,:) = quatnormalized(transpose(Q), true);


% Positions
r_0 = trveczeros();

[A, b]    = odeR(obj, snodes, k_, d_, p_);
[~, r]    = odespec({ A , b }, sspan, r_0, specopts);
k_(5:7,:) = transpose(r);

% Transformation through joint from parent to child
T = tpar2tform(Rod.deval(snodes, k_, sout, 1:7));


% Twists
eta_0 = zeros(6, 1);

[A, b]     = odeEta(obj, snodes, k_, d_, p_);
[~, eta_]  = odespec({ A , b }, sspan, eta_0, specopts);
k_(8:13,:) = transpose(eta_);

eta    = Rod.deval(snodes, k_, sout, 8:13);


% Twist Velocities
etadot_0 = zeros(6, 1);

[A, b]       = odeEtadot(obj, snodes, k_, d_, p_);
[~, etadot_] = odespec({ A , b }, sspan, etadot_0, specopts);
k_(14:19,:)  = transpose(etadot_);

etadot = Rod.deval(snodes, k_, sout, 14:19);


% Variation of Transformations
vzeta_0 = twistzeros(nv);

[A, b]       = odeTangentZeta(obj, snodes, k_, d_, vk_, vd_, p_);
[~, vzeta__] = odespec({ A , b }, sspan, vzeta_0, specopts);
vzeta_       = permute(vzeta__, [2, 1]);
vk_(1:6,:,:) = reshape(vzeta_, 6, [], nn);

vzeta = reshape(Rod.deval(snodes, vzeta_, sout), 6, [], nout);


% Variation of Twists
veta_0 = twistzeros(nv);

[A, b]        = odeTangentEta(obj, snodes, k_, d_, vk_, vd_, p_);
[~, veta__]   = odespec({ A , b }, sspan, veta_0, specopts);
veta_         = permute(veta__, [2, 1]);
vk_(7:12,:,:) = reshape(veta_, 6, [], nn);

veta = reshape(Rod.deval(snodes, veta_, sout), 6, [], nout);


% Variation of Twist Velocities
vetadot_0 = twistzeros(nv);

[A, b]         = odeTangentEtadot(obj, snodes, k_, d_, vk_, vd_, p_);
[~, vetadot__] = odespec({ A , b }, sspan, vetadot_0, specopts);
vetadot_       = permute(vetadot__, [2, 1]);
vk_(13:18,:,:) = reshape(vetadot_, 6, [], nn);

vetadot = reshape(Rod.deval(snodes, vetadot_, sout), 6, [], nout);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
