function dvkds = odeTangentForward(obj, s, k, d, vk, vd, p)%#codegen
%% ODETANGENTFORWARD ODE of the tangent forward projection
%
% DVKDS = ODETANGENTFORWARD(OBJ, S, K, D, VK, VD, P)
%
% Inputs:
%
%   OBJ                     Rod object.
%
%   S                       1x1 scalar path coordinate.
%
%   K                       Forward kinematics structure with fields
%                             g       7x1
%                             eta     6x1
%                             etadot  6x1
%
%   D                       Deformation state structure with fields
%                             xi      Ex1
%                             xidot   Ex1
%                             xiddot  Ex1
%
%   VK                      Forward kinematics variation structure with fields
%                             zeta    6xV
%                             eta     6xV
%                             etadot  6xV
%
%   VD                      Deformation variation state structure with fields
%                             xi      ExV
%                             xidot   ExV
%                             xiddot  ExV
%
%   P                       Parameter structure with fields
%                             rod     Rod object
%                             time    Scalar time
%
% Outputs:
%
%   DVKDS                    19x(1+K) array of 19x1 change of kinematic state and
%                           19xK change of kinematic state variation.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% ODETANGENTFORWARD(OBJ, S, K, D, VK, VD, P)
narginchk(7, 7);

% ODETANGENTFORWARD(___)
% DVKDS = ODETANGENTFORWARD(___)
nargoutchk(0, 1);



%% Algorithm

% Change of variation state
dvkds = cat( ...
    1 ...
  ,   odeTangentZeta(obj, s, k, d, vk, vd, p) ...
  ,    odeTangentEta(obj, s, k, d, vk, vd, p) ...
  , odeTangentEtadot(obj, s, k, d, vk, vd, p) ....
);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
