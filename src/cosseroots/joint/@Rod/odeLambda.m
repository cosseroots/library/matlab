function [A, b] = odeLambda(obj, s, k, d, e, p)%#codegen
%% ODELAMBDA Differential equation of internal stresses with respect to path coordinate
%
% DLAMBDADS = ODELAMBDA(OBJ, S, K, D, E, P)
%
% [A, B] = ODELAMBDA(___)
%
% Inputs:
%
%   OBJ                     Rod object.
%
%   S                       1xN array of path coordinates.
%
%   K                       Kinematics structure with fields
%                             g       7xN
%                             eta     6xN
%                             etadot  6xN
%
%   D                       Deformation structure with fields
%                             xi      ExN
%                             xidot   ExN
%                             xiddot  ExN
%
%   E                       Effort structure with fields
%                             f       6xN
%                             qa      ExN
%
%   P                       Parameter structure with fields
%                             rod     1x1
%                             time    1x1
%
% Outputs:
%
%   DLAMBDADS               6xN array of changes of cross section forces over
%                           path coordinate.
%
%   A                       6x6xN array of linear form of the ODE.
%
%   B                       6xN vector of linear form of the ODE.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% ODELAMBDA(OBJ, S, K, D, E, P)
narginchk(6, 6);

% ODELAMBDA(___)
% DLAMBDADS = ODELAMBDA(___)
% [A, B] = ODELAMBDA(___)
nargoutchk(0, 2);



%% Algorithm

% Quick variables
L = obj.Length;
M = obj.SpatialInertiaMatrix;

% Extract variables from states
% g      = hslice(k, 1, 1:7);
eta    = hslice(k, 1, 8:13);
etadot = hslice(k, 1, 14:19);
xi     = hslice(d, 1, 1:6);
% xidot  = hslice(d, 1, 7:12);
% xiddot = hslice(d, 1, 13:18);
f      = hslice(e, 1, 1:6);
% qa     = hslice(e, 1, 7:size(e, 1));

% % Distributed forces along rod in some frame
% fd = value(obj.DistributedForces, s, p.time, k);
% % Transform into body coordinates?
% if obj.DistributedForces.IsGlobal
%   fd = robotics.force.transform(fd, quat2tpar(tpar2quat(g)));
% end
fd = zeros(6, numel(s));

% Build matrices for linear ODE
A = permute(ad(xi), [2, 1, 3]);
b = M * etadot + ...
  - permute( ...
      pagemult( ...
          permute( ...
              ad(eta) ...
            , [2, 1, 3] ...
          ) ...
        , permute( ...
            M * eta ...
          , [1, 3, 2] ...
        ) ...
      ) ...
    , [1, 3, 2] ...
  ) + ...
  - fd;

% Scaling to take ODE boundary and rod length into account
A = L * A;
b = L * b;

% DLAMBDADS = ODELAMBDA(___)
if nargout < 2
  % Compose right hand side of ODE
  A = permute(pagemult(A, permute(f, [1, 3, 2])), [1, 3, 2]) + b;

end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
