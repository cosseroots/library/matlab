function [sn, lsn] = strainNames(xia)
%% STRAINNAMES
%
% SN = STRAINNAMES(XIA) returns the human readable strain names SN for the
% allowed strain vector XIA.
%
% [SN, LSN] = STRAINNAMES(XIA) returns the LaTeX version of each allowed strain
% in LSN.

% Inputs:
%  
%   XIA                     6x1 vector of allowed strains with XIA = [ KA , GA ]
%                           where KA = 3x1 are allowed angular strains and
%                           GA = 3x1 are allowed linear strains.
%
% Outputs:
%
%   SN                      1xE cell array of names of allowed strains selected
%                           as intersection of XIA and 
%                             {
%                               'K1'
%                               'K2'
%                               'K3'
%                               'G1'
%                               'G2'
%                               'G3'
%                             }
%
%   LSN                     LaTeX formatted text of human readable strain names
%                           selected from
%                             { 
%                               '$ \kappa_{1} $'
%                               '$ \kappa_{2} $'
%                               '$ \kappa_{3} $'
%                               '$ \Gamma_{1} $'
%                               '$ \Gamma_{2} $'
%                               '$ \Gamma_{3} $'
%                             }
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% STRAINNAMES()
% STRAINNAMES(XIA)
narginchk(0, 1);

% STRAINNAMES(___)
% SN = STRAINNAMES(___)
% [SN, LSN] = STRAINNAMES(___)
nargoutchk(0, 2);

% STRAINNAMES()
if nargin < 1 || isempty(xia)
  xia = ones(6, 1);
end



%% Algorithm

persistent sn_ lsn_

if isempty(sn_)
  sn_ = { ...
      'K1' ...
    , 'K2' ...
    , 'K3' ...
    , 'G1' ...
    , 'G2' ...
    , 'G3' ...
    };

  lsn_ = { ...
      '\kappa_{1}' ...
    , '\kappa_{2}' ...
    , '\kappa_{3}' ...
    , '\Gamma_{1}' ...
    , '\Gamma_{2}' ...
    , '\Gamma_{3}' ...
  };

end

% STRAINNAMES(XIA)
% SN = STRAINNAMES(XIA)
% [SN, LSN] = STRAINNAMES(XIA)
sn = sn_(logical(xia));

% [SN, LSN] = STRAINNAMES(XIA)
if nargout > 1
  lsn = lsn_(logical(xia));
end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
