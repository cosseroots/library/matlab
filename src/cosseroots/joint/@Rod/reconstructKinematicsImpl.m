function [T, eta, etadot] = reconstructKinematicsImpl(obj, xq, sout)%#codegen
%% RECONSTRUCTKINEMATICSIMPL
%
% Inputs:
% 
%   JOINT                   Joint object.
%
%   XQ                      XQ = { Q , QDOT , QDDOT }
%
% Outputs:
%
%   T                       4x4xN array of joint transmitted homogeneous
%                           transformation matrices.
% 
%   ETA                     6xN array of joint transmitted twists.
% 
%   ETADOT                  6xN array of joint transmitted twist velocities.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse Arguments

% RECONSTRUCTKINEMATICSIMPL(OBJ, XQ)
if nargin < 3 || isempty(sout)
  sout = [];
end

% % Distal rigid body state
% T1      = x1{1};
% eta1    = x1{2};
% eta1dot = x1{3};
% Joint state
q       = xq{1};
qdot    = xq{2};
qddot   = xq{3};

% % Distal rigid body variation
% vzeta1   = vx1{1};
% veta1    = vx1{2};
% veta1dot = vx1{3};
% % Joint state variation
% vq       = vxq{1};
% vqdot    = vxq{2};
% vqddot   = vxq{3};



%% Setup of Integration

% Integration options for spectral integration
specopts = obj.SpectralIntegrationOptions;
nn = specopts.Nodes;

% Interval of integration
sspan = [ 0.0 , 1.0 ];
% Node points on forward and backward abscissae
snodes = obj.SpectralNodes;
if isempty(sout)
  sout = snodes;

% If the out path is given in decreasing order, we will change the direction of
% integration from positive to negative
elseif all(sign(sout) <= 0) % numel(sout) > 1 && all(diff(sout) < 0)
  sout   = abs(sout);
  sspan  = flip(sspan);
  snodes = flip(snodes);
  
end

% Evaluate strains
[xi, xidot, xiddot] = strain(obj, snodes, q, qdot, qddot);

% Build state of strain coordinates
d_ = cat(1, xi, xidot, xiddot);

% Result of integrations:
% Kinematics
k_ = cat(1, tparzeros(nn), twistzeros(nn), twistzeros(nn));

% Empty parameter structure
p_ = struct();



%% Forward Reconstruction

% Quaternions
Q_0 = quatzeros();

[A, b]    = odeQ(obj, snodes, k_, d_, p_);
[~, Q]    = odespec({ A , b }, sspan, Q_0, specopts);
k_(1:4,:) = quatnormalized(transpose(Q), true);

% Positions
r_0 = trveczeros();

[A, b]    = odeR(obj, snodes, k_, d_, p_);
[~, r]    = odespec({ A , b }, sspan, r_0, specopts);
k_(5:7,:) = transpose(r);

% Transformation through joint from parent to child
T = tpar2tform(Rod.deval(snodes, k_, sout, 1:7));


% Twists
eta_0    = twistzeros();

[A, b]     = odeEta(obj, snodes, k_, d_, p_);
[~, eta_]  = odespec({ A , b }, sspan, eta_0, specopts);
k_(8:13,:) = transpose(eta_);

eta    = Rod.deval(snodes, k_, sout, 8:13);


% Twist Velocities
etadot_0 = twistzeros();

[A, b]       = odeEtadot(obj, snodes, k_, d_, p_);
[~, etadot_] = odespec({ A , b }, sspan, etadot_0, specopts);
k_(14:19,:)  = transpose(etadot_);

etadot = Rod.deval(snodes, k_, sout, 14:19);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
