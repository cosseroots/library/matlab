function b = shapefuns(obj, s)%#codegen
%% SHAPEFUNS Evaluate the rod's shape function at a given path coordinate
%
% B = SHAPEFUNS(ROD, S) evaluates the rod ROD's basis function at path
% coordinate S.
%
% Inputs:
%
%   ROD                     Rod object.
% 
%   S                       1xK array of path coordinates at which to evaluate
%                           basis/shape functions.
%
% Outputs:
%
%   B                       NxMxK array of evaluated shape functions where N is
%                           the number of parametrized strains, M is the number
%                           of degrees of freedom associated with the rod's
%                           generalized coordinates, and K is the number of
%                           knots.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% SHAPEFUNS(ROD, S)
narginchk(2, 2);

% SHAPEFUNS(___)
% B = SHAPEFUNS(___)
nargoutchk(0, 1);



%% Algorithm

% Number of parametrized strains
ns = obj.StrainNumber;

% Some magic trickery to quickly expand a 1xDxK array (the shape function
% evaluated over path S with a degree of D) into an NxMxK array.
b = kron(eye(ns, ns), ones(1, obj.PositionNumber / ns)) .* repmat(permute(evaluate(obj.BasisFunction, s, [0, 1]), [3, 1, 2]), ns, ns, 1);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
