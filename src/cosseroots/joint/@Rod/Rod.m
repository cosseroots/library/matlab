classdef Rod < ElasticJoint
  %% ROD
  
  
  
  %% PUBLIC PROPERTIES
  properties
    
    SpectralIntegrationOptions struct = struct( ...
        'Nodes', 20 ...
    );
    
  end
  
  
  
  %% CONSTRUCTABLE PUBLIC READ-ONLY PROPERTIES
  properties
    
    % 6x6 global damping matrix
    DampingMatrix (6, 6) double
    
    % 6x6 Hookean matrix of block((3x3) , (3x3)) angular and linear hookean
    % matrices
    HookeanMatrix (6, 6) double
    
    % Unstrained length of rod [ m ]
    Length (1, 1) double
    
    % 6x1 vector of full reference strains
    ReferenceStrain (6, 1) double
    
  end
  
  
  
  %% CALCULTAED READ-ONLY PROPERTIES
  properties ( SetAccess = { ?smms.internal.InternalAccess } )
    
    % Vector of allowed strains, inferred from OBJ.STRAINMODEL
    AllowedStrain (1, 6) logical
    
    % Shape function object used to evaluate $\Phi$
    BasisFunction (1, 1)
    
    % Mapping matrix of all strains to allowed strains
    B (6, :) double
    
    % Mapping matrix of all strains to constrained strains
    BBar (6, :) double
    
    % ExE matrix of generalized damping terms
    GeneralizedDampingMatrix (:, :) double
    
    % ExE matrix of generalized stiffness terms
    GeneralizedStiffnessMatrix (:, :) double
    
    % SxS damping matrix
    ReducedDampingMatrix (:, :) double
    
    % SxS hookean matrix
    ReducedHookeanMatrix (:, :) double
    
    % SxS mass matrix
    ReducedSpatialInertiaMatrix (:, :) double
    
    % Sx1 vector of reference strains of allowed strains  ( B^T * xi_ref)
    % @TODO Make this a callback of path coordinate (X)
    ReferenceStrainAllowed (:, 1) double
    
    % (6-S)x1 vector of reference strains of constrained strains ( BBar^T *
    % xi_ref)
    % @TODO Make this a callback of path coordinate (X)
    ReferenceStrainConstrained (:, 1) double
    
    % Number of free strains, inferred from OBJ.ALLOWEDSTRAIN
    StrainNumber (1, 1) double
    
  end
  
  
  
  %% INTERNAL PROPERTIES
  properties ( SetAccess = { ?smms.internal.InternalAccess } )
    
    SpectralNodes (1, :) double
    
  end
  
  
  
  %% CONSTRUCTOR METHODS
  methods
    
    function obj = Rod(varargin)
      %% ROD
      
      
      
      obj@ElasticJoint();
      
      obj.AllowedStrain        = [ 1 , 1 , 1 , 0 , 0 , 0 ];
      obj.HookeanMatrix        = eye(6, 6);
      obj.DampingMatrix        = 5 * 1e-3 * obj.HookeanMatrix;
      obj.Length               = 1.0;
      obj.ReferenceStrain      = [ 0.0 , 0.0 , 0.0 , 1.0 , 0.0 , 0.0 ];
      obj.BasisFunction        = LegendrePolynomial(2);
      obj.SpatialInertiaMatrix = eye(6, 6);
      
      setProperties(obj, nargin, varargin{:});
      
    end
    
  end
  
  
  
  %% SETTERS
  methods
    
    function set.SpectralIntegrationOptions(obj, v)
      %% SET.SPECTRALINTEGRATIONOPTIONS
      
      
      
      if ~isempty(fieldnames(v))
        v = obj.SpectralIntegrationOptions * v;
      end
      
      obj.SpectralIntegrationOptions = v;
      
    end
    
    
    function set.BasisFunction(obj, v)
      %% SET.BASISFUNCTION
      
      
      
      validateattributes(v, { 'BasisFunction' }, { 'nonempty' });
      
      obj.BasisFunction = v;
      
    end
    
  end
  
  
  
  %% MODEL METHODS
  methods ( Sealed )
    
    phi = shapefuns(obj, s)
      %% SHAPEFUNS
    
    
    [xi, xidot, xiddot] = strain(obj, s, q, qdot, qddot)
      %% STRAIN
    
    
    [vxi, vxidot, vxiddot] = tangentStrain(obj, s, vq, vqdot, vqddot)
      %% TANGENTSTRAIN
    
    
    Kee = generalizedStiffness(obj, mode, options)
      %% GENERALIZEDSTIFFNESS
    
    
    Dee = generalizedDamping(obj, mode, options)
      %% GENERALIZEDDAMPING
    
    
    E = twistEnergy(obj, q)
      %% TWISTENERGY
    
    
    E = bendEnergy(obj, q)
      %% BENDENERGY
    
    
    E = stretchEnergy(obj, q)
      %% STRETCHENERGY
    
    
    E = shearEnergy(obj, q)
      %% SHEARENERGY
    
  end
  
  
  
  %% PUBLIC STATIC METHODS
  methods ( Static , Sealed )
    
    [B, Bbar] = strainDecomposition(xia)
      %% STRAINDECOMPOSITION
    
    [sn, lsn] = strainNames(xia)
      %% STRAINNAMES
    
  end
  
  
  
  %% FINAL KINEMATICS METHODS
  methods ( Sealed , Access = protected )
    
    T = tformImpl(obj, q)
      %% TFORMIMPL
    
    
    [T, eta, etadot] = reconstructKinematicsImpl(obj, xq, sout)
      %% RECONSTRUCTIONIMPL
    
    
    [T, eta, etadot] = forwardKinematicsImpl(obj, xq)
      %% FORWARDKINEMATICSIMPL
    
    
    [F0, Qa] = backwardDynamicsImpl(obj, x1, xq, F1)
      %% BACKWARDDYNAMICSIMPL
    
    
    [T, eta, etadot, vzeta, veta, vetadot] = tangentReconstructKinematicsImpl(obj, xq, vxq, sout)
      %% TANGENTRECONSTRUCTKINEMATICSIMPL
    
    
    [T, eta, etadot, vzeta, veta, vetadot] = tangentForwardKinematicsImpl(obj, xq, vxq)
      %% TANGENTFORWARDKINEMATICSIMPL
    
    
    [F0, Qa, vF0, vQa] = tangentBackwardDynamicsImpl(obj, x1, xq, F1, vx1, vxzq, vF1)
      %% TANGENTBACKWARDDYNAMICSIMPL
      
      
  end
  
  
  
  %% STATIC INTERNAL METHODS
  methods ( Static , Access = protected )
    
    function yint = deval(t, y, xq, idx)
      %% DEVAL
      %
      % Interpolation for spectral integration is based on Barycentric
      % interpolation
      
      
      % DEVAL(T, Y, XQ)
      if nargin < 4 || isempty(idx)
        idx = 1:size(y, 1);
      end
      
      % Ensure query points are a row vector
      tq = reshape(xq, [], 1);
      
      % Get data from spectral result array
      y_ = permute(y(idx,:), [2, 1]);
      ny = size(y_, 2);
      
      % Lagrange interpolation for result of spectral integration
      yint = laginterp(t, y_, tq);
      
      % Make sure that YINT is an NxT array
      if ny > 1
        yint = permute(yint, [2, 1]);
        
      end
      
    end
    
  end
  
  
  
  %% INTERNAL CONCRETE METHODS
  methods ( Access = protected )
    
    function setupImpl(obj)
      %% SETUPIMPL
      
      
      
      % Create nodes for spectral integration
      nn = obj.SpectralIntegrationOptions.Nodes;
      obj.SpectralNodes = flip(chebpts2(nn - 1, [ 0.0 , 1.0 ]), 2);
      
      % Strain decomposition matrices
      [obj.B, obj.BBar] = Rod.strainDecomposition(obj.AllowedStrain);
      
      % Some local variables for quicker calculation
      B_     = obj.B;
      BBar_  = obj.BBar;
      Bt_    = transpose(B_);
      BBart_ = transpose(BBar_);
      
      % Number of strains
      obj.StrainNumber = sum(obj.AllowedStrain);
      
      % Number of generalized coordinates
      obj.PositionNumber = obj.BasisFunction.DegreesOfFreedom * obj.StrainNumber;
      obj.VelocityNumber = obj.PositionNumber;
      
      % Home position and position limits
      if size(obj.HomePosition, 1) == 0
        obj.HomePosition = zeros(obj.PositionNumber, 1);
      end
      if size(obj.PositionLimits, 1) == 0
        obj.PositionLimits = [ -Inf(obj.PositionNumber, 1) , +Inf(obj.PositionNumber, 1) ];
      end
      
      % Matrices of allowed/reduced beam dynamics
      obj.ReducedDampingMatrix        = reduceMatrix(obj, obj.DampingMatrix);
      obj.ReducedHookeanMatrix        = reduceMatrix(obj, obj.HookeanMatrix);
      obj.ReducedSpatialInertiaMatrix = reduceMatrix(obj, obj.SpatialInertiaMatrix);
      
      % Reference strain of constrained strains and of free strains
      obj.ReferenceStrainConstrained = BBart_ * obj.ReferenceStrain;
      obj.ReferenceStrainAllowed     = Bt_    * obj.ReferenceStrain;
      
      % Generalized stiffness and damping matrix for elastic coordinates
      obj.GeneralizedDampingMatrix   = generalizedMatrix(obj, obj.ReducedDampingMatrix, 'ode');
      obj.GeneralizedStiffnessMatrix = generalizedMatrix(obj, obj.ReducedHookeanMatrix, 'ode');
      
    end
    
    
    function Mr = reduceMatrix(obj, M)
      %% REDUCEMATRIX
      
      
      
      Mr = transpose(obj.B) * M * obj.B;
      
    end
    
    
    K = generalizedMatrix(obj, m, mode, options)
      %% GENERALIZEDMATRIX
    
  end
  
  
  
  %% CONCRETE IMPLEMENTATION METHODS
  methods ( Access = protected )
    
    function styleImpl(obj, h, varargin)
      %% STYLEIMPL
      
      
      set( ...
          findobj(findobj(h, 'Tag', 'Deformation'), 'Tag', 'Surface') ...
        , varargin{:} ...
      );
      
    end
    
    
    function hp = drawDeformationImpl(obj, ax, q, varargin)
      %% DRAWDEFORMATIONIMPL
      
      
      
      % Array to hold all graphics objects
      hp = gobjects(1, 4);
      
      % Get faces and vertices of visual shape
      [f, v, Tc, Txyz] = visualShapeImpl(obj, q);
      
      % Group for all parts of the plot
      pc = tform2trvec(Tc);
      hp(1) = plot3( ...
          ax ...
        , pc(1,:), pc(2,:), pc(3,:) ...
        , 'LineStyle' , '-' ... 
        , 'Color'     , color('black') ...
        , 'LineWidth' , 2.5 ...
        , 'Tag'       , 'Centerline' ...
      );
      
      % Draw patch for surface
      hp(2) = patch( ...
          ax ...
        , 'Faces'             , f ...
        , 'Vertices'          , v ...
        , 'FaceColor'         , color('LightGray') ...
        , 'EdgeColor'         , rgbdarker(color('LightGray'), 0.05) ...
        , 'FaceLighting'      , 'gouraud' ...
        , 'AmbientStrength'   , 0.5 ...
        , 'DiffuseStrength'   , 0.3 ...
        , 'SpecularStrength'  , 0.6 ...
        , 'BackFaceLighting'  , 'reverselit' ...
        , 'Tag'               , 'Surface' ...
        , obj.StylingCell{:} ...
      );
      
      % Draw a line on the surface of the rod indicating the $X$, $Y$ and $Z$
      % axes
      c  = { 'x' , 'y' , 'z' };
      nc = numel(c);
      for ic = 1:nc
        p_ = tform2trvec(Txyz(:,:,:,ic));
        hp(2 + ic) = plot3( ...
            ax ...
          , p_(1,:), p_(2,:), p_(3,:) ...
          , 'Color'     , evec3(ic) ...
          , 'LineStyle' , '-' ...
          , 'LineWidth' , 2.5 ...
          , 'Tag'       , sprintf('Surface axis %s', c{ic}) ...
        );
        
      end
      
    end
    
    
    function redrawDeformationImpl(obj, h, q, varargin)
      %% REDRAWDEFORMATIONIMPL
      
      
      
      % Obtain the new vertices
      [~, v, Tc, Txyz] = visualShapeImpl(obj, q);
      
      % Centerline
      pc = tform2trvec(Tc);
      set( ...
          findobj(h, 'Tag', 'Centerline') ...
        , 'XData', pc(1,:) ...
        , 'YData', pc(2,:) ...
        , 'ZData', pc(3,:) ...
      );
      
      % And update the surface object with the new vertices positions
      set( ...
          findobj(h, 'Tag', 'Surface') ...
        , 'Vertices', v ...
      );
      
      % Draw a line on the surface of the rod indicating the $X$, $Y$ and $Z$
      % axes
      c  = { 'x' , 'y' , 'z' };
      nc = numel(c);
      hsl = findobj(h, '-regexp', 'Tag', 'Surface axis.*');
      for ic = 1:nc
        p_ = tform2trvec(Txyz(:,:,:,ic));
        set( ...
            findobj(hsl, 'Tag', sprintf('Surface axis %s', c{ic})) ...
          , 'XData', p_(1,:) ...
          , 'YData', p_(2,:) ...
          , 'ZData', p_(3,:) ...
        );
        
      end
      
    end
    
    
    function [f, v, T, Txyz] = visualShapeImpl(obj, q)
      %% VISUALSHAPEIMPL
      
      
      
      % Number of faces and cross sections
      nf = 7;
      ns = 31;
      % Path abscissae of where to plot cross sections
      s = linspace(0, 1, ns);
      
      % Geometric properties of swept cross section
      d = 0.04;
      r = d / 2;
      
      % Reconstruct centerline
      T = reconstructKinematics( ...
          obj ...
        , q, [] ...
        , [], [] ...
        , [], [] ...
        , s ...
      );
      
      % Reconstruct shape
      [R, p] = tform2cart(T);
      
      Txyz = repmat(tformzeros(ns), 1, 1, 1, 3);
      EE = 1.01 * r * eye(3, 3);
      for ic = 1:3
        Txyz(:,:,:,ic) = tformmul(T, trvec2tform(EE(:,ic)));
      end
      
      % Angle of circle to reconstruct shape
      theta = linspace(0, 2 * pi, nf + 1);
      theta = reshape(theta(1:end-1), [], 1);
      
      % Circular shape of rod in XY plane but in 3D
      xyz = cat( ...
          2 ...
        , r * cos(theta) ...
        , r * sin(theta) ...
        , zeros(size(theta)) ...
      );
      
      % Transform vertices to not lie in the XY plane but in the YZ plane
      xyz = circshift(xyz, 1, 2);
      
      % And sweep the shape along the path
      XYZ = sweep3(permute(xyz, [2, 1]), p, R);
      
      % Reconstruct vertices from the XYZ surface coordinates
      ns = ns - 1;
      v = permute(reshape(XYZ, nf * (ns + 1), 1, 3), [1, 3, 2]);
      
      % Faces of the circle
      f = 1:nf;
      
      % Pre-calculate some faces values
      fn = reshape(1:nf, [], 1);
      fnn = fn + nf;
      
      % Put all faces together
      f = [ ...
        ... % Bottom face
        f ; ...
        ... % Side faces
        [ ...
            reshape(permute([ fn , circshift(fn, -1) , circshift(fnn, -1), fnn ] + permute(nf * (0:(ns - 1)), [3, 1, 2]), [1, 3, 2]), [], 4) ...
          , NaN(nf * ns, size(f, 2) - 4) ...
        ] ; ...
        ... % Top faces
        f + nf * ns;
      ];
      
    end
    
  end
  
  
  
  %% INTERNAL ENERGY METHODS
  methods ( Access = protected )
    
    e = generalizedDeformationEnergy(obj, q, SE)
      %% GENERALIZEDDEFORMATIONENERGY
    
  end
  
  
  
  %% INTERNAL ODE METHODS
  methods ( Access = protected )
    
    [A, b] = odeForward(obj, s, k, d, p)
      %% ODEFORWARD
    
    
    
    [A, b] = odeBackward(obj, s, k, d, p)
      %% ODEBACKWARD
    
    
    
    [A, b] = odeReconstruction(obj, s, k, d, p)
      %% ODERECONSTRUCTION
    
    
    
    [A, b] = odeTForm(obj, s, k, d, p)
      %% ODETFORM
    
    
    
    [A, b] = odeQ(obj, s, k, d, p)
      %% ODEQ
    
    
    
    [A, b] = odeR(obj, s, k, d, p)
      %% ODER
    
    
    
    [A, b] = odeEta(obj, s, k, d, p)
      %% ODEETA
    
    
    
    [A, b] = odeEtadot(obj, s, k, d, p)
      %% ODEETADOT
    
    
    
    [A, b] = odeLambda(obj, s, k, d, e, p)
      %% ODELAMBDA
    
    
    
    [A, b] = odeQa(obj, s, k, d, e, p)
      %% ODEQA
    
    
    
    [A, b] = odeTangentForward(obj, s, k, d, vk, vd, p)
      %% ODETANGENTFORWARD
    
    
    
    [A, b] = odeTangentBackward(obj, s, k, d, vk, vd, p)
      %% ODETANGENTBACKWARD
    
    
    
    [A, b] = odeTangentReconstruction(obj, s, k, d, vk, vd, p)
      %% ODETANGENTRECONSTRUCTION
    
    
    
    [A, b] = odeTangentZeta(obj, s, k, d, vk, vd, p)
      %% ODETANGENTZETA
    
    
    
    [A, b] = odeTangentEta(obj, s, k, d, vk, vd, p)
      %% ODETANGENTETA
    
    
    
    [A, b] = odeTangentEtadot(obj, s, k, d, vk, vd, p)
      %% ODETANGENTETADOT
    
    
    
    [A, b] = odeTangentLambda(obj, s, k, d, e, vk, vd, ve, p)
      %% ODETANGENTLAMBDA
    
    
    
    [A, b] = odeTangentQa(obj, s, k, d, e, vk, vd, ve, p)
      %% ODETANGENTQA
    
    
    
    function dkds = odeGeneralizedMatrix(obj, K, s)
      %% ODEGENERALIZEDMATRIX
      
      
      
      % Evaluate basis function
      phi = shapefuns(obj, s);
      
      % Integrand part in matrix form, then reshaped into column vector
      dkds = reshape(obj.Length * transpose(phi) * K * phi, [], 1);
      
    end
    
  end
  
end
