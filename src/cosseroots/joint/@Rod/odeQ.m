function [A, b] = odeQ(obj, s, k, d, p)%#codegen
%% ODEQ Differential equation of Quaternions with respect to path coordinate
%
% DQDS = ODEQ(OBJ, S, K, D, P)
%
% [A, B] = ODEQ(___)
%
% Inputs:
%
%   OBJ                     Rod object.
%
%   S                       1xN array of path coordinates.
%
%   K                       Kinematics structure with fields
%                             g       7xN
%                             eta     6xN
%                             etadot  6xN
%
%   D                       Deformation state structure with fields
%                             xi      ExN
%                             xidot   ExN
%                             xiddot  ExN
%
%   P                       Parameter structure
%
% Outputs:
%
%   DQDS                    4xN array of changes of quaternion over path
%                           coordinate.
%
%   A                       4x4xN array of linear form of the ODE.
%
%   B                       4xN vector of linear form of the ODE.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% ODEQ(OBJ, S, K, D, P)
narginchk(5, 5);

% ODEQ(___)
% DQDS = ODEQ(___)
% [A, B] = ODEQ(___)
nargoutchk(0, 2);



%% Algorithm

% Quick variables
L = obj.Length;

% Extract variables from states
g      = hslice(k, 1, 1:7);
% eta    = hslice(k, 1, 8:13);
% etadot = hslice(k, 1, 14:19);
xi     = hslice(d, 1, 1:6);
% xidot  = hslice(d, 1, 7:12);
% xiddot = hslice(d, 1, 13:18);

% Extract angular strain from spatial strain
K = hslice(xi, 1, 1:3);

% Build matrices for linear ODE
A = 1.0 / 2.0 * quat2conjmat(padarray(K, 1, 0, 'pre'));
b = zeros(4, numel(s));

% Scaling to take ODE boundary and rod length into account
A = L * A;
b = L * b;

% DQDS = ODEQ(___)
if nargout < 2
  % Compose right hand side of ODE
  A = permute(pagemult(A, permute(tpar2quat(g), [1, 3, 2])), [1, 3, 2]) + b;

end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
