function [T, eta, etadot, vzeta, veta, vetadot] = tangentForwardKinematicsImpl(obj, xq, vxq)%#codegen
%% TANGENTFORWARDKINEMATICSIMPL Tangent of forward projection
%
% Inputs:
% 
%   JOINT                   Joint object.
%
%   XQ                      XQ  = {  Q ,  QDOT ,  QDDOT }
%
%   VXQ                     VXQ = { VQ , VQDOT , VQDDOT }
%
% Outputs:
%
%   T                       4x4
%
%   ETA                     6x1
%
%   ETADOT                  6x1
%
%   VZETA                   6xV array of variation of joint transmitted
%                           homogeneous transformation matrices.
% 
%   VETA                    6xV array of variation of joint transmitted
%                           twists.
% 
%   VETADOT                 6xV array of variation of joint transmitted twist
%                           velocities.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse Arguments

% Joint state
q       = xq{1};
qdot    = xq{2};
qddot   = xq{3};

% Joint state variation
vq       = vxq{1};
vqdot    = vxq{2};
vqddot   = vxq{3};



%% Setup of Integration

% Integration options for spectral integration
specopts = obj.SpectralIntegrationOptions;
nn = specopts.Nodes;
nv = size(vq, 2);

% Interval of integration
sspan = [ 0.0 , 1.0 ];
% Node points on forward and backward abscissae
snodes = obj.SpectralNodes;

% Evaluate strains and their variation
[ xi,  xidot,  xiddot] = strain(obj, snodes, q, qdot, qddot);
[vxi, vxidot, vxiddot] = tangentStrain(obj, snodes, vq, vqdot, vqddot);

% Build state of deformation coordinates
% 18 x N
d_ = cat(1, xi, xidot, xiddot);

% State vector of variations of deformations
% 18 x V x N
vd_ = cat(1, vxi, vxidot, vxiddot);

% Result of integrations
% Kinematics
 k_ = cat(1, tparzeros(nn), twistzeros(nn), twistzeros(nn));
% Variation of kinematics
vk_ = zeros(18, nv, nn);

% Options structure
p_ = struct();



%% Forward Propagation of Kinematics and Tangent

% Quaternion
Q_0 = quatzeros();

[A, b]    = odeQ(obj, snodes, k_, d_, p_);
[~, Q_]   = odespec({ A , b }, sspan, Q_0, specopts);
k_(1:4,:) = quatnormalized(transpose(Q_), true);

% Positions
r_0 = trveczeros();

[A, b]    = odeR(obj, snodes, k_, d_, p_);
[~, r_]   = odespec({ A , b }, sspan, r_0, specopts);
k_(5:7,:) = transpose(r_);

T = tpar2tform(k_(1:7,end));

% Twists
eta_0 = zeros(6, 1);

[A, b]     = odeEta(obj, snodes, k_, d_, p_);
[~, eta_]  = odespec({ A , b }, sspan, eta_0, specopts);
k_(8:13,:) = transpose(eta_);

eta = k_(8:13,end);

% Twist Velocities
etadot_0 = zeros(6, 1);

[A, b]      = odeEtadot(obj, snodes, k_, d_, p_);
[~, etadot_] = odespec({ A , b }, sspan, etadot_0, specopts);
k_(14:19,:) = transpose(etadot_);

etadot = k_(14:19,end);

% Variation of Transformations
vzeta_0 = twistzeros(nv);

[A, b]       = odeTangentZeta(obj, snodes, k_, d_, vk_, vd_, p_);
[~, vzeta_]  = odespec({ A , b }, sspan, vzeta_0, specopts);
vk_(1:6,:,:) = reshape(permute(vzeta_, [2, 1]), 6, [], nn);

vzeta = vk_(1:6,:,end);

% Variation of Twists
veta_0 = twistzeros(nv);

[A, b]        = odeTangentEta(obj, snodes, k_, d_, vk_, vd_, p_);
[~, veta_]    = odespec({ A , b }, sspan, veta_0, specopts);
vk_(7:12,:,:) = reshape(permute(veta_, [2, 1]), 6, [], nn);

veta = vk_(7:12,:,end);

% Variation of Twist Velocities
vetadot_0 = twistzeros(nv);

[A, b]         = odeTangentEtadot(obj, snodes, k_, d_, vk_, vd_, p_);
[~, vetadot_]   = odespec({ A , b }, sspan, vetadot_0, specopts);
vk_(13:18,:,:) = reshape(permute(vetadot_, [2, 1]), 6, [], nn);

vetadot = vk_(13:18,:,end);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
