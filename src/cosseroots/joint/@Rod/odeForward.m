function dxds = odeForward(obj, s, k, d, p)%#codegen
%% ODEFORWARD Right-hand side of the path-forward ODE
%
% DKDS = ODEFORWARD(OBJ, S, K, D, P)
%
% Inputs:
%
%   OBJ                     Rod object.
%
%   S                       1xN array of path coordinates.
%
%   K                       Kinematics structure with fields
%                             g       7xN
%                             eta     6xN
%                             etadot  6xN
%
%   D                       Deformation state structure with fields
%                             xi      ExN
%                             xidot   ExN
%                             xiddot  ExN
%
%   P                       Parameter structure with fields
%                             rod     1x1
%                             time    1x1
%
% Outputs:
%
%   DKDS                    19xN array of changes of state of forward ODE.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% ODEFORWARD(OBJ, S, K, D, P)
narginchk(5, 5);

% ODEFORWARD(___)
% DKDS = ODEFORWARD(___)
nargoutchk(0, 1);



%% Algorithm

% Change of state
dxds = cat( ...
    1 ...
  ,      odeG(obj, s, k, d, p) ...
  ,    odeEta(obj, s, k, d, p) ...
  , odeEtadot(obj, s, k, d, p) ...
);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
