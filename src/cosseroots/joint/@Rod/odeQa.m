function [A, b] = odeQa(obj, s, k, d, e, p)%#codegen
%% ODEQA Differential equation of generalized strains with respect to path coordinate
%
% DQADS = ODEQA(OBJ, S, K, D, E, P)
%
% [A, B] = ODEQA(___)
%
% Inputs:
%
%   OBJ                     Rod object.
%
%   S                       1xN array of path coordinates.
%
%   K                       Kinematics structure with fields
%                             g       7xN
%                             eta     6xN
%                             etadot  6xN
%
%   D                       Deformation state structure with fields
%                             xi      QxN
%                             xidot   QxN
%                             xiddot  QxN
%
%   E                       Effort structure with fields
%                             f       6xN
%                             qa      QxN
%
%   P                       Parameter structure with fields
%                             rod     1x1
%                             time    1x1
%
% Outputs:
%
%   DQADS                   QxN array of changes of generalized internal
%                           stresses over path coordinate.
%
%   A                       QsxN array of linear form of the ODE.
%
%   B                       QxN vector of linear form of the ODE.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% ODEQA(OBJ, S, K, D, E, P)
narginchk(6, 6);

% ODEQA(___)
% DQADS = ODEQA(___)
% [A, B] = ODEQA(___)
nargoutchk(0, 2);



%% Algorithm

% Quick variables
L  = obj.Length;
nq = obj.PositionNumber;
% M  = obj.SpatialInertiaMatrix;

% Extract variables from states
% g      = hslice(k, 1, 1:7);
% eta    = hslice(k, 1, 8:13);
% etadot = hslice(k, 1, 14:19);
% xi     = hslice(d, 1, 1:6);
% xidot  = hslice(d, 1, 7:12);
% xiddot = hslice(d, 1, 13:18);
f      = hslice(e, 1, 1:6);
qa     = hslice(e, 1, 7:size(e, 1));

% Build matrices for linear ODE
A = zeros(nq, nq, numel(s));
b = permute( ...
    pagemult( ...
        -permute( ...
            pagemult( ...
                obj.B ...
              , shapefuns(obj, s) ...
            ) ...
          , [2, 1, 3] ...
        ) ...
      , permute(f, [1, 3, 2]) ...
    ) ...
  , [1, 3, 2] ...
);

% Scaling to take ODE boundary and rod length into account
A = L * A;
b = L * b;

% DQADS = ODEQA(___)
if nargout < 2
  % Compose right hand side of ODE
  A = permute(pagemult(A, permute(qa, [1, 3, 2])), [1, 3, 2]) + b;

end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
