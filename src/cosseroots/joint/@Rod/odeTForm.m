function dgds = odeTForm(obj, s, k, d, p)%#codegen
%% ODETFORM Differential equation of coordinate transformation with respect to path coordinate
%
% DGDS = ODETFORM(OBJ, S, K, D, P)
%
% Inputs:
%
%   OBJ                     Rod object.
%
%   S                       1xN array of path coordinates.
%
%   K                       Kinematics structure with fields
%                             g       7xN
%                             eta     6xN
%                             etadot  6xN
%
%   D                       Deformation state structure with fields
%                             xi      ExN
%                             xidot   ExN
%                             xiddot  ExN
%
%   P                       Parameter structure with fields
%                             rod     1x1
%                             time    1x1
%
% Outputs:
%
%   DGDS                    7xN array of changes of cross section configuration
%                           over path coordinate.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% ODETFORM(OBJ, S, K, D, P)
narginchk(5, 5);

% ODETFORM(___)
% DGDS = ODETFORM(___)
nargoutchk(0, 1);



%% Algorithm

dgds = cat( ...
    1 ...
  , odeQ(obj, s, k, d, p) ...
  , odeR(obj, s, k, d, p) ...
);


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
