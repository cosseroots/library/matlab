function [A, b] = odeTangentLambda(obj, s, k, d, e, vk, vd, ve, p)%#codegen
%% ODETANGENTLAMBDA ODE of tangent of internal stresses
%
% DVLAMBDADS = ODETANGENTLAMBDA(OBJ, S, K, D, E, VK, VD, VE, P)
%
% [A, B] = ODETANGENTLAMBDA(___)
%
% Inputs:
%
%   OBJ                     Rod object.
%
%   S                       1xN array of path coordinates.
%
%   K                       Backward kinematics structure with fields
%                             g       7xN
%                             eta     6xN
%                             etadot  6xN
%
%   D                       Deformation state structure with fields
%                             xi      ExN
%                             xidot   ExN
%                             xiddot  ExN
%
%   E                       Efforts structure with fields
%                             f       6xN
%                             qa      6xN
%
%   VK                      Backward kinematics variation structure with fields
%                             zeta    6xVxN
%                             eta     6xVxN
%                             etadot  6xVxN
%
%   VD                      Deformation variation structure with fields
%                             xi      ExVxN
%                             xidot   ExVxN
%                             xiddot  ExVxN
%
%   VE                      Efforts variation structure with fields
%                             f       6xVxN
%                             qa      6xVxN
%
%   P                       Parameter structure with fields
%                             rod     Rod object
%                             time    Scalar time
%
% Outputs:
%
%   A                       Description of argument A
%
%   B                       Description of argument B
%
%   DVLAMBDADS              Description of argument DVLAMBDADS
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% ODETANGENTLAMBDA(OBJ, S, K, D, E, VK, VD, VE, P)
narginchk(9, 9);

% ODETANGENTLAMBDA(___)
% DVLAMBDADS = ODETANGENTLAMBDA(___)
% [A, B] = ODETANGENTLAMBDA(___)
nargoutchk(0, 2);



%% Algorithm

% Quick variables
L = obj.Length;
M = obj.SpatialInertiaMatrix;

% Extract values from state vectors
% g      = hslice(k, 1, 1:7);
eta    = hslice(k, 1, 8:13);
% etadot = hslice(k, 1, 14:19);
xi     = hslice(d, 1, 1:6);
% xidot  = hslice(d, 1, 7:12);
% xiddot = hslice(d, 1, 13:18);
f      = hslice(e, 1, 1:6);
% qa     = hslice(e, 1, 7:size(e, 1));

vzeta   = hslice(vk, 1, 1:6);
veta    = hslice(vk, 1, 7:12);
vetadot = hslice(vk, 1, 13:18);
vxi     = hslice(vd, 1, 1:6);
% vxidot  = hslice(vd, 1, 7:12);
% vxiddot = hslice(vd, 1, 13:18);
vf      = hslice(ve, 1, 1:6);
% vqa     = hslice(ve, 1, 7:size(ve, 1));

% Distributed forces on rod
fd = zeros(6, numel(s));
vfd = zeros(6, size(vk, 2), numel(s));

% Build matrices for linear ODE
A = permute(ad(xi), [2, 1, 3]);
b = pagemult(M, vetadot) + ...
  - pagemult(permute(ad(eta), [2, 1, 3]), pagemult(M, veta)) + ...
  - pagemult(-permute(adt(M * eta), [2, 1, 3]), veta) + ... - transpose(ad(veta)) * M * eta
  + pagemult(-permute(adt(f), [2, 1, 3]), vxi) + ... +transpose(ad(vxi)) * f
  - ( pagemult(permute(-adt(fd), [2, 1, 3]), ang2twist(twist2ang(vzeta))) + vfd );

% Scaling to take ODE boundary and rod length into account
A = L * A;
b = L * b;

% DVLAMBDADS = ODETANGENTLAMBDA(___)
if nargout < 2
  % Compose right hand side of ODE
  A = pagemult(A, vf) + b;

end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
