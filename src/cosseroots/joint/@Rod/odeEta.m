function [A, b] = odeEta(obj, s, k, d, p)%#codegen
%% ODEETA Differential equation of twists with respect to path coordinate
%
% DETADS = ODEETA(OBJ, S, K, D, P)
%
% [A, B] = ODEETA(___)
%
% Inputs:
%
%   OBJ                     Rod object.
%
%   S                       1xN array of path coordinates.
%
%   K                       Kinematics structure with fields
%                             g       7xN
%                             eta     6xN
%                             etadot  6xN
%
%   D                       Deformation state structure with fields
%                             xi      ExN
%                             xidot   ExN
%                             xiddot  ExN
%
%   P                       Parameter structure with fields
%                             rod     1x1
%                             time    1x1
%
% Outputs:
%
%   DETADS                  6xN array of changes of cross section twists over
%                           path coordinate.
%
%   A                       6x6xN matrix of linear form of the ODE.
%
%   B                       6xN vector of linear form of the ODE.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% ODEETA(OBJ, S, K, D, P)
narginchk(5, 5);

% ODEETA(___)
% DETAS = ODEETA(___)
% [A, B] = ODEETA(___)
nargoutchk(0, 2);



%% Algorithm

% Quick variables
L = obj.Length;

% Extract variables from states
% g      = hslice(k, 1, 1:7);
eta    = hslice(k, 1, 8:13);
% etadot = hslice(k, 1, 14:19);
xi     = hslice(d, 1, 1:6);
xidot  = hslice(d, 1, 7:12);
% xiddot = hslice(d, 1, 13:18);

% Build matrices for linear ODE
A = -ad(xi);
b = xidot;

% Scaling to take ODE boundary and rod length into account
A = L * A;
b = L * b;

% DETADS = ETA(___)
if nargout < 2
  % Compose right hand side of ODE
  A = permute(pagemult(A, permute(eta, [1, 3, 2])), [1, 3, 2]) + b;

end


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
