function e = generalizedDeformationEnergy(obj, q, SE)
%% GENERALIZEDDEFORMATIONENERGY
%
% E = GENERALIZEDDEFORMATIONENERGY(ROD, Q, SE)
%
% Inputs:
%
%   ROD                     Rod object.
%
%   Q                       Px1 vector of generalized position variables.
%
%   SE                      6x1 selector vector selecting specific components of
%                           the deformation vector. Defaults to ones(6, 1).
%
% Outputs:
%
%   E                       Vx1 scalar deformation energy of rod
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse arguments

% GENERALIZEDDEFORMATIONENERGY(OBJ, Q)
% GENERALIZEDDEFORMATIONENERGY(OBJ, Q, SE)
narginchk(2, 3);

% GENERALIZEDDEFORMATIONENERGY(___)
% E = GENERALIZEDDEFORMATIONENERGY(___)
nargoutchk(0, 1);

% GENERALIZEDDEFORMATIONENERGY(OBJ, Q)
if nargin < 2 || isempty(SE)
  SE = ones(6, 1);
end



%% Integrate

% Integration options for spectral integration
specopts = obj.SpectralIntegrationOptions;
nn = specopts.Nodes;

% Interval of integration
sspan = [ 0.0 , 1.0 ];
% Node points on forward and backward abscissae
snodes = obj.SpectralNodes;

% Evaluate strains
xi = strain(obj, snodes, q);

% Turn selector vector into a selector matrix
nse = sum(SE);
E   = eye(6, 6);
SE_ = E(logical(SE),:);

% Calculate actual deformation and select the requested deformation measures
% from it
ep = pagemult(SE_, permute(xi - obj.ReferenceStrain, [1, 3, 2]));

% Calculate reduced Hookean matrix based on selected deformation measures
Hr = SE_ * obj.HookeanMatrix * transpose(SE_);

% Calculate matrix A and b of linear ODE for strain
A = zeros(nse, nse, nn);
b = obj.Length * dot(ep, pagemult(Hr, ep), 2);
e_0 = zeros(nse, 1);
[~, e_] = odespec({ A , b }, sspan, e_0, specopts);

% And pick the final value of integration
e = full(permute(e_(end,:), [2, 1]));


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
