function [F0, Qa, vF0, vQa] = tangentBackwardDynamicsImpl(obj, x1, xq, F1, vx1, vxq, vF1)%#codegen
%% TANGENTBACKWARDDYNAMICSIMPL Tangent of backward projection
%
% Inputs:
% 
%   JOINT                   Joint object.
%
%   X1                      X1 = { T1 , ETA1 , ETA1DOT }
%
%   XQ                      XQ = {  Q , QDOT , QDDOT   }
%
%   F1                      F1
%
%   VX1                     VX1 = { VZETA1 , VETA1 , VETA1DOT }
%
%   VXQ                     VXQ = { VQ     , VQDOT , VQDDOT   }
%
%   VF1                     VF1
%
% Outputs:
%
%   F0                      6x1
%
%   QA                      VxM
%
%   VF0                     6xM array of variation of joint transmitted forces
%                           onto the parent body expressed in the parent's body.
% 
%   VQA                     VxM array of variation of generalized joint
%                           acutation forces.
%
% Authors:
%   Philipp Tempel <philipp.tempel@ls2n.fr>



%% Parse Arguments

% Distal rigid body state
T1      = x1{1};
eta1    = x1{2};
eta1dot = x1{3};
% Joint state
q       = xq{1};
qdot    = xq{2};
qddot   = xq{3};

% Distal rigid body variation
vzeta1   = vx1{1};
veta1    = vx1{2};
veta1dot = vx1{3};
% Joint state variation
vq       = vxq{1};
vqdot    = vxq{2};
vqddot   = vxq{3};



%% Setup of Integration

% Integration options for spectral integration
specopts = obj.SpectralIntegrationOptions;
nn = specopts.Nodes;
nq = obj.PositionNumber;
nv = size(vq, 2);

% Interval of integration
sspan = [ 1.0 , 0.0 ];
% Node points on forward and backward abscissae
snodes = flip(obj.SpectralNodes);

% Evaluate strains and their variation
[ xi,  xidot,  xiddot] = strain(obj, snodes, q, qdot, qddot);
[vxi, vxidot, vxiddot] = tangentStrain(obj, snodes, vq, vqdot, vqddot);

% Build state of deformation coordinates
% 18 x N
d_ = cat(1, xi, xidot, xiddot);

% Build state structure of deformation variation
% 18 x V x N
vd_ = cat(1, vxi, vxidot, vxiddot);

% Result of integrations:
% Kinematics
k_ = cat(1, tparzeros(nn), twistzeros(nn), twistzeros(nn));
% Efforts
e_ = zeros(6 + nq, nn);
% Variation of kinematics
vk_ = zeros(18, nv, nn);
% Variation of Efforts
ve_ = zeros(6 + nq, nv, nn);

% Options structure
p_ = struct();



%% Reconstruction of Backward Kinematics

% Quaternions
Q_0 = tform2quat(T1);

[A, b]    = odeQ(obj, snodes, k_, d_, p_);
[~, Q_]   = odespec({ A , b }, sspan, Q_0, specopts);
k_(1:4,:) = quatnormalized(transpose(Q_), true);


% Positions
r_0 = tform2trvec(T1);

[A, b]    = odeR(obj, snodes, k_, d_, p_);
[~, r_]   = odespec({ A , b }, sspan, r_0, specopts);
k_(5:7,:) = transpose(r_);


% Twists
eta_0 = eta1;

[A, b]     = odeEta(obj, snodes, k_, d_, p_);
[~, eta_]  = odespec({ A , b }, sspan, eta_0, specopts);
k_(8:13,:) = transpose(eta_);


% Twist Velocities
etadot_0 = eta1dot;

[A, b]       = odeEtadot(obj, snodes, k_, d_, p_);
[~, etadot_] = odespec({ A , b }, sspan, etadot_0, specopts);
k_(14:19,:)  = transpose(etadot_);



%% Reconstruction of Backward Tangent

% Variation of Positions
vzeta_0 = vzeta1;

[A, b]       = odeTangentZeta(obj, snodes, k_, d_, vk_, vd_, p_);
[~, vzeta_]  = odespec({ A , b }, sspan, vzeta_0, specopts);
vk_(1:6,:,:) = reshape(permute(vzeta_, [2, 1]), 6, [], nn);


% Variation of Twists
veta_0 = veta1;

[A, b]        = odeTangentEta(obj, snodes, k_, d_, vk_, vd_, p_);
[~, veta_]    = odespec({ A , b }, sspan, veta_0, specopts);
vk_(7:12,:,:) = reshape(permute(veta_, [2, 1]), 6, [], nn);


% Variation of Twist Velocities
vetadot_0 = veta1dot;

[A, b]         = odeTangentEtadot(obj, snodes, k_, d_, vk_, vd_, p_);
[~, vetadot_]  = odespec({ A , b }, sspan, vetadot_0, specopts);
vk_(13:18,:,:) = reshape(permute(vetadot_, [2, 1]), 6, [], nn);



%% Backward Tangent Propagation

% Internal Stresses
lambda_0 = -1 * F1(:,1);

[A, b]       = odeLambda(obj, snodes, k_, d_, e_, p_);
[~, lambda_] = odespec({ A , b }, sspan, lambda_0, specopts);
e_(1:6,:)    = transpose(lambda_);

% Proximal forces
F0 = -e_(1:6,end);


% Generalized internal actuation
qa_0 = zeros(nq, 1);

[A, b]      = odeQa(obj, snodes, k_, d_, e_, p_);
[~, qa_]    = odespec({ A , b }, sspan, qa_0, specopts);
e_(7:end,:) = transpose(qa_);

% Internal actuation forces
Qa = -e_(7:end,end) + ...
                    + obj.GeneralizedDampingMatrix   * qdot + ...
                    + obj.GeneralizedStiffnessMatrix * q ; ...


% Variation of Internal Stresses
vlambda_0 = -1 * vF1;

[A, b]        = odeTangentLambda(obj, snodes, k_, d_, e_, vk_, vd_, ve_, p_);
[~, vlambda_] = odespec({ A , b }, sspan, vlambda_0, specopts);
ve_(1:6,:,:)  = reshape(permute(vlambda_, [2, 1]), 6, nv, nn);

% Variation of proximal forces
vF0 = -ve_(1:6,:,end);


% Variation of internal actuation
vqa_0 = zeros(nq, nv);

[A, b]         = odeTangentQa(obj, snodes, k_, d_, e_, vk_, vd_, ve_, p_);
[~, vqa_]      = odespec({ A , b }, sspan, vqa_0, specopts);
ve_(7:end,:,:) = reshape(permute(vqa_, [2, 1]), nq, nv, nn);

% Variation of proximal internal actuation
vQa = -ve_(7:end,:,end) + ...
                        + obj.GeneralizedDampingMatrix   * vqdot + ...
                        + obj.GeneralizedStiffnessMatrix * vq ; ...


end

%------------- END OF CODE --------------
% Please send suggestions for improvement of this file to the original author as
% can be found in the header. Your contribution towards improving this function
% will be acknowledged in the "Changelog" section of the header.
